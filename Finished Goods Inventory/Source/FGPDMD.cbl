       IDENTIFICATION DIVISION.
       PROGRAM-ID.                 FGPDMD.
      *
      *        FINISHED GOODS
      *        PRODUCT MASTER DELETE
      *
      *        SWITCHES
      *        ********
      *
      *            SWITCH-1 - IGNORE DELETE EDITS
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           SWITCH-1
               ON  IS SW-1-ON
               OFF IS SW-1-OFF.
      
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           COPY "sm/cpy/slstmt".
           COPY "fg/cpy/slfgmt".
           COPY "sa/cpy/slsaht".
           COPY "fg/cpy/slfgdt".
           COPY "sa/cpy/slsaid".
           COPY "op/cpy/slopod".
      
       DATA DIVISION.
       FILE SECTION.
           COPY "sm/cpy/fdstmt".
           COPY "fg/cpy/fdfgmt".
           COPY "sa/cpy/fdsaht".
           COPY "fg/cpy/fdfgdt".
           COPY "sa/cpy/fdsaid".
           COPY "op/cpy/fdopod".
      
       WORKING-STORAGE SECTION.
      
       77  MP-STATUS-77            PIC XX.
       77  MM-STATUS-77            PIC XX.
       77  FM-STATUS-77            PIC XX.
       77  SH-STATUS-77            PIC XX.
       77  FX-STATUS-77            PIC XX.
       77  SX-STATUS-77            PIC XX.
       77  OD-STATUS-77            PIC XX.
       77  SCREEN-ID               PIC X(35)   VALUE
           "$PMISDIR/appl/obj/c/dsplscrn fgpdmd".
       77  DUMMY-77                PIC X.
       77  MAIN-77                 PIC XX.
       77  I-77                    PIC 99.
      
           COPY "uv/cpy/uvfckv".
           COPY "uv/cpy/uvpgid".
           COPY "uv/cpy/uvsfemov".
           COPY "uv/cpy/uvlkmg".
      
           COPY "uv/cpy/rcsymtst".
           COPY "uv/cpy/rcsymtcp".
           COPY "sm/cpy/rcstmt61".
           COPY "fg/cpy/rcfgmt31".
           COPY "fg/cpy/rcfgmt41".
           COPY "sa/cpy/rcsaht41".
           COPY "fg/cpy/rcfgdt".
           COPY "fg/cpy/rcfgdt11".
           COPY "fg/cpy/rcfgdt21".
           COPY "op/cpy/rcopod".
           COPY "sa/cpy/rcsaid".
      
           COPY "uv/cpy/lsuvgpid".
           COPY "uv/cpy/lsuvcusf".
           COPY "uv/cpy/lsuvdssh".
           COPY "fg/cpy/lsfggsii".
      
       01  CONTROL-FIELDS.
           03  WS-ERSW             PIC X        VALUE SPACES.
           03  WS-EFSW             PIC X        VALUE SPACES.
           03  WS-PDID             PIC X(12)    VALUE SPACES.
      
       01  DISPLAY-FIELDS.
           03  DS-QNTY             PIC ZZZ,ZZZ,ZZ9-.
      
       PROCEDURE DIVISION.
      
       DECLARATIVES.
           COPY "sm/cpy/dcstmt".
           COPY "fg/cpy/dcfgmt".
           COPY "sa/cpy/dcsaht".
           COPY "fg/cpy/dcfgdt".
           COPY "sa/cpy/dcsaid".
           COPY "op/cpy/dcopod".
       END DECLARATIVES.
      
       A000-MAINLINE-PGM SECTION.
      
       A000-START-PROGRAM.
           PERFORM B000-INIT THRU B990-EXIT.
       A100-MAINLINE.
           PERFORM D000-PRODUCT-ID THRU D990-EXIT.
           IF MAIN-77 = "QT"
               GO TO A800-CLOSING.
           IF MAIN-77 = "DL"
               GO TO A600-DELETE-ROUTINE.
       A190-MALFUNCTION.
           MOVE "01" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
      *
       A600-DELETE-ROUTINE.
           PERFORM F000-DELETE-ROUTINE THRU F990-EXIT.
           GO TO A100-MAINLINE.
      *
       A800-CLOSING.
           PERFORM Y000-CLOSE THRU Y990-EXIT.
       A990-EXIT.
           STOP RUN.
      
      
       B000-INIT.
           MOVE "FG"     TO WS-STNM.
           MOVE "PDMD"   TO WS-PGNM.
           MOVE "FGPDMD" TO PROGRAM-NAME.
           COPY "uv/cpy/uvcusf".
           CALL "SYSTEM" USING SCREEN-ID.
           COPY "uv/cpy/uvdssh".
       B100-OPEN-FILS.
           OPEN INPUT MM-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN I-O   FM-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN I-O   SH-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN INPUT SX-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN INPUT OD-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN INPUT FX-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
       B310-REVIEW-SW.
           IF SW-1-ON
               DISPLAY "SWITCH-1" LINE 23 POSITION 1.
       B990-EXIT.
           EXIT.
      
      
       D000-PRODUCT-ID.
           PERFORM X900-CLEAR THRU X990-EXIT.
       D100-ACK-PDID.
      *    ACCEPT WS-PDID
      *        LINE 07 POSITION 33
      *        PROMPT TAB ECHO
      *      ON EXCEPTION
      *        FUNC-KEY
      *        GO TO D190-CHECK-FUNC.
      *---new---
           DISPLAY SPACES LINE 07 POSITION 33 SIZE 40.
           move sy-st-cpno to ls-uvgpid-cpno.
           MOVE "PR"  TO LS-uvgpid-CMCD.
           MOVE spaces TO LS-uvgpid-pdid.
           MOVE 07 TO LS-uvgpid-line.
           MOVE 33 TO LS-uvgpid-colm.
           call "UVGPID" using ls-uvgpid-rcrd.
           IF LS-uvgpid-CMCD = "02"
               DISPLAY SPACES LINE 07 POSITION 33 SIZE 12
               MOVE "QT" TO MAIN-77
               GO TO D990-EXIT.
           MOVE LS-uvgpid-pdid TO WS-pdid.
      *---new---
           IF WS-PDID = SPACES
               DISPLAY "PRODUCT ID IS MANDATORY "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO D100-ACK-PDID.
           DISPLAY WS-PDID LINE 07 POSITION 33.
           GO TO D200-READ-FM.
      *
      *D190-CHECK-FUNC.
      *    IF FUNC-02
      *        DISPLAY SPACES LINE 07 POSITION 33 SIZE 12
      *        MOVE "QT" TO MAIN-77
      *        GO TO D990-EXIT.
      *    PERFORM X000-FUNC-ERR THRU X090-EXIT.
      *    GO TO D100-ACK-PDID.
      *
       D200-READ-FM.
           PERFORM X300-READ-FM THRU X350-EXIT.
           IF WS-ERSW = "Y"
               DISPLAY "PRODUCT ID NOT ON FILE "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO D100-ACK-PDID.
       D300-SET-DP.
       D320-READ-MM.
           MOVE SPACES         TO MM-61-KEY-1-WS MM-KEY-1.
           MOVE SY-ST-CPNO     TO MM-61-CPNO-1.
           MOVE 61             TO MM-61-LFCD-1.
           MOVE FM-31-PCCD-2   TO MM-61-PCCD-1.
           MOVE MM-61-KEY-1-WS TO MM-KEY-1.
           READ MM-FILE
               INTO MM-61-RCRD-WS
               KEY IS MM-KEY-1
             INVALID KEY
               MOVE "* NOT ON FILE *" TO MM-61-DESC
               GO TO D330-SBID.
           IF RECORD-LOCKED
               MOVE "STMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO D320-READ-MM.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       D330-SBID.
           IF FM-31-SBID = SPACES
               MOVE SPACES TO LS-FGGSII-DESC
               GO TO D340-SET-QNTY.
           MOVE "00"       TO LS-FGGSII-CMCD.
           MOVE SY-ST-CPNO TO LS-FGGSII-CPNO.
           MOVE FM-31-SBID TO LS-FGGSII-SBID.
           MOVE SPACES     TO LS-FGGSII-DESC.
           MOVE SPACES     TO LS-FGGSII-PCCD.
           PERFORM X200-FGGSII THRU X205-EXIT.
           IF LS-FGGSII-CMCD = "05"
               MOVE "* NOT ON FILE *" TO LS-FGGSII-DESC.
       D340-SET-QNTY.
           MOVE FM-31-PKQT TO DS-QNTY.
       D400-DISPLY.
           DISPLAY FM-31-DESC   LINE 09 POSITION 33
                   FM-31-PCCD-2 LINE 10 POSITION 41
                   MM-61-DESC   LINE 10 POSITION 47
                   FM-31-SBID   LINE 11 POSITION 33
                   LS-FGGSII-DESC   LINE 11 POSITION 47
                   DS-QNTY      LINE 13 POSITION 34.
           MOVE FM-31-PQWT TO DS-QNTY.
           DISPLAY DS-QNTY      LINE 14 POSITION 34.
           MOVE FM-31-CTUN TO DS-QNTY.
           DISPLAY DS-QNTY      LINE 15 POSITION 34.
           MOVE FM-31-PRUN TO DS-QNTY.
           DISPLAY DS-QNTY      LINE 16 POSITION 34.
       D710-SET-VALUES.
           MOVE "DL" TO MAIN-77.
       D990-EXIT.
           EXIT.
      
      
       F000-DELETE-ROUTINE.
           DISPLAY "ARE YOU SURE? (Y/N) "
               LINE 24 POSITION 1 HIGH.
           ACCEPT DUMMY-77
               LINE 24 POSITION 21
               PROMPT TAB ECHO.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           IF DUMMY-77 = "N"
               PERFORM X250-UNLOCK THRU X255-EXIT
               GO TO F990-EXIT.
           IF DUMMY-77 NOT = "Y"
               GO TO F000-DELETE-ROUTINE.
       F090-DISPLY.
           IF SW-1-OFF
               DISPLAY "EDIT PROGRESS"
                   LINE 18 POSITION 11 HIGH.
       F100-READ-HISTORY.
           PERFORM X600-READ-SAHT THRU X625-EXIT.
           IF WS-ERSW = "Y"
               MOVE "F1" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
       F110-CHECK-SWITCH.
           IF SW-1-ON
               GO TO F800-DELETE-HISTORY.
       F115-REVIEW-VALUES.

           IF FM-31-QTBK = ZERO
             AND FM-31-IVVL = ZERO
               GO TO F130-START-OPOD.

           if fm-31-qtbk > zero
             or fm-31-qtbk < zero
               go to f120-error.
           if fm-31-ivvl > zero
             or fm-31-ivvl < zero
               go to f120-error.
           GO TO F130-START-OPOD.

       F120-ERROR.
           DISPLAY "BOTH QUANTITY BOOKED AND INVENTORY VALUE "
                       LINE 24 POSITION 1 HIGH
                   "MUST BE ZERO "
                       LINE 00 POSITION 0 HIGH.
           PERFORM X050-REPLY THRU X090-EXIT.
           PERFORM X250-UNLOCK THRU X255-EXIT.
           GO TO F990-EXIT.
       F130-START-OPOD.
           PERFORM X370-START-OPOD THRU X375-EXIT.
           IF WS-ERSW = "Y"
               GO TO F190-START-FM.
       F135-DISPLY.
           DISPLAY "OP" LINE 18 POSITION 33.
       F140-READ-OPOD.
           PERFORM X380-READ-OPOD THRU X385-EXIT.
           IF WS-EFSW = "Y"
               GO TO F190-START-FM.
       F145-EDIT.
           IF OD-CPNO-2 NOT = SY-ST-CPNO
               GO TO F190-START-FM.
           DISPLAY OD-ODNO-2 LINE 18 POSITION 37.
           IF OD-PDID-2 NOT = WS-PDID
               GO TO F140-READ-OPOD.
           DISPLAY "FINISHED GOODS ORDER ON FILE "
               LINE 24 POSITION 01 HIGH
           PERFORM X050-REPLY THRU X090-EXIT
           PERFORM X250-UNLOCK THRU X255-EXIT
           GO TO F990-EXIT.
      *
       F190-START-FM.
           MOVE SPACES         TO FM-41-KEY-2-WS FM-KEY-2.
           MOVE SY-ST-CPNO     TO FM-41-CPNO-2.
           MOVE 41             TO FM-41-LFCD-2.
           MOVE WS-PDID        TO FM-41-PDID-2.
           MOVE FM-41-KEY-2-WS TO FM-KEY-2.
           START FM-FILE
               KEY IS NOT LESS THAN FM-KEY-2
             INVALID KEY
               GO TO F250-READ-FM.
           IF RECORD-LOCKED
               MOVE "FGMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO F190-START-FM.
           IF NOT FILE-OK
               MOVE "ST" TO FILE-IO
               GO TO Z100-FILE-ERR.
       F195-DISPLY.
           DISPLAY SPACES LINE 18 POSITION 33 SIZE 10
                   "FG"   LINE 18 POSITION 33.
       F200-READ-FM.
           READ FM-FILE
               NEXT RECORD
               INTO FM-41-RCRD-WS
             AT END
               GO TO F250-READ-FM.
           IF NOT RECORD-LOCKED
               GO TO F210-CHECK-FILE.
           MOVE "FGMT" TO WS-LOCK-FILE.
           PERFORM Z200-ACK-LOCK THRU Z290-EXIT.
           GO TO F200-READ-FM.
      *
       F210-CHECK-FILE.
           IF NOT FILE-OK
               MOVE "RS" TO FILE-IO
               GO TO Z100-FILE-ERR.
       F220-EDIT.
           IF FM-41-CPNO-2 NOT = SY-ST-CPNO
               GO TO F250-READ-FM.
           IF FM-41-LFCD-2 NOT = 41
               GO TO F250-READ-FM.
           IF FM-41-PDID-2 NOT = WS-PDID
               GO TO F250-READ-FM.
       F230-ERROR.
           DISPLAY "ZONE PRODUCT MASTER RECORD STILL ON FILE "
               LINE 24 POSITION 1 HIGH
           display "( ZONE " 
               line 00 position 00 high.
           display fm-41-pzcd-1 
               line 00 position 00 high.
           display " ) "
               line 00 position 00 high.
           PERFORM X050-REPLY THRU X090-EXIT.
           PERFORM X250-UNLOCK THRU X255-EXIT.
           GO TO F990-EXIT.
      *
       F250-READ-FM.
           PERFORM X300-READ-FM THRU X350-EXIT.
           IF WS-ERSW = "Y"
               MOVE "31" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
      *
      *    REVIEW FINISHED GOODS DETAIL FILE
      *    REVIEW 1 - ACCOUNTING TRANSACTIONS
      *
       F290-START-FX.
           MOVE SPACES         TO FX-11-KEY-1-WS FX-KEY-1.
           MOVE SY-ST-CPNO     TO FX-11-CPNO-1.
           MOVE 11             TO FX-11-LFCD-1.
           MOVE FM-31-PCCD-2   TO FX-11-PCCD-1.
           MOVE FM-31-PDID-2   TO FX-11-PDID-1.
           MOVE FX-11-KEY-1-WS TO FX-KEY-1.
           PERFORM X460-START-FX THRU X490-EXIT.
           IF WS-ERSW = "Y"
               GO TO F450-START-SAID.
       F300-READ-FX.
           PERFORM X500-READ-FX THRU X590-EXIT.
           IF WS-ERSW = "Y"
               GO TO F450-START-SAID.
           MOVE FX-RCRD-WS TO FX-11-RCRD-WS.
       F310-EDIT.
           IF FX-11-CPNO-1 NOT = SY-ST-CPNO
               GO TO F450-START-SAID.
           IF FX-11-LFCD-1 NOT = 11
               GO TO F450-START-SAID.
           IF FX-11-PCCD-1 NOT = FM-31-PCCD-2
               GO TO F450-START-SAID.
           IF FX-11-PDID-1 NOT = FM-31-PDID-2
               GO TO F450-START-SAID.
       F330-ERROR.
           GO TO F430-ERROR.
      *
      * FOR FUTURE CONSIDERATIONS - WAREHOUSE
      *
       F390-START-FX.
           MOVE SPACES         TO FX-21-KEY-1-WS FX-KEY-1.
           MOVE SY-ST-CPNO     TO FX-21-CPNO-1.
           MOVE 21             TO FX-21-LFCD-1.
           MOVE FM-31-PCCD-2   TO FX-21-PCCD-1.
           MOVE FM-31-PDID-1   TO FX-21-PDID-1.
           MOVE FX-21-KEY-1-WS TO FX-KEY-1.
           PERFORM X460-START-FX THRU X490-EXIT.
           IF WS-ERSW = "Y"
               GO TO F450-START-SAID.
       F400-READ-FX.
           PERFORM X500-READ-FX THRU X590-EXIT.
           IF WS-ERSW = "Y"
               GO TO F450-START-SAID.
           MOVE FX-RCRD-WS TO FX-21-RCRD-WS.
       F410-EDIT.
           IF FX-21-CPNO-1 NOT = SY-ST-CPNO
               GO TO F450-START-SAID.
           IF FX-21-LFCD-1 NOT = 21
               GO TO F450-START-SAID.
           IF FX-21-PCCD-1 NOT = FM-31-PCCD-2
               GO TO F450-START-SAID.
           IF FX-21-PDID-1 NOT = WS-PDID
               GO TO F450-START-SAID.
       F430-ERROR.
           DISPLAY "FINISHED GOODS DETAIL RECORDS ON FILE "
               LINE 24 POSITION 1 HIGH.
           PERFORM X050-REPLY THRU X090-EXIT.
           PERFORM X250-UNLOCK THRU X255-EXIT.
           GO TO F990-EXIT.
      *
       F450-START-SAID.
           PERFORM X410-START-SAID THRU X415-EXIT.
           IF WS-ERSW = "Y"
               GO TO F800-DELETE-HISTORY.
       F460-DISPLY.
           DISPLAY SPACES LINE 18 POSITION 33 SIZE 10
                   "SA"   LINE 18 POSITION 33.
       F470-READ-SAID.
           PERFORM X420-READ-SAID THRU X425-EXIT.
           IF WS-EFSW = "Y"
               GO TO F800-DELETE-HISTORY.
       F480-EDIT.
           IF SX-CPNO-3 NOT = SY-ST-CPNO
               GO TO F800-DELETE-HISTORY.
           IF SX-PCCD-3 NOT = FM-31-PCCD-2
               GO TO F800-DELETE-HISTORY.
           IF SX-PDID-3 NOT = WS-PDID
               GO TO F800-DELETE-HISTORY.
           DISPLAY "SALES ANALYSIS DETAIL RECORD ON FILE "
                       LINE 24 POSITION 1 HIGH
           PERFORM X050-REPLY THRU X090-EXIT
           PERFORM X250-UNLOCK THRU X255-EXIT
           GO TO F990-EXIT.
      *
       F800-DELETE-HISTORY.
           IF SW-1-OFF
               DISPLAY SPACES     LINE 18 POSITION 33 SIZE 10
                       "FINISHED" LINE 18 POSITION 33.
           PERFORM X600-READ-SAHT THRU X625-EXIT.
       F800-DELETE-SH.
           DELETE SH-FILE RECORD.
           IF NOT FILE-OK
               MOVE "DL" TO FILE-IO
               GO TO Z100-FILE-ERR.
       F810-DELETE-FM.
           PERFORM X300-READ-FM THRU X350-EXIT.
           IF WS-ERSW = "Y"
               MOVE "F2" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
           DELETE FM-FILE RECORD.
           IF NOT FILE-OK
               MOVE "DL" TO FILE-IO
               GO TO Z100-FILE-ERR.
       F850-DISPLY.
           DISPLAY "PRODUCT ID HAS BEEN DELETED "
               LINE 24 POSITION 1 HIGH
           PERFORM X050-REPLY THRU X090-EXIT.
       F990-EXIT.
           EXIT.
      
      
       X000-FUNC-ERR.
           DISPLAY "FUNCTION KEY NOT VALID "
               LINE 24 POSITION 1 HIGH.
       X050-REPLY.
           ACCEPT DUMMY-77 LINE 00 POSITION 00 TAB OFF.
           DISPLAY SPACES LINE 24 POSITION 01 SIZE 80.
       X090-EXIT.
           EXIT.
      
      
       X200-FGGSII.
           CALL "FGGSII" USING
               LS-FGGSII-RCRD.
       X205-EXIT.
           EXIT.
      
      
       X250-UNLOCK.
           UNLOCK FM-FILE RECORD.
           IF NOT FILE-OK
               MOVE "UL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           UNLOCK SH-FILE RECORD.
           IF NOT FILE-OK
               MOVE "UL" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X255-EXIT.
           EXIT.
      
      
       X300-READ-FM.
           MOVE SPACES         TO WS-ERSW.
           MOVE SPACES         TO FM-31-KEY-1-WS FM-KEY-1.
           MOVE SY-ST-CPNO     TO FM-31-CPNO-1.
           MOVE 31             TO FM-31-LFCD-1.
           MOVE WS-PDID        TO FM-31-PDID-1.
           MOVE FM-31-KEY-1-WS TO FM-KEY-1.
           READ FM-FILE
               INTO FM-31-RCRD-WS
               KEY IS FM-KEY-1
             INVALID KEY
               MOVE "Y" TO WS-ERSW
               GO TO X350-EXIT.
           IF RECORD-LOCKED
               MOVE "FGMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X300-READ-FM.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X350-EXIT.
           EXIT.
      
      
       X370-START-OPOD.
           MOVE SPACES         TO OD-KEY-2 WS-ERSW.
           MOVE SY-ST-CPNO     TO OD-CPNO-K2.
           START OD-FILE
               KEY IS NOT LESS THAN OD-KEY-2
             INVALID KEY
               MOVE "Y" TO WS-ERSW
               GO TO X375-EXIT.
           IF RECORD-LOCKED
               MOVE "OPOD" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X370-START-OPOD.
           IF NOT FILE-OK
               MOVE "ST" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X375-EXIT.
           EXIT.
      
      
       X380-READ-OPOD.
           MOVE SPACES TO WS-EFSW.
           READ OD-FILE
               NEXT RECORD
               WITH NO LOCK
               INTO OD-RCRD-WS
             AT END
               MOVE "Y" TO WS-EFSW
               GO TO X385-EXIT.
           IF NOT RECORD-LOCKED
               GO TO X384-CHECK-FILE.
           MOVE "OPOD" TO WS-LOCK-FILE.
           PERFORM Z200-ACK-LOCK THRU Z290-EXIT.
           GO TO X380-READ-OPOD.
      *
       X384-CHECK-FILE.
           IF NOT FILE-OK
               MOVE "RS" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X385-EXIT.
           EXIT.
      
      
       X410-START-SAID.
           MOVE SPACES         TO SX-KEY-3 WS-ERSW.
           MOVE SY-ST-CPNO     TO SX-CPNO-K3.
           MOVE FM-31-PCCD-2   TO SX-PCCD-K3.
           MOVE WS-PDID        TO SX-PDID-K3.
           START SX-FILE
               KEY IS NOT LESS THAN SX-KEY-3
             INVALID KEY
               MOVE "Y" TO WS-ERSW
               GO TO X415-EXIT.
           IF RECORD-LOCKED
               MOVE "SAID" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X410-START-SAID.
           IF NOT FILE-OK
               MOVE "ST" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X415-EXIT.
           EXIT.
      
       X420-READ-SAID.
           MOVE SPACES TO WS-EFSW.
           READ SX-FILE
               NEXT RECORD
               WITH NO LOCK
               INTO SX-RCRD-WS
             AT END
               MOVE "Y" TO WS-EFSW
               GO TO X425-EXIT.
           IF NOT RECORD-LOCKED
               GO TO X424-CHECK-FILE.
           MOVE "SAID" TO WS-LOCK-FILE.
           PERFORM Z200-ACK-LOCK THRU Z290-EXIT.
           GO TO X420-READ-SAID.
      *
       X424-CHECK-FILE.
           IF NOT FILE-OK
               MOVE "RS" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X425-EXIT.
           EXIT.
      
      
       X460-START-FX.
           MOVE SPACES TO WS-ERSW.
           START FX-FILE
               KEY IS NOT LESS THAN FX-KEY-1
             INVALID KEY
               MOVE "Y" TO WS-ERSW
               GO TO X490-EXIT.
           IF RECORD-LOCKED
               MOVE "FGDT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X460-START-FX.
           IF NOT FILE-OK
               MOVE "ST" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X490-EXIT.
           EXIT.
      
      
       X500-READ-FX.
           MOVE SPACES TO WS-ERSW.
           READ FX-FILE
               NEXT RECORD
               INTO FX-RCRD-WS
             AT END
               MOVE "Y" TO WS-ERSW
               GO TO X590-EXIT.
           IF NOT RECORD-LOCKED
               GO TO X510-CHECK-FILE.
           MOVE "FGDT" TO WS-LOCK-FILE.
           PERFORM Z200-ACK-LOCK THRU Z290-EXIT.
           GO TO X500-READ-FX.
      *
       X510-CHECK-FILE.
           IF NOT FILE-OK
               MOVE "RS" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X520-EDIT.
           IF FX-CPNO-1 NOT = SY-ST-CPNO
               MOVE "Y" TO WS-ERSW.
       X590-EXIT.
           EXIT.
      
       X600-READ-SAHT.
           MOVE SPACES         TO SH-41-KEY-1-WS WS-ERSW.
           MOVE SY-ST-CPNO     TO SH-41-CPNO-1.
           MOVE 41             TO SH-41-LFCD-1.
           MOVE FM-31-PCCD-2   TO SH-41-PCCD-1.
           MOVE WS-PDID        TO SH-41-PDID-1.
           MOVE SH-41-KEY-1-WS TO SH-KEY-1.
           READ SH-FILE
               INTO SH-41-RCRD-WS
               KEY IS SH-KEY-1
             INVALID KEY
               MOVE "Y" TO WS-ERSW
               GO TO X625-EXIT.
           IF RECORD-LOCKED
               MOVE "SAHT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X600-READ-SAHT.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X625-EXIT.
           EXIT.
      
       X900-CLEAR.
           MOVE SPACES TO MAIN-77.
           DISPLAY SPACES LINE 18 POSITION 11 SIZE 43
                   SPACES LINE 16 POSITION 34 SIZE 12
                   SPACES LINE 15 POSITION 34 SIZE 12
                   SPACES LINE 14 POSITION 34 SIZE 12
                   SPACES LINE 13 POSITION 34 SIZE 12
                   SPACES LINE 11 POSITION 33 SIZE 39
                   SPACES LINE 10 POSITION 41 SIZE 31
                   SPACES LINE 09 POSITION 33 SIZE 25.
       X990-EXIT.
           EXIT.
      
      
       Y000-CLOSE.
           CLOSE MM-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE FM-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE SH-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE FX-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE SX-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE OD-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
       Y990-EXIT.
           EXIT.
      
           COPY "uv/cpy/uvdfemov".
           COPY "uv/cpy/uvaflk".
