       IDENTIFICATION DIVISION.
       PROGRAM-ID.                 FGPDAR.
      *
      *    FINISHED GOODS
      *    PRODUCT ACTIVITY REPORT
      *
      *    SWITCHES
      *    ********
      *
      *    SWITCH-7 - MONTH END
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           SWITCH-7
               ON  IS SW-7-ON
               OFF IS SW-7-OFF.
      
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           COPY "sm/cpy/slstmt".
           COPY "fg/cpy/slfgmt".
           COPY "fg/cpy/slfgdt".
           COPY "uv/cpy/slprnt".
           COPY "uv/cpy/slsort".
      
       DATA DIVISION.
       FILE SECTION.
           COPY "sm/cpy/fdstmt".
           COPY "fg/cpy/fdfgmt".
           COPY "fg/cpy/fdfgdt".
           COPY "uv/cpy/fdprnt".
      
       SD  SORT-FILE
           DATA RECORD IS SORT-RCRD.
       01  SORT-RCRD.
           03  SORT-KEY-1.
               05  SORT-PCCD-K1    PIC X(4).
               05  SORT-PDID-K1    PIC X(12).
               05  SORT-TRNO-K1    PIC X(5).
           03  FILLER              PIC X(38).

       WORKING-STORAGE SECTION.
      
       77  MP-STATUS-77            PIC XX.
       77  MM-STATUS-77            PIC XX.
       77  FM-STATUS-77            PIC XX.
       77  FX-STATUS-77            PIC XX.
       77  PR-STATUS-77            PIC XX.
       77  SCREEN-ID               PIC X(35)   VALUE
           "$PMISDIR/appl/obj/c/dsplscrn fgpdar".
       77  DUMMY-77                PIC X.
       77  MAIN-77                 PIC XX.
      
           COPY "uv/cpy/uvfckv".
           COPY "uv/cpy/uvpgid".
           COPY "uv/cpy/uvsfemov".
           COPY "uv/cpy/uvlkmg".
           COPY "uv/cpy/uvedpr".
      
           COPY "uv/cpy/rcsymtst".
           COPY "uv/cpy/rcsymtcp".
           COPY "sm/cpy/rcstmt61".
           COPY "fg/cpy/rcfgmt".
           COPY "fg/cpy/rcfgmt01".
           COPY "fg/cpy/rcfgmt31".
           COPY "fg/cpy/rcfgdt11".
      
           COPY "uv/cpy/lsuvcusf".
           COPY "uv/cpy/lsuvdssh".
           COPY "fg/cpy/lsfggpdi".
           COPY "fg/cpy/lsfgemsd".
           COPY "uv/cpy/lsuvacdt".
           COPY "uv/cpy/lsuvdtcv".
      
       01  PR-HDR1.
           03  FILLER              PIC X(11)   VALUE "REPORT ID:".
           03  PR-PGID             PIC X(7).
           03  FILLER              PIC X(34)   VALUE SPACES.
           03  PR-CPNO             PIC 99.
           03  FILLER              PIC X       VALUE SPACES.
           03  PR-CPNM             PIC X(25).
           03  FILLER              PIC X(32)   VALUE SPACES.
           03  FILLER              PIC X(16)   VALUE "PAGE:".
           03  PR-PGNO             PIC ZZZ9.
      
       01  PR-HDR2.
           03  PR-RPT-RANGE.
               05  FILLER          PIC X(11)   VALUE "RPT RANGE:".
               05  PR-S-PCCD       PIC X(4).
               05  FILLER          PIC X(4)    VALUE " TO".
               05  PR-E-PCCD       PIC X(4).
               05  FILLER          PIC X(2)    VALUE ",".
               05  PR-S-PDID       PIC X(12).
               05  FILLER          PIC X(4)    VALUE " TO".
               05  PR-E-PDID       PIC X(12).
               05  FILLER          PIC X(2)    VALUE ",".
               05  PR-IPCD         PIC X.
           03  FILLER              PIC X(49)   VALUE SPACES.
           03  FILLER              PIC X(19)   VALUE "REPORT DATE:".
           03  PR-SODT             PIC 99/99/99.
      
       01  PR-HDR3.
           03  FILLER              PIC X(11)   VALUE "RUN BY   :".
           03  PR-USID             PIC XXX.

           03  FILLER              PIC X(21)   VALUE SPACES.
           03  FILLER              PIC X(24)   VALUE
                   "PRODUCT ACTIVITY - FROM ".
           03  PR-S-DATE           PIC 99/99/99.
           03  FILLER              PIC X(4)    value " TO ".
           03  PR-E-DATE           PIC 99/99/99.
           03  FILLER              PIC X(22)   VALUE SPACES.

           03  FILLER              PIC X(17)   VALUE "RUN DATE & TIME:".
           03  PR-RNDT             PIC 99/99/99.
           03  FILLER              PIC X       VALUE SPACES.
           03  PR-HHMM             PIC 99/99.
      
       01  PR-HDR4.
           03  FILLER              PIC X(49)   VALUE SPACES.
           03  FILLER              PIC X(14)   VALUE "PRODUCT CLASS".
           03  PR-PCCD             PIC X(4).
           03  FILLER              PIC X(1)    VALUE SPACES.
           03  PR-PCDC             PIC X(25).
      
       01  PR-HDR5.
           03  FILLER              PIC X(30)   VALUE "TRANS".
           03  FILLER              PIC X(12)   VALUE "USER".
           03  FILLER              PIC X(66)   VALUE "SYSTEM".
           03  FILLER              PIC X(6)    VALUE "BOOKED".
      
       01  PR-HDR6.
           03  FILLER              PIC X(10)   VALUE "   NO".
           03  FILLER              PIC X(10)   VALUE "SRC".
           03  FILLER              PIC X(10)   VALUE "DATE".
           03  FILLER              PIC X(12)   VALUE "RFRNCE".
           03  FILLER              PIC X(13)   VALUE "RFRNCE".
           03  FILLER              PIC X(20)   VALUE "QUANTITY".
           03  FILLER              PIC X(17)   VALUE "RATE".
           03  FILLER              PIC X(14)   VALUE "COST".
           03  FILLER              PIC X(20)   VALUE "QUANTITY".
           03  FILLER              PIC X(5)    VALUE "VALUE".
      
       01  PR-HDR7.
           03  FILLER              PIC X(10)   VALUE "*****".
           03  FILLER              PIC X(12)   VALUE "PRODUCT ID".
           03  PR-PDID             PIC X(12).
           03  FILLER              PIC X(1)    VALUE SPACES.
           03  PR-PIDC             PIC X(25).
      
       01  PR-HDR8.
           03  FILLER              PIC X(132)  VALUE ALL "=".
      
       01  PR-DTL1.
           03  FILLER              PIC X(10)   VALUE SPACES.
           03  FILLER              PIC X(8)    VALUE "***".
           03  PR-OCDT             PIC 99/99/99.
           03  FILLER              PIC X(4)    VALUE SPACES.
           03  PR-DTWD             PIC X(7).
           03  FILLER              PIC X(66)   VALUE SPACES.
           03  PR-1-QTBK           PIC ZZZ,ZZZ,ZZ9-.
           03  FILLER              PIC X(4)    VALUE SPACES.
           03  PR-1-IVVL           PIC Z,ZZZ,ZZZ.99-.
      
       01  PR-DTL2.
           03  PR-TRNO             PIC ZZZZ9.
           03  FILLER              PIC X(5)    VALUE SPACES.
           03  PR-SRCD             PIC XXX.
           03  FILLER              PIC X(5)    VALUE SPACES.
           03  PR-TRDT             PIC 99/99/99.
           03  FILLER              PIC X(4)    VALUE SPACES.
           03  PR-RFNO             PIC X(8).
           03  FILLER              PIC X(4)    VALUE SPACES.
           03  PR-SYRF             PIC X(6).
           03  PR-SYRF-1           REDEFINES PR-SYRF.
               05  FILLER          PIC X.
               05  PR-VDNO         PIC 9(5).
           03  PR-SYRF-2           REDEFINES PR-SYRF.
               05  PR-SRNO         PIC ZZZZZ9.
           03  FILLER              PIC X(4)    VALUE SPACES.
           03  PR-TRQT             PIC ZZZ,ZZZ,ZZ9-.
           03  FILLER              PIC X(5)    VALUE SPACES.
           03  PR-RATE             PIC ZZZ,ZZZ.99-.
           03  FILLER              PIC X(4)    VALUE SPACES.
           03  PR-TRCT             PIC Z,ZZZ,ZZZ.99-.
           03  FILLER              PIC X(6)    VALUE SPACES.
           03  PR-QTBK             PIC ZZZ,ZZZ,ZZ9-.
           03  FILLER              PIC X(4)    VALUE SPACES.
           03  PR-IVVL             PIC Z,ZZZ,ZZZ.99-.
      
       01  PR-TOT1.
           03  FILLER              PIC X(30)   VALUE SPACES.
           03  PR-TLWD             PIC X(11).
           03  FILLER              PIC X(77)   VALUE SPACES.
           03  PR-TL-IVVL          PIC ZZ,ZZZ,ZZZ.99-.
      
       01  SORT-RCRD-WS.
           03  SORT-KEY.
               05  SORT-PCCD         PIC X(4).
               05  SORT-PDID         PIC X(12).
               05  SORT-TRNO         PIC 9(5).
           03  SORT-DATA.
               05  SORT-SRCD         PIC X(3).
               05  SORT-TRDT         PIC 9(8).
               05  SORT-RFNO         PIC X(8).
               05  SORT-SRNO         PIC 9(7)      COMP-3.
               05  SORT-TRQT         PIC S9(9)     COMP-3.
               05  SORT-TRCT         PIC S9(7)V99  COMP-3.
               05  SORT-CTUN         PIC S9(9)     COMP-3.
      
       01  CONTROL-FIELDS.
           03  WS-PRSW             PIC X.
           03  WS-SORT             PIC X       VALUE SPACES.
           03  WS-ERSW             PIC X.
           03  WS-EFSW             PIC X.
           03  WS-PGNO             PIC 9(4).
           03  WS-RFSW             PIC X.
           03  WS-OPSW             PIC X.
           03  WS-LINE             PIC 9(4).
           03  WS-TIME             PIC 9(8).
           03  RED-WS-TIME         REDEFINES WS-TIME.
               05  WS-HHMM         PIC 9(4).
               05  FILLER          PIC 9(4).
           03  WS-RNDT             PIC 9(6).
           03  WS-S-PCCD           PIC X(4).
           03  WS-E-PCCD           PIC X(4).
           03  WS-PCCD             PIC X(4).
           03  WS-S-PDID           PIC X(12).
           03  WS-E-PDID           PIC X(12).
           03  WS-PDID             PIC X(12).
           03  WS-IPCD             PIC X.
               88 VALID-IPCD       VALUES ARE
                  "A", "B", "C", "X".

           03  WS-S-DATE          PIC 9(8).

           03  WS-MIN-DATE-NUMERIC  PIC 9(8).
           03  WS-MIN-DATE redefines WS-MIN-DATE-NUMERIC.
               05  filler          pic xxxx.
               05  filler          pic xx.
               05  WS-MIN-DAY      pic 99.

           03  WS-E-DATE           PIC 9(8).
      
       01  STORAGE-FIELDS.
           03  WS-RATE             PIC S9(6)V99    COMP-3.
           03  WS-QTBK             PIC S9(9)       COMP-3.
           03  WS-IVVL             PIC S9(9)V99    COMP-3.
           03  WS-CLTL             PIC S9(9)V99    COMP-3.
           03  WS-GRTL             PIC S9(9)V99    COMP-3.
      
       01  DISPLAY-FIELDS.
           03  DS-DATE             PIC 99/99/99.
           03  DS-7-0              PIC z,zzz,zz9-.
      
       PROCEDURE DIVISION.
      
       DECLARATIVES.
           COPY "sm/cpy/dcstmt".
           COPY "fg/cpy/dcfgmt".
           COPY "fg/cpy/dcfgdt".
           COPY "uv/cpy/dcprnt".
       END DECLARATIVES.
      
       A000-MAINLINE-PGM SECTION.
      
       A000-START-PROGRAM.
           PERFORM B000-INIT THRU B990-EXIT.
           IF MAIN-77 = "QT"
               GO TO A800-CLOSING.
           PERFORM D000-PARAMETERS THRU D990-EXIT.
           IF MAIN-77 = "QT"
               GO TO A800-CLOSING.
       A100-MAINLINE.
           SORT SORT-FILE ON ASCENDING KEY SORT-KEY-1
                INPUT PROCEDURE IS 
                     F000-SORT-DATA THRU F990-EXIT
                OUTPUT PROCEDURE IS 
                     H000-PROCESS THRU H990-EXIT.
       A800-CLOSING.
           PERFORM Y000-CLOSE THRU Y990-EXIT.
       A990-EXIT.
           STOP RUN.
      
       B000-INIT.
           MOVE "FG"     TO WS-STNM.
           MOVE "PDAR"   TO WS-PGNM.
           MOVE "FGPDAR" TO PROGRAM-NAME.
           COPY "uv/cpy/uvcusf".
           CALL "SYSTEM" USING SCREEN-ID.
           COPY "uv/cpy/uvdssh".
           MOVE "PDA "   TO WS-PGNM.
       B100-OPEN-FILS.
           OPEN INPUT MM-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN INPUT FM-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN INPUT FX-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
       B300-REVIEW-DATE.
           MOVE SPACES     TO FM-KEY-1.
           MOVE SY-ST-CPNO TO FM-CPNO-K1.
           MOVE "01"       TO FM-LFCD-K1.
           PERFORM X500-READ-FM THRU X590-EXIT.
           IF WS-ERSW = "Y"
               MOVE "03" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
           MOVE FM-RCRD-WS TO FM-01-RCRD-WS.
       B310-CALC-DATE.
           MOVE FM-01-CRYR TO WS-UVEDPR-CRYR.
           IF FM-01-FYOS = 00
               MOVE FM-01-CRMO TO WS-UVEDPR-CRMO
               GO TO B320-CONTINUE.
           ADD FM-01-CRMO FM-01-FYOS
               GIVING WS-UVEDPR-CRMO.
           IF WS-UVEDPR-CRMO > 12
               SUBTRACT 12 FROM WS-UVEDPR-CRMO
               GO TO B320-CONTINUE.
           SUBTRACT 1 FROM WS-UVEDPR-CRYR.
       B320-CONTINUE.
           MOVE SY-ST-SODT TO WS-UVEDPR-SODT.
           IF WS-UVEDPR-CRYM > WS-UVEDPR-SOYM
               DISPLAY "SIGNON DATE MUST BE WITHIN THE "
                           LINE 24 POSITION 1 HIGH
                       "CURRENT ACCOUNTING MONTH "
                           LINE 00 POSITION 0 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               MOVE "QT" TO MAIN-77
               GO TO B990-EXIT.
       B400-SET-MIN-DATE.
           MOVE WS-UVEDPR-CRYR TO WS-UVEDPR-YEAR.
           MOVE WS-UVEDPR-CRMO TO WS-UVEDPR-MONTH.
           IF WS-UVEDPR-SOYM = WS-UVEDPR-CRYM
               GO TO B410-SET-DAY.
           IF WS-UVEDPR-MONTH = 12
               ADD 01 TO WS-UVEDPR-YEAR
               MOVE 01 TO WS-UVEDPR-MONTH
               GO TO B410-SET-DAY.
           ADD 01 TO WS-UVEDPR-MONTH.
       B410-SET-DAY.
           MOVE 01 TO WS-UVEDPR-DAY.
       B420-MIN-DATE.
           MOVE WS-UVEDPR-DATE TO WS-MIN-DATE-NUMERIC.
       B700-SET-VALUES.
           MOVE SPACES TO WS-PRSW MAIN-77.
           GO TO B990-EXIT.
       
       B800-OPEN-PRNT.
           OPEN OUTPUT PR-FILE NO REWIND.
           MOVE "Y"        TO WS-PRSW.
           MOVE WS-PGID    TO PR-PGID.
           MOVE SY-ST-USID TO PR-USID.
           MOVE SY-ST-CPNO TO PR-CPNO.
           MOVE SY-CP-NAME TO PR-CPNM.
           MOVE ZERO       TO WS-PGNO.
           MOVE SY-ST-SODT TO PR-SODT.
           MOVE sy-st-cldt    TO PR-RNDT.
           ACCEPT WS-TIME FROM TIME.
           MOVE WS-HHMM    TO PR-HHMM.
           INSPECT PR-HHMM
               REPLACING ALL "/" BY ":".
       B810-RPT-RANGE.
           IF WS-S-PCCD = "0000"
            AND WS-E-PCCD = "ZZZZ"
             AND WS-S-PDID = "000000000000"
              AND WS-E-PDID = "ZZZZZZZZZZZZ"
               AND WS-IPCD = "X"
                MOVE SPACES TO PR-RPT-RANGE
                GO TO B990-EXIT.
           MOVE WS-S-PCCD TO PR-S-PCCD.
           MOVE WS-E-PCCD TO PR-E-PCCD.
           MOVE WS-S-PDID TO PR-S-PDID.
           MOVE WS-E-PDID TO PR-E-PDID.
           MOVE WS-IPCD   TO PR-IPCD.
       B990-EXIT.
           EXIT.
      
       D000-PARAMETERS.
           IF SW-7-ON
               PERFORM X800-MONTH-END THRU X890-EXIT
               GO TO D990-EXIT.
           DISPLAY "0000"         LINE 08 POSITION 55
                   "ZZZZ"         LINE 09 POSITION 55
                   "000000000000" LINE 11 POSITION 47
                   "ZZZZZZZZZZZZ" LINE 12 POSITION 47
                   "X"            LINE 17 POSITION 58.
           MOVE WS-MIN-DATE-NUMERIC TO DS-DATE.
           display DS-DATE LINE 14 POSITION 51.
           MOVE SY-ST-SODT TO DS-DATE.
           display DS-DATE LINE 15 POSITION 51.
       D100-S-PCCD.
           DISPLAY "0000" LINE 08 POSITION 55
                   "ZZZZ" LINE 09 POSITION 55.
           MOVE "0000" TO WS-S-PCCD.
           ACCEPT WS-S-PCCD
               LINE 08 POSITION 55
               TAB ECHO UPDATE
             ON EXCEPTION
               FUNC-KEY
               GO TO D190-CHECK-FUNC.
           IF WS-S-PCCD = SPACES
               DISPLAY "STARTING PRODUCT CLASS MAY NOT BE SPACES "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO D100-S-PCCD.
           DISPLAY WS-S-PCCD LINE 08 POSITION 55.
           GO TO D200-E-PCCD.
      *
       D190-CHECK-FUNC.
           IF FUNC-02
               PERFORM X900-CLEAR-SCREEN THRU X990-EXIT
               MOVE "QT" TO MAIN-77
               GO TO D990-EXIT.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO D100-S-PCCD.
      *
       D200-E-PCCD.
           DISPLAY "ZZZZ"         LINE 09 POSITION 55
                   "000000000000" LINE 11 POSITION 47.
           MOVE "ZZZZ" TO WS-E-PCCD.
           ACCEPT WS-E-PCCD
               LINE 09 POSITION 55
               TAB ECHO UPDATE
             ON EXCEPTION
               FUNC-KEY
               GO TO D290-CHECK-FUNC.
           IF WS-E-PCCD = SPACES
               DISPLAY "ENDING PRODUCT CLASS MAY NOT BE SPACES "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO D200-E-PCCD.
           IF WS-E-PCCD < WS-S-PCCD
               DISPLAY "ENDING PRODUCT CLASS MAY NOT BE LESS "
                           LINE 24 POSITION 1 HIGH
                       "THAN STARTING PRODUCT CLASS "
                           LINE 00 POSITION 0 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO D200-E-PCCD.
           DISPLAY WS-E-PCCD LINE 09 POSITION 55.
           GO TO D300-S-PDID.
      *
       D290-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 09 POSITION 55 SIZE 4
               GO TO D100-S-PCCD.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO D200-E-PCCD.
      *
       D300-S-PDID.
           DISPLAY "000000000000" LINE 11 POSITION 47
                   "ZZZZZZZZZZZZ" LINE 12 POSITION 47.
           MOVE "000000000000" TO WS-S-PDID.
           ACCEPT WS-S-PDID
               LINE 11 POSITION 47
               TAB ECHO UPDATE
             ON EXCEPTION
               FUNC-KEY
               GO TO D390-CHECK-FUNC.
           IF WS-S-PDID = SPACES
               DISPLAY "STARTING PRODUCT ID MAY NOT BE SPACES "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO D300-S-PDID.
           DISPLAY WS-S-PDID LINE 11 POSITION 47.
           GO TO D400-E-PDID.
      *
       D390-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 11 POSITION 47 SIZE 12
               GO TO D200-E-PCCD.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO D300-S-PDID.
      *
       D400-E-PDID.
      *    following line not necessary
      *    DISPLAY "ZZZZZZZZZZZZ" LINE 12 POSITION 47.
           MOVE ws-min-date-numeric TO DS-DATE.
           display DS-DATE LINE 14 POSITION 51.
           MOVE "ZZZZZZZZZZZZ" TO WS-E-PDID.
           ACCEPT WS-E-PDID
               LINE 12 POSITION 47
               TAB ECHO UPDATE
             ON EXCEPTION
               FUNC-KEY
               GO TO D490-CHECK-FUNC.
           IF WS-E-PDID = SPACES
               DISPLAY "ENDING PRODUCT ID MAY NOT BE SPACES "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO D400-E-PDID.
           IF WS-E-PDID < WS-S-PDID
               DISPLAY "ENDING PRODUCT ID MAY NOT BE LESS "
                           LINE 24 POSITION 1 HIGH
                       "THAN STARTING PRODUCT ID "
                           LINE 00 POSITION 0 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO D400-E-PDID.
           DISPLAY WS-E-PDID LINE 12 POSITION 47.
           GO TO d500-s-date.
      *
       D490-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 12 POSITION 47 SIZE 12
               GO TO D300-S-PDID.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO D400-E-PDID.
      *
       d500-s-date.
      *    MOVE sy-st-sodt TO DS-DATE.
      *    DISPLAY DS-DATE LINE 14 POSITION 51.
           MOVE "NP"       TO LS-UVACDT-CMCD.
           MOVE SPACES     TO LS-UVACDT-NMSW.
           MOVE 14         TO LS-UVACDT-LINE.
           MOVE 51         TO LS-UVACDT-COLM.
           MOVE ws-min-date-numeric TO LS-UVACDT-DATE.
      * trap
      *    display ws-min-date line 16 position 1.
           PERFORM X200-UVACDT THRU X205-EXIT.
           IF LS-UVACDT-CMCD = "02"
               GO TO D400-E-PDID.
           IF LS-UVACDT-CMCD = "00"
               GO TO D500-s-EDIT.
           MOVE "07" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
       D500-s-EDIT.
           move ls-uvacdt-date to ls-uvdtcv-indt.
           move "EX"           to ls-uvdtcv-cmcd.
           perform x999-uvdtcv thru x999-exit.
           IF LS-UVdtcv-otdt > SY-ST-SODT
               DISPLAY "STARTING DATE MUST BE <= SIGNON DATE "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO d500-s-date.
           MOVE LS-UVdtcv-otdt TO ws-s-date pr-s-date.

       d500-e-date.
           display "X"     LINE 17 POSITION 58.
           MOVE "NP"       TO LS-UVACDT-CMCD.
           MOVE SPACES     TO LS-UVACDT-NMSW.
           MOVE 15         TO LS-UVACDT-LINE.
           MOVE 51         TO LS-UVACDT-COLM.
           MOVE sy-st-sodt TO LS-UVACDT-DATE.
      * trap
      *    display sy-st-sodt line 17 position 1.
           PERFORM X200-UVACDT THRU X205-EXIT.
           IF LS-UVACDT-CMCD = "02"
               GO TO D500-s-date.
           IF LS-UVACDT-CMCD = "00"
               GO TO D500-e-EDIT.
           MOVE "D7" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
       D500-e-EDIT.
           move ls-uvacdt-date to ls-uvdtcv-indt.
           move "EX"           to ls-uvdtcv-cmcd.
           perform x999-uvdtcv thru x999-exit.
           IF LS-UVdtcv-otdt > SY-ST-SODT
               DISPLAY "ENDING DATE MUST BE <= SIGNON DATE "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO D500-E-DATE.
           IF LS-UVdtcv-otdt < WS-S-DATE
               DISPLAY "ENDING DATE MUST BE >= STARTING DATE "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO D500-E-DATE.
           MOVE LS-UVdtcv-otdt TO ws-e-date pr-e-date.

       D600-IPCD.
           DISPLAY "X" LINE 17 POSITION 58.
           MOVE "X" TO WS-IPCD.
           ACCEPT WS-IPCD
               LINE 17 POSITION 58
               TAB ECHO UPDATE
             ON EXCEPTION
               FUNC-KEY
               GO TO D690-CHECK-FUNC.
           IF NOT VALID-IPCD
               DISPLAY "INVENTORY PRIORITY MUST BE "
                           LINE 24 POSITION 1 HIGH
                       "'A', 'B', 'C', OR 'X' ( FOR ALL ) "
                           LINE 00 POSITION 0 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO D600-IPCD.
           DISPLAY WS-IPCD LINE 17 POSITION 58.
           GO TO D800-querie.
       D690-CHECK-FUNC.
           IF FUNC-02
               DISPLAY "X" LINE 17 POSITION 58
               GO TO d500-e-date.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO D600-IPCD.

       D800-QUERIE.
           DISPLAY "CONTINUE? (Y/N) "
               LINE 24 POSITION 01 HIGH.
           MOVE "Y" TO DUMMY-77.
           ACCEPT DUMMY-77
               LINE 24 POSITION 17
               TAB ECHO UPDATE
             on exception
               func-key
               go to D800-querie.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           IF DUMMY-77 = "N"
               GO TO D600-ipcd.
           IF DUMMY-77 NOT = "Y"
               GO TO D800-QUERIE.
       D990-EXIT.
           EXIT.
      
       F000-SORT-DATA.
       F090-START-FM.
           MOVE SPACES         TO WS-RFSW.
           MOVE SPACES         TO FM-31-KEY-1-WS FM-KEY-1.
           MOVE SY-ST-CPNO     TO FM-31-CPNO-1.
           MOVE 31             TO FM-31-LFCD-1.
           MOVE WS-S-PDID      TO FM-31-PDID-1.
           MOVE FM-31-KEY-1-WS TO FM-KEY-1.
           START FM-FILE
               KEY IS NOT LESS THAN FM-KEY-1
             INVALID KEY
               GO TO F800-INPUT-COMPLETE.
           IF RECORD-LOCKED
               MOVE "FGMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO F090-START-FM.
           IF NOT FILE-OK
               MOVE "ST" TO FILE-IO
               GO TO Z100-FILE-ERR.
       F100-READ-FM.
           READ FM-FILE
               NEXT RECORD
               INTO FM-31-RCRD-WS
             AT END
               GO TO F800-INPUT-COMPLETE.
           IF NOT RECORD-LOCKED
               GO TO F110-CHECK-FILE.
           MOVE "FGMT" TO WS-LOCK-FILE.
           PERFORM Z200-ACK-LOCK THRU Z290-EXIT.
           GO TO F100-READ-FM.
      *
       F110-CHECK-FILE.
           IF NOT FILE-OK
               MOVE "RS" TO FILE-IO
               GO TO Z100-FILE-ERR.
       F115-EDIT.
           IF FM-31-CPNO-1 NOT = SY-ST-CPNO
               GO TO F800-INPUT-COMPLETE.
           IF FM-31-LFCD-1 NOT = 31
               GO TO F800-INPUT-COMPLETE.
           IF FM-31-PDID-1 > WS-E-PDID
               GO TO F800-INPUT-COMPLETE.
       F120-EXTRACT-PROGRESS.
           DISPLAY FM-31-PDID-1 LINE 19 POSITION 47.
       F130-EDIT.
           IF FM-31-PCCD-2 < WS-S-PCCD
               GO TO F100-READ-FM.
           IF FM-31-PCCD-2 > WS-E-PCCD
               GO TO F100-READ-FM.
           IF WS-IPCD = "X"
               GO TO F140-FGGPDI.
           IF FM-31-IPCD NOT = WS-IPCD
               GO TO F100-READ-FM.
       F140-FGGPDI.
           MOVE SPACES       TO LS-FGGPDI-RCRD.
           MOVE "00"         TO LS-FGGPDI-CMCD.
           MOVE SY-ST-CPNO   TO LS-FGGPDI-CPNO.
           MOVE FM-31-PDID-1 TO LS-FGGPDI-PDID.
      *    MOVE ws-min-date-numeric   TO LS-FGGPDI-OPDT.
           MOVE ws-s-date             TO LS-FGGPDI-OPDT.
           MOVE sy-st-sodt    TO LS-FGGPDI-CLDT.
      *    trap
      *    if fm-31-pdid-1 = "171846-01A"
      *         move ws-s-date       to ds-date
      *         display ds-date line 12 position 1
      *         move sy-st-sodt        to ds-date
      *         display ds-date line 13 position 1
      *         PERFORM X050-REPLY THRU X090-EXIT.
           MOVE ZEROS        TO LS-FGGPDI-QTBK-OP
                                LS-FGGPDI-QTBK-CL
                                LS-FGGPDI-IVVL-OP
                                LS-FGGPDI-IVVL-CL
                                LS-FGGPDI-PCDT
                                LS-FGGPDI-PHCT
                                LS-FGGPDI-STCT.
           PERFORM X210-FGGPDI THRU X215-EXIT.
           IF LS-FGGPDI-CMCD NOT = "00"
               MOVE "12" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
      *    trap
      *    if fm-31-pdid-1 = "TEST"
      *         move ls-fggpdi-qtbk-op to ds-7-0
      *         display ds-7-0 line 13 position 1
      *         PERFORM X050-REPLY THRU X090-EXIT.
           MOVE "Y" TO WS-OPSW.
           GO TO F300-SET-SORT-RCRD.
      *
       F190-START-FX.
           MOVE SPACES         TO WS-OPSW.
           MOVE SPACES         TO FX-11-KEY-1-WS FX-KEY-1.
           MOVE SY-ST-CPNO     TO FX-11-CPNO-1.
           MOVE 11             TO FX-11-LFCD-1.
           MOVE FM-31-PCCD-2   TO FX-11-PCCD-1.
           MOVE FM-31-PDID-1   TO FX-11-PDID-1.
           MOVE FX-11-KEY-1-WS TO FX-KEY-1.
           START FX-FILE
               KEY IS NOT LESS THAN FX-KEY-1
             INVALID KEY
               GO TO F720-NEXT-PDID.
           IF RECORD-LOCKED
               MOVE "FGDT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO F190-START-FX.
           IF NOT FILE-OK
               MOVE "ST" TO FILE-IO
               GO TO Z100-FILE-ERR.
       F200-READ-FX.
           READ FX-FILE
               NEXT RECORD
               INTO FX-11-RCRD-WS
             AT END
               GO TO F720-NEXT-PDID.
           IF NOT RECORD-LOCKED
               GO TO F210-CHECK-FILE.
           MOVE "FGDT" TO WS-LOCK-FILE.
           PERFORM Z200-ACK-LOCK THRU Z290-EXIT.
           GO TO F200-READ-FX.
      *
       F210-CHECK-FILE.
           IF NOT FILE-OK
               MOVE "RS" TO FILE-IO
               GO TO Z100-FILE-ERR.
       F215-EDIT.
           IF FX-11-CPNO-1 NOT = SY-ST-CPNO
               GO TO F720-NEXT-PDID.
           IF FX-11-LFCD-1 NOT = 11
               GO TO F720-NEXT-PDID.
           IF FX-11-PCCD-1 NOT = FM-31-PCCD-2
               GO TO F720-NEXT-PDID.
           IF FX-11-PDID-1 NOT = FM-31-PDID-1
               GO TO F720-NEXT-PDID.
       F220-CHECK-DATE.
           IF FX-11-TRDT-1 < WS-s-date
               GO TO F200-READ-FX.
           IF FX-11-TRDT-1 > ws-e-date
               GO TO F720-NEXT-PDID.
       F300-SET-SORT-RCRD.
           MOVE "Y"          TO WS-RFSW.
           MOVE SPACES       TO SORT-RCRD-WS.
           MOVE ZEROS        TO SORT-TRDT
                                SORT-SRNO
                                SORT-TRQT
                                SORT-TRCT
                                SORT-CTUN.
           MOVE FM-31-PCCD-2 TO SORT-PCCD.
           MOVE FM-31-PDID-1 TO SORT-PDID.
       F305-CHECK-OPEN.
           IF WS-OPSW = "Y"
               GO TO F310-SET-OPEN.
           MOVE FX-11-TRNO-1 TO SORT-TRNO.
           MOVE FX-11-SRCD   TO SORT-SRCD.
           MOVE FX-11-TRDT-1 TO SORT-TRDT.
           MOVE FX-11-RFNO   TO SORT-RFNO.
           MOVE FX-11-SRNO   TO SORT-SRNO.
           MOVE FX-11-TRQT   TO SORT-TRQT.
           MOVE FX-11-TRCT   TO SORT-TRCT.
           MOVE FX-11-CTUN   TO SORT-CTUN.
           GO TO F700-RELEASE-REC-TO-SORT.
      *
       F310-SET-OPEN.
           MOVE ZEROS         TO SORT-TRNO.
           MOVE WS-s-DATE    TO SORT-TRDT.
           MOVE LS-FGGPDI-QTBK-OP TO SORT-TRQT.
           MOVE LS-FGGPDI-IVVL-OP TO SORT-TRCT.

       F700-RELEASE-REC-TO-SORT.
           RELEASE SORT-RCRD FROM SORT-RCRD-WS.
           IF WS-SORT NOT = "Y"
                MOVE "Y" TO WS-SORT.
           IF WS-OPSW = "Y"
               GO TO F190-START-FX.
           GO TO F200-READ-FX.
      *
       F720-NEXT-PDID.
           GO TO F100-READ-FM.
      *
       F800-INPUT-COMPLETE.
       F805-CHECK-RFSW.
           IF WS-RFSW NOT = "Y"
               DISPLAY "NO DATA ON FILE FOR STARTUP OPTIONS SPECIFIED "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F990-EXIT.
       F820-DISPLY.
           DISPLAY SPACES LINE 19 POSITION 23 SIZE 36.
           IF WS-SORT = "Y"
               DISPLAY "SORTING" LINE 19 POSITION 23 HIGH.
       F850-SET-VALUES.
           MOVE "FT" TO MAIN-77.
       F990-EXIT.
           EXIT.
      
       H000-PROCESS.
           IF WS-SORT NOT = "Y"
               GO TO H990-EXIT.
       H100-RETURN-SORT.
           RETURN SORT-FILE RECORD
               INTO SORT-RCRD-WS
             AT END
               GO TO H600-REVIEW-PRSW.
       H020-REVIEW-MAIN.
           IF MAIN-77 NOT = "FT"
               GO TO H100-PROCESS-SO.
       H050-DISPLY.
           DISPLAY spaces LINE 19 POSITION 23 size 50.
           DISPLAY "REPORT PROGRESS" LINE 19 POSITION 23 HIGH.
       H060-SET-VALUES.
           MOVE SPACES TO MAIN-77 WS-EFSW.
           MOVE SPACES TO WS-PCCD WS-PDID.
           MOVE ZEROS  TO WS-CLTL WS-GRTL.
       H100-PROCESS-SO.
           IF SORT-PCCD NOT = WS-PCCD
               PERFORM X300-CLASS-TOTAL THRU X390-EXIT.
           IF SORT-PDID NOT = WS-PDID
               PERFORM X400-ID-CLOSE THRU X490-EXIT
               GO TO H300-NEW-PRODUCT-ID.
       H110-CHECK-LINES.
           IF WS-LINE > 59
               PERFORM X100-HEADING THRU X190-EXIT.
       H120-SET-VALUES.
           MOVE SORT-TRNO TO PR-TRNO.
           MOVE SORT-SRCD TO PR-SRCD.
           MOVE SORT-TRDT TO PR-TRDT.
           MOVE SORT-RFNO TO PR-RFNO.
           MOVE SPACES  TO PR-SYRF.
           IF SORT-SRCD = "API"
             OR SORT-SRCD = "APA"
               MOVE SORT-SRNO TO PR-VDNO
               GO TO H130-TRQT.
           MOVE SORT-SRNO TO PR-SRNO.
       H130-TRQT.
           MOVE SORT-TRQT TO PR-TRQT.
           IF SORT-TRQT = ZERO
               MOVE ZEROS TO WS-RATE
               GO TO H140-RATE.
           COMPUTE WS-RATE ROUNDED =
               ( ( SORT-TRCT * SORT-CTUN ) / SORT-TRQT ).
       H140-RATE.
           MOVE WS-RATE TO PR-RATE.
           MOVE SORT-TRCT TO PR-TRCT.
       H150-ADD-VALUES.
           ADD SORT-TRQT TO WS-QTBK.
           ADD SORT-TRCT TO WS-IVVL.
           MOVE WS-QTBK TO PR-QTBK.
           MOVE WS-IVVL TO PR-IVVL.
       H200-WRITE.
           WRITE PR-RCRD FROM PR-DTL2.
           ADD 1 TO WS-LINE.
           GO TO H100-RETURN-SORT.
      *
       H300-NEW-PRODUCT-ID.
           DISPLAY SORT-PCCD LINE 19 POSITION 55
                   SORT-PDID LINE 20 POSITION 47.
       H305-CHECK-LINES.
           IF WS-LINE > 57
               PERFORM X100-HEADING THRU X190-EXIT.
       H310-READ-FM.
           MOVE SPACES         TO FM-31-KEY-1-WS FM-KEY-1.
           MOVE SY-ST-CPNO     TO FM-31-CPNO-1.
           MOVE 31             TO FM-31-LFCD-1.
           MOVE SORT-PDID        TO FM-31-PDID-1.
           MOVE FM-31-KEY-1-WS TO FM-KEY-1.
           PERFORM X500-READ-FM THRU X590-EXIT.
           MOVE FM-RCRD-WS TO FM-31-RCRD-WS.
           IF WS-ERSW = "Y"
               MOVE "* NOT ON FILE *" TO FM-31-DESC.
           MOVE SORT-PDID    TO PR-PDID.
           MOVE FM-31-DESC TO PR-PIDC.
       H320-WRITE.
           WRITE PR-RCRD FROM PR-HDR7.
           WRITE PR-RCRD FROM SPACES.
           ADD 2 TO WS-LINE.
       H330-SET-VALUES.
           MOVE WS-s-DATE TO PR-OCDT.
           MOVE "OPENING"  TO PR-DTWD.
           MOVE SORT-TRQT    TO PR-1-QTBK WS-QTBK.
           MOVE SORT-TRCT    TO PR-1-IVVL WS-IVVL.
           WRITE PR-RCRD FROM PR-DTL1.
           ADD 1 TO WS-LINE.
       H390-CONTINUE.
           GO TO H100-RETURN-SORT.
      *
       H600-REVIEW-PRSW.
           IF WS-PRSW NOT = "Y"
               GO TO H900-NO-DATA-ON-FILE.
       H610-CLASS-TOTAL.
           MOVE "Y" TO WS-EFSW.
           PERFORM X300-CLASS-TOTAL THRU X390-EXIT.
       H700-GRAND-TOTAL.
           IF WS-LINE > 59
               PERFORM X100-HEADING THRU X190-EXIT
               GO TO H710-CONTINUE.
           WRITE PR-RCRD FROM SPACES.
       H710-CONTINUE.
           MOVE "GRAND TOTAL" TO PR-TLWD.
           MOVE WS-GRTL       TO PR-TL-IVVL.
           WRITE PR-RCRD FROM PR-TOT1.
           GO TO H990-EXIT.
       
       H900-NO-DATA-ON-FILE.
           DISPLAY "NO DATA ON FILE FOR STARTUP OPTIONS SPECIFIED "
               LINE 24 POSITION 1 HIGH
           PERFORM X050-REPLY THRU X090-EXIT.
       H990-EXIT.
           EXIT.
      
       X000-FUNC-ERR.
           DISPLAY "FUNCTION KEY NOT VALID "
               LINE 24 POSITION 1 HIGH.
       X050-REPLY.
           ACCEPT DUMMY-77 LINE 00 POSITION 00 TAB OFF.
           DISPLAY SPACES LINE 24 POSITION 01 SIZE 80.
       X090-EXIT.
           EXIT.
      
      
       X100-HEADING.
           ADD 1 TO WS-PGNO.
           MOVE WS-PGNO TO PR-PGNO.
           WRITE PR-RCRD FROM PR-HDR1
               AFTER ADVANCING PAGE.
           WRITE PR-RCRD FROM PR-HDR2.
           WRITE PR-RCRD FROM PR-HDR3.
           WRITE PR-RCRD FROM PR-HDR4
               AFTER ADVANCING 2 LINES.
           WRITE PR-RCRD FROM PR-HDR5
               AFTER ADVANCING 2 LINES.
           WRITE PR-RCRD FROM PR-HDR6.
           WRITE PR-RCRD FROM PR-HDR8.
           MOVE 9 TO WS-LINE.
       X190-EXIT.
           EXIT.
      
       X200-UVACDT.
           CALL "UVACDT" USING
               LS-UVACDT-RCRD.
       X205-EXIT.
           EXIT.
      
       X210-FGGPDI.
           CALL "FGGPDI" USING
               LS-FGGPDI-RCRD.
       X215-EXIT.
           EXIT.
      
       X220-FGEMSD.
           CALL "FGEMSD" USING
               LS-FGEMSD-RCRD
               FM-01-RCRD-WS.
       X225-EXIT.
           EXIT.
      
      
       X300-CLASS-TOTAL.
           IF WS-PCCD = SPACES
               GO TO X350-SET-VALUES.
       X305-CLOSE-ID.
           PERFORM X400-ID-CLOSE THRU X490-EXIT.
       X310-SET-RCRD.
           IF WS-LINE > 59
               PERFORM X100-HEADING THRU X190-EXIT.
           MOVE "CLASS TOTAL" TO PR-TLWD.
           MOVE WS-CLTL       TO PR-TL-IVVL.
           WRITE PR-RCRD FROM PR-TOT1.
           ADD 1 TO WS-LINE.
       X340-ADD-VALUES.
           ADD WS-CLTL TO WS-GRTL.
       X350-SET-VALUES.
           IF WS-EFSW = "Y"
               GO TO X390-EXIT.
       X360-SET-VALUES.
           MOVE ZEROS  TO WS-CLTL.
           MOVE SPACES TO WS-PDID.
       X370-SET-PCDC.
           MOVE SPACES         TO MM-61-KEY-1-WS MM-KEY-1.
           MOVE SY-ST-CPNO     TO MM-61-CPNO-1.
           MOVE 61             TO MM-61-LFCD-1.
           MOVE SORT-PCCD        TO MM-61-PCCD-1.
           MOVE MM-61-KEY-1-WS TO MM-KEY-1.
           READ MM-FILE
               INTO MM-61-RCRD-WS
               KEY IS MM-KEY-1
             INVALID KEY
               MOVE "* NOT ON FILE *" TO MM-61-DESC
               GO TO X375-SET-FIELDS.
           IF RECORD-LOCKED
               MOVE "STMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X370-SET-PCDC.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X375-SET-FIELDS.
           MOVE SORT-PCCD    TO PR-PCCD WS-PCCD.
           MOVE MM-61-DESC TO PR-PCDC.
       X380-REVIEW-PRSW.
           IF WS-PRSW NOT = "Y"
               PERFORM B800-OPEN-PRNT THRU B990-EXIT.
           PERFORM X100-HEADING THRU X190-EXIT.
       X390-EXIT.
           EXIT.
      
       X400-ID-CLOSE.
           IF WS-PDID = SPACES
               GO TO X450-REVIEW-VALUES.
       X410-SET-RCRD.
           MOVE ws-e-date TO PR-OCDT.
           MOVE "CLOSING" TO PR-DTWD.
           MOVE WS-QTBK   TO PR-1-QTBK.
           MOVE WS-IVVL   TO PR-1-IVVL.
           WRITE PR-RCRD FROM PR-DTL1.
           WRITE PR-RCRD FROM SPACES.
           ADD 2 TO WS-LINE.
       X420-ADD-VALUES.
           ADD WS-IVVL TO WS-CLTL.
       X450-REVIEW-VALUES.
           IF WS-EFSW = "Y"
               GO TO X490-EXIT.
       X460-SET-VALUES.
           MOVE SORT-PDID TO WS-PDID.
       X490-EXIT.
           EXIT.
      
       X500-READ-FM.
           MOVE SPACES TO WS-ERSW.
           READ FM-FILE
               INTO FM-RCRD-WS
               KEY IS FM-KEY-1
             INVALID KEY
               MOVE "Y" TO WS-ERSW
               GO TO X590-EXIT.
           IF RECORD-LOCKED
               MOVE "FGMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X500-READ-FM.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X590-EXIT.
           EXIT.
      
       X800-MONTH-END.
       X805-FGEMSD.
           MOVE "00"       TO LS-FGEMSD-CMCD.
           MOVE SY-ST-SODT TO LS-FGEMSD-SODT.
           PERFORM X220-FGEMSD THRU X225-EXIT.
           IF LS-FGEMSD-CMCD = "05"
               DISPLAY "SIGNON DATE NOT VALID FOR "
                           LINE 24 POSITION 1 HIGH
                       "MONTH-END PROGRAM "
                           LINE 00 POSITION 0 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               MOVE "QT" TO MAIN-77
               GO TO X890-EXIT.
       X810-SET-PARAMETERS.
           MOVE "0000"         TO WS-S-PCCD.
           MOVE "ZZZZ"         TO WS-E-PCCD.
           MOVE "000000000000" TO WS-S-PDID.
           MOVE "ZZZZZZZZZZZZ" TO WS-E-PDID.
           MOVE ws-min-date-numeric TO ws-s-date pr-s-date.
           MOVE SY-ST-SODT     TO ws-e-date pr-e-date.
           MOVE "X"            TO WS-IPCD.
       X820-DISPLY.
           DISPLAY "0000"         LINE 08 POSITION 55
                   "ZZZZ"         LINE 09 POSITION 55
                   "000000000000" LINE 11 POSITION 47
                   "ZZZZZZZZZZZZ" LINE 12 POSITION 47.
           move ws-min-date-numeric to ds-date.
           display DS-DATE        LINE 14 POSITION 51
           move sy-st-sodt to ds-date.
           display DS-DATE        LINE 15 POSITION 51
           display "X"            LINE 17 POSITION 58.
       X830-QUERIE.
           DISPLAY "CONTINUE? (Y/N) "
               LINE 24 POSITION 1 HIGH.
           ACCEPT DUMMY-77
               LINE 24 POSITION 17
               PROMPT TAB ECHO.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           IF DUMMY-77 = "N"
               PERFORM X900-CLEAR-SCREEN THRU X990-EXIT
               MOVE "QT" TO MAIN-77
               GO TO X890-EXIT.
           IF DUMMY-77 NOT = "Y"
               GO TO X830-QUERIE.
       X890-EXIT.
           EXIT.
      
       X900-CLEAR-SCREEN.
           DISPLAY SPACES LINE 17 POSITION 58
                   SPACES LINE 15 POSITION 51 SIZE 8
                   SPACES LINE 14 POSITION 51 SIZE 8
                   SPACES LINE 12 POSITION 47 SIZE 12
                   SPACES LINE 11 POSITION 47 SIZE 12
                   SPACES LINE 09 POSITION 55 SIZE 4
                   SPACES LINE 08 POSITION 55 SIZE 4.
       X990-EXIT.
           EXIT.
      

       X999-UVdtcv.
           CALL "UVDTCV" USING
               LS-UVDTCV-RCRD.
       X999-EXIT.
           EXIT.


       Y000-CLOSE.
           CLOSE MM-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE FM-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE FX-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           IF WS-PRSW NOT = "Y"
               GO TO Y990-EXIT.
           CLOSE PR-FILE NO REWIND.
       Y990-EXIT.
           EXIT.
      
           COPY "uv/cpy/uvdfemov".
           COPY "uv/cpy/uvaflk".
