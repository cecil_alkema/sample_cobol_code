       IDENTIFICATION DIVISION.
       PROGRAM-ID.                 JCLBRU.
      *
      *        JOB COSTING
      *        LABOUR ENTRY UPDATE
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           COPY "jc/cpy/sliprg".
           COPY "jc/cpy/sljcmt".
           COPY "jc/cpy/sldkip".
           COPY "jc/cpy/slwkip".
           COPY "gl/cpy/slglir".
           COPY "gl/cpy/slglmt".
      
       DATA DIVISION.
       FILE SECTION.
           COPY "jc/cpy/fdiprg".
           COPY "jc/cpy/fdjcmt".
           COPY "jc/cpy/fddkip".
           COPY "jc/cpy/fdwkip".
           COPY "gl/cpy/fdglir".
           COPY "gl/cpy/fdglmt".
      
       WORKING-STORAGE SECTION.
      
       77  IR-STATUS-77            PIC XX.
       77  CM-STATUS-77            PIC XX.
       77  DP-STATUS-77            PIC XX.
       77  WP-STATUS-77            PIC XX.
       77  GI-STATUS-77            PIC XX.
       77  GM-STATUS-77            PIC XX.
       77  DUMMY-77                PIC X.
       77  I-77                    PIC 999.
      
           COPY "uv/cpy/uvfckv".
           COPY "uv/cpy/uvlkmg".
           COPY "uv/cpy/uvpgid".
           COPY "uv/cpy/uvsfemov".
           COPY "uv/cpy/rcsymtcp".
           COPY "uv/cpy/rcsymtst".
           COPY "jc/cpy/rciprglb".
           COPY "jc/cpy/rcjcmt".
           COPY "jc/cpy/rcdkip".
           COPY "jc/cpy/rcwkip".
           COPY "gl/cpy/rcglir".
           COPY "gl/cpy/rcglmt01".
           COPY "uv/cpy/uvedpr".
      
           COPY "uv/cpy/lsuvlaif".
           COPY "uv/cpy/lsuvcmlp".
           COPY "uv/cpy/lsuvcvtm".
           COPY "uv/cpy/lsuvcusf".
           COPY "uv/cpy/lsuvdssh".
      
       01  CONTROL-FIELDS.
           03  WS-EFSW             PIC X.
           03  WS-MINS             PIC S9(7)       VALUE ZERO COMP-3.
           03  WS-LBTM             PIC S9(5)V99    VALUE ZERO COMP-3.
           03  WS-DTNO             PIC 9(5).
           03  WS-DKNO             PIC 9(6).
           03  WS-LBMN             PIC S9(7)       COMP-3.
           03  WS-PROOF            PIC S9(7)V99    COMP-3.
           03  WS-DTNO-JC          pic 9(5).
      
       01  DISPLAY-FIELDS.
           03  DS-STRT             PIC ZZ.99-.
      
       01  GENERAL-LEDGER-FIELDS.
           03  WS-AI-IFCD          PIC X(6).
           03  WS-AI-IFCD-2        REDEFINES WS-AI-IFCD.
               05  WS-AI-WCNO      PIC 999.
               05  FILLER          PIC XXX.
           03  WS-XXXX-ACNO        PIC X(10).
           03  WS-XXXX-ACAM        PIC S9(7)V99    COMP-3.
           03  WS-WPLB-ACNO        PIC X(10).
           03  WS-WPLB-ACAM        PIC S9(7)V99    COMP-3.
           03  WS-LBPC-DATA        OCCURS 200 TIMES.
               05  WS-LBPC-WCNO    PIC 999.
               05  WS-LBPC-ACNO    PIC X(10).
               05  WS-LBPC-ACAM    PIC S9(7)V99    COMP-3.
      
       PROCEDURE DIVISION.
      
       DECLARATIVES.
           COPY "jc/cpy/dciprg".
           COPY "jc/cpy/dcjcmt".
           COPY "jc/cpy/dcdkip".
           COPY "jc/cpy/dcwkip".
           COPY "gl/cpy/dcglir".
           COPY "gl/cpy/dcglmt".
       END DECLARATIVES.
      
       A000-MAINLINE-PGM SECTION.
      
       A000-START-PROGRAM.
           PERFORM B000-INIT THRU B990-EXIT.
           PERFORM D000-LOAD-INTERFACE THRU D990-EXIT.
           PERFORM F000-PROCESS THRU F990-EXIT.
           PERFORM H000-DISTRIBUTION THRU H990-EXIT.
           PERFORM W000-ACK-F1 THRU W990-EXIT.
           PERFORM Y000-CLOSE THRU Y990-EXIT.
       A990-EXIT.
           EXIT PROGRAM.
      
       B000-INIT.
           MOVE "JC"     TO WS-STNM.
           MOVE "LBRU"   TO WS-PGNM.
           MOVE "JCLBRU" TO PROGRAM-NAME.
           COPY "uv/cpy/uvcusf".
           COPY "uv/cpy/uvdssh".
       B100-OPEN-FILS.
           OPEN I-O   IR-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN I-O   CM-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN INPUT DP-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN I-O   WP-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN I-O   GI-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN I-O   GM-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
       B400-SET-VALUES.
           PERFORM X800-REVIEW-DATE THRU X890-EXIT.
       B990-EXIT.
           EXIT.
      
      
       D000-LOAD-INTERFACE.
       D100-LOAD-WORK-IN-PROCESS.
           MOVE "RR"    TO LS-UVLAIF-CMCD.
           MOVE SY-ST-CPNO TO LS-UVLAIF-CPNO.
           MOVE "JC"    TO LS-UVLAIF-IFST.
           MOVE "WPLB"  TO LS-UVLAIF-IFTP.
           MOVE SPACES  TO LS-UVLAIF-IFCD.
           MOVE SPACES  TO LS-UVLAIF-ACNO.
           PERFORM X320-UVLAIF THRU X325-EXIT.
           IF LS-UVLAIF-CMCD NOT = "00"
               MOVE "01" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
       D110-SET-VALUES.
           MOVE ZEROS      TO WS-WPLB-ACAM.
           MOVE LS-UVLAIF-ACNO TO WS-WPLB-ACNO.
       D200-SET-LBPC-VALUES.
           MOVE ZEROS  TO I-77 WS-XXXX-ACAM.
           MOVE SPACES TO WS-XXXX-ACNO.
       D300-START-AI.
           MOVE "ST"    TO LS-UVLAIF-CMCD.
           MOVE SY-ST-CPNO TO LS-UVLAIF-CPNO.
           MOVE "JC"    TO LS-UVLAIF-IFST.
           MOVE "LBPC"  TO LS-UVLAIF-IFTP.
           MOVE SPACES  TO LS-UVLAIF-IFCD.
           MOVE SPACES  TO LS-UVLAIF-ACNO.
           PERFORM X320-UVLAIF THRU X325-EXIT.
           IF LS-UVLAIF-CMCD NOT = "00"
               MOVE "02" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
       D400-LOAD-LBPC-LOOP.
           ADD 1        TO I-77.
       D410-UVLAIF.
           MOVE "RS"    TO LS-UVLAIF-CMCD.
           MOVE SY-ST-CPNO TO LS-UVLAIF-CPNO.
           MOVE "JC"    TO LS-UVLAIF-IFST.
           MOVE "LBPC"  TO LS-UVLAIF-IFTP.
           MOVE SPACES  TO LS-UVLAIF-IFCD.
           MOVE SPACES  TO LS-UVLAIF-ACNO.
           PERFORM X320-UVLAIF THRU X325-EXIT.
           IF LS-UVLAIF-CMCD NOT = "00"
               MOVE ZEROS  TO WS-LBPC-WCNO (I-77)
               MOVE ZEROS  TO WS-LBPC-ACAM (I-77)
               MOVE SPACES TO WS-LBPC-ACNO (I-77)
               GO TO D600-REVIEW-XXXX.
           MOVE LS-UVLAIF-IFCD TO WS-AI-IFCD.
           IF WS-AI-IFCD = "XXXXXX"
               MOVE LS-UVLAIF-ACNO TO WS-XXXX-ACNO
               GO TO D410-UVLAIF.
           MOVE WS-AI-WCNO TO WS-LBPC-WCNO (I-77).
           MOVE LS-UVLAIF-ACNO TO WS-LBPC-ACNO (I-77).
           MOVE ZEROS      TO WS-LBPC-ACAM (I-77).
       D500-REVIEW-COUNTER.
           IF I-77 NOT = 200
               GO TO D400-LOAD-LBPC-LOOP.
           MOVE "03" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
      *
       D600-REVIEW-XXXX.
           IF WS-XXXX-ACNO = SPACES
               MOVE "04" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
       D990-EXIT.
           EXIT.
      
      
       F000-PROCESS.
           MOVE SPACES TO WS-EFSW.
       F090-START-IR.
           MOVE SPACES      TO IR-KEY-2 IR-KEY-2-WS.
           MOVE SY-ST-CPNO     TO IR-CPNO-2.
           MOVE 15          TO IR-LFCD-2.
           MOVE WS-UVEDPR-MNDT  TO IR-OPDT-2.
           MOVE IR-KEY-2-WS TO IR-KEY-2.
           START IR-FILE
               KEY IS NOT LESS THAN IR-KEY-2
             INVALID KEY
               GO TO F900-NO-DATA-ON-FILE.
           IF RECORD-LOCKED
               MOVE "IPRG" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO F090-START-IR.
           IF NOT FILE-OK
               MOVE "ST" TO FILE-IO
               GO TO Z100-FILE-ERR.
       F100-READ-IR.
           READ IR-FILE
               NEXT RECORD
               INTO IR-RCRD-WS
             AT END
               GO TO F700-END-OF-FILE.
           IF NOT RECORD-LOCKED
               GO TO F110-CHECK-FILE.
           MOVE "IPRG" TO WS-LOCK-FILE.
           PERFORM Z200-ACK-LOCK THRU Z290-EXIT.
           GO TO F100-READ-IR.
      *
       F110-CHECK-FILE.
           IF NOT FILE-OK
               MOVE "RS" TO FILE-IO
               GO TO Z100-FILE-ERR.
       F120-EDIT-IR.
           IF IR-CPNO-2 NOT = SY-ST-CPNO
               GO TO F690-UNLOCK-IR.
           IF IR-LFCD-2 NOT = 15
               GO TO F690-UNLOCK-IR.
       F125-DISPLY.
           MOVE IR-STRT-2 TO WS-MINS.
           PERFORM X210-CONVERT-MT THRU X215-EXIT.
           MOVE WS-LBTM   TO DS-STRT.
           INSPECT DS-STRT
               REPLACING ALL "." BY ":".
           DISPLAY SPACES    LINE 11 POSITION 37 SIZE 25
                   IR-OPDT-2 LINE 11 POSITION 37
                   IR-SHCD-2 LINE 11 POSITION 45
                   IR-EPNO-2 LINE 11 POSITION 48
                   DS-STRT   LINE 11 POSITION 55.
       F127-CONT-EDIT.
           IF IR-OPDT-2 > SY-ST-SODT
               GO TO F690-UNLOCK-IR.
           IF IR-STCD NOT = 2
               GO TO F100-READ-IR.
       F130-REVIEW-DP.
           IF IR-DKNO = ZERO
               GO TO F200-PROCESS.
           MOVE IR-DKNO TO WS-DKNO.
       F150-READ-DP.
           MOVE SPACES  TO DP-KEY-1.
           MOVE SY-ST-CPNO TO DP-CPNO-K1.
           MOVE WS-DKNO TO DP-DKNO-K1.
           READ DP-FILE
               INTO DP-RCRD-WS
               KEY IS DP-KEY-1.
           IF RECORD-LOCKED
               MOVE "DKIP" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO F150-READ-DP.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       F160-REVIEW-STATUS.
           IF DP-STCD NOT = 1
             AND DP-STCD NOT = 2
               MOVE "10" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
       F200-PROCESS.
           SUBTRACT IR-STRT-2 FROM IR-STOP
               GIVING WS-LBMN.
       F250-SET-WP-RECORD.
           MOVE SPACES    TO WP-RCRD WP-RCRD-WS.
           MOVE ZEROS     TO WP-TRDT WP-EPNO
                             WP-STRT WP-STOP WP-OPRT WP-OPQT.
           MOVE SY-ST-CPNO   TO WP-CPNO-1 WP-CPNO-2.
           MOVE IR-DKNO   TO WP-DKNO-1.
           MOVE 15        TO WP-TRCD-1 WP-TRCD-2.
           MOVE IR-WCNO   TO WP-WCNO-1 WP-WCNO-2.
           MOVE IR-OPNO   TO WP-OPNO-1 WP-OPNO-2.
           MOVE IR-WRCD   TO WP-WRCD-1.
           MOVE IR-DTNO-1 TO WP-TRNO-1.
           MOVE IR-OPDT-2 TO WP-TRDT.
           MOVE IR-SRCD   TO WP-SRCD.
           MOVE IR-EPNO-2 TO WP-EPNO.
           MOVE IR-SHCD-2 TO WP-SHCD.
           MOVE IR-STRT-2 TO WP-STRT.
           MOVE IR-STOP   TO WP-STOP.
           MOVE IR-PRMP   TO WP-PRMP.
           MOVE IR-OPRT   TO WP-OPRT.
           MOVE IR-OPQT   TO WP-OPQT.
       F280-WRITE-WP.
           WRITE WP-RCRD FROM WP-RCRD-WS.

      *
           if file-status = "22"
               move "XX" to file-status
               perform x800-get-next-trno thru x830-exit
               move ws-dtno-jc to wp-trno-1
               go to F280-WRITE-WP.
      *

           IF NOT FILE-OK
               MOVE "WR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       F400-COMPUTE-PROD-CONT.
           MOVE "LB"    TO LS-UVCMLP-CMCD.
           MOVE SPACES  TO LS-UVCMLP-DATA.
           MOVE WS-LBMN TO LS-UVCMLP-LBMN.
           MOVE IR-OPRT TO LS-UVCMLP-OPRT.
           MOVE ZEROS   TO LS-UVCMLP-LBPC.
           PERFORM X300-UVCMLP THRU X305-EXIT.
           IF LS-UVCMLP-CMCD NOT = "00"
               MOVE "11" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
       F410-ADD-WPLB.
           ADD LS-UVCMLP-LBPC TO WS-WPLB-ACAM.
       F440-DECIDE-LBPC-WC.
           MOVE ZERO TO I-77.
       F450-LBPC-LOOP.
           ADD 1 TO I-77.
           IF WS-LBPC-WCNO (I-77) = ZERO
               GO TO F490-SUBTRACT-XXXX.
           IF IR-WCNO = WS-LBPC-WCNO (I-77)
               SUBTRACT LS-UVCMLP-LBPC FROM WS-LBPC-ACAM (I-77)
               GO TO F500-DELETE-IR.
       F460-REVIEW-COUNTER.
           IF I-77 NOT = 200
               GO TO F450-LBPC-LOOP.
       F490-SUBTRACT-XXXX.
           SUBTRACT LS-UVCMLP-LBPC FROM WS-XXXX-ACAM.
       F500-DELETE-IR.
           DELETE IR-FILE RECORD.
           IF NOT FILE-OK
               MOVE "DL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           GO TO F100-READ-IR.
      *
       F690-UNLOCK-IR.
           UNLOCK IR-FILE RECORD.
           IF NOT FILE-OK
               MOVE "UL" TO FILE-IO
               GO TO Z100-FILE-ERR.
       F700-END-OF-FILE.
           GO TO F990-EXIT.
      *
       F900-NO-DATA-ON-FILE.
           MOVE "19" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
      *
       F990-EXIT.
           EXIT.
      
       H000-DISTRIBUTION.
           MOVE ZERO TO WS-PROOF.
       H100-WPLB.
           IF WS-WPLB-ACAM = ZERO
               GO TO H290-SET-VALUES.
           PERFORM X600-GET-DTNO THRU X690-EXIT.
           MOVE WS-WPLB-ACNO TO GI-ACNO-2.
           MOVE WS-WPLB-ACAM TO GI-TRAM.
           PERFORM X400-WRITE-GI THRU X405-EXIT.
       H120-ADD-VALUES.
           ADD WS-WPLB-ACAM TO WS-PROOF.
       H290-SET-VALUES.
           MOVE ZERO TO I-77.
       H300-LBPC-LOOP.
           ADD 1 TO I-77.
           IF WS-LBPC-WCNO (I-77) = ZERO
               GO TO H500-LBPC-XXXX.
           IF WS-LBPC-ACAM (I-77) = ZERO
               GO TO H390-REVIEW-COUNTER.
           PERFORM X600-GET-DTNO THRU X690-EXIT.
           MOVE WS-LBPC-ACNO (I-77) TO GI-ACNO-2.
           MOVE WS-LBPC-ACAM (I-77) TO GI-TRAM.
           PERFORM X400-WRITE-GI THRU X405-EXIT.
       H320-ADD-VALUES.
           ADD WS-LBPC-ACAM (I-77) TO WS-PROOF.
       H390-REVIEW-COUNTER.
           IF I-77 NOT = 200
               GO TO H300-LBPC-LOOP.
       H500-LBPC-XXXX.
           IF WS-XXXX-ACAM = ZERO
               GO TO H700-PROOF.
           PERFORM X600-GET-DTNO THRU X690-EXIT.
           MOVE WS-XXXX-ACNO TO GI-ACNO-2.
           MOVE WS-XXXX-ACAM TO GI-TRAM.
           PERFORM X400-WRITE-GI THRU X405-EXIT.
       H520-ADD-VALUES.
           ADD WS-XXXX-ACAM TO WS-PROOF.
       H700-PROOF.
           IF WS-PROOF NOT = ZERO
               MOVE "31" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
       H990-EXIT.
           EXIT.
      
       W000-ACK-F1.
           DISPLAY SPACES     LINE 11 POSITION 37 SIZE 25
                   "FINISHED" LINE 11 POSITION 37
                   "REGISTER UPDATE COMPLETE - PRESS [F1] "
                   LINE 24 POSITION 1 HIGH.
           ACCEPT DUMMY-77
               LINE 00 POSITION 0
               TAB OFF
             ON EXCEPTION
               FUNC-KEY
               GO TO W190-CHECK-FUNC.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           GO TO W000-ACK-F1.
       W190-CHECK-FUNC.
           IF FUNC-01
               GO TO W990-EXIT.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           GO TO W000-ACK-F1.
       W990-EXIT.
           EXIT.
      
      
      *        CALL SUBROUTINES
      
       X210-CONVERT-MT.
           MOVE "MT"    TO LS-UVCVTM-CMCD.
           MOVE WS-MINS TO LS-UVCVTM-LBMN.
           MOVE ZEROS   TO LS-UVCVTM-LBTM.
           CALL "UVCVTM" USING
               LS-UVCVTM-RCRD.
           IF LS-UVCVTM-CMCD NOT = "00"
               MOVE "X1" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
           MOVE LS-UVCVTM-LBTM TO WS-LBTM.
       X215-EXIT.
           EXIT.
      
       X300-UVCMLP.
           CALL "UVCMLP" USING
               LS-UVCMLP-RCRD.
       X305-EXIT.
           EXIT.
      
       X320-UVLAIF.
           CALL "UVLAIF" USING
               LS-UVLAIF-RCRD.
       X325-EXIT.
           EXIT.
      
       X400-WRITE-GI.
           WRITE GI-RCRD FROM GI-RCRD-WS.
           IF NOT FILE-OK
               MOVE "WR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X405-EXIT.
           EXIT.
      
       X600-GET-DTNO.
           MOVE SPACES  TO GM-KEY-1.
           MOVE SY-ST-CPNO TO GM-CPNO-K1.
           MOVE "01"    TO GM-LFCD-K1.
           READ GM-FILE
               INTO GM-01-RCRD-WS
               KEY IS GM-KEY-1.
           IF RECORD-LOCKED
               MOVE "GLMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X600-GET-DTNO.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X610-REVIEW-TRANS.
           MOVE GM-01-NATN TO WS-DTNO.
           ADD 1 TO GM-01-NATN.
           IF GM-01-NATN = ZERO
               MOVE 1 TO GM-01-NATN.
       X620-REWRITE.
           REWRITE GM-RCRD FROM GM-01-RCRD-WS.
           IF NOT FILE-OK
               MOVE "RW" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X650-SET-NEW-RECORD.
           MOVE SPACES  TO GI-RCRD GI-RCRD-WS.
           MOVE SY-ST-CPNO TO GI-CPNO-1 GI-CPNO-2.
           MOVE WS-DTNO TO GI-DTNO-1.
           MOVE "JCL"   TO GI-SRCD-2.
           move sy-st-sodt     to gi-trdt-2.     
      *
           MOVE ZEROS   TO GI-TRAM.
           MOVE 1       TO GI-STSW.
       X690-EXIT.
           EXIT.

      *
      *    get next available transaction number
      *
       x800-get-next-trno.
           MOVE SPACES   TO CM-KEY-1.
           MOVE SY-ST-CPNO  TO CM-CPNO-K1.
           MOVE "01"     TO CM-LFCD-K1.
           READ CM-FILE
               INTO CM-RCRD-WS
               KEY IS CM-KEY-1.
           IF RECORD-LOCKED
               MOVE "JCMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO x800-get-next-trno.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X810-SET-VALUES.
           MOVE CM-SC-NATN TO WS-DTNO-JC.
           ADD 1 TO CM-SC-NATN.
           IF CM-SC-NATN = ZERO
               MOVE 1 TO CM-SC-NATN.
       X820-REWRITE-CM.
           REWRITE CM-RCRD FROM CM-RCRD-WS.
           IF NOT FILE-OK
               MOVE "RW" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X830-EXIT.
           EXIT.
      
      *
      *    READ COSTING CONTROL
      *
       X800-REVIEW-DATE.
       X810-READ-CM.
           MOVE SPACES  TO CM-KEY-1.
           MOVE SY-ST-CPNO TO CM-CPNO-K1.
           MOVE "01"    TO CM-LFCD-K1.
           READ CM-FILE
      *
               with no lock
      *
               INTO CM-RCRD-WS
               KEY IS CM-KEY-1.
           IF RECORD-LOCKED
               MOVE "JCMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X810-READ-CM.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X820-REVIEW-DATES.
           IF CM-SC-FYOS = 00
               MOVE CM-SC-CRYR TO WS-UVEDPR-CRYR
               MOVE CM-SC-CRMO TO WS-UVEDPR-CRMO
               GO TO X850.
           ADD CM-SC-CRMO CM-SC-FYOS GIVING WS-UVEDPR-CRMO.
           MOVE CM-SC-CRYR TO WS-UVEDPR-CRYR.
           IF WS-UVEDPR-CRMO > 12
               SUBTRACT 12 FROM WS-UVEDPR-CRMO
               GO TO X850.
           SUBTRACT 01 FROM WS-UVEDPR-CRYR.
       X850.
           MOVE SY-ST-SODT TO WS-UVEDPR-SODT.
           IF WS-UVEDPR-SOYM NOT > WS-UVEDPR-CRYM
               MOVE ZEROS TO WS-UVEDPR-MNDT
               GO TO X890-EXIT.
           MOVE WS-UVEDPR-CRYR TO WS-UVEDPR-YEAR.
           MOVE WS-UVEDPR-CRMO TO WS-UVEDPR-MONTH.
           MOVE 01 TO WS-UVEDPR-DAY.
           IF WS-UVEDPR-MONTH = 12
               MOVE 00 TO WS-UVEDPR-MONTH
               ADD 01 TO WS-UVEDPR-YEAR.
           ADD 01 TO WS-UVEDPR-MONTH.
       X890-EXIT.
           EXIT.

      
      *        CLOSING OF PROGRAM
      
       Y000-CLOSE.
           CLOSE IR-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE CM-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE DP-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE WP-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE GI-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE GM-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
       Y990-EXIT.
           EXIT.
      
           COPY "uv/cpy/uvdfemov".
           COPY "uv/cpy/uvaflk".
