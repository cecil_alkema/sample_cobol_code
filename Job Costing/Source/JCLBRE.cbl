       IDENTIFICATION DIVISION.
       PROGRAM-ID.                 JCLBRE.
      *
      *        JOB COSTING
      *        LABOUR ENTRY
      *
      *        SWITCHES
      *        ********
      *
      *            SWITCH-1 - DISPLAY DESCRIPTION BESIDE DOCKET NO
      *            ... instead of customer name
      *
      *            SWITCH-4 - JOB MAY BE ENTERED FOR NON-CHARGE. OPERAT.
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           SWITCH-1
               ON  IS SW-1-ON
               OFF IS SW-1-OFF
           SWITCH-4
               ON  IS SW-4-ON
               OFF IS SW-4-OFF.
      
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           COPY "jc/cpy/sliprg".
           COPY "jc/cpy/slepmt".
           COPY "jc/cpy/slwcmt".
           COPY "jc/cpy/sldkip".
           COPY "jc/cpy/slopmt".
           COPY "jc/cpy/sljcmt".
           COPY "ar/cpy/slctmt".
      
       DATA DIVISION.
       FILE SECTION.
           COPY "jc/cpy/fdiprg".
           COPY "jc/cpy/fdepmt".
           COPY "jc/cpy/fdwcmt".
           COPY "jc/cpy/fddkip".
           COPY "jc/cpy/fdopmt".
           COPY "jc/cpy/fdjcmt".
           COPY "ar/cpy/fdctmt".
      
       WORKING-STORAGE SECTION.
      
       77  MP-STATUS-77            PIC XX.
       77  IR-STATUS-77            PIC XX.
       77  EP-STATUS-77            PIC XX.
       77  WC-STATUS-77            PIC XX.
       77  DP-STATUS-77            PIC XX.
       77  OP-STATUS-77            PIC XX.
       77  CM-STATUS-77            PIC XX.
       77  CT-STATUS-77            PIC XX.
       77  DUMMY-77                PIC X.
       77  MAIN-77                 PIC XX.
      
           COPY "uv/cpy/uvfckv".
           COPY "uv/cpy/uvpgid".
           COPY "uv/cpy/uvsfemov".
           COPY "uv/cpy/uvlkmg".
           COPY "uv/cpy/rcsymtcp".
           COPY "uv/cpy/rcsymtst".
           COPY "jc/cpy/rciprglb".
           COPY "jc/cpy/rcepmt".
           COPY "jc/cpy/rcwcmt".
           COPY "jc/cpy/rcdkip".
           COPY "jc/cpy/rcopmt".
           COPY "jc/cpy/rcjcmt".
           COPY "ar/cpy/rcctmt".
           COPY "uv/cpy/uvedpr".
      
           COPY "uv/cpy/lsuvacdt".
           COPY "uv/cpy/lsuvdtcv".
           COPY "uv/cpy/lsuvgeno".
           copy "uv/cpy/lsuvgwrs".
           COPY "uv/cpy/lsuvcvtm".
      
           COPY "uv/cpy/lsuvcusf".
           COPY "uv/cpy/lsuvdssh".
           copy "uv/cpy/lsuvgopr".
           copy "uv/cpy/lsuvgwkc".
      
       01  SCREEN-ID               PIC X(35)   VALUE
           "$PMISDIR/appl/obj/c/dsplscrn jclbre".
      
       01  CONTROL-FIELDS.
           03  WS-CHSW             PIC X.
           03  WS-ERSW             PIC X.
           03  WS-OPTION           PIC X(6).
           03  WS-OPTION-1         REDEFINES WS-OPTION.
               05  WS-FIELD        PIC 99.
               05  FILLER          PIC XXXX.
           03  WS-DTNO-SV          PIC 9(5).
           03  WS-STRT-SV          PIC S99V99.
           03  WS-AENO             PIC 9(5).
           03  WS-CTNO             PIC 9(5).
           03  WS-TMSP             PIC V99.
           03  WS-TMMN             REDEFINES WS-TMSP PIC 99.
      
       01  STORAGE-FIELDS.
           03  WS-DTNO             PIC 9(5).

           03  WS-SODT             PIC 9(8).
           03  WS-DATE-1           REDEFINES WS-SODT.
               05  WS-YY           PIC 9999.
               05  WS-MM           PIC 99.
               05  WS-DD           PIC 99.
           03  ws-yy-minus-one     pic 9999.

           03  WS-OPDT             PIC 9(8).
           03  WS-DATE-2           REDEFINES WS-OPDT.
               05  WS-YEAR         PIC 9999.
               05  WS-MONTH        PIC 99.
               05  WS-DAY          PIC 99.

           03  WS-SHCD             PIC 9.
               88  VALID-SHIFT
                   VALUES ARE 1, 2, 3.
           03  WS-EPNO             PIC 9(5).
           03  WS-WCNO             PIC 999.
           03  WS-DKNO             PIC 9(6).
           03  WS-DKDC             PIC X(35).
           03  WS-OPNO             PIC 999.
           03  WS-OPRT             PIC 999V99.
           03  WS-WRCD             PIC XX.
           03  WS-TIME             PIC S99V99.
           03  WS-STRT             PIC S99V99.
           03  WS-STOP             PIC S99V99.
           03  WS-PRMP             PIC X.
               88  VALID-PAY-RATE
                   VALUES ARE "R", "O", "S".
           03  WS-OPQT             PIC S9(7).
      
       01  DISPLAY-FIELDS.
           03  DS-DTNO             PIC ZZZZ9.
           03  DS-OPDT             PIC 99/99/99.
           03  DS-DKNO             PIC ZZZZZ9.
           03  DS-STRT             PIC ZZ.99-.
           03  DS-STOP             PIC ZZ.99-.
           03  DS-OPQT             PIC Z,ZZZ,ZZ9-.
      
       01  EDIT-FIELDS.
           03  ED-OPQT             PIC ZZZZZZ9-.
      
       PROCEDURE DIVISION.
      
       DECLARATIVES.
           COPY "jc/cpy/dciprg".
           COPY "jc/cpy/dcepmt".
           COPY "jc/cpy/dcwcmt".
           COPY "jc/cpy/dcdkip".
           COPY "jc/cpy/dcopmt".
           COPY "jc/cpy/dcjcmt".
           COPY "ar/cpy/dcctmt".
       END DECLARATIVES.
      
       A000-MAINLINE-PGM SECTION.
      
       A000-START-PROGRAM.
           PERFORM B000-INIT THRU B990-EXIT.
       A100-MAINLINE.
           PERFORM D000-TRANS-KEY THRU D990-EXIT.
           IF MAIN-77 = "QT"
               GO TO A800-CLOSING.
           IF MAIN-77 = "AD"
               GO TO A300-ADD-MODE.
           IF MAIN-77 = "CH"
               GO TO A500-CHANGE-MODE.
           MOVE "01" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
      *
       A300-ADD-MODE.
           PERFORM F000-ADD-MODE THRU F990-EXIT.
           GO TO A100-MAINLINE.
      *
       A500-CHANGE-MODE.
           PERFORM H000-CHANGE-MODE THRU H990-EXIT.
           GO TO A100-MAINLINE.
      *
       A800-CLOSING.
           PERFORM Y000-CLOSE THRU Y990-EXIT.
       A990-EXIT.
           STOP RUN.
      
      
       B000-INIT.
           MOVE "JC"     TO WS-STNM.
           MOVE "LBRE"   TO WS-PGNM.
           MOVE "JCLBRE" TO PROGRAM-NAME.
           COPY "uv/cpy/uvcusf".
           CALL "SYSTEM" USING SCREEN-ID.
           COPY "uv/cpy/uvdssh".
       B100-OPEN-FILS.
           OPEN I-O   IR-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN INPUT EP-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN INPUT WC-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN INPUT DP-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN INPUT OP-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN I-O   CM-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN INPUT CT-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           move sy-st-sodt to ws-sodt.
       B500-SET-VALUES.
           MOVE "FT"    TO MAIN-77.
           MOVE 09      TO LS-UVACDT-LINE.
           MOVE 28      TO LS-UVACDT-COLM.
           MOVE SPACES  TO LS-UVACDT-NMSW.
           MOVE SY-ST-CPNO TO LS-UVGENO-CPNO.
           MOVE 11      TO LS-UVGENO-LINE.
           MOVE 31      TO LS-UVGENO-COLM.
           MOVE SPACES  TO LS-UVGENO-INSW.
       B900-CLEAR-SCREEN.
           PERFORM X900-CLEAR THRU X990-EXIT.
       B990-EXIT.
           EXIT.
      
       D000-TRANS-KEY.
           IF MAIN-77 = "FT"
               PERFORM X800-GET-DTNO THRU X890-EXIT.
           MOVE SPACES TO MAIN-77.
       D010-DISPLY.
           MOVE WS-DTNO-SV TO DS-DTNO.
           DISPLAY DS-DTNO LINE 08 POSITION 31.
       D100-ACK-OPDT.
           MOVE SY-ST-SODT TO LS-UVACDT-DATE.
           PERFORM X100-UVACDT THRU X105-EXIT.
           IF LS-UVACDT-CMCD = "02"
               GO TO D200-ACK-DTNO.
           move ls-uvacdt-date to ls-uvdtcv-indt.
           move "EX"           to ls-uvdtcv-cmcd.
           perform x999-uvdtcv thru x999-exit.
           MOVE LS-UVdtcv-otdt TO WS-OPDT.
           subtract 1 from ws-yy giving ws-yy-minus-one.
           if ws-year < ws-yy-minus-one
               DISPLAY "DATE NOT REASONABLE "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO D100-ack-opdt.

           PERFORM X300-EDIT-OPDT THRU X305-EXIT.
           IF WS-ERSW = "Y"
               GO TO D100-ACK-OPDT.
       D120-PROCESS-ADD.
           MOVE "AD" TO MAIN-77.
           GO TO D990-EXIT.
      *
       D200-ACK-DTNO.
           MOVE WS-DTNO-SV TO DS-DTNO.
           DISPLAY DS-DTNO LINE 08 POSITION 31.
           MOVE WS-DTNO-SV TO WS-DTNO.
           ACCEPT WS-DTNO
               LINE 08 POSITION 31
               TAB ECHO UPDATE
             ON EXCEPTION
               FUNC-KEY
               GO TO D290-CHECK-FUNC.
           INSPECT WS-DTNO
               REPLACING ALL " " BY "0".
           IF WS-DTNO NOT NUMERIC
               DISPLAY "TRANSACTION NUMBER MUST BE NUMERIC "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO D200-ACK-DTNO.
           IF WS-DTNO = WS-DTNO-SV
               GO TO D010-DISPLY.
           GO TO D300-READ-IR.
      *
       D290-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 08 POSITION 31 SIZE 5
               MOVE "QT" TO MAIN-77
               GO TO D990-EXIT.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO D200-ACK-DTNO.
      *
       D300-READ-IR.
           MOVE SPACES  TO IR-KEY-1.
           MOVE SY-ST-CPNO TO IR-CPNO-K1.
           MOVE WS-DTNO TO IR-DTNO-K1.
           READ IR-FILE
               INTO IR-RCRD-WS
               KEY IS IR-KEY-1
             INVALID KEY
               DISPLAY "TRANSACTION NOT ON FILE "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO D200-ACK-DTNO.
           IF RECORD-LOCKED
               MOVE "IPRG" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO D300-READ-IR.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       D310-REVIEW.
           IF IR-LFCD-2 NOT = 15
               DISPLAY "TRANSACTION NOT A LABOUR RECORD "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO D200-ACK-DTNO.
       D320-PROCESS-CHANGE.
           MOVE "CH" TO MAIN-77.
       D990-EXIT.
           EXIT.
      
      
       F000-ADD-MODE.
           MOVE SPACES TO WS-ERSW.
       F020-SHCD.
           DISPLAY "1" LINE 10 POSITION 35.
           MOVE "1" TO WS-SHCD.
           ACCEPT WS-SHCD
               LINE 10 POSITION 35
               TAB ECHO UPDATE
             ON EXCEPTION
               FUNC-KEY
               GO TO F025-CHECK-FUNC.
           PERFORM X310-EDIT-SHIFT THRU X315-EXIT.
           IF WS-ERSW = "Y"
               GO TO F020-SHCD.
           DISPLAY WS-SHCD LINE 10 POSITION 35.
           GO TO F030-EPNO.
      *
       F025-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 10 POSITION 35 SIZE 1
               GO TO F990-EXIT.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F020-SHCD.
      *
       F030-EPNO.
           DISPLAY SPACES LINE 11 POSITION 27 SIZE 50.
           MOVE "PR"  TO LS-UVGENO-CMCD.
           MOVE ZEROS TO LS-UVGENO-EPNO.
           PERFORM X110-UVGENO THRU X115-EXIT.
           IF LS-UVGENO-CMCD = "02"
               GO TO F020-SHCD.
           MOVE LS-UVGENO-EPNO TO WS-EPNO.
           PERFORM X500-READ-EP THRU X505-EXIT.
           IF WS-ERSW = "Y"
               MOVE "11" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
           DISPLAY EP-EPLN LINE 11 POSITION 39
                   EP-EPFN LINE 11 POSITION 54
                   EP-EPIT LINE 11 POSITION 65.
           PERFORM X380-EDIT-EPNO THRU X385-EXIT.
           IF WS-ERSW = "Y"
               GO TO F030-EPNO.
           MOVE ZEROS TO WS-STRT-SV.
       F040-WCNO.
           DISPLAY SPACES LINE 12 POSITION 27 SIZE 50.
           move spaces         to ls-uvgwkc-rcrd.
           move "PR"           to ls-uvgwkc-cmcd.
           move sy-st-cpno     to ls-uvgwkc-cpno.
           move 12             to ls-uvgwkc-line.
           move 33             to ls-uvgwkc-colm.
           move "Y"            to ls-uvgwkc-insw.
           call "UVGWKC" using ls-uvgwkc-rcrd.
           if ls-uvgwkc-cmcd = "02"
               DISPLAY SPACES LINE 12 POSITION 33 SIZE 3
               GO TO F030-EPNO.
           move ls-uvgwkc-wcno to ws-wcno.
           PERFORM X320-EDIT-WCNO THRU X325-EXIT
           IF WS-ERSW = "Y"
               GO TO F040-WCNO.
           DISPLAY WS-WCNO LINE 12 POSITION 33
                   WC-WCDC LINE 12 POSITION 39.
           GO TO F050-DKNO.
      *
       F050-DKNO.
           DISPLAY SPACES LINE 13 POSITION 27 SIZE 50.
           ACCEPT WS-DKNO
               LINE 13 POSITION 30
               PROMPT TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO F055-CHECK-FUNC.
           PERFORM X330-EDIT-DKNO THRU X335-EXIT.
           IF WS-ERSW = "Y"
               GO TO F050-DKNO.
           MOVE WS-DKNO TO DS-DKNO.
           DISPLAY DS-DKNO LINE 13 POSITION 30
                   WS-DKDC LINE 13 POSITION 39.
           GO TO F060-NEW-ENTRY-POINT.
      *
       F055-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 13 POSITION 30 SIZE 6
               GO TO F040-WCNO.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F050-DKNO.
      *
       F060-NEW-ENTRY-POINT.
       F060-OPNO.
           DISPLAY SPACES LINE 14 POSITION 27 SIZE 50.
           move spaces     to ls-uvgopr-rcrd.
           move "PR"       to ls-uvgopr-cmcd.
           move sy-st-cpno to ls-uvgopr-cpno.
           move 14         to ls-uvgopr-line.
           move 33         to ls-uvgopr-colm.
           move "Y"        to ls-uvgopr-insw.
           move ws-wcno    to ls-uvgopr-wcno.
           move spaces     to ls-uvgopr-opno.
           call "UVGOPR" using ls-uvgopr-rcrd.
           if ls-uvgopr-cmcd = "02"
               DISPLAY SPACES LINE 14 POSITION 33 SIZE 3
               GO TO F050-DKNO.
           move ls-uvgopr-opno to ws-opno.
           PERFORM X340-EDIT-OPNO THRU X345-EXIT.
           IF WS-ERSW = "Y"
               GO TO F060-OPNO.
           DISPLAY WS-OPNO LINE 14 POSITION 33
                   OP-OPDC LINE 14 POSITION 39.
       F061-REVIEW-OP.
           IF OP-OPTP = "C"
             AND WS-DKNO = ZERO
               DISPLAY "OPERATION CHARGEABLE - DOCKET REQUIRED "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F060-OPNO.
           IF OP-OPTP = "N"
            AND WS-DKNO NOT = ZERO
              AND SW-4-OFF
               DISPLAY "OPERATION NON-CHARGEABLE - NO DOCKET REQUIRED "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F060-OPNO.
      *
       F070-WRCD.
           DISPLAY SPACES LINE 15 POSITION 27 SIZE 50.
           move spaces         to ls-uvgwrs-rcrd.
           move sy-st-cpno     to ls-uvgwrs-cpno.
           move 15             to ls-uvgwrs-line.
           move 34             to ls-uvgwrs-colm.
           move "PR"           to ls-uvgwrs-cmcd.
           move "N"            to ls-uvgwrs-insw.
           call "UVGWRS" using ls-uvgwrs-rcrd.
           if ls-uvgwrs-cmcd = "02"
               display spaces line 15 position 34 size 47
               go to f060-opno.          
           move ls-uvgwrs-rscd to ws-wrcd.
           PERFORM X350-EDIT-REASON THRU X355-EXIT.
           IF WS-ERSW = "Y"
               GO TO F070-WRCD.
           DISPLAY WS-WRCD    LINE 15 POSITION 34
                   CM-WR-WRDC LINE 15 POSITION 39.
      *
       F080-STRT.
           MOVE WS-STRT-SV TO DS-STRT.
           DISPLAY DS-STRT LINE 16 POSITION 31.
           MOVE WS-STRT-SV TO WS-TIME.
           ACCEPT WS-TIME
               LINE 16 POSITION 31 SIZE 6
               TAB CONVERT UPDATE
             ON EXCEPTION
               FUNC-KEY
               GO TO F085-CHECK-FUNC.
       F080-TIME-EDIT.
           MOVE WS-TIME TO WS-STRT DS-STRT.
           INSPECT DS-STRT
               REPLACING ALL "." BY ":".
           DISPLAY DS-STRT LINE 16 POSITION 31.
           PERFORM X360-EDIT-TIME THRU X365-EXIT.
           IF WS-ERSW = "Y"
               GO TO F080-STRT.
           GO TO F090-STOP.
      *
       F085-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 16 POSITION 31 SIZE 6
               GO TO F070-WRCD.
           IF FUNC-98
               GO TO F080-TIME-EDIT.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F080-STRT.
      *
       F090-STOP.
           ACCEPT WS-TIME
               LINE 17 POSITION 31 SIZE 6
               PROMPT TAB CONVERT
             ON EXCEPTION
               FUNC-KEY
               GO TO F095-CHECK-FUNC.
       F090-TIME-EDIT.
           MOVE WS-TIME TO WS-STOP DS-STOP.
           INSPECT DS-STOP
               REPLACING ALL "." BY ":".
           DISPLAY DS-STOP LINE 17 POSITION 31.
           PERFORM X360-EDIT-TIME THRU X365-EXIT.
           IF WS-ERSW = "Y"
               GO TO F090-STOP.
       F090-REVIEW.
           IF WS-STRT > ZERO
               GO TO F090-STRT-GREATER-THAN-ZERO.
           IF WS-STRT < ZERO
               GO TO F090-STRT-LESS-THAN-ZERO.
      *
      *        NOTE THAT IF START TIME IS ZERO, STOP TIME CAN BE
      *             EITHER POSITIVE OR NEGATIVE BUT NOT ZERO.
      *
       F090-STRT-ZERO.
           IF WS-STOP = ZERO
               DISPLAY "STOP TIME MAY NOT BE ZERO "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F090-STOP.
           GO TO F100-PRMP.
      *
       F090-STRT-GREATER-THAN-ZERO.
           IF WS-STOP NOT > WS-STRT
               DISPLAY "STOP TIME MUST BE GREATER THAN START TIME "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F090-STOP.
           GO TO F100-PRMP.
      *
       F090-STRT-LESS-THAN-ZERO.
           IF WS-STOP NOT < WS-STRT
               DISPLAY "WHEN CREDITING TIME, STOP TIME "
                           LINE 24 POSITION 1 HIGH
                       "MUST BE LESS THAN START TIME "
                           LINE 00 POSITION 0 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F090-STOP.
           GO TO F100-PRMP.
      *
       F095-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 17 POSITION 31 SIZE 6
               GO TO F080-STRT.
           IF FUNC-98
               GO TO F090-TIME-EDIT.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F090-STOP.
      *
       F100-PRMP.
           DISPLAY "R" LINE 18 POSITION 35.
           MOVE "R" TO WS-PRMP.
           ACCEPT WS-PRMP
               LINE 18 POSITION 35
               TAB ECHO UPDATE
             ON EXCEPTION
               FUNC-KEY
               GO TO F105-CHECK-FUNC.
           PERFORM X370-EDIT-PAY-RATE THRU X375-EXIT.
           IF WS-ERSW = "Y"
               GO TO F100-PRMP.
           DISPLAY WS-PRMP LINE 18 POSITION 35.
           GO TO F110-REVIEW-OPERATION.
      *
       F105-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 18 POSITION 35 SIZE 1
               GO TO F090-STOP.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F100-PRMP.
      *
       F110-REVIEW-OPERATION.
           IF OP-DMQT = "Q"
             OR OP-DMQT = "T"
               GO TO F110-OPQT.
           MOVE ZERO TO WS-OPQT DS-OPQT.
           DISPLAY DS-OPQT LINE 19 POSITION 27.
           GO TO F200-SET-RECORD.
      *
       F110-OPQT.
           DISPLAY SPACES LINE 19 POSITION 27 SIZE 10.
           ACCEPT WS-OPQT
               LINE 19 POSITION 29
               PROMPT TAB CONVERT
             ON EXCEPTION
               FUNC-KEY
               GO TO F115-CHECK-FUNC.
       F110-QUANTITY-EDIT.
           MOVE WS-OPQT TO DS-OPQT.
           DISPLAY DS-OPQT LINE 19 POSITION 27.
       F110-REVIEW.
           IF WS-OPQT = ZERO
               DISPLAY "OPERATION DEMANDS QUANTITY "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F110-OPQT.
           IF WS-STOP < ZERO
             AND WS-OPQT > ZERO
               DISPLAY "WHEN TIME NEGATIVE, QUANTITY MUST BE NEGATIVE "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F110-OPQT.
           IF WS-STOP > ZERO
             AND WS-OPQT < ZERO
               DISPLAY "WHEN TIME POSITIVE, QUANTITY MUST BE POSITIVE "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F110-OPQT.
           GO TO F200-SET-RECORD.
      *
       F115-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 19 POSITION 27 SIZE 10
               GO TO F100-PRMP.
           IF FUNC-98
               GO TO F110-QUANTITY-EDIT.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F110-OPQT.
      *
       F200-SET-RECORD.
           MOVE SPACES     TO IR-RCRD IR-RCRD-WS.
           MOVE SY-ST-CPNO    TO IR-CPNO-1 IR-CPNO-2.
           MOVE WS-DTNO-SV TO IR-DTNO-1.
           MOVE 15         TO IR-LFCD-2.
           MOVE WS-OPDT    TO IR-OPDT-2.
           MOVE WS-SHCD    TO IR-SHCD-2.
           MOVE WS-EPNO    TO IR-EPNO-2.
       F205-CONVERT-STRT.
           MOVE "TM"       TO LS-UVCVTM-CMCD.
           MOVE WS-STRT    TO LS-UVCVTM-LBTM.
           PERFORM X120-UVCVTM THRU X125-EXIT.
           MOVE LS-UVCVTM-LBMN TO IR-STRT-2.
       F210-CONTINUE.
           MOVE WS-WCNO    TO IR-WCNO.
           MOVE WS-DKNO    TO IR-DKNO.
           MOVE WS-OPNO    TO IR-OPNO.
           MOVE WS-WRCD    TO IR-WRCD.
           MOVE ZEROS      TO IR-OPRT.
       F215-CONVERT-STOP.
           MOVE "TM"       TO LS-UVCVTM-CMCD.
           MOVE WS-STOP    TO LS-UVCVTM-LBTM WS-STRT-SV.
           PERFORM X120-UVCVTM THRU X125-EXIT.
           MOVE LS-UVCVTM-LBMN TO IR-STOP.
       F220-CONTINUE.
           MOVE WS-PRMP    TO IR-PRMP.
           MOVE WS-OPQT    TO IR-OPQT.
           MOVE "JC"       TO IR-SRCD.
           MOVE 1          TO IR-STCD.
       F230-WRITE-IR.
           WRITE IR-RCRD FROM IR-RCRD-WS.
      *
           if file-status = "22"
               move "XX" to file-status
               PERFORM X800-GET-DTNO THRU X890-EXIT
               MOVE WS-DTNO-SV TO IR-DTNO-1
               go to F230-WRITE-IR.
      *
           IF NOT FILE-OK
               MOVE "WR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       F240-DISPLY-SPACES.
           DISPLAY SPACES  LINE 19 POSITION 27 SIZE 10
                   SPACES  LINE 18 POSITION 35 SIZE 1
                   SPACES  LINE 17 POSITION 31 SIZE 6
                   SPACES  LINE 16 POSITION 31 SIZE 6
                   SPACES  LINE 15 POSITION 27 SIZE 50.
       F250-NEW-TRANS.
           PERFORM X800-GET-DTNO THRU X890-EXIT.
           MOVE WS-DTNO-SV TO DS-DTNO.
           DISPLAY DS-DTNO LINE 08 POSITION 31.
           GO TO F060-NEW-ENTRY-POINT.
       F990-EXIT.
           EXIT.
      
       H000-CHANGE-MODE.
           MOVE SPACES TO WS-CHSW.
       H010-SET-DISPLY.
           MOVE IR-DTNO-1 TO DS-DTNO.
           MOVE IR-OPDT-2 TO DS-OPDT.
           MOVE IR-EPNO-2 TO WS-EPNO.
           PERFORM X500-READ-EP THRU X505-EXIT.
           DISPLAY DS-DTNO   LINE 08 POSITION 31
                   DS-OPDT   LINE 09 POSITION 28
                   IR-SHCD-2 LINE 10 POSITION 35
                   WS-EPNO   LINE 11 POSITION 31
                   EP-EPLN   LINE 11 POSITION 39
                   EP-EPFN   LINE 11 POSITION 54
                   EP-EPIT   LINE 11 POSITION 65.
           MOVE IR-WCNO   TO WS-WCNO.
           PERFORM X510-READ-WC THRU X515-EXIT.
           DISPLAY WS-WCNO   LINE 12 POSITION 33
                   WC-WCDC   LINE 12 POSITION 39.
           MOVE IR-DKNO   TO WS-DKNO DS-DKNO.
           IF IR-DKNO = ZEROS
               MOVE SPACES TO WS-DKDC
               GO TO H020-CONTINUE.
           PERFORM X520-READ-DP THRU X525-EXIT.
           IF WS-ERSW = "Y"
             OR SW-1-ON
               MOVE DP-DKD1 TO WS-DKDC
               GO TO H020-CONTINUE.
           MOVE DP-CTNO   TO WS-CTNO.
           PERFORM X530-READ-CT THRU X535-EXIT.
           MOVE CT-CTNM   TO WS-DKDC.
       H020-CONTINUE.
           DISPLAY DS-DKNO   LINE 13 POSITION 30
                   WS-DKDC   LINE 13 POSITION 39.
           MOVE IR-OPNO   TO WS-OPNO.
           PERFORM X540-READ-OP THRU X545-EXIT.
           DISPLAY WS-OPNO   LINE 14 POSITION 33
                   OP-OPDC   LINE 14 POSITION 39.
           MOVE IR-WRCD   TO WS-WRCD.
           IF IR-WRCD = SPACES
               MOVE SPACES TO CM-WR-WRDC
               GO TO H030-CONTINUE.
           PERFORM X550-READ-CM THRU X555-EXIT.
       H030-CONTINUE.
           DISPLAY WS-WRCD    LINE 15 POSITION 34
                   CM-WR-WRDC LINE 15 POSITION 39.
       H035-CONVERT-STRT.
           MOVE "MT"       TO LS-UVCVTM-CMCD.
           MOVE IR-STRT-2  TO LS-UVCVTM-LBMN.
           PERFORM X120-UVCVTM THRU X125-EXIT.
           MOVE LS-UVCVTM-LBTM TO DS-STRT.
           INSPECT DS-STRT
               REPLACING ALL "." BY ":".
       H040-CONVERT-STOP.
           MOVE "MT"       TO LS-UVCVTM-CMCD.
           MOVE IR-STOP    TO LS-UVCVTM-LBMN.
           PERFORM X120-UVCVTM THRU X125-EXIT.
           MOVE LS-UVCVTM-LBTM TO DS-STOP.
           INSPECT DS-STOP
               REPLACING ALL "." BY ":".
       H050-CONTINUE.
           MOVE IR-OPQT   TO DS-OPQT.
           DISPLAY DS-STRT   LINE 16 POSITION 31
                   DS-STOP   LINE 17 POSITION 31
                   IR-PRMP   LINE 18 POSITION 35
                   DS-OPQT   LINE 19 POSITION 27.
       H100-SELECT.
           DISPLAY "SELECT: "
               LINE 24 POSITION 34 HIGH.
           ACCEPT WS-OPTION
               LINE 00 POSITION 00
               PROMPT TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO H390-CHECK-FUNC.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           IF WS-OPTION = "DELETE"
               GO TO H300-DELETE-MODE.
           IF WS-FIELD = 01
               GO TO H110-OPDT.
           IF WS-FIELD = 02
               GO TO H120-SHCD.
           IF WS-FIELD = 03
               GO TO H130-EPNO.
           IF WS-FIELD = 04
               GO TO H140-WCNO.
           IF WS-FIELD = 05
               GO TO H150-DKNO.
           IF WS-FIELD = 06
               GO TO H160-OPNO.
           IF WS-FIELD = 07
               GO TO H170-WRCD.
           IF WS-FIELD = 08
               GO TO H180-STRT.
           IF WS-FIELD = 09
               GO TO H190-STOP.
           IF WS-FIELD = 10
               GO TO H200-PRMP.
           IF WS-FIELD = 11
               GO TO H210-OPQT.
           GO TO H100-SELECT.
      *
       H110-OPDT.
           MOVE IR-OPDT-2 TO LS-UVACDT-DATE.
           PERFORM X100-UVACDT THRU X105-EXIT.
           IF LS-UVACDT-CMCD = "02"
               PERFORM X000-FUNC-ERR THRU X090-EXIT
               GO TO H110-OPDT.
           move ls-uvacdt-date to ls-uvdtcv-indt.
           move "EX"           to ls-uvdtcv-cmcd.
           perform x999-uvdtcv thru x999-exit.
           MOVE LS-UVdtcv-otdt TO WS-OPDT.
           PERFORM X300-EDIT-OPDT THRU X305-EXIT.
           IF WS-ERSW = "Y"
               GO TO H110-OPDT.
           IF WS-OPDT NOT = IR-OPDT-2
               MOVE WS-OPDT TO IR-OPDT-2
               MOVE "Y" TO WS-CHSW.
           GO TO H100-SELECT.
      *
       H120-SHCD.
           DISPLAY IR-SHCD-2 LINE 10 POSITION 35.
           MOVE IR-SHCD-2 TO WS-SHCD.
           ACCEPT WS-SHCD
               LINE 10 POSITION 35
               TAB ECHO UPDATE.
           PERFORM X310-EDIT-SHIFT THRU X315-EXIT.
           IF WS-ERSW = "Y"
               GO TO H120-SHCD.
           DISPLAY WS-SHCD LINE 10 POSITION 35.
           IF WS-SHCD NOT = IR-SHCD-2
               MOVE WS-SHCD TO IR-SHCD-2
               MOVE "Y" TO WS-CHSW.
           GO TO H100-SELECT.
      *
       H130-EPNO.
           DISPLAY SPACES LINE 11 POSITION 27 SIZE 50.
           MOVE "NP"      TO LS-UVGENO-CMCD.
           MOVE IR-EPNO-2 TO LS-UVGENO-EPNO.
           PERFORM X110-UVGENO THRU X115-EXIT.
           IF LS-UVGENO-CMCD = "02"
               PERFORM X000-FUNC-ERR THRU X090-EXIT
               GO TO H130-EPNO.
           MOVE LS-UVGENO-EPNO TO WS-EPNO.
           PERFORM X500-READ-EP THRU X505-EXIT.
           IF WS-ERSW = "Y"
               MOVE "21" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
           DISPLAY EP-EPLN LINE 11 POSITION 39
                   EP-EPFN LINE 11 POSITION 54
                   EP-EPIT LINE 11 POSITION 65.
           PERFORM X380-EDIT-EPNO THRU X385-EXIT.
           IF WS-ERSW = "Y"
               GO TO H130-EPNO.
           IF WS-EPNO NOT = IR-EPNO-2
               MOVE WS-EPNO TO IR-EPNO-2
               MOVE "Y" TO WS-CHSW.
           GO TO H100-SELECT.
      *
       H140-WCNO.
           MOVE IR-WCNO TO WS-WCNO.
           DISPLAY SPACES  LINE 12 POSITION 27 SIZE 50
                   WS-WCNO LINE 12 POSITION 33.
           MOVE IR-WCNO        TO WS-WCNO.
           move spaces         to ls-uvgwkc-rcrd.
           move spaces         to ls-uvgwkc-rcrd.
           move "NP"           to ls-uvgwkc-cmcd.
           move sy-st-cpno     to ls-uvgwkc-cpno.
           move 12             to ls-uvgwkc-line.
           move 33             to ls-uvgwkc-colm.
           move "Y"            to ls-uvgwkc-insw.
           move ws-wcno        to ls-uvgwkc-wcno.
           call "UVGWKC" using ls-uvgwkc-rcrd.
           if ls-uvgwkc-cmcd = "02"
               perform x000-func-err thru x090-exit
               go to h140-wcno.    
           move ls-uvgwkc-wcno to ws-wcno.
           PERFORM X320-EDIT-WCNO THRU X325-EXIT.
           IF WS-ERSW = "Y"
               GO TO H140-WCNO.
           DISPLAY WS-WCNO LINE 12 POSITION 33
                   WC-WCDC LINE 12 POSITION 39.
           IF WS-WCNO NOT = IR-WCNO
               MOVE WS-WCNO TO IR-WCNO
               MOVE "Y" TO WS-CHSW.
           GO TO H160-OPNO.
      *
       H150-DKNO.
           MOVE IR-DKNO TO DS-DKNO.
           DISPLAY SPACES  LINE 13 POSITION 27 SIZE 50
                   DS-DKNO LINE 13 POSITION 30.
           MOVE IR-DKNO TO WS-DKNO.
           ACCEPT WS-DKNO
               LINE 13 POSITION 30
               TAB ECHO UPDATE.
           INSPECT WS-DKNO
               REPLACING ALL " " BY "0".
           PERFORM X330-EDIT-DKNO THRU X335-EXIT.
           IF WS-ERSW = "Y"
               GO TO H150-DKNO.
           MOVE WS-DKNO TO DS-DKNO.
           DISPLAY DS-DKNO LINE 13 POSITION 30
                   WS-DKDC LINE 13 POSITION 39.
           IF WS-DKNO NOT = IR-DKNO
               MOVE WS-DKNO TO IR-DKNO
               MOVE "Y" TO WS-CHSW.
           GO TO H100-SELECT.
      *
       H160-OPNO.
           MOVE IR-WCNO TO WS-WCNO.
           MOVE IR-OPNO TO WS-OPNO.
           DISPLAY SPACES  LINE 14 POSITION 27 SIZE 50
                   WS-OPNO LINE 14 POSITION 33.
           MOVE IR-OPNO TO WS-OPNO.
           move spaces         to ls-uvgopr-rcrd.
           move "NP"           to ls-uvgopr-cmcd.
           move sy-st-cpno     to ls-uvgopr-cpno.
           move 14             to ls-uvgopr-line.
           move 33             to ls-uvgopr-colm.
           move "Y"            to ls-uvgopr-insw.
           move ws-opno        to ls-uvgopr-opno.
           move ws-wcno        to ls-uvgopr-wcno.
           call "UVGOPR" using ls-uvgopr-rcrd.
           move ls-uvgopr-opno to ws-opno.
           PERFORM X340-EDIT-OPNO THRU X345-EXIT.
           IF WS-ERSW = "Y"
               GO TO H160-OPNO.
           DISPLAY WS-OPNO LINE 14 POSITION 33
                   OP-OPDC LINE 14 POSITION 39.
           IF WS-OPNO NOT = IR-OPNO
               MOVE WS-OPNO TO IR-OPNO
               MOVE "Y" TO WS-CHSW.
           GO TO H100-SELECT.
      *
       H170-WRCD.
           DISPLAY SPACES  LINE 15 POSITION 27 SIZE 50
                   IR-WRCD LINE 15 POSITION 34.
           MOVE IR-WRCD        TO WS-WRCD.
           move spaces         to ls-uvgwrs-rcrd.
           move sy-st-cpno     to ls-uvgwrs-cpno.
           move 15             to ls-uvgwrs-line.
           move 34             to ls-uvgwrs-colm.
           move "NP"           to ls-uvgwrs-cmcd.
           move "N"            to ls-uvgwrs-insw.
           move ws-wrcd        to ls-uvgwrs-rscd.
           call "UVGWRS" using ls-uvgwrs-rcrd.
           if ls-uvgwrs-cmcd = "02"
               perform x000-func-err thru x090-exit
               go to h170-wrcd.
           move ls-uvgwrs-rscd to ws-wrcd.
           PERFORM X350-EDIT-REASON THRU X355-EXIT.
           IF WS-ERSW = "Y"
               GO TO H170-WRCD.
           DISPLAY WS-WRCD    LINE 15 POSITION 34
                   CM-WR-WRDC LINE 15 POSITION 39.
           IF WS-WRCD NOT = IR-WRCD
               MOVE WS-WRCD    TO IR-WRCD
               MOVE "Y"        TO WS-CHSW.
           GO TO H100-SELECT.
      *
       H180-STRT.
           MOVE "MT"       TO LS-UVCVTM-CMCD.
           MOVE IR-STRT-2  TO LS-UVCVTM-LBMN.
           PERFORM X120-UVCVTM THRU X125-EXIT.
       H185-ACK-STRT.
           MOVE LS-UVCVTM-LBTM TO DS-STRT.
           DISPLAY DS-STRT LINE 16 POSITION 31.
           MOVE LS-UVCVTM-LBTM TO WS-TIME.
           ACCEPT WS-TIME
               LINE 16 POSITION 31 SIZE 6
               TAB CONVERT UPDATE.
           MOVE WS-TIME TO DS-STRT.
           INSPECT DS-STRT
               REPLACING ALL "." BY ":".
           DISPLAY DS-STRT LINE 16 POSITION 31.
           PERFORM X360-EDIT-TIME THRU X365-EXIT.
           IF WS-ERSW = "Y"
               GO TO H185-ACK-STRT.
           MOVE "TM"    TO LS-UVCVTM-CMCD.
           MOVE WS-TIME TO LS-UVCVTM-LBTM.
           PERFORM X120-UVCVTM THRU X125-EXIT.
           IF LS-UVCVTM-LBMN NOT = IR-STRT-2
               MOVE LS-UVCVTM-LBMN TO IR-STRT-2
               MOVE "Y" TO WS-CHSW.
           GO TO H100-SELECT.
      *
       H190-STOP.
           MOVE "MT"    TO LS-UVCVTM-CMCD.
           MOVE IR-STOP TO LS-UVCVTM-LBMN.
           PERFORM X120-UVCVTM THRU X125-EXIT.
       H195-ACK-STOP.
           MOVE LS-UVCVTM-LBTM TO DS-STOP.
           DISPLAY DS-STOP LINE 17 POSITION 31.
           MOVE LS-UVCVTM-LBTM TO WS-TIME.
           ACCEPT WS-TIME
               LINE 17 POSITION 31 SIZE 6
               TAB CONVERT UPDATE.
           MOVE WS-TIME TO DS-STOP.
           INSPECT DS-STOP
               REPLACING ALL "." BY ":".
           DISPLAY DS-STOP LINE 17 POSITION 31.
           PERFORM X360-EDIT-TIME THRU X365-EXIT.
           IF WS-ERSW = "Y"
               GO TO H195-ACK-STOP.
           MOVE "TM"    TO LS-UVCVTM-CMCD.
           MOVE WS-TIME TO LS-UVCVTM-LBTM.
           PERFORM X120-UVCVTM THRU X125-EXIT.
           IF LS-UVCVTM-LBMN NOT = IR-STOP
               MOVE LS-UVCVTM-LBMN TO IR-STOP
               MOVE "Y" TO WS-CHSW.
           GO TO H100-SELECT.
      *
       H200-PRMP.
           DISPLAY IR-PRMP LINE 18 POSITION 35.
           MOVE IR-PRMP TO WS-PRMP.
           ACCEPT WS-PRMP
               LINE 18 POSITION 35
               TAB ECHO UPDATE.
           PERFORM X370-EDIT-PAY-RATE THRU X375-EXIT.
           IF WS-ERSW = "Y"
               GO TO H200-PRMP.
           DISPLAY WS-PRMP LINE 18 POSITION 35.
           IF WS-PRMP NOT = IR-PRMP
               MOVE WS-PRMP TO IR-PRMP
               MOVE "Y" TO WS-CHSW.
           GO TO H100-SELECT.
      *
       H210-OPQT.
           MOVE IR-OPQT TO ED-OPQT.
           DISPLAY SPACES  LINE 19 POSITION 27 SIZE 10
                   ED-OPQT LINE 19 POSITION 29.
           MOVE IR-OPQT TO WS-OPQT.
           ACCEPT WS-OPQT
               LINE 19 POSITION 29
               TAB CONVERT UPDATE.
           MOVE WS-OPQT TO DS-OPQT.
           DISPLAY DS-OPQT LINE 19 POSITION 27.
           IF WS-OPQT NOT = IR-OPQT
               MOVE WS-OPQT TO IR-OPQT
               MOVE "Y" TO WS-CHSW.
           GO TO H100-SELECT.
      *
       H300-DELETE-MODE.
           DISPLAY "DELETE - ARE YOU SURE? (Y/N) "
               LINE 24 POSITION 1 HIGH.
           ACCEPT DUMMY-77
               LINE 00 POSITION 00
               PROMPT TAB ECHO.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           IF DUMMY-77 = "N"
               GO TO H100-SELECT.
           IF DUMMY-77 NOT = "Y"
               GO TO H300-DELETE-MODE.
       H310-DELETE-IR.
           DELETE IR-FILE RECORD.
           IF NOT FILE-OK
               MOVE "DL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           GO TO H900-CLEAR-SCREEN.
      *
       H390-CHECK-FUNC.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           IF FUNC-02
               GO TO H400-CROSS-EDIT.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO H100-SELECT.
      *
       H400-CROSS-EDIT.
           IF WS-CHSW NOT = "Y"
               GO TO H890-UNLOCK-IR.
       H410-VERIFY-DATA.
           MOVE IR-WCNO TO WS-WCNO.
           MOVE IR-OPNO TO WS-OPNO.
           PERFORM X540-READ-OP THRU X545-EXIT.
           IF WS-ERSW = "Y"
               DISPLAY "OPERATION/WORK CENTRE COMBINATION NOT VALID "
                   LINE 24 POSITION 1 HIGH
               GO TO H800-ERROR.
           IF OP-OPTP = "C"
             AND IR-DKNO = ZERO
               DISPLAY "OPERATION CHARGEABLE - DOCKET REQUIRED "
                   LINE 24 POSITION 1 HIGH
               GO TO H800-ERROR.
           IF OP-OPTP = "N"
            AND IR-DKNO NOT = ZERO
             AND SW-4-OFF
               DISPLAY "OPERATION NON-CHARGEABLE - NO DOCKET REQUIRED "
                   LINE 24 POSITION 1 HIGH
               GO TO H800-ERROR.
           IF IR-STRT-2 > ZERO
               GO TO H430-START-GREATER-THAN-ZERO.
           IF IR-STRT-2 < ZERO
               GO TO H440-START-LESS-THAN-ZERO.
       H420-START-ZERO.
           IF IR-STOP = ZERO
               DISPLAY "STOP TIME MAY NOT BE ZERO "
                   LINE 24 POSITION 1 HIGH
               GO TO H800-ERROR.
           GO TO H450-QUANTITY.
      *
       H430-START-GREATER-THAN-ZERO.
           IF IR-STOP NOT > IR-STRT-2
               DISPLAY "STOP TIME MUST BE GREATER THAN START TIME "
                   LINE 24 POSITION 1 HIGH
               GO TO H800-ERROR.
           GO TO H450-QUANTITY.
      *
       H440-START-LESS-THAN-ZERO.
           IF IR-STOP NOT < IR-STRT-2
               DISPLAY "WHEN CREDITING TIME, STOP TIME "
                           LINE 24 POSITION 1 HIGH
                       "MUST BE LESS THAN START TIME "
                           LINE 00 POSITION 0 HIGH
               GO TO H800-ERROR.
      *
       H450-QUANTITY.
           IF ( OP-DMQT = "Q" OR OP-DMQT = "T" )
             AND IR-OPQT = ZERO
               DISPLAY "OPERATION DEMANDS QUANTITY "
                   LINE 24 POSITION 1 HIGH
               GO TO H800-ERROR.
           IF OP-DMQT = "N"
             AND IR-OPQT NOT = ZERO
               DISPLAY "QUANTITY MUST BE ZERO FOR THIS OPERATION "
                   LINE 24 POSITION 1 HIGH
               GO TO H800-ERROR.
           IF OP-DMQT = "N"
               GO TO H700-REWRITE-IR.
       H460-REVIEW-TIME-QUANTITY.
           IF IR-STOP < ZERO
             AND IR-OPQT > ZERO
               DISPLAY "WHEN TIME NEGATIVE, QUANTITY MUST BE NEGATIVE "
                   LINE 24 POSITION 1 HIGH
               GO TO H800-ERROR.
           IF IR-STOP > ZERO
             AND IR-OPQT < ZERO
               DISPLAY "WHEN TIME POSITIVE, QUANTITY MUST BE POSITIVE "
                   LINE 24 POSITION 1 HIGH
               GO TO H800-ERROR.
      *
       H700-REWRITE-IR.
           MOVE 1 TO IR-STCD.
           REWRITE IR-RCRD FROM IR-RCRD-WS.
           IF NOT FILE-OK
               MOVE "RW" TO FILE-IO
               GO TO Z100-FILE-ERR.
           GO TO H900-CLEAR-SCREEN.
      *
       H800-ERROR.
           PERFORM X050-REPLY THRU X090-EXIT.
           GO TO H100-SELECT.
      *
       H890-UNLOCK-IR.
           UNLOCK IR-FILE RECORD.
           IF NOT FILE-OK
               MOVE "UL" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       H900-CLEAR-SCREEN.
           PERFORM X900-CLEAR THRU X990-EXIT.
       H990-EXIT.
           EXIT.
      
      
       X000-FUNC-ERR.
           DISPLAY "FUNCTION KEY NOT VALID "
               LINE 24 POSITION 1 HIGH.
       X050-REPLY.
           ACCEPT DUMMY-77 LINE 00 POSITION 00 TAB OFF.
           DISPLAY SPACES LINE 24 POSITION 01 SIZE 80.
       X090-EXIT.
           EXIT.
      
       X100-UVACDT.
           MOVE "NP" TO LS-UVACDT-CMCD.
           CALL "UVACDT" USING
               LS-UVACDT-RCRD.
       X105-EXIT.
           EXIT.
      
       X110-UVGENO.
           CALL "UVGENO" USING
               LS-UVGENO-RCRD.
       X115-EXIT.
           EXIT.
      
       X120-UVCVTM.
           CALL "UVCVTM" USING
               LS-UVCVTM-RCRD.
           IF LS-UVCVTM-CMCD NOT = "00"
               MOVE "41" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
       X125-EXIT.
           EXIT.
      
      *        EDIT ROUTINES
      
       X300-EDIT-OPDT.
           MOVE SPACES TO WS-ERSW.
           IF WS-OPDT > SY-ST-SODT
               DISPLAY "OPERATION DATE > SIGNON DATE "
                   LINE 24 POSITION 1 HIGH
               GO TO X300-ERROR.
           IF WS-OPDT < WS-UVEDPR-MNDT
               DISPLAY "OPERATION, SIGNON, AND "
                           LINE 24 POSITION 1 HIGH
                       "ACCOUNTING DATES ARE NOT CONSISTENT "
                           LINE 00 POSITION 0 HIGH
               GO TO X300-ERROR.
           GO TO X305-EXIT.
      *
       X300-ERROR.
           PERFORM X050-REPLY THRU X090-EXIT.
           MOVE "Y" TO WS-ERSW.
       X305-EXIT.
           EXIT.
      
      
       X310-EDIT-SHIFT.
           MOVE SPACES TO WS-ERSW.
           IF WS-SHCD NOT NUMERIC
               DISPLAY "SHIFT CODE MUST BE NUMERIC "
                   LINE 24 POSITION 1 HIGH
               GO TO X310-ERROR.
           IF NOT VALID-SHIFT
               DISPLAY "SHIFT CODE NOT VALID "
                   LINE 24 POSITION 1 HIGH
               GO TO X310-ERROR.
           GO TO X315-EXIT.
      *
       X310-ERROR.
           PERFORM X050-REPLY THRU X090-EXIT.
           MOVE "Y" TO WS-ERSW.
       X315-EXIT.
           EXIT.
      
      
       X320-EDIT-WCNO.
           MOVE SPACES TO WS-ERSW.
           IF WS-WCNO NOT NUMERIC
               DISPLAY "WORK CENTRE NUMBER MUST BE NUMERIC "
                   LINE 24 POSITION 1 HIGH
               GO TO X320-ERROR.
           PERFORM X510-READ-WC THRU X515-EXIT.
           IF WS-ERSW = "Y"
               DISPLAY "WORK CENTRE NOT ON FILE "
                   LINE 24 POSITION 1 HIGH
               GO TO X320-ERROR.
           GO TO X325-EXIT.
      *
       X320-ERROR.
           PERFORM X050-REPLY THRU X090-EXIT.
           MOVE "Y" TO WS-ERSW.
       X325-EXIT.
           EXIT.
      
      
       X330-EDIT-DKNO.
           MOVE SPACES TO WS-ERSW.
           IF WS-DKNO NOT NUMERIC
               DISPLAY "DOCKET NUMBER MUST BE NUMERIC "
                   LINE 24 POSITION 1 HIGH
               GO TO X330-ERROR.
           IF WS-DKNO = ZERO
               MOVE SPACES TO WS-DKDC
               GO TO X335-EXIT.
           PERFORM X520-READ-DP THRU X525-EXIT.
           IF WS-ERSW = "Y"
               DISPLAY "DOCKET NOT ON FILE "
                   LINE 24 POSITION 1 HIGH
               GO TO X330-ERROR.
           IF DP-STCD NOT = 1
             AND DP-STCD NOT = 2
               DISPLAY "DOCKET NOT ACTIVE - NO ENTRY ALLOWED "
                   LINE 24 POSITION 1 HIGH
               GO TO X330-ERROR.
           IF SW-1-ON
               MOVE DP-DKD1 TO WS-DKDC
               GO TO X335-EXIT.
           MOVE DP-CTNO TO WS-CTNO.
           PERFORM X530-READ-CT THRU X535-EXIT.
           MOVE CT-CTNM TO WS-DKDC.
           GO TO X335-EXIT.
      *
       X330-ERROR.
           PERFORM X050-REPLY THRU X090-EXIT.
           MOVE "Y" TO WS-ERSW.
       X335-EXIT.
           EXIT.
      
      
       X340-EDIT-OPNO.
           MOVE SPACES TO WS-ERSW.
           IF WS-OPNO NOT NUMERIC
               DISPLAY "OPERATION NUMBER MUST BE NUMERIC "
                   LINE 24 POSITION 1 HIGH
               GO TO X340-ERROR.
           PERFORM X540-READ-OP THRU X545-EXIT.
           IF WS-ERSW = "Y"
               DISPLAY "OPERATION NOT ON FILE "
                   LINE 24 POSITION 1 HIGH
               GO TO X340-ERROR.
           GO TO X345-EXIT.
      *
       X340-ERROR.
           PERFORM X050-REPLY THRU X090-EXIT.
           MOVE "Y" TO WS-ERSW.
       X345-EXIT.
           EXIT.
      
      
       X350-EDIT-REASON.
           MOVE SPACES TO WS-ERSW.
           IF WS-WRCD = SPACES
               MOVE SPACES TO CM-WR-WRDC
               GO TO X355-EXIT.
           PERFORM X550-READ-CM THRU X555-EXIT.
           IF WS-ERSW = "Y"
               DISPLAY "REASON CODE NOT VALID "
                   LINE 24 POSITION 1 HIGH
               GO TO X350-ERROR.
           GO TO X355-EXIT.
      *
       X350-ERROR.
           PERFORM X050-REPLY THRU X090-EXIT.
           MOVE "Y" TO WS-ERSW.
       X355-EXIT.
           EXIT.
      
      
       X360-EDIT-TIME.
           MOVE SPACES TO WS-ERSW.
           IF WS-TIME > 24.00
               DISPLAY "TIME MAY NOT BE GREATER THAN 24:00 HOURS "
                   LINE 24 POSITION 1 HIGH
               GO TO X360-ERROR.
           IF WS-TIME < -24.00
               DISPLAY "TIME MAY NOT BE LESS THAN -24:00 HOURS "
                   LINE 24 POSITION 1 HIGH
               GO TO X360-ERROR.
           MOVE WS-TIME TO WS-TMSP.
           IF WS-TMMN > 59
               DISPLAY "MINUTES MAY ONLY BE FROM :00 TO :59 "
                   LINE 24 POSITION 1 HIGH
               GO TO X360-ERROR.
           GO TO X365-EXIT.
      *
       X360-ERROR.
           PERFORM X050-REPLY THRU X090-EXIT.
           MOVE "Y" TO WS-ERSW.
       X365-EXIT.
           EXIT.
      
      
       X370-EDIT-PAY-RATE.
           MOVE SPACES TO WS-ERSW.
           IF NOT VALID-PAY-RATE
               DISPLAY "PAY RATE CODE NOT VALID "
                   LINE 24 POSITION 1 HIGH
               GO TO X370-ERROR.
           GO TO X375-EXIT.
      *
       X370-ERROR.
           PERFORM X050-REPLY THRU X090-EXIT.
           MOVE "Y" TO WS-ERSW.
       X375-EXIT.
           EXIT.
      
      
       X380-EDIT-EPNO.
           MOVE SPACES TO WS-ERSW.
           IF WS-EPNO = WS-AENO
               DISPLAY "ADJUSTMENT EMPLOYEE NOT VALID "
                   LINE 24 POSITION 1 HIGH
               GO TO X380-ERROR.
           GO TO X385-EXIT.
      *
       X380-ERROR.
           PERFORM X050-REPLY THRU X090-EXIT.
           MOVE "Y" TO WS-ERSW.
       X385-EXIT.
           EXIT.
      
      *        READ ROUTINES
      
       X500-READ-EP.
           MOVE SPACES  TO EP-KEY-1.
           MOVE SY-ST-CPNO TO EP-CPNO-K1.
           MOVE WS-EPNO TO EP-EPNO-K1.
           READ EP-FILE
               INTO EP-RCRD-WS
               KEY IS EP-KEY-1
             INVALID KEY
               MOVE "Y" TO WS-ERSW
               MOVE "* EMPLOYEE NOT" TO EP-EPLN
               MOVE "ON FILE *"      TO EP-EPFN
               MOVE SPACE            TO EP-EPIT
               GO TO X505-EXIT.
           IF RECORD-LOCKED
               MOVE "EPMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X500-READ-EP.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
           MOVE SPACES TO WS-ERSW.
       X505-EXIT.
           EXIT.
      
      
       X510-READ-WC.
           MOVE LOW-VALUES TO WC-KEY-1.
           MOVE SY-ST-CPNO    TO WC-CPNO-K1.
           MOVE WS-WCNO    TO WC-WCNO-K1.
           READ WC-FILE
               INTO WC-RCRD-WS
               KEY IS WC-KEY-1
             INVALID KEY
               MOVE "Y" TO WS-ERSW
               MOVE "* WC NOT ON FILE *" TO WC-WCDC
               GO TO X515-EXIT.
           IF RECORD-LOCKED
               MOVE "WCMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X510-READ-WC.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
           MOVE SPACES TO WS-ERSW.
       X515-EXIT.
           EXIT.
      
      
       X520-READ-DP.
           MOVE SPACES  TO DP-KEY-1.
           MOVE SY-ST-CPNO TO DP-CPNO-K1.
           MOVE WS-DKNO TO DP-DKNO-K1.
           READ DP-FILE
               INTO DP-RCRD-WS
               KEY IS DP-KEY-1
             INVALID KEY
               MOVE "Y" TO WS-ERSW
               MOVE "* DOCKET NOT ON FILE *" TO DP-DKD1
               GO TO X525-EXIT.
           IF RECORD-LOCKED
               MOVE "DKIP" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X520-READ-DP.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
           MOVE SPACES TO WS-ERSW.
       X525-EXIT.
           EXIT.
      
      
       X530-READ-CT.
           MOVE SPACES  TO CT-KEY-1.
           MOVE SY-ST-CPNO TO CT-CPNO-K1.
           MOVE WS-CTNO TO CT-CTNO-K1.
           READ CT-FILE
               INTO CT-RCRD-WS
               KEY IS CT-KEY-1
             INVALID KEY
               MOVE "* DOCKET CUSTOMER NOT ON FILE *" TO CT-CTNM
               GO TO X535-EXIT.
           IF RECORD-LOCKED
               MOVE "CTMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X530-READ-CT.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X535-EXIT.
           EXIT.
      
      
       X540-READ-OP.
           MOVE SPACES  TO OP-KEY-1.
           MOVE SY-ST-CPNO TO OP-CPNO-K1.
           MOVE WS-WCNO TO OP-WCNO-K1.
           MOVE WS-OPNO TO OP-OPNO-K1.
           READ OP-FILE
               INTO OP-RCRD-WS
               KEY IS OP-KEY-1
             INVALID KEY
               MOVE "Y" TO WS-ERSW
               MOVE "* OPERATION NOT ON FILE *" TO OP-OPDC
               GO TO X545-EXIT.
           IF RECORD-LOCKED
               MOVE "OPMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X540-READ-OP.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
           MOVE SPACES TO WS-ERSW.
       X545-EXIT.
           EXIT.
      
      
       X550-READ-CM.
           MOVE SPACES  TO CM-KEY-1 CM-WR-KEY-1-WS.
           MOVE SY-ST-CPNO TO CM-WR-CPNO-1.
           MOVE 31      TO CM-WR-LFCD-1.
           MOVE WS-WRCD TO CM-WR-RSCD-1.
           MOVE CM-WR-KEY-1-WS TO CM-KEY-1.
           READ CM-FILE
               WITH NO LOCK
               INTO CM-WR-RCRD-WS
               KEY IS CM-KEY-1
             INVALID KEY
               MOVE "Y" TO WS-ERSW
               MOVE "* REASON NOT ON FILE *" TO CM-WR-WRDC
               GO TO X555-EXIT.
           IF RECORD-LOCKED
               MOVE "JCMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X550-READ-CM.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
           MOVE SPACES TO WS-ERSW.
       X555-EXIT.
           EXIT.
      
      *        OTHER ROUTINES
      
       X800-GET-DTNO.
           MOVE SPACES   TO CM-KEY-1.
           MOVE SY-ST-CPNO  TO CM-CPNO-K1.
           MOVE "01"     TO CM-LFCD-K1.
           READ CM-FILE
               INTO CM-RCRD-WS
               KEY IS CM-KEY-1.
           IF RECORD-LOCKED
               MOVE "JCMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X800-GET-DTNO.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X810-SET-VALUES.
           MOVE CM-SC-NATN TO WS-DTNO-SV.
           ADD 1 TO CM-SC-NATN.
           IF CM-SC-NATN = ZERO
               MOVE 1 TO CM-SC-NATN.
       X820-REWRITE-CM.
           REWRITE CM-RCRD FROM CM-RCRD-WS.
           IF NOT FILE-OK
               MOVE "RW" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X830-REVIEW-MAIN.
           IF MAIN-77 NOT = "FT"
               GO TO X890-EXIT.
       X835-ADJUSTMENT-EPNO.
           MOVE CM-SC-AENO TO WS-AENO.
       X840-REVIEW-DATES.
           IF CM-SC-FYOS = 00
               MOVE CM-SC-CRYR TO WS-UVEDPR-CRYR
               MOVE CM-SC-CRMO TO WS-UVEDPR-CRMO
               GO TO X850.
           ADD CM-SC-CRMO CM-SC-FYOS GIVING WS-UVEDPR-CRMO.
           MOVE CM-SC-CRYR TO WS-UVEDPR-CRYR.
           IF WS-UVEDPR-CRMO > 12
               SUBTRACT 12 FROM WS-UVEDPR-CRMO
               GO TO X850.
           SUBTRACT 01 FROM WS-UVEDPR-CRYR.
       X850.
           MOVE SY-ST-SODT TO WS-UVEDPR-SODT.
           IF WS-UVEDPR-SOYM NOT > WS-UVEDPR-CRYM
               MOVE ZEROS TO WS-UVEDPR-MNDT
               GO TO X890-EXIT.
           MOVE WS-UVEDPR-CRYR TO WS-UVEDPR-YEAR.
           MOVE WS-UVEDPR-CRMO TO WS-UVEDPR-MONTH.
           MOVE 01 TO WS-UVEDPR-DAY.
           IF WS-UVEDPR-MONTH = 12
               MOVE 00 TO WS-UVEDPR-MONTH
               ADD 01 TO WS-UVEDPR-YEAR.
           ADD 01 TO WS-UVEDPR-MONTH.
       X890-EXIT.
           EXIT.
      
      
       X900-CLEAR.
           DISPLAY SPACES LINE 19 POSITION 27 SIZE 10
                   SPACES LINE 18 POSITION 35 SIZE 1
                   SPACES LINE 17 POSITION 31 SIZE 6
                   SPACES LINE 16 POSITION 31 SIZE 6
                   SPACES LINE 15 POSITION 27 SIZE 50
                   SPACES LINE 14 POSITION 27 SIZE 50
                   SPACES LINE 13 POSITION 27 SIZE 50
                   SPACES LINE 12 POSITION 27 SIZE 50
                   SPACES LINE 11 POSITION 27 SIZE 50
                   SPACES LINE 10 POSITION 35 SIZE 1
                   SPACES LINE 09 POSITION 28 SIZE 8
                   SPACES LINE 08 POSITION 31 SIZE 5.
       X990-EXIT.
           EXIT.
      

       X999-UVdtcv.
           CALL "UVDTCV" USING
               LS-UVDTCV-RCRD.
       X999-EXIT.
           EXIT.
      
       Y000-CLOSE.
           CLOSE IR-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE EP-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE WC-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE DP-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE OP-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE CM-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE CT-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
       Y990-EXIT.
           EXIT.
      
           COPY "uv/cpy/uvdfemov".
           COPY "uv/cpy/uvaflk".
