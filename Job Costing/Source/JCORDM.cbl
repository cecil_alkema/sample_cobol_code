       IDENTIFICATION DIVISION.
       PROGRAM-ID.                 JCORDM.
      *
      *     JOB COSTING
      *     ORDER ENTRY and MAINTENANCE
      *
      *     Estimate entry/maintenance programs.
      *     jcedte - hacked from jemt41
      *     jced05 - hacked from jemt61
      *     jced06 - streamlined version of jced05
      *     jced10 - hacked from jemt71
      *     jced11 - streamlined version of jced10
      *     jced15 - hacked from jemt81
      *
      *     SWITCHES
      *     ********
      *
      *     Note : sw4 on or sw5 on ... but not both
      *
      *     SWITCH-1 - ALLOW MAINTENANCE
      *     SWITCH-2 - IGNORE CREDIT CHECK
      *     SWITCH-3 - Monroe Custom
      *     SWITCH-4 - est trans dtl entry - simple mat and res
      *     SWITCH-5 - PMIS job estimating implemented
      *     SWITCH-6 - print sales order without asking first
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           SWITCH-1
               ON  IS SW-1-ON
               OFF IS SW-1-OFF
           SWITCH-2
               ON  IS SW-2-ON
               OFF IS SW-2-OFF
           SWITCH-3
               ON  IS SW-3-ON
               OFF IS SW-3-OFF
           SWITCH-4
               ON  IS SW-4-ON
               OFF IS SW-4-OFF
           SWITCH-5
               ON  IS SW-5-ON
               OFF IS SW-5-OFF
           SWITCH-6
               ON  IS SW-6-ON
               OFF IS SW-6-OFF.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           COPY "uv/cpy/slsymt".
           COPY "jc/cpy/sldkip".
           COPY "je/cpy/slethd".
           COPY "ar/cpy/slctmt".
           COPY "sa/cpy/slsmmt".
           COPY "sm/cpy/slstmt".
           COPY "jc/cpy/sljcmt".
           COPY "jc/cpy/slwcmt".
           COPY "jc/cpy/slcdrf".
           COPY "ar/cpy/slarid".
           COPY "uv/cpy/slprnt".
      
       DATA DIVISION.
       FILE SECTION.
           COPY "uv/cpy/fdsymt".
           COPY "jc/cpy/fddkip".
           COPY "je/cpy/fdethd".
           COPY "ar/cpy/fdctmt".
           COPY "sa/cpy/fdsmmt".
           COPY "sm/cpy/fdstmt".
           COPY "jc/cpy/fdjcmt".
           COPY "jc/cpy/fdwcmt".
           COPY "jc/cpy/fdcdrf".
           COPY "ar/cpy/fdarid".
           COPY "uv/cpy/fdprnt80".
      
       WORKING-STORAGE SECTION.
      
       77  MP-STATUS-77            PIC XX.
       77  SY-STATUS-77            PIC XX.
       77  DP-STATUS-77            PIC XX.
       77  EH-STATUS-77            PIC XX.
       77  CT-STATUS-77            PIC XX.
       77  SM-STATUS-77            PIC XX.
       77  MM-STATUS-77            PIC XX.
       77  WC-STATUS-77            PIC XX.
       77  CM-STATUS-77            PIC XX.
       77  CD-STATUS-77            PIC XX.
       77  ID-STATUS-77            PIC XX.
       77  PR-STATUS-77            PIC XX.
       77  SCREEN-ID               PIC X(35)   VALUE
           "$PMISDIR/appl/obj/c/dsplscrn jcordm".
       77  DUMMY-77                PIC X.
      
           COPY "uv/cpy/uvfckv".
           COPY "uv/cpy/uvpgid".
           COPY "uv/cpy/uvsfemov".
           COPY "uv/cpy/uvlkmg".
      
           COPY "uv/cpy/rcsymtcp".
           COPY "uv/cpy/rcsymtst".
           COPY "uv/cpy/rcsymtcc".
           COPY "jc/cpy/rcdkip".
           COPY "je/cpy/rcethd".
           COPY "je/cpy/rcethd01".
           COPY "je/cpy/rcethd02".
           COPY "je/cpy/rcethd03".
           COPY "ar/cpy/rcctmt".
           COPY "sa/cpy/rcsmmt".
           COPY "sm/cpy/rcstmt61".
           COPY "jc/cpy/rcjcmt".
           COPY "jc/cpy/rcjcmt01".
           COPY "jc/cpy/rcwcmt".
           COPY "jc/cpy/rccdrf".
           COPY "ar/cpy/rcarid".

           COPY "jc/cpy/lsjcordm".
      
           COPY "uv/cpy/lsuvcusf".
           COPY "uv/cpy/lsuvdssh".
           COPY "uv/cpy/lsuvgcno".
           COPY "uv/cpy/lsuvgsno".
           COPY "uv/cpy/lsuvfdno".
           COPY "uv/cpy/lsuvacdt".
           COPY "uv/cpy/lsuvdtcv".
           COPY "uv/cpy/lsuvgpcl".
      
       01  CONTROL-FIELDS.
           03  WS-seg-chg-sw       PIC X.
           03  WS-qty-chg-sw       PIC X.
           03  ws-mainline-control PIC XX.
           03  ws-est-entered-sw   PIC X.
           03  ws-cl-exceeded-ack  pic x(12).
           03  WS-ERSW             PIC X.
           03  WS-CHSW             PIC X.
           03  WS-CRSW             PIC X.
           03  ws-skip-sec-wc      PIC X.
           03  WS-FIELD            PIC 99.
           03  WS-CSCD             PIC 9.
           03  WS-est-linked-sw    PIC X.
      
       01  STORAGE-FIELDS.
           03  ws-job-no           PIC 9(6).
           03  WS-ETNO             PIC 9(4).
           03  WS-QTCD             PIC 9.
           03  WS-CTNO             PIC 9(5).
           03  WS-SMNO             PIC 9(5).
           03  WS-PCCD             PIC X(4).
           03  WS-ACNO             PIC X(10).
           03  WS-QTPR             PIC S9(7)V99.
           03  WS-etmg             PIC S9(7)V99.
           03  WS-CCAM             PIC S9(7)V99    COMP-3.
           03  WS-QNTY             PIC S9(9).
           03  WS-DKDC             PIC X(30).
           03  WS-PRCT             PIC S99V9.
           03  WS-FLSW             PIC X.
           03  WS-PVDK             PIC 9(6).
           03  WS-CPON             PIC X(8).
           03  WS-RFNO             PIC X(10).
           03  WS-CTCT             PIC X(20).
           03  WS-JBTP             PIC XX.
               88  VALID-JOB-TYPE
                   VALUES ARE "NW", "RR", "RC", "XX".
           03  WS-WCNO             PIC 999.
           03  WS-PWNO             PIC 999.
           03  WS-SWNO             PIC 999.
           03  WS-DATE             PIC 9(8).
           03  WS-IVNO             PIC 9(6).
           03  WS-PBAM             PIC S9(7)V99.
      
       01  DISPLAY-FIELDS.
           03  DS-DKNO             PIC ZZZZZ9.
           03  DS-ETNO             PIC 9999.
           03  DS-QNTY             PIC ZZZ,ZZZ,ZZ9-.
           03  DS-AMNT             PIC Z,ZZZ,ZZZ.99-.
           03  DS-PRCT             PIC ZZ.9-.
           03  DS-DATE             PIC 99/99/99.
           03  DS-IVNO             PIC ZZZZZ9.
      
       01  SAVE-FIELDS.
           03  WS-SV-ETNO          PIC 9(4).
           03  WS-SV-QTCD          PIC 9.
           03  WS-SV-CTNO          PIC 9(5).
           03  WS-SV-SMNO          PIC 9(5).
           03  WS-SV-PCCD          PIC X(4).
           03  WS-SV-ETQT          PIC 9(9).
           03  WS-SV-ETCT          PIC 9(7)V99.
           03  WS-SV-QTPR          PIC 9(7)V99.
           03  WS-SV-MUPC          PIC 99V9.
           03  WS-SV-DCPC          PIC 99V9.
           03  WS-SV-PVDK          PIC 9(6).
           03  WS-SV-ETDC          PIC X(30).
      
       01  EDIT-FIELDS.
           03  ED-AMNT             PIC ZZZZZZZ.99-.
           03  ED-QNTY             PIC ZZZZZZZZ9-.
      *
      *  The following working storage is required to facilitate
      *  sales order print.
      *
       01  pr-blank.      
           03  filler              pic xx    value " |".
           03  filler              pic x(75) value spaces.
           03  filler              pic xxx   value "|  ".
 
       01  pr-dash.
           03  filler              pic xx    value " |".
           03  filler              pic x(75) value all "-".
           03  filler              pic xxx   value "|  ".
 
       01  pr-dash2.
           03  filler              pic xx    value spaces.
           03  filler              pic x(75) value all "-".
           03  filler              pic xxx   value spaces. 
 
       01  pr-line1.
           03  filler              pic x(34) value spaces.
           03  filler              pic x(11) value "SALES ORDER".
           03  filler              pic x(35) value spaces.

       01  pr-line2.
           03  filler              pic xx    value " |".
           03  filler              pic x(06) value " Job# ".
           03  pr-jbno             pic 999999.
           03  filler              pic x(25) value spaces.
           03  filler              pic x(05) value "Est# ".
           03  pr-etno             pic 9999.
           03  filler              pic x(29) value spaces.
           03  filler              pic x(03) value "|  ".

       01  pr-line3.
           03  filler              pic xx    value " |".
           03  filler              pic x(11) value
               " Customer  ".
           03  filler              pic x(02) value spaces.
           03  pr-ctno             pic 9(05).
           03  filler              pic xx    value spaces.
           03  pr-ctnm             pic x(35).
           03  filler              pic x(20) value spaces.
           03  filler              pic x(03) value "|  ".

       01  pr-line4.
           03  filler              pic xx    value " |".
           03  filler              pic x(11) value
               " Salesman  ".
           03  filler              pic x(02) value spaces.
           03  pr-smno             pic 9(05).
           03  filler              pic xx    value spaces.
           03  pr-smnm             pic x(35).
           03  filler              pic x(20) value spaces.
           03  filler              pic x(03) value "|  ".

       01  pr-line5.
           03  filler              pic xx    value " |".
           03  filler              pic x(11) value
               " Prod Class".
           03  filler              pic x(02) value spaces.
           03  pr-pdcl             pic x(04).
           03  filler              pic xxx   value spaces.
           03  pr-pddc             pic x(25).
           03  filler              pic x(6)  value spaces.
           03  filler              pic x(12) value
               "Quantity    ".
           03  pr-qnty             pic zzz,zzz,zz9-.
           03  filler              pic x(03) value "|  ".

       01  pr-line6.
           03  filler              pic xx    value " |".
           03  filler              pic x(13) value
               " Quoted Price".
           03  pr-qtpr             pic z,zzz,zzz.99-.
           03  filler              pic x(11) value
               " Est'd Cost".
           03  pr-etct             pic zzzz,zzz.99-.
           03  filler              pic x(14) value
               "  Est'd Margin".
           03  pr-etmg             pic zzzz,zzz.99-.
           03  filler              pic x(03) value "|  ".

       01  pr-line7.
           03  filler              pic xx    value " |".
           03  filler              pic x(16) value
               " Description    ".
           03  filler              pic x(02) value spaces.
           03  pr-desc             pic x(30).          
           03  filler              pic x(27) value spaces.
           03  filler              pic x(03) value "|  ".

       01  pr-line8.
           03  filler              pic xx    value " |".
           03  filler              pic x(16) value
               " Desc2/B&L Part#".
           03  filler              pic x(02) value spaces.
           03  pr-desc2            pic x(30).          
           03  filler              pic x(27) value spaces.
           03  filler              pic x(03) value "|  ".

       01  pr-line9.
           03  filler              pic xx    value " |".
           03  filler              pic x(16) value
               " Purchase Order ".
           03  filler              pic x(02) value spaces.
           03  pr-pono             pic x(08).          
           03  filler              pic x(11) value spaces.
           03  lit-ref-sb          pic x(10) value spaces.
           03  pr-stby             pic x(10).
           03  filler              pic x(18) value spaces.
           03  filler              pic x(03) value "|  ".

       01  pr-line10.
           03  filler              pic xx    value " |".
           03  filler              pic x(08) value
               " Contact".        
           03  filler              pic x(02) value spaces.
           03  pr-cntc             pic x(20).          
           03  filler              pic x(07) value spaces.
           03  filler              pic x(10) value
               "Job Type  ".
           03  pr-jbtp             pic xx.
           03  filler              pic x(26) value spaces.
           03  filler              pic x(03) value "|  ".

       01  pr-line11.
           03  filler              pic xx    value " |".
           03  filler              pic x(16) value
               " Primary WC     ".
           03  pr-wcno             pic 999.
           03  filler              pic x(02) value spaces.
           03  pr-wcdc             pic x(25).          
           03  filler              pic x(29) value spaces.
           03  filler              pic x(03) value "|  ".

       01  pr-line12.
           03  filler              pic xx    value " |".
           03  filler              pic x(16) value
               " Secondary WC   ".
           03  pr-wcno2            pic 999.
           03  filler              pic x(02) value spaces.
           03  pr-wcdc2            pic x(25).          
           03  filler              pic x(29) value spaces.
           03  filler              pic x(03) value "|  ".

       01  pr-line13.
           03  filler              pic xx    value " |".
           03  filler              pic x(11) value
               " Start Date".     
           03  filler              pic x(02) value spaces.
           03  pr-stdt             pic x(08).          
           03  filler              pic x(16) value spaces.
           03  filler              pic x(13) value
               "Target Date  ".
           03  pr-tgdt             pic x(08).
           03  filler              pic x(17) value spaces.
           03  filler              pic x(03) value "|  ".
 
       01  pr-line14.
           03  filler              pic xx    value " |".
           03  filler              pic x(21) value
               " Invoice Description ".
           03  filler              pic x(35) value spaces.
           03  filler              pic x(06) value
               "Amount".
           03  filler              pic x(13) value spaces.
           03  filler              pic x(03) value "|  ".
       
       01  pr-underscore.
           03  filler              pic xx    value " |".
           03  filler              pic x(75)  value all "_".
           03  filler              pic x(03) value "|  ".
 
       01  ws-misc.
           03  ws-stdt             pic 9(08).
           03  ws-stdt-r redefines ws-stdt.
               05  ws-stdt-yy      pic 9999.
               05  ws-stdt-mm      pic 99.
               05  ws-stdt-dd      pic 99.
           03  ws-tgdt             pic 9(08).
           03  ws-tgdt-r redefines ws-tgdt.
               05  ws-tgdt-yy      pic 9999.
               05  ws-tgdt-mm      pic 99.
               05  ws-tgdt-dd      pic 99.
           03  ws-ds-date.
               05  ws-date-yy      pic 99.
               05  filler          pic x  value "/".
               05  ws-date-mm      pic 99.
               05  filler          pic x  value "/".
               05  ws-date-dd      pic 99.
      *
      * This ends working storage requirements for sales
      * order print.
      *
       PROCEDURE DIVISION.
      
       DECLARATIVES.
           COPY "uv/cpy/dcsymt".
           COPY "jc/cpy/dcdkip".
           COPY "je/cpy/dcethd".
           COPY "ar/cpy/dcctmt".
           COPY "sa/cpy/dcsmmt".
           COPY "sm/cpy/dcstmt".
           COPY "jc/cpy/dcjcmt".
           COPY "jc/cpy/dcwcmt".
           COPY "jc/cpy/dccdrf".
           COPY "ar/cpy/dcarid".
           COPY "uv/cpy/dcprnt".
       END DECLARATIVES.
      
       A000-MAINLINE-PGM SECTION.
      
       A000-START-PROGRAM.
           PERFORM B000-INIT THRU B990-EXIT.
       A100-MAINLINE.
           perform C000-add-chg thru c990-exit.
           IF ws-mainline-control = "QT"
               GO TO A800-CLOSE.
           IF ws-mainline-control = "AD"
               GO TO A300-ADD-MODE.
           IF ws-mainline-control = "CH"
               GO TO A600-CHANGE-MODE.
           MOVE "A1" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
       A300-ADD-MODE.
           PERFORM F000-ADD-MODE THRU F990-EXIT.
           GO TO A100-MAINLINE.
       A600-CHANGE-MODE.
           PERFORM D000-JOB-NUMBER THRU D990-EXIT.
           IF ws-mainline-control = "QT"
               GO TO A100-mainline.
           PERFORM H000-CHANGE-MODE THRU H990-EXIT.
           GO TO A100-MAINLINE.
       A800-CLOSE.
           PERFORM Y000-CLOSE THRU Y990-EXIT.
       A990-EXIT.
           STOP RUN.
      
       B000-INIT.
           MOVE "JC"     TO WS-STNM.
           MOVE "ORDM"   TO WS-PGNM.
           MOVE "JCORDM" TO PROGRAM-NAME.
           COPY "uv/cpy/uvcusf".
           CALL "SYSTEM" USING SCREEN-ID.
           COPY "uv/cpy/uvdssh".
       B100-OPEN-FILS.
           OPEN INPUT SY-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN I-O   DP-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN I-O   EH-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN INPUT CT-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN INPUT SM-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN INPUT MM-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN i-o cm-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN INPUT WC-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN I-O   CD-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN INPUT ID-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN output PR-FILE no rewind.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           IF SW-1-ON
               DISPLAY "SWITCH-1"
                   LINE 22 POSITION 3.
       B400-READ-SY.
           MOVE SPACES         TO SY-KEY-1 SY-CC-KEY-1-WS.
           MOVE SY-ST-CPNO     TO SY-CC-CPNO-1.
           MOVE 21             TO SY-CC-LFCD-1.
           MOVE SY-CC-KEY-1-WS TO SY-KEY-1.
           READ SY-FILE
               INTO SY-CC-RCRD-WS
               KEY IS SY-KEY-1.
           IF RECORD-LOCKED
               MOVE "SYMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO B400-READ-SY.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       B500-SET-VALUES.
           MOVE ZERO TO WS-CSCD.
           move "N" to ws-est-linked-sw.
           move "N" to ws-est-entered-sw.
       B990-EXIT.
           EXIT.

       C000-add-chg.
           PERFORM X900-CLEAR-SCREEN    THRU X905-EXIT.
       C200-ADD-OR-CHANGE.
           display "[F2]-ESCAPE" 
               line 24 position 03 high.
           DISPLAY "ADD A NEW JOB OR CHANGE AN EXISTING ONE? (A/C) "
               LINE 07 POSITION 08 HIGH.
           ACCEPT dummy-77
               LINE 0 POSITION 0
               PROMPT TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO C290-CHECK-FUNC.
           display spaces line 24 position 01 size 80.
           if dummy-77 = "A"
               move "AD" to ws-mainline-control
               go to C990-exit.
           if dummy-77 = "C"
               move "CH" to ws-mainline-control
               go to C990-exit.
           GO TO C200-ADD-OR-CHANGE.
       C290-CHECK-FUNC.
           display spaces line 24 position 01 size 80.
           IF FUNC-02
               move "QT" to ws-mainline-control
               DISPLAY SPACES LINE 07 POSITION 02 SIZE 78
               GO TO C990-EXIT.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO C200-ADD-OR-CHANGE.
       C990-exit.
           exit.
      
       D000-JOB-NUMBER.
           IF WS-CSCD = 1
               PERFORM X920-CLEAR-SCREEN-1 THRU X925-EXIT
               GO TO D100-ACCEPT-job.
           PERFORM X900-CLEAR-SCREEN    THRU X905-EXIT.
           PERFORM X910-display-screen-1-lit THRU X915-EXIT.
           DISPLAY "JOB NO" LINE 07 POSITION 11 HIGH.
       D100-ACCEPT-job.
           display "[F2]-ESCAPE" 
               line 24 position 03 high.
           ACCEPT ws-job-no
               LINE 07 POSITION 35
               PROMPT TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO D190-CHECK-FUNC.
           display spaces line 24 position 01 size 80.
       D120-DISPLY.
           MOVE ws-job-no TO DS-DKNO.
           DISPLAY DS-DKNO LINE 07 POSITION 35.
       D110-EDIT.
           IF ws-job-no NOT NUMERIC
               DISPLAY "JOB NUMBER MUST BE NUMERIC "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO D100-ACCEPT-job.
           IF ws-job-no = ZERO
               DISPLAY "JOB NUMBER MUST BE BETWEEN 000001 AND 999999 "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO D100-ACCEPT-job.
           GO TO D200-READ-DP.
       D190-CHECK-FUNC.
           display spaces line 24 position 01 size 80.
           IF FUNC-02
               DISPLAY SPACES LINE 07 POSITION 35 SIZE 6
               MOVE "QT" TO ws-mainline-control
               GO TO D990-EXIT.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO D100-ACCEPT-job.
       D200-READ-DP.
           MOVE SPACES     TO DP-KEY-1.
           MOVE SY-ST-CPNO TO DP-CPNO-K1.
           MOVE ws-job-no    TO DP-DKNO-K1.
           READ DP-FILE
               INTO DP-RCRD-WS
               KEY IS DP-KEY-1
             INVALID KEY
               display "JOB NOT ON FILE "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO D100-ACCEPT-job.
           IF RECORD-LOCKED
               MOVE "DKIP" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO D200-READ-DP.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
           move ws-job-no to pr-jbno.
       D990-EXIT.
           EXIT.
      
       F000-ADD-MODE.
           MOVE SPACES     TO DP-RCRD DP-RCRD-WS.
           MOVE SY-ST-CPNO TO DP-CPNO-1.
           MOVE ZEROS      TO DP-ETNO DP-QTCD
                              DP-CTNO DP-SMNO
                              DP-QTPR DP-QNTY
                              DP-MUPC DP-DCPC
                              DP-PVDK
                              DP-PWNO DP-SWNO
                              DP-STDT DP-TGDT DP-CLDT DP-SHDT
                              DP-IVNO
                              DP-PBAM.
           MOVE 1          TO DP-STCD.
           MOVE ZEROS TO  WS-SV-ETNO.
           MOVE ZEROS TO  WS-SV-QTCD.
           MOVE ZEROS TO  WS-SV-CTNO.
           MOVE SPACES TO  WS-SV-PCCD.
           MOVE ZEROS TO  WS-SV-ETQT.
           MOVE ZEROS TO  WS-SV-ETCT.
           MOVE ZEROS TO  WS-SV-QTPR.
           MOVE ZEROS TO  WS-SV-MUPC.
           MOVE ZEROS TO  WS-SV-DCPC.
           MOVE ZEROS TO  WS-SV-PVDK.
           MOVE SPACES TO  WS-SV-ETDC.

           PERFORM X900-CLEAR-SCREEN    THRU X905-EXIT.
           PERFORM X910-display-screen-1-lit THRU X915-EXIT.

           if sw-5-off
               go to F028-estimate-no.

       F010-ESTIMATE-NO.
           move "N" to ws-est-linked-sw.
           DISPLAY SPACES LINE 09 POSITION 43 SIZE 30.
           ACCEPT WS-ETNO
               LINE 09 POSITION 37
               PROMPT TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO F015-CHECK-FUNC.
           MOVE WS-ETNO TO DS-ETNO.
           move ws-etno to pr-etno.
           DISPLAY DS-ETNO LINE 09 POSITION 37.
           PERFORM X300-EDIT-ESTIMATE THRU X305-EXIT
           IF WS-ERSW = "Y"
               GO TO F010-ESTIMATE-NO.
           MOVE WS-ETNO TO DP-ETNO.
           DISPLAY EH-ETDC LINE 09 POSITION 43.
           IF WS-ETNO = ZERO
               move "N" to ws-est-linked-sw
               MOVE ZERO TO DP-QTCD
               DISPLAY DP-QTCD LINE 10 POSITION 40
               GO TO F030-CUSTOMER.
           move "Y" to ws-est-linked-sw.
           MOVE EH-CTNO-2 TO WS-SV-CTNO.
           MOVE EH-SMNO-2 TO WS-SV-SMNO.
           MOVE EH-PCCD   TO WS-SV-PCCD.
           MOVE EH-MUPC   TO WS-SV-MUPC.
           MOVE EH-DCPC   TO WS-SV-DCPC.
           MOVE EH-PVDK   TO WS-SV-PVDK.
           MOVE EH-ETDC   TO WS-SV-ETDC.
           GO TO F020-QUANTITY-CODE.
       F015-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 09 POSITION 37 SIZE 4
               GO TO F990-EXIT.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F010-ESTIMATE-NO.
       
       F020-QUANTITY-CODE.
           DISPLAY SPACES LINE 10 POSITION 43 SIZE 12.
           ACCEPT WS-QTCD
               LINE 10 POSITION 40
               PROMPT TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO F025-CHECK-FUNC.
           DISPLAY WS-QTCD LINE 10 POSITION 40.
           PERFORM X310-EDIT-QUANTITY-CODE THRU X315-EXIT.
           IF WS-ERSW = "Y"
               GO TO F020-QUANTITY-CODE.
           MOVE WS-QTCD TO DP-QTCD.
           MOVE EH-QTPR TO WS-SV-QTPR.
           MOVE EH-ETCT TO WS-SV-ETCT.
           MOVE EH-ETQT TO WS-SV-ETQT DS-QNTY.
           DISPLAY DS-QNTY LINE 10 POSITION 43.
           GO TO F030-CUSTOMER.
       F025-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 10 POSITION 40 SIZE 1
               GO TO F010-ESTIMATE-NO.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F020-QUANTITY-CODE.

       F028-ESTIMATE-NO.
           display spaces line 09 position 37 size 40.
           move zeros to ds-etno.
           DISPLAY DS-ETNO LINE 09 POSITION 37.
       F028-EST-QUANTITY-CODE.
           DISPLAY SPACES LINE 10 POSITION 40 SIZE 39.
           move zeros to ws-qtcd.
           DISPLAY WS-QTCD LINE 10 POSITION 40.

       F030-CUSTOMER.
           DISPLAY SPACES LINE 11 POSITION 43 SIZE 35.
           IF ws-est-linked-sw = "Y"
             AND WS-SV-CTNO NOT = ZERO
               MOVE "NP"       TO LS-UVGCNO-CMCD
               MOVE WS-SV-CTNO TO LS-UVGCNO-CTNO
             ELSE
               MOVE "PR"  TO LS-UVGCNO-CMCD
               MOVE ZEROS TO LS-UVGCNO-CTNO.

           PERFORM X200-UVGCNO THRU X205-EXIT.

           IF LS-UVGCNO-CMCD = "02" and sw-5-off
               DISPLAY SPACES LINE 11 POSITION 36 SIZE 40
               GO TO F990-exit.

           IF LS-UVGCNO-CMCD = "02" and sw-5-on
             AND DP-ETNO = ZERO
               DISPLAY SPACES LINE 10 POSITION 40 SIZE 1
               GO TO F010-ESTIMATE-NO.
           IF LS-UVGCNO-CMCD = "02" and sw-5-on
               GO TO F020-QUANTITY-CODE.

           MOVE LS-UVGCNO-CTNO TO WS-CTNO DP-CTNO.
           PERFORM X610-READ-CT THRU X615-EXIT.
           IF WS-ERSW = "Y"
               MOVE "21" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
           move ws-ctno to pr-ctno.
           DISPLAY CT-CTNM LINE 11 POSITION 43.
           move ct-ctnm to pr-ctnm.
       F040-SALESMAN.
           DISPLAY SPACES LINE 12 POSITION 43 SIZE 35.
           IF ws-est-linked-sw = "Y"
             AND WS-SV-SMNO NOT = ZERO
               MOVE "NP"       TO LS-UVGSNO-CMCD
               MOVE WS-SV-SMNO TO LS-UVGSNO-SMNO
             ELSE
               MOVE "NP"    TO LS-UVGSNO-CMCD
               MOVE CT-SMNO TO LS-UVGSNO-SMNO.
           PERFORM X210-UVGSNO THRU X215-EXIT.
           IF LS-UVGSNO-CMCD = "02"
               GO TO F030-CUSTOMER.
           MOVE LS-UVGSNO-SMNO TO WS-SMNO DP-SMNO.
           PERFORM X620-READ-SM THRU X625-EXIT.
           IF WS-ERSW = "Y"
               MOVE "22" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
           DISPLAY SM-SMNM LINE 12 POSITION 43.
           move ws-smno to pr-smno.
           move sm-smnm to pr-smnm.
       F050-product-class.
           DISPLAY SPACES LINE 13 POSITION 43 SIZE 25.
           IF ws-est-linked-sw = "Y"
               DISPLAY WS-SV-PCCD LINE 13 POSITION 37
               GO TO F050-ACCEPT-NO-PRMP.
       F050-ACCEPT-PRMP.
           move spaces         to ls-uvgpcl-rcrd.
           move "PR"           to ls-uvgpcl-cmcd.
           move sy-st-cpno     to ls-uvgpcl-cpno.
           move 13             to ls-uvgpcl-line.
           move 37             to ls-uvgpcl-colm.
           move "Y"            to ls-uvgpcl-insw.
           call "UVGPCL" using ls-uvgpcl-rcrd.
           if ls-uvgpcl-cmcd = "02"
               DISPLAY SPACES LINE 13 POSITION 37 SIZE 4
               GO TO F040-SALESMAN.
           move ls-uvgpcl-pccd to ws-pccd.
           GO TO F050-EDIT-PCCD.
       F050-ACCEPT-NO-PRMP.
           MOVE WS-SV-PCCD     TO WS-PCCD.
           move spaces         to ls-uvgpcl-rcrd.
           move "NP"           to ls-uvgpcl-cmcd.
           move sy-st-cpno     to ls-uvgpcl-cpno.
           move 13             to ls-uvgpcl-line.
           move 37             to ls-uvgpcl-colm.
           move "Y"            to ls-uvgpcl-insw.
           move ws-pccd        to ls-uvgpcl-pccd.
           call "UVGPCL" using ls-uvgpcl-rcrd.
           if ls-uvgpcl-cmcd = "02"
               DISPLAY SPACES LINE 13 POSITION 37 SIZE 4
               GO TO F040-SALESMAN.
           move ls-uvgpcl-pccd to ws-pccd.
       F050-edit-pccd.
           DISPLAY WS-PCCD    LINE 13 POSITION 37.
           PERFORM X320-EDIT-PRODUCT-CLASS THRU X325-EXIT.
           IF WS-ERSW = "Y"
               GO TO F050-PRODUCT-CLASS.
           MOVE WS-PCCD TO DP-PDTP.
           move ws-pccd to pr-pdcl.
           move MM-61-DESC to pr-pddc.
           DISPLAY MM-61-DESC LINE 13 POSITION 43.
           GO TO F060-QUOTED-PRICE.
       F055-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 13 POSITION 37 SIZE 4
               GO TO F040-SALESMAN.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F050-PRODUCT-CLASS.
       
       F060-QUOTED-PRICE.
           DISPLAY SPACES LINE 14 POSITION 29 SIZE 13.
           IF ws-est-linked-sw = "Y"
               GO TO F060-ACCEPT-NO-PRMP.
       F060-ACCEPT-PRMP.
           ACCEPT WS-QTPR
               LINE 14 POSITION 31 SIZE 11
               PROMPT TAB CONVERT
             ON EXCEPTION
               FUNC-KEY
               GO TO F065-CHECK-FUNC.
           GO TO F060-DISPLaY.
       F060-ACCEPT-NO-PRMP.
           MOVE WS-SV-QTPR TO ED-AMNT.
           DISPLAY ED-AMNT LINE 14 POSITION 31.
           MOVE WS-SV-QTPR TO WS-QTPR.
           ACCEPT WS-QTPR
               LINE 14 POSITION 31 SIZE 11
               TAB CONVERT UPDATE
             ON EXCEPTION
               FUNC-KEY
               GO TO F065-CHECK-FUNC.
       F060-display.
           MOVE WS-QTPR TO DP-QTPR DS-AMNT.
           MOVE WS-QTPR TO pr-qtpr.
           DISPLAY DS-AMNT LINE 14 POSITION 29.
           PERFORM X330-EDIT-QUOTED-PRICE THRU X335-EXIT.
           IF WS-ERSW = "Y"
               GO TO F060-QUOTED-PRICE.
       F062-CREDIT-CHECK.
           IF SW-2-ON
               GO TO F070-QUANTITY.
           IF CT-CDLM = ZERO
               GO TO F070-QUANTITY.
           PERFORM K000-CREDIT-CHECK THRU K990-EXIT.
           IF WS-ERSW = "Y"
               GO TO F060-QUOTED-PRICE.
           GO TO F070-QUANTITY.
       F065-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 14 POSITION 31 SIZE 11
               GO TO F050-PRODUCT-CLASS.
           IF FUNC-98
             OR FUNC-57
               GO TO F060-display.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F060-QUOTED-PRICE.
       
       F070-QUANTITY.
           DISPLAY SPACES LINE 15 POSITION 30 SIZE 12.
           IF ws-est-linked-sw = "Y"
               GO TO F070-ACCEPT-NO-PRMP.
       F070-ACCEPT-PRMP.
           ACCEPT WS-QNTY
               LINE 15 POSITION 32 SIZE 10
               PROMPT TAB CONVERT
             ON EXCEPTION
               FUNC-KEY
               GO TO F075-CHECK-FUNC.
           GO TO F070-DISPLY.
       F070-ACCEPT-NO-PRMP.
           MOVE WS-SV-ETQT TO ED-QNTY.
           DISPLAY ED-QNTY LINE 15 POSITION 32.
           MOVE WS-SV-ETQT TO WS-QNTY.
           ACCEPT WS-QNTY
               LINE 15 POSITION 32 SIZE 10
               TAB CONVERT UPDATE
             ON EXCEPTION
               FUNC-KEY
               GO TO F075-CHECK-FUNC.
       F070-DISPLY.
           MOVE WS-QNTY TO DS-QNTY DP-QNTY.
           MOVE WS-QNTY TO pr-qnty.
           DISPLAY DS-QNTY LINE 15 POSITION 30.
           PERFORM X340-EDIT-QUANTITY THRU X345-EXIT.
           IF WS-ERSW = "Y"
               GO TO F070-QUANTITY.
           GO TO F080-DESCRIPTION-1.
       F075-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 15 POSITION 32 SIZE 10
               GO TO F060-QUOTED-PRICE.
           IF FUNC-98
             OR FUNC-57
               GO TO F070-DISPLY.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F070-QUANTITY.
       
       F080-DESCRIPTION-1.
           IF ws-est-linked-sw = "Y"
               DISPLAY WS-SV-ETDC LINE 16 POSITION 29
               GO TO F080-ACCEPT-NO-PRMP.
       F080-ACCEPT-PRMP.
           ACCEPT WS-DKDC
               LINE 16 POSITION 29
               PROMPT TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO F085-CHECK-FUNC.
           GO TO F080-EDIT-DKDC.
       F080-ACCEPT-NO-PRMP.
           MOVE WS-SV-ETDC TO WS-DKDC.
           ACCEPT WS-DKDC
               LINE 16 POSITION 29
               TAB ECHO UPDATE
             ON EXCEPTION
               FUNC-KEY
               GO TO F085-CHECK-FUNC.
       F080-EDIT-DKDC.
           DISPLAY WS-DKDC LINE 16 POSITION 29.
           PERFORM X350-EDIT-DESCRIPTION-1 THRU X355-EXIT.
           IF WS-ERSW = "Y"
               GO TO F080-DESCRIPTION-1.
           MOVE WS-DKDC TO DP-DKD1.
           MOVE WS-DKDC TO pr-desc.
           GO TO F090-DESCRIPTION-2.
       F085-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 16 POSITION 29 SIZE 30
               GO TO F070-QUANTITY.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F080-DESCRIPTION-1.
       
       F090-DESCRIPTION-2.
           ACCEPT WS-DKDC
               LINE 17 POSITION 29
               PROMPT TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO F095-CHECK-FUNC.
           MOVE WS-DKDC TO DP-DKD2.
           DISPLAY WS-DKDC LINE 17 POSITION 29.
           MOVE WS-DKDC TO pr-desc2.
           GO TO F100-MARK-UP.
       F095-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 17 POSITION 29 SIZE 30
               GO TO F080-DESCRIPTION-1.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F090-DESCRIPTION-2.

       F100-MARK-UP.
           IF ws-est-linked-sw = "Y"
               MOVE WS-SV-MUPC TO DS-PRCT
               GO TO F100-ACCEPT-NO-PRMP.
           MOVE SY-CC-MUPC TO DS-PRCT.
       F100-ACCEPT-NO-PRMP.
           DISPLAY DS-PRCT LINE 18 POSITION 37.
           MOVE SY-CC-MUPC TO WS-PRCT.
           if sw-3-on 
               go to F100-disply.
       F100-accept-mark-up.
           ACCEPT WS-PRCT
               LINE 18 POSITION 37 SIZE 5
               TAB CONVERT UPDATE
             ON EXCEPTION
               FUNC-KEY
               GO TO F102-CHECK-FUNC.
       F100-DISPLY.
           MOVE WS-PRCT TO DS-PRCT DP-MUPC.
           DISPLAY DS-PRCT LINE 18 POSITION 37.
           PERFORM X360-EDIT-PERCENT THRU X365-EXIT.
           IF WS-ERSW = "Y"
               GO TO F100-MARK-UP.
           GO TO F105-LABOUR-FACTOR.
       F102-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 18 POSITION 37 SIZE 5
               GO TO F090-DESCRIPTION-2.
           IF FUNC-98
             OR FUNC-57
               GO TO F100-DISPLY.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F100-MARK-UP.
       
       F105-LABOUR-FACTOR.
           DISPLAY "N" LINE 19 POSITION 40.
           MOVE "N" TO WS-FLSW.
           if sw-3-on 
               go to F105-edit-lab-fact.
           ACCEPT WS-FLSW
               LINE 19 POSITION 40
               TAB ECHO UPDATE
             ON EXCEPTION
               FUNC-KEY
               GO TO F107-CHECK-FUNC.
       F105-edit-lab-fact.
           IF WS-FLSW NOT = "N"
             AND WS-FLSW NOT = "Y"
               GO TO F105-LABOUR-FACTOR.
           MOVE WS-FLSW TO DP-FLSW.
           DISPLAY WS-FLSW LINE 19 POSITION 40.
           GO TO F110-DISCOUNT.
       F107-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 19 POSITION 40 SIZE 1
               GO TO F100-MARK-UP.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F105-LABOUR-FACTOR.
       
       F110-DISCOUNT.
           IF ws-est-linked-sw = "Y"
               MOVE WS-SV-DCPC TO DS-PRCT
               GO TO F110-ACCEPT-NO-PRMP.
           MOVE CT-GDPC TO DS-PRCT.
       F110-ACCEPT-NO-PRMP.
           DISPLAY DS-PRCT LINE 20 POSITION 37.
           MOVE CT-GDPC TO WS-PRCT.
           if sw-3-on
               go to F110-disply.
           ACCEPT WS-PRCT
               LINE 20 POSITION 37 SIZE 5
               TAB CONVERT UPDATE
             ON EXCEPTION
               FUNC-KEY
               GO TO F115-CHECK-FUNC.
       F110-DISPLY.
           MOVE WS-PRCT TO DS-PRCT DP-DCPC.
           DISPLAY DS-PRCT LINE 20 POSITION 37.
           PERFORM X360-EDIT-PERCENT THRU X365-EXIT.
           IF WS-ERSW = "Y"
               GO TO F110-DISCOUNT.
           GO TO F120-PREV-DKT-NO.
       F115-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 20 POSITION 37 SIZE 5
               GO TO F105-LABOUR-FACTOR.
           IF FUNC-98
             OR FUNC-57
               GO TO F110-DISPLY.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F110-DISCOUNT.
       
       F120-PREV-DKT-NO.
           IF ws-est-linked-sw = "Y"
               GO TO F120-ACCEPT-NO-PRMP.
       F120-ACCEPT-PRMP.
           if sw-3-on
               go to F120-edit-pvdk.
           ACCEPT WS-PVDK
               LINE 21 POSITION 35
               PROMPT TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO F125-CHECK-FUNC.
           GO TO F120-EDIT-PVDK.
       F120-ACCEPT-NO-PRMP.
           DISPLAY WS-SV-PVDK LINE 21 POSITION 35.
           MOVE WS-SV-PVDK TO WS-PVDK.
           ACCEPT WS-PVDK
               LINE 21 POSITION 35
               TAB ECHO UPDATE
             ON EXCEPTION
               FUNC-KEY
               GO TO F125-CHECK-FUNC.
       F120-EDIT-PVDK.
           MOVE WS-PVDK TO DS-DKNO.
           DISPLAY DS-DKNO LINE 21 POSITION 35.
           PERFORM X370-EDIT-PREV-JOB THRU X375-EXIT.
           IF WS-ERSW = "Y"
               GO TO F120-PREV-DKT-NO.
           MOVE WS-PVDK TO DP-PVDK.
           GO TO F150-QUERIE.
       F125-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 21 POSITION 35 SIZE 6
               GO TO F110-DISCOUNT.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F120-PREV-DKT-NO.
       
       F150-QUERIE.
           DISPLAY "CONTINUE? (Y/N) Y"
               LINE 24 POSITION 1 HIGH.
           MOVE "Y" TO DUMMY-77.
           ACCEPT DUMMY-77
               LINE 24 POSITION 17
               TAB ECHO UPDATE
             on exception 
               func-key
               go to F150-QUERIE.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           if sw-3-on
             and dummy-77 = "N"
               display spaces line 18 position 35 size 40
               display spaces line 19 position 35 size 40
               display spaces line 20 position 35 size 40
               display spaces line 21 position 35 size 40
               go to F090-description-2.
           IF DUMMY-77 = "N"
               GO TO F120-PREV-DKT-NO.
           IF DUMMY-77 NOT = "Y"
               GO TO F150-QUERIE.
       F200-SET-SCREEN-2.
           PERFORM X900-CLEAR-SCREEN THRU X905-EXIT.
           PERFORM x930-display-screen-2-lit THRU X935-EXIT.

       F210-PURCHASE-ORDER.
           display "[F2]-BACK TO SCREEN ONE" 
               line 24 position 03 high.
           ACCEPT WS-CPON
               LINE 09 POSITION 33
               PROMPT TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO F215-CHECK-FUNC.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           MOVE WS-CPON TO DP-CPON.
           MOVE WS-CPON TO pr-pono.
           DISPLAY WS-CPON LINE 09 POSITION 33.
           GO TO F220-REFERENCE.
       F215-CHECK-FUNC.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           if func-02 and sw-3-on
               DISPLAY SPACES LINE 09 POSITION 33
               PERFORM X900-CLEAR-SCREEN THRU X905-EXIT
               PERFORM X910-display-screen-1-lit THRU X915-EXIT
               PERFORM X940-DISPLY-DATA-SCREEN-1 THRU X945-EXIT
               display spaces line 18 position 35 size 40
               display spaces line 19 position 35 size 40
               display spaces line 20 position 35 size 40
               display spaces line 21 position 35 size 40
               go to F090-description-2.
           if func-02
               DISPLAY SPACES LINE 09 POSITION 33
               PERFORM X900-CLEAR-SCREEN THRU X905-EXIT
               PERFORM X910-display-screen-1-lit THRU X915-EXIT
               PERFORM X940-DISPLY-DATA-SCREEN-1 THRU X945-EXIT
               go to F120-prev-dkt-no.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F210-PURCHASE-ORDER.
       
       F220-REFERENCE.
           ACCEPT WS-RFNO
               LINE 10 POSITION 31
               PROMPT TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO F225-CHECK-FUNC.
           MOVE WS-RFNO TO DP-RFNO.
           MOVE WS-RFNO TO pr-stby.
           DISPLAY WS-RFNO LINE 10 POSITION 31.
           GO TO F230-CONTACT.
       F225-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 10 POSITION 31 SIZE 10
               GO TO F210-PURCHASE-ORDER.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F220-REFERENCE.
       
       F230-CONTACT.
           ACCEPT WS-CTCT
               LINE 11 POSITION 29
               PROMPT TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO F235-CHECK-FUNC.
           MOVE WS-CTCT TO DP-CTCT.
           MOVE WS-CTCT TO pr-cntc.
           DISPLAY WS-CTCT LINE 11 POSITION 29.
           GO TO F240-JOB-TYPE.
       F235-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 11 POSITION 29 SIZE 20
               GO TO F220-REFERENCE.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F230-CONTACT.
       
       F240-JOB-TYPE.
           DISPLAY "NW" LINE 12 POSITION 39.
           MOVE "NW" TO WS-JBTP.
           ACCEPT WS-JBTP
               LINE 12 POSITION 39
               TAB ECHO UPDATE
             ON EXCEPTION
               FUNC-KEY
               GO TO F245-CHECK-FUNC.
           PERFORM X380-EDIT-JOB-TYPE THRU X385-EXIT.
           IF WS-ERSW = "Y"
               GO TO F240-JOB-TYPE.
           MOVE WS-JBTP TO DP-JBTP.
           MOVE WS-JBTP TO pr-jbtp.
           DISPLAY WS-JBTP LINE 12 POSITION 39.
           GO TO F250-PRIMARY-PRESS.
       F245-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 12 POSITION 39 SIZE 2
               GO TO F230-CONTACT.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F240-JOB-TYPE.
       
       F250-PRIMARY-PRESS.
           MOVE SPACES TO ws-skip-sec-wc.
           DISPLAY SPACES LINE 13 POSITION 43 SIZE 25.
           ACCEPT WS-PWNO
               LINE 13 POSITION 38
               PROMPT TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO F255-CHECK-FUNC.
           IF WS-PWNO = ZEROS
               MOVE ZEROS  TO DP-SWNO
               MOVE SPACES TO WC-WCDC
               MOVE "Y"    TO ws-skip-sec-wc
               move zeros to pr-wcno2
               move spaces to pr-wcdc2
               GO TO F252-DISPLY.
           MOVE WS-PWNO TO WS-WCNO.
           PERFORM X390-EDIT-WORK-CENTRE THRU X395-EXIT.
           IF WS-ERSW = "Y"
               GO TO F250-PRIMARY-PRESS.
       F252-DISPLY.
           MOVE WS-PWNO TO DP-PWNO.
           DISPLAY WS-PWNO LINE 13 POSITION 38
                   WC-WCDC LINE 13 POSITION 43.
           move ws-pwno to pr-wcno.
           move wc-wcdc to pr-wcdc.
           GO TO F260-SECONDARY-PRESS.
       F255-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 13 POSITION 38 SIZE 3
               GO TO F240-JOB-TYPE.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F250-PRIMARY-PRESS.
       
       F260-SECONDARY-PRESS.
           IF ws-skip-sec-wc = "Y"
               GO TO F262-DISPLY.
           DISPLAY SPACES LINE 14 POSITION 43 SIZE 25.
           ACCEPT WS-SWNO
               LINE 14 POSITION 38
               PROMPT TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO F265-CHECK-FUNC.
           IF WS-SWNO = WS-PWNO
               DISPLAY "SECONDARY WC MAY NOT BE THE "
                           LINE 24 POSITION 1 HIGH
                       "SAME AS THE PRIMARY WC "
                           LINE 00 POSITION 0 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F260-SECONDARY-PRESS.
           MOVE WS-SWNO TO WS-WCNO.
           PERFORM X390-EDIT-WORK-CENTRE THRU X395-EXIT.
           IF WS-ERSW = "Y"
               GO TO F260-SECONDARY-PRESS.
           MOVE WS-SWNO TO DP-SWNO.
           move dp-swno to pr-wcno2.
           move wc-wcdc to pr-wcdc2.
       F262-DISPLY.
           DISPLAY DP-SWNO LINE 14 POSITION 38
                   WC-WCDC LINE 14 POSITION 43.
           GO TO F270-STARTING-DATE.
       F265-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 14 POSITION 38 SIZE 3
               GO TO F250-PRIMARY-PRESS.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F260-SECONDARY-PRESS.
       
       F270-STARTING-DATE.
           MOVE SPACES  TO LS-UVACDT-NMSW.
           MOVE SY-ST-SODT TO LS-UVACDT-DATE.
           MOVE 15      TO LS-UVACDT-LINE.
           PERFORM X230-UVACDT THRU X235-EXIT.
           IF LS-UVACDT-CMCD = "02"
             AND ws-skip-sec-wc = "Y"
               DISPLAY SPACES LINE 14 POSITION 38 SIZE 3
               GO TO F250-PRIMARY-PRESS.
           IF LS-UVACDT-CMCD = "02"
               GO TO F260-SECONDARY-PRESS.
           move ls-uvacdt-date to ls-uvdtcv-indt.
           move "EX"           to ls-uvdtcv-cmcd.
           perform x999-uvdtcv thru x999-exit.
           MOVE LS-UVdtcv-otdt TO WS-DATE.
           PERFORM X400-EDIT-STARTING-DATE THRU X405-EXIT.
           IF WS-ERSW = "Y"
               GO TO F270-STARTING-DATE.
           MOVE LS-UVdtcv-otdt TO DP-STDT.
           move dp-stdt        to ws-stdt.
           move ws-stdt-yy     to ws-date-yy.
           move ws-stdt-mm     to ws-date-mm.
           move ws-stdt-dd     to ws-date-dd.
           move ws-ds-date     to pr-stdt.
       F280-TARGET-DATE.
           MOVE SPACES  TO LS-UVACDT-NMSW.
           if sw-3-on
               MOVE dp-stdt TO LS-UVACDT-DATE
             else
               MOVE SY-ST-SODT TO LS-UVACDT-DATE.
           MOVE 16      TO LS-UVACDT-LINE.
           PERFORM X230-UVACDT THRU X235-EXIT.
           IF LS-UVACDT-CMCD = "02"
               GO TO F270-STARTING-DATE.
           move ls-uvacdt-date to ls-uvdtcv-indt.
           move "EX"           to ls-uvdtcv-cmcd.
           perform x999-uvdtcv thru x999-exit.
           MOVE LS-UVdtcv-otdt TO WS-DATE.
           PERFORM X410-EDIT-TARGET-DATE THRU X415-EXIT.
           IF WS-ERSW = "Y"
               GO TO F280-TARGET-DATE.
           MOVE LS-UVdtcv-otdt TO DP-TGDT.
           move dp-tgdt        to ws-tgdt.
           move ws-tgdt-yy     to ws-date-yy.
           move ws-tgdt-mm     to ws-date-mm.
           move ws-tgdt-dd     to ws-date-dd.
           move ws-ds-date     to pr-tgdt.
       F320-DISPLY-PBAM.
           MOVE ZEROS TO DS-AMNT.
           DISPLAY DS-AMNT LINE 20 POSITION 29.
       F600-QUERIE.
           DISPLAY "CONTINUE? (Y/N) Y"
               LINE 24 POSITION 1 HIGH.
           MOVE "Y" TO DUMMY-77.
           ACCEPT DUMMY-77
               LINE 24 POSITION 17
               TAB ECHO UPDATE
             on exception 
               func-key
               go to F600-QUERIE.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           IF DUMMY-77 = "N"
               DISPLAY SPACES LINE 20 POSITION 29 SIZE 13
               GO TO F280-TARGET-DATE.
           IF DUMMY-77 NOT = "Y"
               GO TO F600-QUERIE.

       F610-get-next-available-job-no.
           MOVE SPACES   TO CM-KEY-1.
           MOVE SY-ST-CPNO  TO CM-CPNO-K1.
           MOVE "01"     TO CM-LFCD-K1.
           READ CM-FILE
               INTO CM-01-RCRD-WS
               KEY IS CM-KEY-1.
           IF RECORD-LOCKED
               MOVE "JCMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO F610-get-next-available-job-no.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       F610-SET-VALUES.
           MOVE CM-01-NAJN TO ws-job-no.
           ADD 1 TO CM-01-NAJN.
           IF CM-01-NAJN = ZERO
               MOVE 1 TO CM-01-NAJN.
           REWRITE CM-RCRD FROM CM-01-RCRD-WS.
           IF NOT FILE-OK
               MOVE "RW" TO FILE-IO
               GO TO Z100-FILE-ERR.
           move ws-job-no to dp-dkno-1.
           move ws-job-no to pr-jbno.

       F700-enter-estimate.
           if sw-5-on
               go to F750-print-sales-order.
           display spaces line 24 position 01 size 80.
           move "Y" to ws-est-entered-sw.
           perform R000-display-est-hdr thru r990-exit.
           perform V000-enter-estimate thru V990-exit.
           move eh-etno-1 to pr-etno.
           move ws-sv-etct to pr-etct.
           compute ws-etmg = dp-qtpr - ws-sv-etct.
           move ws-etmg to pr-etmg.

       F750-print-sales-order.
           if sw-6-on
               perform p000-print-sales-order thru p990-exit
               GO TO F750-SET-EH-RECORDS.
       F750-print-query.
           display spaces line 24 position 01 size 80.
           DISPLAY "PRINT SALES ORDER? (Y/N) "
               LINE 24 POSITION 1 HIGH.
           ACCEPT DUMMY-77
               LINE 0 POSITION 0
               prompt TAB ECHO
             on exception 
               func-key
               go to F750-print-query.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           IF DUMMY-77 = "N"
               GO TO F750-SET-EH-RECORDS.
           IF DUMMY-77 = "Y"
               perform P000-print-sales-order thru P990-exit
               GO TO F750-SET-EH-RECORDS.
           GO TO F750-print-query.

       F750-SET-EH-RECORDS.
           if sw-5-off
               GO TO F950-SET-job-history-RECORD.
           IF ws-est-linked-sw = "N"
               GO TO F950-SET-job-history-RECORD.
           MOVE DP-ETNO TO WS-ETNO.
           PERFORM X500-SET-READ-ESTIMATE THRU X505-EXIT.
           IF WS-ERSW = "Y"
               MOVE "15" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
           MOVE DP-DKNO-1 TO EH-DKNO.
           PERFORM X700-REWRITE-EH THRU X700-EXIT.
           MOVE DP-QTCD TO WS-QTCD.
           PERFORM X510-SET-READ-QUANTITY THRU X515-EXIT.
           IF WS-ERSW = "Y"
               MOVE "16" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
           MOVE EH-ETCT TO WS-SV-ETCT.

       F950-SET-job-history-RECORD.
           MOVE SPACES     TO CD-RCRD CD-RCRD-WS.
           MOVE ZEROS      TO CD-RFNO CD-QNTY.
           MOVE ZEROS      TO CD-TRDT CD-JBCT CD-JBSP.
           MOVE SY-ST-CPNO    TO CD-CPNO-1.
           MOVE DP-CTNO    TO WS-CTNO.
           MOVE WS-CTNO    TO CD-CTNO-1.
           MOVE ws-job-no    TO LS-UVFDNO-DKNO.
           PERFORM X240-UVFDNO THRU X245-EXIT.
           MOVE LS-UVFDNO-DKNO TO CD-DKNO-1.
           MOVE DP-PVDK    TO CD-RFNO.
           MOVE DP-QNTY    TO CD-QNTY.
           MOVE DP-DKD1    TO CD-DKD1.
           MOVE DP-TGDT    TO CD-TRDT.
      *
      *    pass estimate cost to job history file
      *
           IF ws-est-entered-sw = "Y" and sw-5-off
               MOVE WS-SV-ETCT TO CD-JBCT
             else
               MOVE zeros TO CD-JBCT.

           IF ws-est-linked-sw = "Y" and sw-5-on
               MOVE WS-SV-ETCT TO CD-JBCT
             else
               MOVE zeros TO CD-JBCT.

           MOVE DP-QTPR    TO CD-JBSP.
           MOVE "A"        TO CD-STCD.
           PERFORM X710-WRITE-CD THRU X715-EXIT.

       F980-WRITE-DP.
           IF ws-est-entered-sw = "Y" and sw-5-off
               move eh-etno-1 to dp-etno
               move 1 to dp-qtcd
             else
               move zeros to dp-etno
               move zeros to dp-qtcd.

           WRITE DP-RCRD FROM DP-RCRD-WS.
           IF NOT FILE-OK
               MOVE "WR" TO FILE-IO
               GO TO Z100-FILE-ERR.

       F990-EXIT.
           EXIT.
      
       H000-CHANGE-MODE.
           move "N" to ws-seg-chg-sw.
           move "N" to ws-qty-chg-sw.
           MOVE SPACES  TO WS-CHSW WS-CRSW.
           MOVE DP-ETNO TO WS-SV-ETNO.
           MOVE DP-QTCD TO WS-SV-QTCD.
           MOVE DP-CTNO TO WS-SV-CTNO.
           perform X910-display-screen-1-lit thru x915-exit.
           PERFORM X940-DISPLY-DATA-SCREEN-1 THRU X945-EXIT.
       H000-SELECT.
           IF WS-CSCD = 1
               display "[F1]-NEXT SCREEN, [F2]-ESCAPE, SELECT "
                   line 24 position 3 high.
           IF WS-CSCD = 2
               display "[F2]-ESCAPE, [F3]-PREVIOUS SCREEN, SELECT "
                   line 24 position 3 high.
           ACCEPT WS-FIELD
               LINE 0 POSITION 0
               PROMPT TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO H590-CHECK-FUNC.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
       H000-REVIEW-CSCD.
           IF WS-CSCD = 1
               GO TO H000-REVIEW-SCREEN-1.
           IF WS-CSCD = 2
               GO TO H000-REVIEW-SCREEN-2.
           MOVE "31" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
      *
       H000-REVIEW-SCREEN-1.
           PERFORM X150-EDIT-SCREEN-1 THRU X155-EXIT.
           IF WS-ERSW = "Y"
               GO TO H000-SELECT.
           IF WS-FIELD = 01
               GO TO H010-ESTIMATE.
           IF WS-FIELD = 02
               GO TO H020-QUANTITY-CODE.
           IF WS-FIELD = 03
               GO TO H030-CUSTOMER.
           IF WS-FIELD = 04
               GO TO H040-SALESMAN.
           IF WS-FIELD = 05
               GO TO H050-PRODUCT-CLASS.
           IF WS-FIELD = 06
               GO TO H060-QUOTED-PRICE.
           IF WS-FIELD = 07
               GO TO H070-QUANTITY.
           IF WS-FIELD = 08
               GO TO H080-DESCRIPTION-1.
           IF WS-FIELD = 09
               GO TO H090-DESCRIPTION-2.
           IF WS-FIELD = 10
               GO TO H100-MARK-UP.
           IF WS-FIELD = 11
               GO TO H105-LABOUR-FACTOR.
           IF WS-FIELD = 12
               GO TO H110-DISCOUNT.
           IF WS-FIELD = 13
               GO TO H120-PREV-DKT-NO.
           GO TO H000-SELECT.
      *
       H000-REVIEW-SCREEN-2.
           PERFORM X160-EDIT-SCREEN-2 THRU X165-EXIT.
           IF WS-ERSW = "Y"
               GO TO H000-SELECT.
           IF WS-FIELD = 21
               GO TO H210-PURCHASE-ORDER.
           IF WS-FIELD = 22
               GO TO H220-REFERENCE.
           IF WS-FIELD = 23
               GO TO H230-CONTACT.
           IF WS-FIELD = 24
               GO TO H240-JOB-TYPE.
           IF WS-FIELD = 25
               GO TO H250-PRIMARY-PRESS.
           IF WS-FIELD = 26
               GO TO H260-SECONDARY-PRESS.
           IF WS-FIELD = 27
               GO TO H270-STARTING-DATE.
           IF WS-FIELD = 28
               GO TO H280-TARGET-DATE.
           IF WS-FIELD = 29
               GO TO H290-SHIPPED-DATE.
           IF WS-FIELD = 30
               GO TO H300-CLOSED-DATE.
           IF WS-FIELD = 31
               GO TO H310-INVOICE-NO.
           IF WS-FIELD = 32
               GO TO H320-PRE-BILL.
           GO TO H000-SELECT.
      *
       H010-ESTIMATE.
           MOVE DP-ETNO TO DS-ETNO.
           DISPLAY SPACES  LINE 09 POSITION 43 SIZE 30
                   DS-ETNO LINE 09 POSITION 37.
           MOVE DP-ETNO TO WS-ETNO.
           ACCEPT WS-ETNO
               LINE 09 POSITION 37
               TAB ECHO UPDATE.
           INSPECT WS-ETNO
               REPLACING ALL " " BY "0".
           PERFORM X300-EDIT-ESTIMATE THRU X305-EXIT.
           IF WS-ERSW = "Y"
               GO TO H010-ESTIMATE.
           MOVE WS-ETNO TO DS-ETNO.
           DISPLAY DS-ETNO LINE 09 POSITION 37
                   EH-ETDC LINE 09 POSITION 43.
           IF WS-ETNO NOT = DP-ETNO
               MOVE WS-ETNO TO DP-ETNO
               MOVE "Y" TO WS-CHSW
               MOVE "Y" TO WS-CRSW.
           IF DP-ETNO = ZERO
               MOVE ZERO TO DP-QTCD
               DISPLAY DP-QTCD LINE 10 POSITION 40
                       SPACES  LINE 10 POSITION 43 SIZE 12
               GO TO H000-SELECT.
       H020-QUANTITY-CODE.
           IF DP-ETNO = ZERO
               DISPLAY "ESTIMATE NUMBER ZERO - NO MAINTENANCE ALLOWED "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO H000-SELECT.
       H020-CONTINUE.
           MOVE DP-ETNO TO WS-ETNO.
           DISPLAY SPACES  LINE 10 POSITION 43 SIZE 12
                   DP-QTCD LINE 10 POSITION 40.
           MOVE DP-QTCD TO WS-QTCD.
           ACCEPT WS-QTCD
               LINE 10 POSITION 40
               TAB ECHO UPDATE
             ON EXCEPTION
               FUNC-KEY
               GO TO H025-CHECK-FUNC.
           PERFORM X310-EDIT-QUANTITY-CODE THRU X315-EXIT.
           IF WS-ERSW = "Y"
               GO TO H020-CONTINUE.
           MOVE EH-ETQT TO DS-QNTY.
           DISPLAY WS-QTCD LINE 10 POSITION 40
                   DS-QNTY LINE 10 POSITION 43.
           IF WS-QTCD NOT = DP-QTCD
               MOVE WS-QTCD TO DP-QTCD
               MOVE "Y" TO WS-CHSW
               MOVE "Y" TO WS-CRSW.
           MOVE EH-ETCT TO WS-SV-ETCT.
           GO TO H000-SELECT.
      *
       H025-CHECK-FUNC.
           IF FUNC-02
               DISPLAY DP-QTCD LINE 10 POSITION 40
               GO TO H010-ESTIMATE.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO H020-CONTINUE.
      *
       H030-CUSTOMER.
           DISPLAY SPACES LINE 11 POSITION 43 SIZE 35.
           MOVE "NP"    TO LS-UVGCNO-CMCD.
           MOVE DP-CTNO TO LS-UVGCNO-CTNO.
           PERFORM X200-UVGCNO THRU X205-EXIT.
           IF LS-UVGCNO-CMCD = "02"
               PERFORM X000-FUNC-ERR THRU X090-EXIT
               GO TO H030-CUSTOMER.
           MOVE LS-UVGCNO-CTNO TO WS-CTNO.
           PERFORM X610-READ-CT THRU X615-EXIT.
           IF WS-ERSW = "Y"
               MOVE "32" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
           DISPLAY CT-CTNM LINE 11 POSITION 43.
           IF WS-CTNO NOT = DP-CTNO
               MOVE WS-CTNO TO DP-CTNO
               MOVE "Y" TO WS-CHSW
               MOVE "Y" TO WS-CRSW.
           GO TO H000-SELECT.
      *
       H040-SALESMAN.
           DISPLAY SPACES LINE 12 POSITION 43 SIZE 35.
           MOVE DP-SMNO TO LS-UVGSNO-SMNO.
           PERFORM X210-UVGSNO THRU X215-EXIT.
           IF LS-UVGSNO-CMCD = "02"
               PERFORM X000-FUNC-ERR THRU X090-EXIT
               GO TO H040-SALESMAN.
           MOVE LS-UVGSNO-SMNO TO WS-SMNO.
           PERFORM X620-READ-SM THRU X625-EXIT.
           IF WS-ERSW = "Y"
               MOVE "33" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
           DISPLAY SM-SMNM LINE 12 POSITION 43.
           IF WS-SMNO NOT = DP-SMNO
               MOVE WS-SMNO TO DP-SMNO
               MOVE "Y" TO WS-CHSW.
           GO TO H000-SELECT.
      *
       H050-PRODUCT-CLASS.
           DISPLAY SPACES  LINE 13 POSITION 43 SIZE 35
                   DP-PDTP LINE 13 POSITION 37.
           MOVE DP-PDTP TO WS-PCCD.
           ACCEPT WS-PCCD
               LINE 13 POSITION 37
               TAB ECHO UPDATE.
           PERFORM X320-EDIT-PRODUCT-CLASS THRU X325-EXIT.
           IF WS-ERSW = "Y"
               GO TO H050-PRODUCT-CLASS.
           DISPLAY WS-PCCD    LINE 13 POSITION 37
                   MM-61-DESC LINE 13 POSITION 43.
           IF WS-PCCD NOT = DP-PDTP
               MOVE WS-PCCD TO DP-PDTP
               MOVE "Y" TO WS-CHSW.
           GO TO H000-SELECT.
      *
       H060-QUOTED-PRICE.
           MOVE DP-QTPR TO ED-AMNT.
           DISPLAY SPACES  LINE 14 POSITION 29 SIZE 13
                   ED-AMNT LINE 14 POSITION 31.
           MOVE ED-AMNT TO WS-QTPR.
           ACCEPT WS-QTPR
               LINE 14 POSITION 31 SIZE 11
               TAB CONVERT UPDATE.
           MOVE WS-QTPR TO DS-AMNT.
           DISPLAY DS-AMNT LINE 14 POSITION 29.
           PERFORM X330-EDIT-QUOTED-PRICE THRU X335-EXIT.
           IF WS-ERSW = "Y"
               GO TO H060-QUOTED-PRICE.
           IF WS-QTPR NOT = DP-QTPR
               MOVE WS-QTPR TO DP-QTPR
               MOVE "Y" TO ws-qty-chg-sw
               MOVE "Y" TO WS-CHSW
               MOVE "Y" TO WS-CRSW.
           GO TO H000-SELECT.
      *
       H070-QUANTITY.
           MOVE DP-QNTY TO ED-QNTY.
           DISPLAY SPACES  LINE 15 POSITION 30 SIZE 12
                   ED-QNTY LINE 15 POSITION 32.
           MOVE ED-QNTY TO WS-QNTY.
           ACCEPT WS-QNTY
               LINE 15 POSITION 32 SIZE 10
               TAB CONVERT UPDATE.
           MOVE WS-QNTY TO DS-QNTY.
           DISPLAY DS-QNTY LINE 15 POSITION 30.
           PERFORM X340-EDIT-QUANTITY THRU X345-EXIT.
           IF WS-ERSW = "Y"
               GO TO H070-QUANTITY.
           IF WS-QNTY NOT = DP-QNTY
               MOVE WS-QNTY TO DP-QNTY
               MOVE "Y" TO ws-qty-chg-sw
               MOVE "Y" TO WS-CHSW
               MOVE "Y" TO WS-CRSW.
           GO TO H000-SELECT.
      *
       H080-DESCRIPTION-1.
           DISPLAY DP-DKD1 LINE 16 POSITION 29.
           MOVE DP-DKD1 TO WS-DKDC.
           ACCEPT WS-DKDC
               LINE 16 POSITION 29
               TAB ECHO UPDATE.
           PERFORM X350-EDIT-DESCRIPTION-1 THRU X355-EXIT.
           IF WS-ERSW = "Y"
               GO TO H080-DESCRIPTION-1.
           DISPLAY WS-DKDC LINE 16 POSITION 29.
           IF WS-DKDC NOT = DP-DKD1
               MOVE WS-DKDC TO DP-DKD1
               MOVE "Y" TO ws-seg-chg-sw
               MOVE "Y" TO WS-CHSW
               MOVE "Y" TO WS-CRSW.
           GO TO H000-SELECT.
      *
       H090-DESCRIPTION-2.
           MOVE DP-DKD2 TO WS-DKDC.
           ACCEPT WS-DKDC
               LINE 17 POSITION 29
               TAB ECHO UPDATE.
           DISPLAY WS-DKDC LINE 17 POSITION 29.
           IF WS-DKDC NOT = DP-DKD2
               MOVE WS-DKDC TO DP-DKD2
               MOVE "Y" TO WS-CHSW.
           GO TO H000-SELECT.
      *
       H100-MARK-UP.
           MOVE DP-MUPC TO DS-PRCT.
           DISPLAY DS-PRCT LINE 18 POSITION 37.
           MOVE DP-MUPC TO WS-PRCT.
           ACCEPT WS-PRCT
               LINE 18 POSITION 37 SIZE 5
               TAB CONVERT UPDATE.
           MOVE WS-PRCT TO DS-PRCT.
           DISPLAY DS-PRCT LINE 18 POSITION 37.
           PERFORM X360-EDIT-PERCENT THRU X365-EXIT.
           IF WS-ERSW = "Y"
               GO TO H100-MARK-UP.
           IF WS-PRCT NOT = DP-MUPC
               MOVE WS-PRCT TO DP-MUPC
               MOVE "Y" TO WS-CHSW.
           GO TO H000-SELECT.
      *
       H105-LABOUR-FACTOR.
           DISPLAY DP-FLSW LINE 19 POSITION 40.
           MOVE DP-FLSW TO WS-FLSW.
           ACCEPT WS-FLSW
               LINE 19 POSITION 40
               TAB ECHO UPDATE.
           IF WS-FLSW NOT = "Y"
             AND WS-FLSW NOT = "N"
               GO TO H105-LABOUR-FACTOR.
           DISPLAY WS-FLSW LINE 19 POSITION 40.
           IF WS-FLSW NOT = DP-FLSW
               MOVE WS-FLSW TO DP-FLSW
               MOVE "Y" TO WS-CHSW.
           GO TO H000-SELECT.
      *
       H110-DISCOUNT.
           MOVE DP-DCPC TO DS-PRCT.
           DISPLAY DS-PRCT LINE 20 POSITION 37.
           MOVE DP-DCPC TO WS-PRCT.
           ACCEPT WS-PRCT
               LINE 20 POSITION 37 SIZE 5
               TAB CONVERT UPDATE.
           MOVE WS-PRCT TO DS-PRCT.
           DISPLAY DS-PRCT LINE 20 POSITION 37.
           PERFORM X360-EDIT-PERCENT THRU X365-EXIT.
           IF WS-ERSW = "Y"
               GO TO H110-DISCOUNT.
           IF WS-PRCT NOT = DP-DCPC
               MOVE WS-PRCT TO DP-DCPC
               MOVE "Y" TO WS-CHSW.
           GO TO H000-SELECT.
      *
       H120-PREV-DKT-NO.
           MOVE DP-PVDK TO WS-PVDK.
           DISPLAY WS-PVDK LINE 21 POSITION 35.
           MOVE DP-PVDK TO WS-PVDK.
           ACCEPT WS-PVDK
               LINE 21 POSITION 35
               TAB ECHO UPDATE.
           PERFORM X370-EDIT-PREV-JOB THRU X375-EXIT.
           IF WS-ERSW = "Y"
               GO TO H120-PREV-DKT-NO.
           MOVE WS-PVDK TO DS-DKNO.
           DISPLAY DS-DKNO LINE 21 POSITION 35.
           IF WS-PVDK NOT = DP-PVDK
               MOVE WS-PVDK TO DP-PVDK
               MOVE "Y" TO WS-CRSW
               MOVE "Y" TO WS-CHSW.
           GO TO H000-SELECT.
      *
       H210-PURCHASE-ORDER.
           MOVE DP-CPON TO WS-CPON.
           ACCEPT WS-CPON
               LINE 09 POSITION 33
               TAB ECHO UPDATE.
           DISPLAY WS-CPON LINE 09 POSITION 33.
           IF WS-CPON NOT = DP-CPON
               MOVE WS-CPON TO DP-CPON
               MOVE "Y" TO WS-CHSW.
           GO TO H000-SELECT.
      *
       H220-REFERENCE.
           MOVE DP-RFNO TO WS-RFNO.
           ACCEPT WS-RFNO
               LINE 10 POSITION 31
               TAB ECHO UPDATE.
           DISPLAY WS-RFNO LINE 10 POSITION 31.
           IF WS-RFNO NOT = DP-RFNO
               MOVE WS-RFNO TO DP-RFNO
               MOVE "Y" TO WS-CHSW.
           GO TO H000-SELECT.
      *
       H230-CONTACT.
           MOVE DP-CTCT TO WS-CTCT.
           ACCEPT WS-CTCT
               LINE 11 POSITION 29
               TAB ECHO UPDATE.
           DISPLAY WS-CTCT LINE 11 POSITION 29.
           IF WS-CTCT NOT = DP-CTCT
               MOVE WS-CTCT TO DP-CTCT
               MOVE "Y" TO WS-CHSW.
           GO TO H000-SELECT.
      *
       H240-JOB-TYPE.
           DISPLAY DP-JBTP LINE 12 POSITION 39.
           MOVE DP-JBTP TO WS-JBTP.
           ACCEPT WS-JBTP
               LINE 12 POSITION 39
               TAB ECHO UPDATE.
           PERFORM X380-EDIT-JOB-TYPE THRU X385-EXIT.
           IF WS-ERSW = "Y"
               GO TO H240-JOB-TYPE.
           DISPLAY WS-JBTP LINE 12 POSITION 39.
           IF WS-JBTP NOT = DP-JBTP
               MOVE WS-JBTP TO DP-JBTP
               MOVE "Y" TO WS-CHSW.
           GO TO H000-SELECT.
      *
       H250-PRIMARY-PRESS.
           MOVE DP-PWNO TO WS-WCNO.
           DISPLAY SPACES  LINE 13 POSITION 43 SIZE 25
                   WS-WCNO LINE 13 POSITION 38.
           MOVE DP-PWNO TO WS-WCNO.
           ACCEPT WS-WCNO
               LINE 13 POSITION 38
               TAB ECHO UPDATE.
           PERFORM X390-EDIT-WORK-CENTRE THRU X395-EXIT.
           IF WS-ERSW = "Y"
               GO TO H250-PRIMARY-PRESS.
           DISPLAY WS-WCNO LINE 13 POSITION 38
                   WC-WCDC LINE 13 POSITION 43.
           IF WS-WCNO NOT = DP-PWNO
               MOVE WS-WCNO TO DP-PWNO
               MOVE "Y" TO WS-CHSW.
           GO TO H000-SELECT.
      *
       H260-SECONDARY-PRESS.
           MOVE DP-SWNO TO WS-WCNO.
           DISPLAY SPACES  LINE 14 POSITION 43 SIZE 25
                   WS-WCNO LINE 14 POSITION 38.
           MOVE DP-SWNO TO WS-WCNO.
           ACCEPT WS-WCNO
               LINE 14 POSITION 38
               TAB ECHO UPDATE.
           PERFORM X390-EDIT-WORK-CENTRE THRU X395-EXIT.
           IF WS-ERSW = "Y"
               GO TO H260-SECONDARY-PRESS.
           DISPLAY WS-WCNO LINE 14 POSITION 38
                   WC-WCDC LINE 14 POSITION 43.
           IF WS-WCNO NOT = DP-SWNO
               MOVE WS-WCNO TO DP-SWNO
               MOVE "Y" TO WS-CHSW.
           GO TO H000-SELECT.
      *
       H270-STARTING-DATE.
           MOVE DP-STDT TO LS-UVACDT-DATE.
           MOVE 15      TO LS-UVACDT-LINE.
           MOVE SPACES  TO LS-UVACDT-NMSW.
           PERFORM X230-UVACDT THRU X235-EXIT.
           IF LS-UVACDT-CMCD = "02"
               PERFORM X000-FUNC-ERR THRU X090-EXIT
               GO TO H270-STARTING-DATE.
           move ls-uvacdt-date to ls-uvdtcv-indt.
           move "EX"           to ls-uvdtcv-cmcd.
           perform x999-uvdtcv thru x999-exit.
           MOVE LS-UVdtcv-otdt TO WS-DATE.
           PERFORM X400-EDIT-STARTING-DATE THRU X405-EXIT.
           IF WS-ERSW = "Y"
               GO TO H270-STARTING-DATE.
           IF WS-DATE NOT = DP-STDT
               MOVE WS-DATE TO DP-STDT
               MOVE "Y" TO WS-CHSW.
           GO TO H000-SELECT.
      *
       H280-TARGET-DATE.
           MOVE DP-TGDT TO LS-UVACDT-DATE.
           MOVE 16      TO LS-UVACDT-LINE.
           MOVE SPACES  TO LS-UVACDT-NMSW.
           PERFORM X230-UVACDT THRU X235-EXIT.
           IF LS-UVACDT-CMCD = "02"
               PERFORM X000-FUNC-ERR THRU X090-EXIT
               GO TO H280-TARGET-DATE.
           move ls-uvacdt-date to ls-uvdtcv-indt.
           move "EX"           to ls-uvdtcv-cmcd.
           perform x999-uvdtcv thru x999-exit.
           MOVE LS-UVdtcv-otdt TO WS-DATE.
           IF WS-DATE NOT = DP-TGDT
               MOVE WS-DATE TO DP-TGDT
               MOVE "Y" TO WS-CHSW
               MOVE "Y" TO WS-CRSW.
           GO TO H000-SELECT.
      *
       H290-SHIPPED-DATE.
           MOVE DP-SHDT TO LS-UVACDT-DATE.
           MOVE 17      TO LS-UVACDT-LINE.
           MOVE "Y"     TO LS-UVACDT-NMSW.
           PERFORM X230-UVACDT THRU X235-EXIT.
           IF LS-UVACDT-CMCD = "02"
               PERFORM X000-FUNC-ERR THRU X090-EXIT
               GO TO H290-SHIPPED-DATE.
           move ls-uvacdt-date to ls-uvdtcv-indt.
           move "EX"           to ls-uvdtcv-cmcd.
           perform x999-uvdtcv thru x999-exit.
           MOVE LS-UVdtcv-otdt TO WS-DATE.
           IF WS-DATE NOT = DP-SHDT
               MOVE WS-DATE TO DP-SHDT
               MOVE "Y" TO WS-CHSW.
           GO TO H000-SELECT.
      *
       H300-CLOSED-DATE.
           IF SW-1-OFF
               PERFORM X100-NO-MAINT THRU X105-EXIT
               GO TO H000-SELECT.
           MOVE DP-CLDT TO LS-UVACDT-DATE.
           MOVE 18      TO LS-UVACDT-LINE.
           MOVE "Y"     TO LS-UVACDT-NMSW.
           PERFORM X230-UVACDT THRU X235-EXIT.
           IF LS-UVACDT-CMCD = "02"
               PERFORM X000-FUNC-ERR THRU X090-EXIT
               GO TO H300-CLOSED-DATE.
           move ls-uvacdt-date to ls-uvdtcv-indt.
           move "EX"           to ls-uvdtcv-cmcd.
           perform x999-uvdtcv thru x999-exit.
           MOVE LS-UVdtcv-otdt TO WS-DATE.
           IF WS-DATE NOT = DP-CLDT
               MOVE WS-DATE TO DP-CLDT
               MOVE "Y"     TO WS-CHSW.
           GO TO H000-SELECT.
      *
       H310-INVOICE-NO.
           IF SW-1-OFF
               PERFORM X100-NO-MAINT THRU X105-EXIT
               GO TO H000-SELECT.
           MOVE DP-IVNO TO WS-IVNO.
           DISPLAY WS-IVNO LINE 19 POSITION 35.
           MOVE DP-IVNO TO WS-IVNO.
           ACCEPT WS-IVNO
               LINE 19 POSITION 35
               TAB ECHO UPDATE.
           IF WS-IVNO NOT NUMERIC
               DISPLAY "INVOICE NO MUST BE NUMERIC "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO H310-INVOICE-NO.
           MOVE WS-IVNO TO DS-IVNO.
           DISPLAY DS-IVNO LINE 19 POSITION 35.
           IF WS-IVNO NOT = DP-IVNO
               MOVE WS-IVNO TO DP-IVNO
               MOVE "Y"     TO WS-CHSW.
           GO TO H000-SELECT.
      *
       H320-PRE-BILL.
           IF SW-1-OFF
               PERFORM X100-NO-MAINT THRU X105-EXIT
               GO TO H000-SELECT.
       H320-CONTINUE.
           MOVE DP-PBAM TO ED-AMNT.
           DISPLAY SPACES  LINE 20 POSITION 29 SIZE 13
                   ED-AMNT LINE 20 POSITION 31.
           MOVE DP-PBAM TO WS-PBAM.
           ACCEPT WS-PBAM
               LINE 20 POSITION 31 SIZE 11
               TAB CONVERT UPDATE.
           MOVE WS-PBAM TO DS-AMNT.
           DISPLAY DS-AMNT LINE 20 POSITION 29.
           IF WS-PBAM NOT = DP-PBAM
               MOVE WS-PBAM TO DP-PBAM
               MOVE "Y" TO WS-CHSW.
           GO TO H000-SELECT.
      *
       H590-CHECK-FUNC.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           IF FUNC-01
             AND WS-CSCD = 1
               PERFORM X900-CLEAR-SCREEN THRU X905-EXIT
               PERFORM x930-display-screen-2-lit THRU X935-EXIT
               PERFORM X950-DISPLY-DATA-SCREEN-2 THRU X955-EXIT
               GO TO H000-SELECT.
           IF FUNC-03
             AND WS-CSCD = 2
               PERFORM X900-CLEAR-SCREEN THRU X905-EXIT
               PERFORM X910-display-screen-1-lit THRU X915-EXIT
               PERFORM X940-DISPLY-DATA-SCREEN-1 THRU X945-EXIT
               GO TO H000-SELECT.
           IF FUNC-02
               GO TO H600-CROSS-EDIT.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO H000-SELECT.
       
       H600-CROSS-EDIT.
           IF WS-CHSW NOT = "Y"
               GO TO H900-UNLOCK-DP.

       H610-REVIEW-WCNO.
           IF DP-PWNO = ZEROS
             AND DP-SWNO NOT = ZEROS
               DISPLAY "SECONDARY WC MUST BE ZERO WHEN "
                           LINE 24 POSITION 1 HIGH
                       "PRIMARY WC IS ZERO "
                           LINE 00 POSITION 0 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO H000-SELECT.
           IF DP-PWNO NOT = ZEROS
             AND DP-PWNO = DP-SWNO
               DISPLAY "SECONDARY WC MAY NOT BE THE SAME "
                           LINE 24 POSITION 1 HIGH
                       "AS THE PRIMARY WC "
                           LINE 00 POSITION 00 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO H000-SELECT.
       H620-REVIEW-DP.
           IF DP-TGDT < DP-STDT
               DISPLAY "TARGET DATE MAY NOT BE < STARTING DATE "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO H000-SELECT.
           IF DP-STCD = 9
             AND DP-CLDT < DP-STDT
               DISPLAY "CLOSED DATE MAY NOT BE < STARTING DATE "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO H000-SELECT.
           IF DP-SHDT > ZERO
             AND DP-SHDT < DP-STDT
               DISPLAY "SHIPPED DATE MAY NOT BE < STARTING DATE "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO H000-SELECT.

       H680-REWRITE-DP.
           REWRITE DP-RCRD FROM DP-RCRD-WS.
           IF NOT FILE-OK
               MOVE "RW" TO FILE-IO
               GO TO Z100-FILE-ERR.

       H700-REVIEW-EH.
           IF DP-ETNO = WS-SV-ETNO
             AND DP-QTCD = WS-SV-QTCD
               GO TO H800-rewrite-est-hdrs.
           MOVE ZEROS TO WS-SV-ETCT.
           IF DP-ETNO = WS-SV-ETNO
               GO TO H740-REVIEW-OV-QTCD.
       H720-REVIEW-OV-ETNO.
           IF WS-SV-ETNO = ZERO
               GO TO H760-REVIEW-ETNO.
           MOVE WS-SV-ETNO TO WS-ETNO.
           PERFORM X500-SET-READ-ESTIMATE THRU X505-EXIT.
           IF WS-ERSW = "Y"
               GO TO H760-REVIEW-ETNO.
           MOVE ZEROS TO EH-DKNO.
           PERFORM X700-REWRITE-EH THRU X700-EXIT.
       H740-REVIEW-OV-QTCD.
           MOVE WS-SV-ETNO TO WS-ETNO.
           MOVE WS-SV-QTCD TO WS-QTCD.
           PERFORM X510-SET-READ-QUANTITY THRU X515-EXIT.
           IF WS-ERSW = "Y"
             AND DP-ETNO = WS-SV-ETNO
               GO TO H780-REVIEW-QTCD.
           IF WS-ERSW = "Y"
               GO TO H760-REVIEW-ETNO.
       H760-REVIEW-ETNO.
           IF DP-ETNO = ZERO
               GO TO H800-rewrite-est-hdrs.
           MOVE DP-ETNO TO WS-ETNO.
           PERFORM X500-SET-READ-ESTIMATE THRU X505-EXIT.
           IF WS-ERSW = "Y"
               MOVE "25" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
           IF EH-DKNO > ZERO
               MOVE "26" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
           MOVE ws-job-no TO EH-DKNO.
           PERFORM X700-REWRITE-EH THRU X700-EXIT.
       H780-REVIEW-QTCD.
           MOVE DP-ETNO TO WS-ETNO.
           MOVE DP-QTCD TO WS-QTCD.
           PERFORM X510-SET-READ-QUANTITY THRU X515-EXIT.
           IF WS-ERSW = "Y"
               MOVE "27" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
           MOVE EH-ETCT TO WS-SV-ETCT.

      *
      *    This code added to ABSOLUTELY rewrite estimate headers
      *    after job header re-written - in that way, the two will
      *    always be in sync.
      *
       H800-rewrite-est-hdrs.
           IF DP-ETNO = ZERO
               go to H810-review-job-hist.
       H800-job-hdr.
           MOVE SPACES  TO EH-KEY-1-ws.
           MOVE SY-ST-CPNO TO EH-CPNO-1.
           MOVE DP-ETNO TO EH-ETNO-1.
           MOVE 1 TO EH-RCCD-1.
           MOVE 0 TO EH-SQCD-1.
           move eh-key-1-ws to eh-key-1.
           perform X600-READ-EH thru x600-exit.
           if ws-ersw = "Y"
               MOVE "HJ" TO FILE-status
               GO TO Z100-FILE-ERR.
           move dp-smno to eh-smno-2.
           move dp-ctno to eh-ctno-2.
           move sy-st-usid to EH-ETIT.
           move dp-stdt to EH-ETDT.
           move ct-ctnm to EH-CTAL.
           move dp-dkd1 to EH-ETDC.
           move dp-dkno-1 to EH-DKNO.
           move dp-pvdk to EH-PVDK.
           move dp-mupc to EH-MUPC.
           move dp-dcpc to EH-DCPC.
           move dp-pdtp to EH-PCCD.
           move "N" to EH-JASW.
           perform X700-REWRITE-EH thru x700-exit.
       H800-seg-hdr.
           if ws-seg-chg-sw = "N"
               go to H800-qty-hdr.
           MOVE SPACES  TO EH-KEY-1-ws.
           MOVE SY-ST-CPNO TO EH-CPNO-1.
           MOVE DP-ETNO TO EH-ETNO-1.
           MOVE 2 TO EH-RCCD-1.
           MOVE 1 TO EH-SQCD-1.
           move eh-key-1-ws to eh-key-1.
           perform X600-READ-EH thru x600-exit.
           if ws-ersw = "Y"
               MOVE "HS" TO FILE-status
               GO TO Z100-FILE-ERR.
           move dp-dkd1 to EH-SGDC.
           perform X700-REWRITE-EH thru x700-exit.
       H800-seg9-hdr.
           MOVE SPACES  TO EH-KEY-1-ws.
           MOVE SY-ST-CPNO TO EH-CPNO-1.
           MOVE DP-ETNO TO EH-ETNO-1.
           MOVE 2 TO EH-RCCD-1.
           MOVE 9 TO EH-SQCD-1.
           move eh-key-1-ws to eh-key-1.

      *    display eh-key-1 line 24 position 01.
      *    perform x050-reply thru x090-exit.

           perform X600-READ-EH thru x600-exit.
           if ws-ersw = "Y"
               MOVE "H9" TO FILE-status
               GO TO Z100-FILE-ERR.
           move dp-dkd1 to EH-SGDC.
           perform X700-REWRITE-EH thru x700-exit.
       H800-qty-hdr.
           if ws-qty-chg-sw = "N"
               go to H810-review-job-hist.
           MOVE SPACES  TO EH-KEY-1-ws.
           MOVE SY-ST-CPNO TO EH-CPNO-1.
           MOVE DP-ETNO TO EH-ETNO-1.
           MOVE 3 TO EH-RCCD-1.
           MOVE dp-qtcd TO EH-SQCD-1.
           move eh-key-1-ws to eh-key-1.
           perform X600-READ-EH thru x600-exit.
           if ws-ersw = "Y"
               MOVE "H9" TO FILE-status
               GO TO Z100-FILE-ERR.
           move dp-qnty to EH-ETQT.
           move dp-qtpr to EH-QTPR.
           perform X700-REWRITE-EH thru x700-exit.

       H810-review-job-hist.
           IF WS-CRSW NOT = "Y"
               GO TO H990-EXIT.
       H810-FLIP-job.
           MOVE ws-job-no TO LS-UVFDNO-DKNO.
           PERFORM X240-UVFDNO THRU X245-EXIT.
       H820-READ-CD.
           MOVE SPACES     TO CD-KEY-1.
           MOVE SY-ST-CPNO    TO CD-CPNO-K1.
           MOVE WS-SV-CTNO TO CD-CTNO-K1.
           MOVE LS-UVFDNO-DKNO TO CD-DKNO-K1.
           READ CD-FILE
               INTO CD-RCRD-WS
               KEY IS CD-KEY-1
             INVALID KEY
               GO TO H990-EXIT.
           IF RECORD-LOCKED
               MOVE "CDRF" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO H820-READ-CD.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       H840-DELETE-CD.
           DELETE CD-FILE RECORD.
           IF NOT FILE-OK
               MOVE "DL" TO FILE-IO
               GO TO Z100-FILE-ERR.
       H850-SET-NEW-RECORD.
           MOVE DP-CTNO TO WS-CTNO.
           MOVE WS-CTNO TO CD-CTNO-1.
           MOVE DP-PVDK TO CD-RFNO.
           MOVE DP-QNTY TO CD-QNTY.
           MOVE DP-DKD1 TO CD-DKD1.
           MOVE DP-TGDT TO CD-TRDT.
           MOVE DP-QTPR TO CD-JBSP.
           IF WS-SV-ETNO = DP-ETNO
             AND WS-SV-QTCD = DP-QTCD
               GO TO H860-WRITE-CD.
           MOVE WS-SV-ETCT TO CD-JBCT.
       H860-WRITE-CD.
           PERFORM X710-WRITE-CD THRU X715-EXIT.
           GO TO H990-EXIT.
       
       H900-UNLOCK-DP.
           UNLOCK DP-FILE RECORD.
           IF NOT FILE-OK
               MOVE "UL" TO FILE-IO
               GO TO Z100-FILE-ERR.

       H990-EXIT.
           EXIT.
      
       K000-CREDIT-CHECK.
           DISPLAY "CREDIT CHECK IN PROGRESS"
               LINE 24 POSITION 01 HIGH.
       K010-SET-VALUES.
           MOVE SPACES TO WS-ERSW.
           MOVE ZEROS  TO WS-CCAM.
       K090-START-ID.
           MOVE SPACES  TO ID-KEY-1.
           MOVE SY-ST-CPNO TO ID-CPNO-K1.
           MOVE WS-CTNO TO ID-CTNO-K1.
           START ID-FILE
               KEY IS NOT LESS THAN ID-KEY-1
             INVALID KEY
               GO TO K290-START-CD.
           IF RECORD-LOCKED
               MOVE "ARID" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO K090-START-ID.
           IF NOT FILE-OK
               MOVE "ST" TO FILE-IO
               GO TO Z100-FILE-ERR.
       K100-READ-ID.
           READ ID-FILE
               NEXT RECORD
               INTO ID-RCRD-WS
             AT END
               GO TO K290-START-CD.
           IF NOT RECORD-LOCKED
               GO TO K110-CHECK-FILE.
           MOVE "ARID" TO WS-LOCK-FILE.
           PERFORM Z200-ACK-LOCK THRU Z290-EXIT.
           GO TO K100-READ-ID.
       
       K110-CHECK-FILE.
           IF NOT FILE-OK
               MOVE "RS" TO FILE-IO
               GO TO Z100-FILE-ERR.
           IF ID-CPNO-1 NOT = SY-ST-CPNO
               GO TO K290-START-CD.
           IF ID-CTNO-1 NOT = WS-CTNO
               GO TO K290-START-CD.
       K120-REVIEW-ID.
           move id-trdt        to ls-uvdtcv-indt.
           move "CM"           to ls-uvdtcv-cmcd.
           perform x999-uvdtcv thru x999-exit.
           IF ls-uvdtcv-otdt > SY-ST-SODT
               GO TO K100-READ-ID.
       K130-ADD-VALUES.
           ADD ID-TRAM TO WS-CCAM.
           GO TO K100-READ-ID.
      *
       K290-START-CD.
           MOVE SPACES  TO CD-KEY-1.
           MOVE SY-ST-CPNO TO CD-CPNO-K1.
           MOVE WS-CTNO TO CD-CTNO-K1.
           START CD-FILE
               KEY IS NOT LESS THAN CD-KEY-1
             INVALID KEY
               GO TO K500-ADD-QUOTE.
           IF RECORD-LOCKED
               MOVE "CDRF" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO K290-START-CD.
           IF NOT FILE-OK
               MOVE "ST" TO FILE-IO
               GO TO Z100-FILE-ERR.
       K300-READ-CD.
           READ CD-FILE
               NEXT RECORD
               WITH NO LOCK
               INTO CD-RCRD-WS
             AT END
               GO TO K500-ADD-QUOTE.
           IF NOT RECORD-LOCKED
               GO TO K310-CHECK-FILE.
           MOVE "CDRF" TO WS-LOCK-FILE.
           PERFORM Z200-ACK-LOCK THRU Z290-EXIT.
           GO TO K300-READ-CD.
      *
       K310-CHECK-FILE.
           IF NOT FILE-OK
               MOVE "RS" TO FILE-IO
               GO TO Z100-FILE-ERR.
           IF CD-CPNO-1 NOT = SY-ST-CPNO
               GO TO K500-ADD-QUOTE.
           IF CD-CTNO-1 NOT = WS-CTNO
               GO TO K500-ADD-QUOTE.
       K320-REVIEW-CD.
           IF CD-STCD NOT = "A"
               GO TO K300-READ-CD.
       K330-ADD-VALUES.
           ADD CD-JBSP TO WS-CCAM.
           GO TO K300-READ-CD.
      *
       K500-ADD-QUOTE.
           ADD WS-QTPR TO WS-CCAM.
       K600-REVIEW-CREDIT.
           IF WS-CCAM NOT > CT-CDLM
               GO TO K900-CLEAR-SCREEN.
       K700-LIMIT-EXCEEDED.
           DISPLAY "CUSTOMER CREDIT LIMIT HAS BEEN EXCEEDED "
                       LINE 24 POSITION 1 HIGH.
           ACCEPT ws-cl-exceeded-ack 
               LINE 00 POSITION 00 
               TAB OFF.
           DISPLAY SPACES LINE 24 POSITION 01 SIZE 80.
           if ws-cl-exceeded-ack = "OVERRIDE"
               MOVE "N" TO WS-ERSW
             else
               MOVE "Y" TO WS-ERSW.
       K900-CLEAR-SCREEN.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
       K990-EXIT.
           EXIT.

       P000-print-sales-order. 
           if sw-5-on
               move ws-sv-etct to pr-etct
               compute ws-etmg = dp-qtpr - ws-sv-etct
               move ws-etmg to pr-etmg.
           if sw-5-on and ws-est-linked-sw = "N"
               move zeros to pr-etct
               move zeros to pr-etmg.
           if sw-3-on
               move "Setup By" to lit-ref-sb
             else
               move "Reference" to lit-ref-sb.
           write pr-rcrd from pr-dash2      after advancing page.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-line1      after advancing 0 lines.
           write pr-rcrd from pr-line1      after advancing 0 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-line2      after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-line3      after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-line4      after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-line5      after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-line6      after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-line7      after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-line8      after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-line9      after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-line10     after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-line11     after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-line12     after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-line13     after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-dash       after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-line14     after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-underscore after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-underscore after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-underscore after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-underscore after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-underscore after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-underscore after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-underscore after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-underscore after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-underscore after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-underscore after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-underscore after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-underscore after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-underscore after advancing 1 lines.
           write pr-rcrd from pr-blank      after advancing 1 lines.
           write pr-rcrd from pr-underscore after advancing 1 lines.
       p990-exit.
           exit.
      
       R000-DISPLaY-est-HDR.
           perform X900-CLEAR-SCREEN thru x905-exit.
           MOVE dp-qnty TO DS-qnty.
           MOVE dp-stdt TO DS-DATE.

           display "JOB #"         line 07 position 03 high
                   "QUANTITY"      line 07 position 17 high
                   "DATE"          LINE 07 POSITION 66 HIGH.

           display "CUSTOMER"      LINE 08 POSITION 03 HIGH
                   "SALESMAN"      LINE 08 POSITION 34 HIGH
                   "PRODUCT CLASS" LINE 08 POSITION 61 HIGH.

           display dp-dkno-1       LINE 07 POSITION 9
                   DS-qnty         LINE 07 POSITION 26
                   DS-DATE         LINE 07 POSITION 71.

           display CT-CTNM         LINE 08 POSITION 12 SIZE 20
                   SM-SMNM         LINE 08 POSITION 43 SIZE 16
                   dp-pdtp         LINE 08 POSITION 75.
       R990-EXIT.
           EXIT.

       V000-enter-estimate. 
       V100-est-job-hdr.
           MOVE ZEROS      TO EH-KEY-1.
           MOVE sy-st-CPNO TO EH-CPNO-K1.
           PERFORM X600-READ-EH THRU X600-EXIT.
           IF WS-ERSW = "Y"
               MOVE "V1" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
           move eh-naen to ws-etno.
           display "ESTIMATE # " line 9 position 3 high.
           display ws-etno line 9 position 14.
           display "-> DETAIL "
               line 9 position 19 high.
           display "ENTRY/DISPLAY/MAINTENANCE ... "
               line 0 position 0 high.
           perform X720-bump-est-no thru x720-exit.
           move spaces to eh-rcrd-ws.
           move sy-st-cpno to eh-cpno-1.
           move ws-etno to eh-etno-1.
           move 1 to eh-rccd-1.
           move 0 to eh-sqcd-1.
           move sy-st-cpno to eh-cpno-2.
           move dp-smno to eh-smno-2.
           move dp-ctno to eh-ctno-2.
           move ws-etno to eh-etno-2.
           move 1 to eh-rccd-2.
           move 0 to eh-sqcd-2.
           move sy-st-usid to EH-ETIT.
           move dp-stdt to EH-ETDT.
           move ct-ctnm to EH-CTAL.
           move dp-dkd1 to EH-ETDC.
           move 1 to EH-OSQT.
           move dp-dkno-1 to EH-DKNO.
           move dp-pvdk to EH-PVDK.
           move dp-mupc to EH-MUPC.
           move dp-dcpc to EH-DCPC.
           move dp-pdtp to EH-PCCD.
           move "N" to EH-JASW.
           perform X702-WRITE-EH thru x702-exit.
           move eh-rcrd-ws to eh-rcrd-ws-01.
       V100-est-seg-hdr.
           move 2 to eh-rccd-1.
           move 1 to eh-sqcd-1.
           move 2 to eh-rccd-2.
           move 1 to eh-sqcd-2.
           move spaces to eh-data.
           move dp-dkd1 to EH-SGDC.
           move "C" to EH-SGST.
           move "A" to EH-SGQC.
           move 0 to EH-IDSG.
           perform X702-WRITE-EH thru x702-exit.
           move eh-rcrd-ws to eh-rcrd-ws-02.
       V100-est-seg9-hdr.
           move 2 to eh-rccd-1.
           move 9 to eh-sqcd-1.
           move 2 to eh-rccd-2.
           move 9 to eh-sqcd-2.
           move spaces to eh-data.
           move dp-dkd1 to EH-SGDC.
           move "C" to EH-SGST.
           move "A" to EH-SGQC.
           move 0 to EH-IDSG.
           perform X702-WRITE-EH thru x702-exit.
       V100-est-qty-hdr.
           move 3 to eh-rccd-1.
           move 1 to eh-sqcd-1.
           move 3 to eh-rccd-2.
           move 1 to eh-sqcd-2.
           move spaces to eh-data.
           move dp-qnty to EH-ETQT.
           move "C" to EH-QTST.
           move zeros to EH-ETCT.
           move dp-qtpr to EH-QTPR.
           perform X702-WRITE-EH thru x702-exit.
           move eh-rcrd-ws to eh-rcrd-ws-03.

           move zeros to ls-jcordm-cost.
       V200-est-dtl.
           move sy-st-cpno to ls-jcordm-cpno.
           move spaces to ls-jcordm-cmcd.
           call "JCEDTE" using 
               ls-jcordm-rcrd
               eh-rcrd-ws-01
               eh-rcrd-ws-02
               eh-rcrd-ws-03.
           IF LS-JCORDM-CMCD = "MT"
               GO TO V300-MATERIAL.
           IF LS-JCORDM-CMCD = "RS"
               GO TO V400-RESOURCE.
           IF LS-JCORDM-CMCD = "LB"
               GO TO V500-LABOUR.
           IF LS-JCORDM-CMCD = "F2"
               GO TO V900-mop-up.
           move ls-jcordm-cmcd to file-io.
           MOVE "V2" TO FILE-STATUS
           GO TO Z100-FILE-ERR.
       
       V300-MATERIAL.
           MOVE "00" TO LS-JCORDM-CMCD.
           if sw-4-on
               go to V300-mat-simple.
           CALL "JCED05" USING
               LS-JCORDM-RCRD
               EH-RCRD-WS-01
               EH-RCRD-WS-02
               EH-RCRD-WS-03.
           IF LS-JCORDM-CMCD = "F2"
               GO TO V200-est-dtl.
           move ls-jcordm-cmcd to file-io.
           MOVE "V3" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
       V300-mat-simple.
           CALL "JCED06" USING
               LS-JCORDM-RCRD
               EH-RCRD-WS-01
               EH-RCRD-WS-02
               EH-RCRD-WS-03.
           GO TO V200-est-dtl.

       V400-resource.
           MOVE "00" TO LS-JCORDM-CMCD.
           if sw-4-on
               go to V400-res-simple.
           CALL "JCED10" USING
               LS-JCORDM-RCRD
               EH-RCRD-WS-01
               EH-RCRD-WS-02
               EH-RCRD-WS-03.
           IF LS-JCORDM-CMCD = "F2"
               GO TO V200-est-dtl.
           move ls-jcordm-cmcd to file-io.
           MOVE "V4" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
       V400-res-simple.
           CALL "JCED11" USING
               LS-JCORDM-RCRD
               EH-RCRD-WS-01
               EH-RCRD-WS-02
               EH-RCRD-WS-03.
           GO TO V200-est-dtl.

       V500-labour.
           MOVE "00" TO LS-JCORDM-CMCD.
           CALL "JCED15" USING
               LS-JCORDM-RCRD
               EH-RCRD-WS-01
               EH-RCRD-WS-02
               EH-RCRD-WS-03.
           IF LS-JCORDM-CMCD = "F2"
               GO TO V200-est-dtl.
           move ls-jcordm-cmcd to file-io.
           MOVE "V5" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.

       V900-mop-up.
           move eh-etct-03 to ws-sv-etct.
       V990-exit.
           exit.
      
       X000-FUNC-ERR.
           DISPLAY "FUNCTION KEY NOT VALID "
               LINE 24 POSITION 1 HIGH.
       X050-REPLY.
           ACCEPT DUMMY-77 LINE 00 POSITION 00 TAB OFF.
           DISPLAY SPACES LINE 24 POSITION 01 SIZE 80.
       X090-EXIT.
           EXIT.
      
       X100-NO-MAINT.
           DISPLAY "MAINTENANCE NOT ALLOWED "
               LINE 24 POSITION 1 HIGH.
           PERFORM X050-REPLY THRU X090-EXIT.
       X105-EXIT.
           EXIT.
      
       X150-EDIT-SCREEN-1.
           MOVE SPACES TO WS-ERSW.
           IF WS-FIELD < 01
             OR WS-FIELD > 13
               MOVE "Y" TO WS-ERSW
               GO TO X155-EXIT.
       X150-CHECK-job.
           IF SW-1-ON
               GO TO X155-EXIT.
           IF DP-STCD = 9
               DISPLAY "JOB CLOSED - MAINTENANCE NOT ALLOWED "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               MOVE "Y" TO WS-ERSW
               GO TO X155-EXIT.
           IF DP-STCD NOT = 1
               DISPLAY "JOB 'IN TRANSIT' - MAINTENANCE NOT ALLOWED "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               MOVE "Y" TO WS-ERSW.
       X155-EXIT.
           EXIT.
      
       X160-EDIT-SCREEN-2.
           MOVE SPACES TO WS-ERSW.
           IF WS-FIELD < 21
             OR WS-FIELD > 32
               MOVE "Y" TO WS-ERSW
               GO TO X165-EXIT.
       X160-CHECK-job.
           IF SW-1-ON
               GO TO X165-EXIT.
           IF DP-STCD = 9
               DISPLAY "JOB CLOSED - MAINTENANCE NOT ALLOWED "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               MOVE "Y" TO WS-ERSW
               GO TO X165-EXIT.
           IF DP-STCD NOT = 1
               DISPLAY "JOB NOT ACTIVE - MAINTENANCE NOT ALLOWED "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               MOVE "Y" TO WS-ERSW.
       X165-EXIT.
           EXIT.
      
       X200-UVGCNO.
           MOVE SY-ST-CPNO TO LS-UVGCNO-CPNO.
           MOVE 11      TO LS-UVGCNO-LINE.
           MOVE 36      TO LS-UVGCNO-COLM.
           MOVE SPACES  TO LS-UVGCNO-INSW.
           CALL "UVGCNO" USING
               LS-UVGCNO-RCRD.
       X205-EXIT.
           EXIT.
      
       X210-UVGSNO.
           MOVE "NP"    TO LS-UVGSNO-CMCD.
           MOVE SY-ST-CPNO TO LS-UVGSNO-CPNO.
           MOVE 12      TO LS-UVGSNO-LINE.
           MOVE 36      TO LS-UVGSNO-COLM.
           MOVE SPACES  TO LS-UVGSNO-INSW.
           CALL "UVGSNO" USING
               LS-UVGSNO-RCRD.
       X215-EXIT.
           EXIT.
      
       X230-UVACDT.
           MOVE "NP" TO LS-UVACDT-CMCD.
           MOVE 33   TO LS-UVACDT-COLM.
           CALL "UVACDT" USING
               LS-UVACDT-RCRD.
       X235-EXIT.
           EXIT.
      
       X240-UVFDNO.
           MOVE "00" TO LS-UVFDNO-CMCD.
           CALL "UVFDNO" USING
               LS-UVFDNO-RCRD.
       X245-EXIT.
           EXIT.
      
       X300-EDIT-ESTIMATE.
           MOVE SPACES TO WS-ERSW.
           IF WS-ETNO NOT NUMERIC
               DISPLAY "ESTIMATE NUMBER MUST BE NUMERIC "
                   LINE 24 POSITION 1 HIGH
               GO TO X305-ACK-ERROR.
           IF WS-ETNO = ZERO
               MOVE SPACES TO EH-ETDC
               GO TO X305-EXIT.
           PERFORM X500-SET-READ-ESTIMATE THRU X505-EXIT.
           IF WS-ERSW = "Y"
               DISPLAY "ESTIMATE NOT ON FILE "
                   LINE 24 POSITION 1 HIGH
               GO TO X305-ACK-ERROR.
           PERFORM X810-UNLOCK-EH THRU X815-EXIT.
           IF EH-JASW = "Y"
               DISPLAY "THIS ESTIMATE IS CURRENTLY ACTIVE "
                   LINE 24 POSITION 1 HIGH
               GO TO X305-ACK-ERROR.
           IF EH-OSQT = ZERO
               DISPLAY "NO QUANTITY HEADERS ON FILE FOR THIS ESTIMATE "
                   LINE 24 POSITION 1 HIGH
               GO TO X305-ACK-ERROR.
           IF ws-mainline-control = "AD"
             AND EH-DKNO NOT = ZERO
               DISPLAY "THIS ESTIMATE IS ASSIGNED TO ANOTHER JOB "
                   LINE 24 POSITION 1 HIGH
               GO TO X305-ACK-ERROR.
           IF ws-mainline-control = "CH"
             AND WS-ETNO NOT = WS-SV-ETNO
             AND EH-DKNO NOT = ZERO
               DISPLAY "THIS ESTIMATE IS ASSIGNED TO ANOTHER JOB "
                   LINE 24 POSITION 1 HIGH
               GO TO X305-ACK-ERROR.
           GO TO X305-EXIT.
       X305-ACK-ERROR.
           PERFORM X050-REPLY THRU X090-EXIT.
           MOVE "Y" TO WS-ERSW.
       X305-EXIT.
           EXIT.
      
       X310-EDIT-QUANTITY-CODE.
           MOVE SPACES TO WS-ERSW.
           IF WS-QTCD NOT NUMERIC
               DISPLAY "QUANTITY CODE MUST BE NUMERIC "
                   LINE 24 POSITION 1 HIGH
               GO TO X315-ACK-ERROR.
           IF WS-QTCD = ZERO
               DISPLAY "QUANTITY CODE OF ZERO NOT VALID "
                   LINE 24 POSITION 1 HIGH
               GO TO X315-ACK-ERROR.
           PERFORM X510-SET-READ-QUANTITY THRU X515-EXIT.
           IF WS-ERSW = "Y"
               DISPLAY "QUANTITY CODE NOT VALID FOR THIS ESTIMATE "
                   LINE 24 POSITION 1 HIGH
               GO TO X315-ACK-ERROR.
           PERFORM X810-UNLOCK-EH THRU X815-EXIT.
           IF EH-QTST = "I"
               DISPLAY "QUANTITY CODE STATUS NOT VALID "
                   LINE 24 POSITION 1 HIGH
               GO TO X315-ACK-ERROR.
           GO TO X315-EXIT.
       X315-ACK-ERROR.
           PERFORM X050-REPLY THRU X090-EXIT.
           MOVE "Y" TO WS-ERSW.
       X315-EXIT.
           EXIT.
      
       X320-EDIT-PRODUCT-CLASS.
           MOVE SPACES TO WS-ERSW.
           IF WS-PCCD = SPACES
               DISPLAY "PRODUCT CLASS IS MANDATORY "
                   LINE 24 POSITION 1 HIGH
               GO TO X325-ACK-ERROR.
           PERFORM X630-READ-MM THRU X635-EXIT.
           IF WS-ERSW = "Y"
               DISPLAY "PRODUCT CLASS NOT ON FILE "
                   LINE 24 POSITION 1 HIGH
               GO TO X325-ACK-ERROR.
           IF MM-61-TPCD NOT = "PD"
               DISPLAY "PRODUCT CLASS NOT VALID - "
                           LINE 24 POSITION 1 HIGH
                       "TYPE MUST BE 'PD' "
                           LINE 00 POSITION 0 HIGH
               GO TO X325-ACK-ERROR.
           GO TO X325-EXIT.
       X325-ACK-ERROR.
           PERFORM X050-REPLY THRU X090-EXIT.
           MOVE "Y" TO WS-ERSW.
       X325-EXIT.
           EXIT.
      
       X330-EDIT-QUOTED-PRICE.
           MOVE SPACES TO WS-ERSW.
           IF WS-QTPR < ZERO
               DISPLAY "QUOTED PRICE MAY NOT BE LESS THAN ZERO "
                   LINE 24 POSITION 1 HIGH
               GO TO X335-ACK-ERROR.
           GO TO X335-EXIT.
       X335-ACK-ERROR.
           PERFORM X050-REPLY THRU X090-EXIT.
           MOVE "Y" TO WS-ERSW.
       X335-EXIT.
           EXIT.
      
       X340-EDIT-QUANTITY.
           MOVE SPACES TO WS-ERSW.
           IF WS-QNTY < ZERO
               DISPLAY "QUANTITY MAY NOT BE LESS THAN ZERO "
                   LINE 24 POSITION 1 HIGH
               GO TO X345-ACK-ERROR.
           GO TO X345-EXIT.
       X345-ACK-ERROR.
           PERFORM X050-REPLY THRU X090-EXIT.
           MOVE "Y" TO WS-ERSW.
       X345-EXIT.
           EXIT.
      
       X350-EDIT-DESCRIPTION-1.
           MOVE SPACES TO WS-ERSW.
           IF WS-DKDC = SPACES
               DISPLAY "DESCRIPTION 1 IS MANDATORY "
                   LINE 24 POSITION 1 HIGH
               GO TO X355-ACK-ERROR.
           GO TO X355-EXIT.
       X355-ACK-ERROR.
           PERFORM X050-REPLY THRU X090-EXIT.
           MOVE "Y" TO WS-ERSW.
       X355-EXIT.
           EXIT.
      
       X360-EDIT-PERCENT.
           MOVE SPACES TO WS-ERSW.
           IF WS-PRCT < ZERO
               DISPLAY "PERCENT MAY NOT BE LESS THAN ZERO "
                   LINE 24 POSITION 1 HIGH
               GO TO X365-ACK-ERROR.
           GO TO X365-EXIT.
       X365-ACK-ERROR.
           PERFORM X050-REPLY THRU X090-EXIT.
           MOVE "Y" TO WS-ERSW.
       X365-EXIT.
           EXIT.
      
       X370-EDIT-PREV-job.
           MOVE SPACES TO WS-ERSW.
           INSPECT WS-PVDK
               REPLACING ALL " " BY "0".
           IF WS-PVDK NOT NUMERIC
               DISPLAY "JOB NUMBER MUST BE NUMERIC "
                   LINE 24 POSITION 1 HIGH
               GO TO X375-ACK-ERROR.
           GO TO X375-EXIT.
       X375-ACK-ERROR.
           PERFORM X050-REPLY THRU X090-EXIT.
           MOVE "Y" TO WS-ERSW.
       X375-EXIT.
           EXIT.
      
       X380-EDIT-JOB-TYPE.
           MOVE SPACES TO WS-ERSW.
           IF WS-JBTP = SPACES
               DISPLAY "JOB TYPE IS MANDATORY "
                   LINE 24 POSITION 1 HIGH
               GO TO X385-ACK-ERROR.
           IF NOT VALID-JOB-TYPE
               DISPLAY "JOB TYPE MUST BE NW, RR, RC OR XX "
                   LINE 24 POSITION 1 HIGH
               GO TO X385-ACK-ERROR.
           GO TO X385-EXIT.
       X385-ACK-ERROR.
           PERFORM X050-REPLY THRU X090-EXIT.
           MOVE "Y" TO WS-ERSW.
       X385-EXIT.
           EXIT.
      
       X390-EDIT-WORK-CENTRE.
           MOVE SPACES TO WS-ERSW.
           IF WS-WCNO NOT NUMERIC
               DISPLAY "WORK CENTRE NUMBER MUST BE NUMERIC "
                   LINE 24 POSITION 1 HIGH
               GO TO X395-ACK-ERROR.
           IF WS-WCNO = ZERO
               MOVE SPACES TO WC-WCDC
               GO TO X395-EXIT.
           PERFORM X640-READ-WC THRU X645-EXIT.
           IF WS-ERSW = "Y"
               DISPLAY "WORK CENTRE NOT ON FILE "
                   LINE 24 POSITION 1 HIGH
               GO TO X395-ACK-ERROR.
           GO TO X395-EXIT.
       X395-ACK-ERROR.
           PERFORM X050-REPLY THRU X090-EXIT.
           MOVE "Y" TO WS-ERSW.
       X395-EXIT.
           EXIT.
      
       X400-EDIT-STARTING-DATE.
           MOVE SPACES TO WS-ERSW.
           if sw-3-on
               go to X400-other-edit.
 
           IF WS-DATE > SY-ST-SODT
               DISPLAY "STARTING DATE MUST BE <= SIGNON DATE "
                   LINE 24 POSITION 1 HIGH
               GO TO X405-ACK-ERROR.
           GO TO X405-EXIT.

       X400-other-edit.
           IF WS-DATE < SY-ST-SODT
               DISPLAY "STARTING DATE MUST BE >= SIGNON DATE "
                   LINE 24 POSITION 1 HIGH
               GO TO X405-ACK-ERROR.
           GO TO X405-EXIT.

       X405-ACK-ERROR.
           PERFORM X050-REPLY THRU X090-EXIT.
           MOVE "Y" TO WS-ERSW.
       X405-EXIT.
           EXIT.
      
       X410-EDIT-TARGET-DATE.
           MOVE SPACES TO WS-ERSW.
           IF WS-DATE < DP-STDT
               DISPLAY "TARGET DATE MAY NOT BE < STARTING DATE "
                   LINE 24 POSITION 1 HIGH
               GO TO X415-ACK-ERROR.
           GO TO X415-EXIT.
       X415-ACK-ERROR.
           PERFORM X050-REPLY THRU X090-EXIT.
           MOVE "Y" TO WS-ERSW.
       X415-EXIT.
           EXIT.
      
       X500-SET-READ-ESTIMATE.
           MOVE SPACES  TO EH-KEY-1 WS-ERSW.
           MOVE SY-ST-CPNO TO EH-CPNO-K1.
           MOVE WS-ETNO TO EH-ETNO-K1.
           MOVE "1"     TO EH-RCCD-K1.
           MOVE "0"     TO EH-SQCD-K1.
           PERFORM X600-READ-EH THRU X600-EXIT.
           IF WS-ERSW = "Y"
               MOVE "* ESTIMATE NOT ON FILE *" TO EH-ETDC.
       X505-EXIT.
           EXIT.
      
       X510-SET-READ-QUANTITY.
           MOVE SPACES  TO EH-KEY-1 WS-ERSW.
           MOVE SY-ST-CPNO TO EH-CPNO-K1.
           MOVE WS-ETNO TO EH-ETNO-K1.
           MOVE "3"     TO EH-RCCD-K1.
           MOVE WS-QTCD TO EH-SQCD-K1.
           PERFORM X600-READ-EH THRU X600-EXIT.
       X515-EXIT.
           EXIT.
      
       X600-READ-EH.
           MOVE SPACES TO WS-ERSW.
           READ EH-FILE
               INTO EH-RCRD-WS
               KEY IS EH-KEY-1
             INVALID KEY
               MOVE "Y" TO WS-ERSW
               GO TO X600-EXIT.
           IF RECORD-LOCKED
               MOVE "ETHD" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X600-READ-EH.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X600-EXIT.
           EXIT.
      
       X610-READ-CT.
           MOVE SPACES  TO CT-KEY-1 WS-ERSW.
           MOVE SY-ST-CPNO TO CT-CPNO-K1.
           MOVE WS-CTNO TO CT-CTNO-K1.
           READ CT-FILE
               INTO CT-RCRD-WS
               KEY IS CT-KEY-1
             INVALID KEY
               MOVE "Y" TO WS-ERSW
               MOVE "* CUSTOMER NOT ON FILE *" TO CT-CTNM
               GO TO X615-EXIT.
           IF RECORD-LOCKED
               MOVE "CTMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X610-READ-CT.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X615-EXIT.
           EXIT.
      
       X620-READ-SM.
           MOVE SPACES  TO SM-KEY-1 WS-ERSW.
           MOVE SY-ST-CPNO TO SM-CPNO-K1.
           MOVE WS-SMNO TO SM-SMNO-K1.
           READ SM-FILE
               INTO SM-RCRD-WS
               KEY IS SM-KEY-1
             INVALID KEY
               MOVE "Y" TO WS-ERSW
               MOVE "* SALESMAN NOT ON FILE *" TO SM-SMNM
               GO TO X625-EXIT.
           IF RECORD-LOCKED
               MOVE "SMMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X620-READ-SM.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X625-EXIT.
           EXIT.
      
       X630-READ-MM.
           MOVE SPACES         TO WS-ERSW.
           MOVE SPACES         TO MM-61-KEY-1-WS MM-KEY-1.
           MOVE SY-ST-CPNO        TO MM-61-CPNO-1.
           MOVE 61             TO MM-61-LFCD-1.
           MOVE WS-PCCD        TO MM-61-PCCD-1.
           MOVE MM-61-KEY-1-WS TO MM-KEY-1.
           READ MM-FILE
               INTO MM-61-RCRD-WS
               KEY IS MM-KEY-1
             INVALID KEY
               MOVE "Y" TO WS-ERSW
               GO TO X635-EXIT.
           IF RECORD-LOCKED
               MOVE "STMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X630-READ-MM.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X635-EXIT.
           EXIT.
      
       X640-READ-WC.
           MOVE SPACES     TO WS-ERSW.
           MOVE LOW-VALUES TO WC-KEY-1.
           MOVE SY-ST-CPNO    TO WC-CPNO-K1.
           MOVE WS-WCNO    TO WC-WCNO-K1.
           READ WC-FILE
               INTO WC-RCRD-WS
               KEY IS WC-KEY-1
             INVALID KEY
               MOVE "Y" TO WS-ERSW
               MOVE "* WC NOT ON FILE *" TO WC-WCDC
               GO TO X645-EXIT.
           IF RECORD-LOCKED
               MOVE "WCMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X640-READ-WC.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X645-EXIT.
           EXIT.
      
       X700-REWRITE-EH.
           REWRITE EH-RCRD FROM EH-RCRD-WS.
           IF NOT FILE-OK
               MOVE "RW" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X700-EXIT.
           EXIT.

       X702-WRITE-EH.
           WRITE EH-RCRD FROM EH-RCRD-WS.
           IF NOT FILE-OK
               MOVE "WR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X702-EXIT.
           EXIT.
      
       X710-WRITE-CD.
           WRITE CD-RCRD
               FROM CD-RCRD-WS
             INVALID KEY
               GO TO X710-READ-CD.
           IF NOT FILE-OK
               MOVE "WR" TO FILE-IO
               GO TO Z100-FILE-ERR.
           GO TO X715-EXIT.
       X710-READ-CD.
           MOVE CD-KEY-1-WS TO CD-KEY-1.
           READ CD-FILE
               KEY IS CD-KEY-1.
           IF RECORD-LOCKED
               MOVE "CDRF" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X710-READ-CD.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X710-DELETE.
           DELETE CD-FILE RECORD.
           IF NOT FILE-OK
               MOVE "DL" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X710-TRY-WRITE-AGAIN.
           WRITE CD-RCRD FROM CD-RCRD-WS.
           IF NOT FILE-OK
               MOVE "WR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X715-EXIT.
           EXIT.

       X720-bump-est-no.
           ADD 1 TO EH-NAEN.
           IF EH-NAEN = ZERO
               MOVE 1 TO EH-NAEN.
           REWRITE EH-RCRD FROM EH-RCRD-WS.
           IF NOT FILE-OK
               MOVE "RW" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X720-EXIT.
           EXIT.
      
       X800-UNLOCK-DP.
           UNLOCK DP-FILE RECORD.
           IF NOT FILE-OK
               MOVE "UL" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X805-EXIT.
           EXIT.
      
       X810-UNLOCK-EH.
           UNLOCK EH-FILE RECORD.
           IF NOT FILE-OK
               MOVE "UL" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X815-EXIT.
           EXIT.
      
       X900-CLEAR-SCREEN.
           DISPLAY SPACES LINE 21 POSITION 2 SIZE 78
                   SPACES LINE 20 POSITION 2 SIZE 78
                   SPACES LINE 19 POSITION 2 SIZE 78
                   SPACES LINE 18 POSITION 2 SIZE 78
                   SPACES LINE 17 POSITION 2 SIZE 78
                   SPACES LINE 16 POSITION 2 SIZE 78
                   SPACES LINE 15 POSITION 2 SIZE 78
                   SPACES LINE 14 POSITION 2 SIZE 78
                   SPACES LINE 13 POSITION 2 SIZE 78
                   SPACES LINE 12 POSITION 2 SIZE 78
                   SPACES LINE 11 POSITION 2 SIZE 78
                   SPACES LINE 10 POSITION 2 SIZE 78
                   SPACES LINE 09 POSITION 2 SIZE 78
                   SPACES LINE 08 POSITION 2 SIZE 78
                   SPACES LINE 07 POSITION 2 SIZE 78.
       X905-EXIT.
           EXIT.
      
       X910-display-screen-1-lit.
           IF ws-mainline-control = "AD"
               DISPLAY "NEW JOB" LINE 07 POSITION 11 HIGH.
           IF ws-mainline-control = "CH"
               DISPLAY "JOB NO" LINE 07 POSITION 11 HIGH
               display ws-job-no line 07 position 35.
           DISPLAY
               "01 ESTIMATE      "   LINE 09 POSITION 08 HIGH
               "02 QUANTITY CODE "   LINE 10 POSITION 08 HIGH
               "03 CUSTOMER      "   LINE 11 POSITION 08 HIGH
               "04 SALESMAN      "   LINE 12 POSITION 08 HIGH
               "05 PRODUCT CLASS "   LINE 13 POSITION 08 HIGH
               "06 QUOTED PRICE  "   LINE 14 POSITION 08 HIGH
               "07 QUANTITY      "   LINE 15 POSITION 08 HIGH
               "08 DESCRIPTION 1 "   LINE 16 POSITION 08 HIGH
               "09 DESCRIPTION 2 "   LINE 17 POSITION 08 HIGH
               "10 MARK UP %     "   LINE 18 POSITION 08 HIGH
               "11 FACTOR LABOUR?"   LINE 19 POSITION 08 HIGH
               "12 DISCOUNT %    "   LINE 20 POSITION 08 HIGH
               "13 PREV JOB   "   LINE 21 POSITION 08 HIGH.
           MOVE 1 TO WS-CSCD.
       X915-EXIT.
           EXIT.
      
       X920-CLEAR-SCREEN-1.
           DISPLAY SPACES LINE 21 POSITION 29 SIZE 50
                   SPACES LINE 20 POSITION 29 SIZE 50
                   SPACES LINE 19 POSITION 29 SIZE 50
                   SPACES LINE 18 POSITION 29 SIZE 50
                   SPACES LINE 17 POSITION 29 SIZE 50
                   SPACES LINE 16 POSITION 29 SIZE 50
                   SPACES LINE 15 POSITION 29 SIZE 50
                   SPACES LINE 14 POSITION 29 SIZE 50
                   SPACES LINE 13 POSITION 29 SIZE 50
                   SPACES LINE 12 POSITION 29 SIZE 50
                   SPACES LINE 11 POSITION 29 SIZE 50
                   SPACES LINE 10 POSITION 29 SIZE 50
                   SPACES LINE 09 POSITION 29 SIZE 50
                   SPACES LINE 08 POSITION 02 SIZE 77
                   SPACES LINE 07 POSITION 02 SIZE 77.
           DISPLAY "JOB NO" LINE 07 POSITION 11 HIGH.
       X925-EXIT.
           EXIT.
      
       x930-display-screen-2-lit.
           IF ws-mainline-control = "AD"
               DISPLAY "NEW JOB" LINE 07 POSITION 11 HIGH.
           IF ws-mainline-control = "CH"
               DISPLAY "JOB NO" LINE 07 POSITION 11 HIGH
               display ws-job-no line 07 position 35.
           DISPLAY "21 PURCHASE ORDER "  LINE 09 POSITION 08 HIGH
                   "23 CONTACT        "  LINE 11 POSITION 08 HIGH
                   "24 JOB TYPE       "  LINE 12 POSITION 08 HIGH
                   "25 PRIMARY WC     "  LINE 13 POSITION 08 HIGH
                   "26 SECONDARY WC   "  LINE 14 POSITION 08 HIGH
                   "27 STARTING DATE  "  LINE 15 POSITION 08 HIGH
                   "28 TARGET DATE    "  LINE 16 POSITION 08 HIGH
                   "29 SHIPPED DATE   "  LINE 17 POSITION 08 HIGH
                   "30 CLOSED DATE    "  LINE 18 POSITION 08 HIGH
                   "31 INVOICE NO     "  LINE 19 POSITION 08 HIGH
                   "32 PRE-BILL       "  LINE 20 POSITION 08 HIGH.
           if sw-3-on
               display "22 SETUP BY       "  LINE 10 POSITION 08 HIGH
             else
               display "22 REFERENCE      "  LINE 10 POSITION 08 HIGH.
           MOVE 2 TO WS-CSCD.
       X935-EXIT.
           EXIT.
      
       X940-DISPLY-DATA-SCREEN-1.
           MOVE DP-ETNO TO DS-ETNO WS-ETNO.
           DISPLAY DS-ETNO LINE 09 POSITION 37.
           IF DP-ETNO NOT = ZERO
               PERFORM X500-SET-READ-ESTIMATE THRU X505-EXIT
               DISPLAY EH-ETDC LINE 09 POSITION 43
               IF WS-ERSW NOT = "Y"
                   PERFORM X810-UNLOCK-EH THRU X815-EXIT.
           MOVE DP-QTCD TO WS-QTCD.
           DISPLAY DP-QTCD LINE 10 POSITION 40.
           IF DP-ETNO NOT = ZERO
               PERFORM X510-SET-READ-QUANTITY THRU X515-EXIT
               IF WS-ERSW NOT = "Y"
                   MOVE EH-ETQT TO DS-QNTY
                   DISPLAY DS-QNTY LINE 10 POSITION 43
                   PERFORM X810-UNLOCK-EH THRU X815-EXIT.
           MOVE DP-CTNO TO WS-CTNO.
           PERFORM X610-READ-CT THRU X615-EXIT.
           DISPLAY WS-CTNO LINE 11 POSITION 36
                   CT-CTNM LINE 11 POSITION 43.
           MOVE DP-SMNO TO WS-SMNO.
           PERFORM X620-READ-SM THRU X625-EXIT.
           DISPLAY WS-SMNO LINE 12 POSITION 36
                   SM-SMNM LINE 12 POSITION 43.
           MOVE DP-PDTP TO WS-PCCD.
           PERFORM X630-READ-MM THRU X635-EXIT.
           IF WS-ERSW = "Y"
               MOVE "* PRODUCT NOT ON FILE *" TO MM-61-DESC.
           DISPLAY WS-PCCD    LINE 13 POSITION 37
                   MM-61-DESC LINE 13 POSITION 43.
           MOVE DP-QTPR TO DS-AMNT.
           MOVE DP-QNTY TO DS-QNTY.
           DISPLAY DS-AMNT LINE 14 POSITION 29
                   DS-QNTY LINE 15 POSITION 30
                   DP-DKD1 LINE 16 POSITION 29
                   DP-DKD2 LINE 17 POSITION 29.
           MOVE DP-MUPC TO DS-PRCT.
           DISPLAY DS-PRCT LINE 18 POSITION 37
                   DP-FLSW LINE 19 POSITION 40.
           MOVE DP-DCPC TO DS-PRCT.
           DISPLAY DS-PRCT LINE 20 POSITION 37.
           MOVE DP-PVDK TO DS-DKNO.
           DISPLAY DS-DKNO LINE 21 POSITION 35.
       X945-EXIT.
           EXIT.
      
       X950-DISPLY-DATA-SCREEN-2.
           DISPLAY DP-CPON LINE 09 POSITION 33
                   DP-RFNO LINE 10 POSITION 31
                   DP-CTCT LINE 11 POSITION 29
                   DP-JBTP LINE 12 POSITION 39
                   DP-PWNO LINE 13 POSITION 38.
           IF DP-PWNO NOT = ZERO
               MOVE DP-PWNO TO WS-WCNO
               PERFORM X640-READ-WC THRU X645-EXIT
               DISPLAY WC-WCDC LINE 13 POSITION 43.
           DISPLAY DP-SWNO LINE 14 POSITION 38.
           IF DP-SWNO NOT = 000
               MOVE DP-SWNO TO WS-WCNO
               PERFORM X640-READ-WC THRU X645-EXIT
               DISPLAY WC-WCDC LINE 14 POSITION 43.
           MOVE DP-STDT TO DS-DATE.
           DISPLAY DS-DATE LINE 15 POSITION 33.
           MOVE DP-TGDT TO DS-DATE.
           DISPLAY DS-DATE LINE 16 POSITION 33.
           IF DP-SHDT NOT = ZERO
               MOVE DP-SHDT TO DS-DATE
               DISPLAY DS-DATE LINE 17 POSITION 33.
           IF DP-CLDT NOT = ZERO
               MOVE DP-CLDT TO DS-DATE
               DISPLAY DS-DATE LINE 18 POSITION 33.
           IF DP-IVNO NOT = ZERO
               MOVE DP-IVNO TO DS-IVNO
               DISPLAY DS-IVNO LINE 19 POSITION 35.
       X955-CONTINUE.
           MOVE DP-PBAM TO DS-AMNT.
           DISPLAY DS-AMNT LINE 20 POSITION 29.
       X955-EXIT.
           EXIT.
      
       X999-UVdtcv.
           CALL "UVDTCV" USING
               LS-UVDTCV-RCRD.
       X999-EXIT.
           EXIT.

       Y000-CLOSE.
           CLOSE SY-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE DP-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE EH-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE CT-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE SM-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE MM-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE cm-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE WC-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE CD-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE ID-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE PR-FILE no rewind.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
       Y990-EXIT.
           EXIT.
      
           COPY "uv/cpy/uvdfemov".
           COPY "uv/cpy/uvaflk".
