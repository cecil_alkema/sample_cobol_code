       IDENTIFICATION DIVISION.
       PROGRAM-ID.                 JCJBHI.
      *
      *        JOB COSTING
      *        JOB HISTORY INQUIRY
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           COPY "uv/cpy/slsymt".
           COPY "ar/cpy/slctmt".
           COPY "jc/cpy/slcdrf".

       DATA DIVISION.
       FILE SECTION.
           COPY "uv/cpy/fdsymt".
           COPY "ar/cpy/fdctmt".
           COPY "jc/cpy/fdcdrf".

       WORKING-STORAGE SECTION.

       77  MP-STATUS-77            PIC XX.
       77  SY-STATUS-77            PIC XX.
       77  CT-STATUS-77            PIC XX.
       77  CD-STATUS-77            PIC XX.
       77  SCREEN-ID               PIC X(35)   VALUE
           "$PMISDIR/appl/obj/c/dsplscrn jcjbhi".
       77  DUMMY-77                PIC X.
       77  MAIN-77                 PIC XX.

           COPY "uv/cpy/uvfckv".
           COPY "uv/cpy/uvpgid".
           COPY "uv/cpy/uvsfemov".
           COPY "uv/cpy/uvlkmg".

           COPY "uv/cpy/rcsymtst".
           COPY "uv/cpy/rcsymtcp".
           COPY "ar/cpy/rcctmt".
           COPY "jc/cpy/rccdrf".

           COPY "uv/cpy/lsuvcusf".
           COPY "uv/cpy/lsuvdssh".
           COPY "uv/cpy/lsuvgcno".
           COPY "uv/cpy/lsuvfdno".
           COPY "uv/cpy/lsuvacdt".
           COPY "uv/cpy/lsuvdtcv".

       01  CONTROL-FIELDS.
           03  WS-VLCD             PIC 9.
           03  WS-LINE             PIC 9(4).
           03  WS-DSSW             PIC X.
           03  WS-AJSW             PIC X.
           03  WS-DTSW             PIC X.
           03  ws-match-sw         pic x.
           03  WS-STCD             PIC XX.
               88 VALID-STCD VALUES ARE
                   "AT", "IV", "SM", "CP", "CL".
           03  WS-JBNO             PIC 9(6).
           03  WS-FDNO             PIC 9(6).
           03  WS-FDNO-START       PIC 9(6).
           03  WS-SCCT             PIC 99.
           03  WS-S-DATE           PIC 9(8).
           03  WS-E-DATE           PIC 9(8).
           03  WS-SODT             PIC 9(8).
           03  WS-SODT-2           REDEFINES WS-SODT.
               05  WS-SODT-YR      PIC 9999.
               05  WS-SODT-MT      PIC 99.
               05  WS-SODT-DY      PIC 99.
           03  ws-count            pic 99.
           03  ws-counter          pic 99.
           03  ws-desw             pic x.
           03  ws-desc             pic x(30).

       01  JOB-KEY-TABLE.
           03  JOB-KEY             OCCURS 99 TIMES PIC 9(6).
 
       01  description-table.
           03  ws-desc-char        occurs 30 times pic x.
 
       01  thirty-descriptions.
           03  desc-01             pic x(01).
           03  desc-02             pic x(02).
           03  desc-03             pic x(03).
           03  desc-04             pic x(04).
           03  desc-05             pic x(05).
           03  desc-06             pic x(06).
           03  desc-07             pic x(07).
           03  desc-08             pic x(08).
           03  desc-09             pic x(09).
           03  desc-10             pic x(10).
           03  desc-11             pic x(11).
           03  desc-12             pic x(12).
           03  desc-13             pic x(13).
           03  desc-14             pic x(14).
           03  desc-15             pic x(15).
           03  desc-16             pic x(16).
           03  desc-17             pic x(17).
           03  desc-18             pic x(18).
           03  desc-19             pic x(19).
           03  desc-20             pic x(20).
           03  desc-21             pic x(21).
           03  desc-22             pic x(22).
           03  desc-23             pic x(23).
           03  desc-24             pic x(24).
           03  desc-25             pic x(25).
           03  desc-26             pic x(26).
           03  desc-27             pic x(27).
           03  desc-28             pic x(28).
           03  desc-29             pic x(29).
           03  desc-30             pic x(30).

       01  STORAGE-FIELDS.
           03  ws-file-desc        pic x(30).
           03  WS-AT-JCSP          PIC S9(9)V99    COMP-3.
           03  WS-AT-JCCT          PIC S9(9)V99    COMP-3.
           03  WS-IV-JCSP          PIC S9(9)V99    COMP-3.
           03  WS-IV-JCCT          PIC S9(9)V99    COMP-3.
           03  WS-SM-JCSP          PIC S9(9)V99    COMP-3.
           03  WS-SM-JCCT          PIC S9(9)V99    COMP-3.
           03  WS-CP-JCSP          PIC S9(9)V99    COMP-3.
           03  WS-CP-JCCT          PIC S9(9)V99    COMP-3.
           03  WS-CL-JCSP          PIC S9(9)V99    COMP-3.
           03  WS-CL-JCCT          PIC S9(9)V99    COMP-3.

       01  DISPLAY-FIELDS.
           03  DS-JBNO             PIC ZZZZZ9.
           03  DS-DATE             PIC 999999.
           03  DS-DATE-2           PIC 99/99/99.
           03  DS-QNTY             PIC ZZZZZZZZ9-.
           03  DS-DKD1             PIC X(20).
           03  DS-RFNO             PIC ZZZZZ9.
           03  DS-STCD             PIC XX.
           03  DS-JBCT             PIC ZZZZZZZ.99-.
           03  DS-JBSP             PIC ZZZZZZZ.99-.
           03  DS-AT-JCSP          PIC ZZZZZZZZ.99-.
           03  DS-AT-JCCT          PIC ZZZZZZZZ.99-.
           03  DS-IV-JCSP          PIC ZZZZZZZZ.99-.
           03  DS-IV-JCCT          PIC ZZZZZZZZ.99-.
           03  DS-SM-JCSP          PIC ZZZZZZZZ.99-.
           03  DS-SM-JCCT          PIC ZZZZZZZZ.99-.
           03  DS-CP-JCSP          PIC ZZZZZZZZ.99-.
           03  DS-CP-JCCT          PIC ZZZZZZZZ.99-.
           03  DS-CL-JCSP          PIC ZZZZZZZZ.99-.
           03  DS-CL-JCCT          PIC ZZZZZZZZ.99-.

       PROCEDURE DIVISION.

       DECLARATIVES.
           COPY "uv/cpy/dcsymt".
           COPY "ar/cpy/dcctmt".
           COPY "jc/cpy/dccdrf".
       END DECLARATIVES.

       A000-MAINLINE-PGM SECTION.

       A000-START-PROGRAM.
           PERFORM B000-INIT THRU B990-EXIT.
       A100-MAINLINE.
           PERFORM D000-CUSTOMER THRU D990-EXIT.
           IF MAIN-77 = "QT"
               GO TO A800-CLOSING.
       A200-PARAMETERS.
           PERFORM F000-PARAMETERS THRU F990-EXIT.
           IF MAIN-77 = "QT"
               GO TO A100-MAINLINE.
           PERFORM H000-PROCESS THRU H990-EXIT.
           IF MAIN-77 = "PM"
               GO TO A200-PARAMETERS.
           GO TO A100-MAINLINE.
       A800-CLOSING.
           PERFORM Y000-CLOSE THRU Y990-EXIT.
       A990-EXIT.
           STOP RUN.

       B000-INIT.
           MOVE "JC"     TO WS-STNM.
           MOVE "JBHI"   TO WS-PGNM.
           MOVE "JCJBHI" TO PROGRAM-NAME.
           COPY "uv/cpy/uvcusf".
           CALL "SYSTEM" USING SCREEN-ID.
           COPY "uv/cpy/uvdssh".
       B100-OPEN-FILES.
           OPEN INPUT SY-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN INPUT CT-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN INPUT CD-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
       B200-READ-SY.
           MOVE SY-ST-CPNO TO SY-KEY-1.
           READ SY-FILE
               INTO SY-CP-RCRD-WS
               KEY IS SY-KEY-1.
           IF RECORD-LOCKED
               MOVE "SYMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO B200-READ-SY.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       B300-SET-VALUE.
           MOVE SPACES TO MAIN-77.
       B990-EXIT.
           EXIT.


       D000-CUSTOMER.
           PERFORM X900-CLEAR-SCREEN THRU X990-EXIT.
           DISPLAY SPACES LINE 09 POSITION 02 SIZE 78.
       D100-ACCEPT-CTNO.
           DISPLAY SPACES LINE 07 POSITION 59 SIZE 20
                   SPACES LINE 07 POSITION 18 SIZE 32.
           MOVE "PR"       TO LS-UVGCNO-CMCD.
           MOVE SY-ST-CPNO TO LS-UVGCNO-CPNO.
           MOVE 07         TO LS-UVGCNO-LINE.
           MOVE 12         TO LS-UVGCNO-COLM.
           MOVE SPACES     TO LS-UVGCNO-INSW.
           MOVE ZEROS      TO LS-UVGCNO-CTNO.
           PERFORM X200-UVGCNO THRU X205-EXIT.
           IF LS-UVGCNO-CMCD = "00"
               GO TO D120-READ-CT.
           IF LS-UVGCNO-CMCD = "02"
               MOVE "QT" TO MAIN-77
               GO TO D990-EXIT.
           MOVE "11" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
      *
       D120-READ-CT.
           MOVE SPACES     TO CT-KEY-1.
           MOVE SY-ST-CPNO TO CT-CPNO-K1.
           MOVE LS-UVGCNO-CTNO TO CT-CTNO-K1.
           READ CT-FILE
               INTO CT-RCRD-WS
               KEY IS CT-KEY-1
             INVALID KEY
               MOVE "13" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
           IF RECORD-LOCKED
               MOVE "CTMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO D120-READ-CT.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       D130-DISPLY.
           DISPLAY CT-CTNM LINE 07 POSITION 18 SIZE 32
                   CT-CLCT LINE 07 POSITION 59.
       D150-SET-VALUE.
           MOVE "FT" TO MAIN-77.
       D990-EXIT.
           EXIT.


       F000-PARAMETERS.
           PERFORM X800-DISPLY-SCREEN THRU X815-EXIT.
           IF MAIN-77 = "PM"
             and ws-desw = "N"
               go to f700-desc-search. 
           if main-77 = "PM"
             and ws-desw = "Y"
               go to f800-desc.
       F100-ALL-JOBS.
           DISPLAY "Y" LINE 10 POSITION 30.
           MOVE "Y" TO WS-AJSW.
           ACCEPT WS-AJSW
               LINE 10 POSITION 30
               UPDATE TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO F190-CHECK-FUNC.
           IF WS-AJSW NOT = "Y"
             AND WS-AJSW NOT = "N"
               GO TO F100-ALL-JOBS.
           DISPLAY WS-AJSW LINE 10 POSITION 30.
           IF WS-AJSW = "Y"
               MOVE SPACES TO WS-STCD
               GO TO F300-S-JBNO.
           DISPLAY "WHICH ONES?" LINE 10 POSITION 34 HIGH.
           GO TO F200-STCD.
      *
       F190-CHECK-FUNC.
           IF FUNC-02
               DISPLAY "Y" LINE 10 POSITION 30
               MOVE "QT" TO MAIN-77
               GO TO F990-EXIT.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F100-ALL-JOBS.
      *
       F200-STCD.
           ACCEPT WS-STCD
               LINE 10 POSITION 46
               PROMPT TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO F290-CHECK-FUNC.
           IF NOT VALID-STCD
               DISPLAY "STATUS CODE MUST BE 'AT', 'IV', 'CP', "
                           LINE 24 POSITION 1 HIGH
                       "'SM', OR 'CL' "
                           LINE 00 POSITION 0 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F200-STCD.
           DISPLAY WS-STCD LINE 10 POSITION 46.
           GO TO F300-S-JBNO.
      *
       F290-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 10 POSITION 34 SIZE 11
               GO TO F100-ALL-JOBS.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F200-STCD.
      *
       F300-S-JBNO.
           DISPLAY "999999" LINE 12 POSITION 30.
           MOVE "999999" TO WS-JBNO.
           ACCEPT WS-JBNO
               LINE 12 POSITION 30
               UPDATE TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO F390-CHECK-FUNC.
           IF WS-JBNO NOT NUMERIC
               DISPLAY "JOB NUMBER MUST BE NUMERIC "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F300-S-JBNO.
           IF WS-JBNO = ZEROS
               DISPLAY "JOB NUMBER MAY NOT BE ZEROS "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F300-S-JBNO.
       F310-DISPLY.
           MOVE WS-JBNO TO DS-JBNO.
           DISPLAY DS-JBNO LINE 12 POSITION 30.
       F330-UVFDNO.
           MOVE SPACES  TO LS-UVFDNO-CMCD.
           MOVE WS-JBNO TO LS-UVFDNO-DKNO.
           PERFORM X210-UVFDNO THRU X215-EXIT.
           IF LS-UVFDNO-CMCD = "00"
               GO TO F340-SET-VALUE.
           MOVE LS-UVFDNO-CMCD TO LOGICAL-FILE-ID.
           MOVE "21" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
      *
       F340-SET-VALUE.
           MOVE LS-UVFDNO-DKNO TO WS-FDNO.
           MOVE LS-UVFDNO-DKNO TO WS-FDNO-START.
           GO TO F400-DETAIL-TOTALS.
      *
       F390-CHECK-FUNC.
           IF NOT FUNC-02
               PERFORM X000-FUNC-ERR THRU X090-EXIT
               GO TO F300-S-JBNO.
           DISPLAY "999999" LINE 12 POSITION 30.
           IF WS-AJSW = "Y"
               GO TO F100-ALL-JOBS.
           GO TO F200-STCD.
      *
       F400-DETAIL-TOTALS.
           DISPLAY "D"        LINE 14 POSITION 30
                   "00/00/00" LINE 16 POSITION 30.
           MOVE "D" TO WS-DTSW.
           ACCEPT WS-DTSW
               LINE 14 POSITION 30
               UPDATE TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO F490-CHECK-FUNC.
           IF WS-DTSW NOT = "D"
             AND WS-DTSW NOT = "T"
               GO TO F400-DETAIL-TOTALS.
           DISPLAY WS-DTSW LINE 14 POSITION 30.
           GO TO F500-S-DATE.
      *
       F490-CHECK-FUNC.
           IF FUNC-02
               DISPLAY "D" LINE 14 POSITION 30
               GO TO F300-S-JBNO.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F400-DETAIL-TOTALS.
      *
       F500-S-DATE.
           MOVE SY-ST-SODT TO DS-DATE-2.
           DISPLAY DS-DATE-2 LINE 18 POSITION 30.
       F510-ACT-S-DATE.
           MOVE "NP"  TO LS-UVACDT-CMCD.
           MOVE "Y"   TO LS-UVACDT-NMSW.
           MOVE 16    TO LS-UVACDT-LINE.
           MOVE 30    TO LS-UVACDT-COLM.
           MOVE ZEROS TO LS-UVACDT-DATE.
           PERFORM X220-UVACDT THRU X225-EXIT.
           IF LS-UVACDT-CMCD = "02"
               GO TO F400-DETAIL-TOTALS.
           IF LS-UVACDT-CMCD = "08"
               MOVE ZEROS TO DS-DATE-2
               DISPLAY DS-DATE-2 LINE 16 POSITION 30.
           move ls-uvacdt-date to ls-uvdtcv-indt.
           move "EX"           to ls-uvdtcv-cmcd.
           perform x999-uvdtcv thru x999-exit.
           MOVE LS-UVdtcv-otdt TO WS-S-DATE.
       F600-E-DATE.
           MOVE "NP"       TO LS-UVACDT-CMCD.
           MOVE SPACES     TO LS-UVACDT-NMSW.
           MOVE 18         TO LS-UVACDT-LINE.
           MOVE 30         TO LS-UVACDT-COLM.
 
           MOVE SY-ST-SODT TO WS-SODT.
           ADD 3 TO WS-SODT-MT.
           IF WS-SODT-MT > 12
               SUBTRACT 12 FROM WS-SODT-MT
               ADD 1 TO WS-SODT-YR.
           IF WS-SODT-MT = 2
             AND WS-SODT-DY > 28
               MOVE 28 TO WS-SODT-DY.
           IF WS-SODT-MT = 04
             OR WS-SODT-MT = 06
             OR WS-SODT-MT = 09
             OR WS-SODT-MT = 11
             AND WS-SODT-DY > 30
               MOVE 30 TO WS-SODT-DY.
           MOVE WS-SODT TO LS-UVACDT-DATE.
      *    MOVE SY-ST-SODT TO LS-UVACDT-DATE.

           PERFORM X220-UVACDT THRU X225-EXIT.
           IF LS-UVACDT-CMCD = "02"
               GO TO F500-S-DATE.
           move ls-uvacdt-date to ls-uvdtcv-indt.
           move "EX"           to ls-uvdtcv-cmcd.
           perform x999-uvdtcv thru x999-exit.
           IF LS-UVdtcv-otdt < WS-S-DATE
               DISPLAY "ENDING DATE < STARTING DATE "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F600-E-DATE.
           MOVE LS-UVACDT-DATE TO WS-E-DATE.
       f700-desc-search.
           move "N" to ws-desw.
           accept ws-desw
               line 20 position 30
               update tab echo
             on exception
               func-key
               go to f790-check-func.
           if ws-desw not = "N"
             and ws-desw not = "Y"
               go to f700-desc-search.
           if ws-dtsw = "T"
             and ws-desw = "Y"
                display "TOTALS INQUIRY - DESC SEARCH NOT APPLICABLE "
                   line 24 position 1 high
                perform x050-reply thru x090-exit
                go to f700-desc-search.
           display ws-desw line 20 position 30.
           if ws-desw = "N"
              go to f900-querie.
           go to f800-desc.
      *
       f790-check-func.
           if func-02
               display "N" line 20 position 30
               go to f600-e-date.
           perform x000-func-err thru x090-exit.
           go to f700-desc-search.
      *
       f800-desc.
           accept ws-desc
               line 20 position 42
               prompt tab echo
             on exception
               func-key
               go to f890-check-func.
           if ws-desc = spaces
               display "DESCRIPTION MAY NOT BE SPACES "
                  line 24 position 1 high
               perform x050-reply thru x090-exit
               go to f800-desc.
       f810-count-char.
           move 30 to ws-count.
           move ws-desc to description-table.
       f815-loop.
           if ws-desc-char (ws-count) not = spaces
               go to f820-string-length.
           subtract 1 from ws-count.
           if ws-count not = zero
               go to f815-loop.
       f820-string-length.
           go to f900-querie.
       f890-check-func.
           if func-02
               display spaces line 20 position 42 size 30
               go to f700-desc-search.
           perform x000-func-err thru x090-exit.
           go to f800-desc.
       F900-QUERIE.
           DISPLAY "CONTINUE? (Y/N)"
               LINE 24 POSITION 1 HIGH.
           MOVE "Y" TO DUMMY-77.
           ACCEPT DUMMY-77
               LINE 24 POSITION 17
               UPDATE TAB ECHO.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           IF DUMMY-77 = "Y"
               GO TO F990-EXIT.
           IF DUMMY-77 NOT = "N"
               GO TO F900-QUERIE.
           if ws-desw = "N"
               go to f700-desc-search.
           GO TO F800-desc.
       F990-EXIT.
           EXIT.

       H000-PROCESS.
           MOVE SPACES TO MAIN-77 WS-DSSW.
           MOVE 11     TO WS-LINE.
           MOVE 1      TO WS-VLCD
                          WS-SCCT.
           MOVE ZEROS  TO WS-AT-JCCT WS-AT-JCSP
                          WS-IV-JCCT WS-IV-JCSP
                          WS-CP-JCCT WS-CP-JCSP
                          WS-SM-JCCT WS-SM-JCSP
                          WS-CL-JCCT WS-CL-JCSP.
           MOVE WS-FDNO-START TO WS-FDNO.
           IF WS-VLCD = 1
               GO TO H050-DISPLY-HEADING.
           MOVE "25" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
       
       H030-SET-PREV-SCREEN.
           MOVE 11 TO WS-LINE.
           SUBTRACT 1 FROM WS-SCCT.
           MOVE JOB-KEY ( WS-SCCT ) TO WS-FDNO.
           IF WS-VLCD = 4
               MOVE 2 TO WS-VLCD.

       H050-DISPLY-HEADING.
           IF WS-DTSW = "T"
               GO TO H090-START-CD.
           PERFORM X900-CLEAR-SCREEN THRU X990-EXIT.
           DISPLAY SPACES        LINE 09 POSITION 02 SIZE 78
                   "JOB"         LINE 09 POSITION 05 HIGH
                   "QUANTITY"    LINE 09 POSITION 10 HIGH
                   "DESCRIPTION" LINE 09 POSITION 20 HIGH
                   "REF NO"      LINE 09 POSITION 41 HIGH
                   "ST"          LINE 09 POSITION 48 HIGH
                   "DATE"        LINE 09 POSITION 52 HIGH
                   "PRICE"       LINE 09 POSITION 63 HIGH
                   "COST"        LINE 09 POSITION 75 HIGH.
       H090-START-CD.
           MOVE SPACES     TO CD-KEY-1.
           MOVE SY-ST-CPNO TO CD-CPNO-K1.
           MOVE LS-UVGCNO-CTNO TO CD-CTNO-K1.
           MOVE WS-FDNO    TO CD-DKNO-K1.
           START CD-FILE
               KEY IS NOT LESS THAN CD-KEY-1
             INVALID KEY
               GO TO  H600-REVIEW-DSSW.
           IF RECORD-LOCKED
               MOVE "CDRF" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO H090-START-CD.
           IF NOT FILE-OK
               MOVE "ST" TO FILE-IO
               GO TO Z100-FILE-ERR.
       H100-READ-CD.
           READ CD-FILE
               NEXT RECORD
               INTO CD-RCRD-WS
             AT END
               GO TO H600-REVIEW-DSSW.
           IF NOT RECORD-LOCKED
               GO TO H110-CHECK-FILE.
           MOVE "CDRF" TO WS-LOCK-FILE.
           PERFORM Z200-ACK-LOCK THRU Z290-EXIT.
           GO TO H100-READ-CD.
      *
       H110-CHECK-FILE.
           IF NOT FILE-OK
               MOVE "RS" TO FILE-IO
               GO TO Z100-FILE-ERR.
           IF CD-CPNO-1 NOT = SY-ST-CPNO
               GO TO H600-REVIEW-DSSW.
           IF CD-CTNO-1 NOT = LS-UVGCNO-CTNO
               GO TO H600-REVIEW-DSSW.
       H112-CHECK-STCD.
           IF WS-AJSW = "Y"
               GO TO H114-REVIEW-DATE.
           IF WS-STCD = "AT"
             AND CD-STCD NOT = "A"
               GO TO H100-READ-CD.
           IF WS-STCD = "IV"
             AND CD-STCD NOT = "I"
               GO TO H100-READ-CD.
           IF WS-STCD = "CP"
             AND CD-STCD NOT = "C"
               GO TO H100-READ-CD.
           IF WS-STCD = "SM"
             AND CD-STCD NOT = "S"
               GO TO H100-READ-CD.
           IF WS-STCD = "CL"
             AND CD-STCD = "A"
               GO TO H100-READ-CD.
       H114-REVIEW-DATE.
           IF CD-TRDT < WS-S-DATE
             OR CD-TRDT > WS-E-DATE
               GO TO H100-READ-CD.
       h116-review-desc.
           if ws-desw = "N"
               go to h130-review-disply.
           move cd-dkd1 to ws-file-desc.
           perform x300-desc-search thru x390-exit.
           if ws-match-sw = "N"
               go to h100-read-cd.
       H130-REVIEW-DISPLY.
           MOVE "Y" TO WS-DSSW.
           IF WS-DTSW = "T"
               GO TO H140-UVFDNO.
           IF WS-LINE < 23
               GO TO H140-UVFDNO.
           PERFORM X100-QUERIE-FUNC THRU X190-EXIT.
           IF FUNC-02
             AND WS-SCCT = 1
               GO TO H990-EXIT.
           IF FUNC-02
               go to h000-process.
           IF FUNC-03
             AND WS-SCCT = 1
               MOVE "PM" TO MAIN-77
               GO TO H990-EXIT.
           IF FUNC-03 AND
             ( WS-VLCD = 2
                OR WS-VLCD = 4 )
               GO TO H030-SET-PREV-SCREEN.
           IF FUNC-03
             AND WS-VLCD = 1
               MOVE "PM" TO MAIN-77
               GO TO H990-EXIT.
           IF NOT FUNC-01
               MOVE "31" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
           IF WS-VLCD NOT = 2
               MOVE 2 TO WS-VLCD.
           MOVE 11 TO WS-LINE.
       H135-SET-SCREEN-CNTR.
           ADD 1 TO WS-SCCT.
       H140-UVFDNO.
           MOVE SPACES    TO LS-UVFDNO-CMCD.
           MOVE CD-DKNO-1 TO LS-UVFDNO-DKNO.
           PERFORM X210-UVFDNO THRU X215-EXIT.
           IF LS-UVFDNO-CMCD = "00"
               GO TO H150-SET-PR-FLDS.
           MOVE LS-UVFDNO-CMCD TO LOGICAL-FILE-ID.
           MOVE "33" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
      *
       H150-SET-PR-FLDS.
           MOVE LS-UVFDNO-DKNO TO DS-JBNO.
           IF WS-DTSW = "T"
               GO TO H200-DISPLY-PROGRESS.
           MOVE CD-QNTY    TO DS-QNTY.
           MOVE CD-DKD1    TO DS-DKD1.
           MOVE CD-RFNO    TO DS-RFNO.
           MOVE CD-TRDT    TO DS-DATE.
           MOVE CD-JBCT    TO DS-JBCT.
           MOVE CD-JBSP    TO DS-JBSP.
           IF CD-STCD = "A"
               MOVE "AT" TO DS-STCD
               GO TO H160-DISPLY.
           IF CD-STCD = "I"
               MOVE "IV" TO DS-STCD
               GO TO H160-DISPLY.
           IF CD-STCD = "S"
               MOVE "SM" TO DS-STCD
               GO TO H160-DISPLY.
           MOVE "CP" TO DS-STCD.
       H160-DISPLY.
           DISPLAY DS-JBNO LINE WS-LINE POSITION 02
                   DS-QNTY LINE WS-LINE POSITION 09
                   DS-DKD1 LINE WS-LINE POSITION 20
                   DS-RFNO LINE WS-LINE POSITION 41
                   DS-STCD LINE WS-LINE POSITION 48
                   DS-DATE LINE WS-LINE POSITION 51
                   DS-JBSP LINE WS-LINE POSITION 58
                   DS-JBCT LINE WS-LINE POSITION 69.
       H165-SET-JOB-KEY.
           IF WS-LINE = 11
               MOVE ZEROS     TO JOB-KEY ( WS-SCCT )
               MOVE CD-DKNO-1 TO JOB-KEY ( WS-SCCT ).
           ADD 1 TO WS-LINE.
           GO TO H250-NEXT-RCRD.
       H200-DISPLY-PROGRESS.
           DISPLAY "PROGRESS     " LINE 20 POSITION 11 HIGH
                   DS-JBNO    LINE 20 POSITION 30.
       H210-ADD-TOT-FLDS.
           IF CD-STCD = "A"
               ADD CD-JBCT TO WS-AT-JCCT
               ADD CD-JBSP TO WS-AT-JCSP
               GO TO H250-NEXT-RCRD.
           IF CD-STCD = "I"
               ADD CD-JBCT TO WS-IV-JCCT
               ADD CD-JBSP TO WS-IV-JCSP
               GO TO H250-NEXT-RCRD.
           IF CD-STCD = "S"
               ADD CD-JBCT TO WS-SM-JCCT
               ADD CD-JBSP TO WS-SM-JCSP
               GO TO H250-NEXT-RCRD.
           ADD CD-JBCT TO WS-CP-JCCT.
           ADD CD-JBSP TO WS-CP-JCSP.
       H250-NEXT-RCRD.
           GO TO H100-READ-CD.
      *
       H600-REVIEW-DSSW.
           IF WS-DSSW NOT = "Y"
               PERFORM X900-CLEAR-SCREEN THRU X990-EXIT
               DISPLAY "NO INFORMATION ON FILE FOR RANGE SPECIFIED "
                   LINE 11 POSITION 16
               MOVE 3 TO WS-VLCD
               GO TO H650-REVIEW-FUNC.
           IF WS-DTSW = "T"
               GO TO H700-TOTALS.
           IF WS-LINE < 23
               GO TO H630-DISPLY-END-OF-FILE.
           PERFORM X100-QUERIE-FUNC THRU X190-EXIT.
           IF FUNC-01
               MOVE 11 TO WS-LINE
               GO TO H630-DISPLY-END-OF-FILE.
           IF FUNC-02
             AND WS-SCCT = 1
               GO TO H990-EXIT.
           IF FUNC-02
               go to h000-process.
           IF FUNC-03
             AND WS-VLCD = 2
                 GO TO H030-SET-PREV-SCREEN.
           IF FUNC-03
             AND WS-VLCD = 1
                 MOVE "PM" TO MAIN-77
                 GO TO H990-EXIT.
           IF NOT FUNC-01
             AND NOT FUNC-02
               AND NOT FUNC-03
                 MOVE "51" TO FILE-STATUS
                 GO TO Z100-FILE-ERR.
       H630-DISPLY-END-OF-FILE.
           ADD 2 TO WS-VLCD.
           DISPLAY "****** END OF FILE FOR THIS CUSTOMER "
                   LINE WS-LINE POSITION 02.
       H650-REVIEW-FUNC.
           PERFORM X100-QUERIE-FUNC THRU X190-EXIT.
           IF FUNC-02
             AND WS-SCCT = 1
               GO TO H990-EXIT.
           IF FUNC-02
               go to h000-process.
           IF FUNC-03 AND
             ( WS-VLCD = 2
                OR WS-VLCD = 4 )
                 GO TO H030-SET-PREV-SCREEN.
           IF FUNC-03 AND
             ( WS-VLCD = 1
               OR WS-VLCD = 3 )
                 MOVE "PM" TO MAIN-77
                 GO TO H990-EXIT.
           IF NOT FUNC-01
             AND NOT FUNC-02
               AND NOT FUNC-03
                 MOVE "41" TO FILE-STATUS
                 GO TO Z100-FILE-ERR.
      *
       H700-TOTALS.
       H710-SET-FLDS.
           PERFORM X900-CLEAR-SCREEN THRU X990-EXIT.
           IF WS-AJSW = "Y"
             OR WS-STCD = "AT"
               MOVE WS-AT-JCCT TO DS-AT-JCCT
               MOVE WS-AT-JCSP TO DS-AT-JCSP.
           IF WS-AJSW = "Y"
             OR WS-STCD = "IV"
               MOVE WS-IV-JCCT TO DS-IV-JCCT
               MOVE WS-IV-JCSP TO DS-IV-JCSP.
           IF WS-AJSW = "Y"
             OR WS-STCD = "SM"
               MOVE WS-SM-JCCT TO DS-SM-JCCT
               MOVE WS-SM-JCSP TO DS-SM-JCSP.
           IF WS-AJSW = "Y"
             OR WS-STCD = "CP"
               MOVE WS-CP-JCCT TO DS-CP-JCCT
               MOVE WS-CP-JCSP TO DS-CP-JCSP.
           IF WS-AJSW = "Y"
             OR WS-STCD = "CL"
               COMPUTE WS-CL-JCCT = (WS-IV-JCCT +
                                     WS-SM-JCCT + WS-CP-JCCT)
               COMPUTE WS-CL-JCSP = (WS-IV-JCSP +
                                     WS-SM-JCSP + WS-CP-JCSP)
               MOVE WS-CL-JCCT TO DS-CL-JCCT
               MOVE WS-CL-JCSP TO DS-CL-JCSP.
       H720-DISPLY.
           DISPLAY SPACES        LINE 09 POSITION 02 SIZE 78
                   "PRICE"       LINE 09 POSITION 62 HIGH
                   "COST"        LINE 09 POSITION 75 HIGH
                   "TOTALS - "   LINE 11 POSITION 09 HIGH.
           IF WS-AJSW NOT = "Y"
               GO TO H730-DISPLY.
           DISPLAY "ACTIVE"      LINE 11 POSITION 18 HIGH
                   DS-AT-JCSP    LINE 11 POSITION 56
                   DS-AT-JCCT    LINE 11 POSITION 68
                   "INVOICED"    LINE 12 POSITION 18 HIGH
                   DS-IV-JCSP    LINE 12 POSITION 56
                   DS-IV-JCCT    LINE 12 POSITION 68
                   "COMPLETED"   LINE 13 POSITION 18 HIGH
                   DS-CP-JCSP    LINE 13 POSITION 56
                   DS-CP-JCCT    LINE 13 POSITION 68
                   "SUMMARIZED"  LINE 14 POSITION 18 HIGH
                   DS-SM-JCSP    LINE 14 POSITION 56
                   DS-SM-JCCT    LINE 14 POSITION 68
                   "CLOSED"      LINE 15 POSITION 18 HIGH
                   DS-CL-JCSP    LINE 15 POSITION 56
                   DS-CL-JCCT    LINE 15 POSITION 68.
           MOVE 3 TO WS-VLCD.
           GO TO H650-REVIEW-FUNC.
      *
       H730-DISPLY.
           IF WS-STCD = "AT"
               DISPLAY "ACTIVE"     LINE 11 POSITION 18 HIGH
                       DS-AT-JCSP   LINE 11 POSITION 56
                       DS-AT-JCCT   LINE 11 POSITION 68
               GO TO H740-REVIEW-FUNC.
           IF WS-STCD = "IV"
               DISPLAY "INVOICED"   LINE 11 POSITION 18 HIGH
                       DS-IV-JCSP   LINE 11 POSITION 56
                       DS-IV-JCCT   LINE 11 POSITION 68
               GO TO H740-REVIEW-FUNC.
           IF WS-STCD = "CP"
               DISPLAY "COMPLETED"  LINE 11 POSITION 18 HIGH
                       DS-CP-JCSP   LINE 11 POSITION 56
                       DS-CP-JCCT   LINE 11 POSITION 68
               GO TO H740-REVIEW-FUNC.
           IF WS-STCD = "SM"
               DISPLAY "SUMMARIZED" LINE 11 POSITION 18 HIGH
                       DS-SM-JCSP   LINE 11 POSITION 56
                       DS-SM-JCCT   LINE 11 POSITION 68
               GO TO H740-REVIEW-FUNC.
           DISPLAY "CLOSED"   LINE 11 POSITION 18 HIGH
                   DS-CL-JCSP LINE 11 POSITION 56
                   DS-CL-JCCT LINE 11 POSITION 68.
       H740-REVIEW-FUNC.
           MOVE 3 TO WS-VLCD.
           GO TO H650-REVIEW-FUNC.
      *
       H990-EXIT.
           EXIT.


       X000-FUNC-ERR.
           DISPLAY "FUNCTION KEY NOT VALID "
               LINE 24 POSITION 1 HIGH.
       X050-REPLY.
           ACCEPT DUMMY-77 LINE 00 POSITION 00 TAB OFF.
           DISPLAY SPACES LINE 24 POSITION 01 SIZE 80.
       X090-EXIT.
           EXIT.


       X100-QUERIE-FUNC.

           IF WS-VLCD = 1
             OR WS-VLCD = 2
               and ws-scct = 1
               DISPLAY
                "[F1] - NEXT SCREEN  "      LINE 24 POSITION 04 HIGH
                "[F2] - ANOTHER CUSTOMER  " LINE 00 POSITION 00 HIGH
                "[F3] - PREVIOUS SCREEN "   LINE 00 POSITION 00 HIGH
               GO TO X120-ACK-REPLY.

           IF WS-VLCD = 1
             OR WS-VLCD = 2
               DISPLAY
                "[F1] - NEXT SCREEN  "      LINE 24 POSITION 04 HIGH
                "[F2] - FIRST SCREEN  " LINE 00 POSITION 00 HIGH
                "[F3] - PREVIOUS SCREEN "   LINE 00 POSITION 00 HIGH
               GO TO X120-ACK-REPLY.

           IF WS-VLCD = 3
             OR WS-VLCD = 4
               and ws-scct = 1
               DISPLAY
                "[F2] - ANOTHER CUSTOMER  " LINE 24 POSITION 14 HIGH
                "[F3] - PREVIOUS SCREEN "   LINE 00 POSITION 00 HIGH
               GO TO X120-ACK-REPLY.

           IF WS-VLCD = 3
             OR WS-VLCD = 4
               DISPLAY
                "[F2] - FIRST SCREEN  " LINE 24 POSITION 14 HIGH
                "[F3] - PREVIOUS SCREEN "   LINE 00 POSITION 00 HIGH
               GO TO X120-ACK-REPLY.

           MOVE "81" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
       
       X120-ACK-REPLY.
           ACCEPT DUMMY-77
               LINE 00 POSITION 00
               TAB OFF
             ON EXCEPTION
               FUNC-KEY
               GO TO X140-CHECK-FUNC.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           GO TO X100-QUERIE-FUNC.
       X140-CHECK-FUNC.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           IF FUNC-02
               GO TO X190-EXIT.
           IF FUNC-01 AND
             ( WS-VLCD = 1
                OR WS-VLCD = 2 )
                 PERFORM X900-CLEAR-SCREEN THRU X990-EXIT
                 GO TO X190-EXIT.
           IF FUNC-03
               PERFORM X900-CLEAR-SCREEN THRU X990-EXIT
               GO TO X190-EXIT.
           GO TO X100-QUERIE-FUNC.
       X190-EXIT.
           EXIT.

       X200-UVGCNO.
           CALL "UVGCNO" USING
               LS-UVGCNO-RCRD.
       X205-EXIT.
           EXIT.


       X210-UVFDNO.
           CALL "UVFDNO" USING
               LS-UVFDNO-RCRD.
       X215-EXIT.
           EXIT.

       X220-UVACDT.
           CALL "UVACDT" USING
               LS-UVACDT-RCRD.
       X225-EXIT.
           EXIT.

       x300-desc-search.
           move zeros to ws-counter.
       x300-review-string-length. 
           if ws-count = 30
               go to x300-search-30.
           if ws-count = 29
               go to x300-search-29.
           if ws-count = 28
               go to x300-search-28.
           if ws-count = 27
               go to x300-search-27.
           if ws-count = 26
               go to x300-search-26.
           if ws-count = 25
               go to x300-search-25.
           if ws-count = 24
               go to x300-search-24.
           if ws-count = 23
               go to x300-search-23.
           if ws-count = 22
               go to x300-search-22.
           if ws-count = 21
               go to x300-search-21.
           if ws-count = 20
               go to x300-search-20.
           if ws-count = 19
               go to x300-search-19.
           if ws-count = 18
               go to x300-search-18.
           if ws-count = 17
               go to x300-search-17.
           if ws-count = 16
               go to x300-search-16.
           if ws-count = 15
               go to x300-search-15.
           if ws-count = 14
               go to x300-search-14.
           if ws-count = 13
               go to x300-search-13.
           if ws-count = 12
               go to x300-search-12.
           if ws-count = 11
               go to x300-search-11.
           if ws-count = 10
               go to x300-search-10.
           if ws-count = 09
               go to x300-search-09.
           if ws-count = 08
               go to x300-search-08.
           if ws-count = 07
               go to x300-search-07.
           if ws-count = 06
               go to x300-search-06.
           if ws-count = 05
               go to x300-search-05.
           if ws-count = 04
               go to x300-search-04.
           if ws-count = 03
               go to x300-search-03.
           if ws-count = 02
               go to x300-search-02.
           if ws-count = 01
               go to x300-search-01.
           move "X3" to file-status.
           go to z100-file-err.
       x300-search-30.
           move ws-desc to desc-30.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-30.
           go to x380-check-match.
       x300-search-29.
           move ws-desc to desc-29.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-29.
           go to x380-check-match.
       x300-search-28.
           move ws-desc to desc-28.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-28.
           go to x380-check-match.
       x300-search-27.
           move ws-desc to desc-27.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-27.
           go to x380-check-match.
       x300-search-26.
           move ws-desc to desc-26.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-26.
           go to x380-check-match.
       x300-search-25.
           move ws-desc to desc-25.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-25.
           go to x380-check-match.
       x300-search-24.
           move ws-desc to desc-24.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-24.
           go to x380-check-match.
       x300-search-23.
           move ws-desc to desc-23.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-23.
           go to x380-check-match.
       x300-search-22.
           move ws-desc to desc-22.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-22.
           go to x380-check-match.
       x300-search-21.
           move ws-desc to desc-21.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-21.
           go to x380-check-match.
       x300-search-20.
           move ws-desc to desc-20.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-20.
           go to x380-check-match.
       x300-search-19.
           move ws-desc to desc-19.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-19.
           go to x380-check-match.
       x300-search-18.
           move ws-desc to desc-18.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-18.
           go to x380-check-match.
       x300-search-17.
           move ws-desc to desc-17.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-17.
           go to x380-check-match.
       x300-search-16.
           move ws-desc to desc-16.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-16.
           go to x380-check-match.
       x300-search-15.
           move ws-desc to desc-15.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-15.
           go to x380-check-match.
       x300-search-14.
           move ws-desc to desc-14.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-14.
           go to x380-check-match.
       x300-search-13.
           move ws-desc to desc-13.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-13.
           go to x380-check-match.
       x300-search-12.
           move ws-desc to desc-12.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-12.
           go to x380-check-match.
       x300-search-11.
           move ws-desc to desc-11.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-11.
           go to x380-check-match.
       x300-search-10.
           move ws-desc to desc-10.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-10.
           go to x380-check-match.
       x300-search-09.
           move ws-desc to desc-09.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-09.
           go to x380-check-match.
       x300-search-08.
           move ws-desc to desc-08.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-08.
           go to x380-check-match.
       x300-search-07.
           move ws-desc to desc-07.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-07.
           go to x380-check-match.
       x300-search-06.
           move ws-desc to desc-06.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-06.
           go to x380-check-match.
       x300-search-05.
           move ws-desc to desc-05.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-05.
           go to x380-check-match.
       x300-search-04.
           move ws-desc to desc-04.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-04.
           go to x380-check-match.
       x300-search-03.
           move ws-desc to desc-03.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-03.
           go to x380-check-match.
       x300-search-02.
           move ws-desc to desc-02.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-02.
           go to x380-check-match.
       x300-search-01.
           move ws-desc to desc-01.
           inspect ws-file-desc
               tallying ws-counter
               for all desc-01.
       x380-check-match.
           if ws-counter = zeros
               move "N" to ws-match-sw
             else
               move "Y" to ws-match-sw.
       x390-exit.
           exit.

       X800-DISPLY-SCREEN.
           IF MAIN-77 NOT = "FT"
               PERFORM X900-CLEAR-SCREEN THRU X990-EXIT
               DISPLAY SPACES LINE 09 POSITION 02 SIZE 78.
           DISPLAY "ALL JOBS? (Y/N)" LINE 10 POSITION 11 HIGH.
           IF MAIN-77 = "FT"
             OR WS-AJSW = "Y"
               DISPLAY "Y" LINE 10 POSITION 30
               GO TO X805-CONTINUE.
           DISPLAY "N          " LINE 10 POSITION 30
                   "WHICH ONES?" LINE 10 POSITION 34 HIGH
                   WS-STCD       LINE 10 POSITION 46.
       X805-CONTINUE.
           DISPLAY "STARTING JOB" LINE 12 POSITION 11 HIGH.
           IF MAIN-77 = "FT"
              DISPLAY "999999" LINE 12 POSITION 30
              GO TO X806-CONTINUE.
           MOVE WS-JBNO TO DS-JBNO.
           DISPLAY DS-JBNO LINE 12 POSITION 30.
       X806-CONTINUE.
           DISPLAY "DETAIL OR TOTALS?" LINE 14 POSITION 11 HIGH.
           IF MAIN-77 = "FT"
             OR WS-DTSW = "D"
               DISPLAY "D" LINE 14 POSITION 30
               GO TO X807-CONTINUE.
           DISPLAY "T" LINE 14 POSITION 30.
       X807-CONTINUE.
           DISPLAY "STARTING DATE" LINE 16 POSITION 11 HIGH
                   "ENDING DATE"   LINE 18 POSITION 11 HIGH.
           IF MAIN-77 = "FT"
              MOVE SY-ST-SODT TO DS-DATE-2
              DISPLAY "00/00/00" LINE 16 POSITION 30
                      DS-DATE-2  LINE 18 POSITION 30
              GO TO X808-continue.
           MOVE WS-S-DATE TO DS-DATE-2.
           DISPLAY DS-DATE-2 LINE 16 POSITION 30.
           MOVE WS-E-DATE TO DS-DATE-2.
           DISPLAY DS-DATE-2 LINE 18 POSITION 30.
       x808-continue.
           display "DESC SEARCH?" LINE 20 POSITION 11 HIGH.
           if main-77 = "FT"
             or ws-desw = "N"
               display "N" line 20 position 30
               go to x815-exit.
           display "Y"     line 20 position 30
                   ws-desc line 20 position 42.
       X815-EXIT.
           EXIT.

       X900-CLEAR-SCREEN.
           DISPLAY SPACES LINE 22 POSITION 02 SIZE 78
                   SPACES LINE 21 POSITION 02 SIZE 78
                   SPACES LINE 20 POSITION 02 SIZE 78
                   SPACES LINE 19 POSITION 02 SIZE 78
                   SPACES LINE 18 POSITION 02 SIZE 78
                   SPACES LINE 17 POSITION 02 SIZE 78
                   SPACES LINE 16 POSITION 02 SIZE 78
                   SPACES LINE 15 POSITION 02 SIZE 78
                   SPACES LINE 14 POSITION 02 SIZE 78
                   SPACES LINE 13 POSITION 02 SIZE 78
                   SPACES LINE 12 POSITION 02 SIZE 78
                   SPACES LINE 11 POSITION 02 SIZE 78
                   SPACES LINE 10 POSITION 02 SIZE 78.
       X990-EXIT.
           EXIT.


       X999-UVdtcv.
           CALL "UVDTCV" USING
               LS-UVDTCV-RCRD.
       X999-EXIT.
           EXIT.


       Y000-CLOSE.
           CLOSE SY-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE CT-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE CD-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
       Y990-EXIT.
           EXIT.

           COPY "uv/cpy/uvdfemov".
           COPY "uv/cpy/uvaflk".
