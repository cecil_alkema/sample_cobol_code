       IDENTIFICATION DIVISION.
       PROGRAM-ID.                 UVAECP.
      *
      *        UNIVERSAL SUBROUTINE
      *        ACCEPT EDIT COUNTRY & PROVINCE CODE
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           COPY "sm/cpy/slstmt".
      
       DATA DIVISION.
       FILE SECTION.
           COPY "sm/cpy/fdstmt".
      
       WORKING-STORAGE SECTION.
      
       77  MM-STATUS-77            PIC XX.
       77  DUMMY-77                PIC X.
      
           COPY "uv/cpy/uvfckv".
           COPY "uv/cpy/uvsfem".
           COPY "uv/cpy/uvsfemov".
           COPY "uv/cpy/uvlkmg".
      
           COPY "sm/cpy/rcstmt21".
      
       01 CONTROL-FIELDS.
           03  WS-ERSW             PIC XX     VALUE SPACES.
           03  WS-CTCD             PIC XX.
           03  WS-CTCD-1           REDEFINES WS-CTCD.
               05  WS-CT-ALPH1     PIC A.
               05  WS-CT-ALPH2     PIC A.
           03  WS-PVCD             PIC XX.
           03  WS-PVCD-1           REDEFINES WS-PVCD.
               05  WS-PV-ALPH1     PIC A.
               05  WS-PV-ALPH2     PIC A.
      
       LINKAGE SECTION.
      
           COPY "uv/cpy/lsuvaecp".
      
       PROCEDURE DIVISION
           USING
               LS-UVAECP-RCRD.
      
       DECLARATIVES.
           COPY "sm/cpy/dcstmt".
       END DECLARATIVES.
      
       A000-MAINLINE-PGM SECTION.
      
       A000-START-PROGRAM.
           MOVE "UVAECP" TO UNVL-SFEM-PGNM.
           MOVE "UVAECP" TO PROGRAM-NAME.
       A010-OPEN.
           OPEN INPUT MM-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
       A020-MAINLINE.
           IF LS-UVAECP-CPSW = "Y"
               PERFORM A100-ACCEPT-EDIT-CTCD THRU A990-EXIT
           ELSE PERFORM B000-ACCEPT-EDIT-PVCD THRU B990-EXIT.
           PERFORM Y000-CLOSE THRU Y990-EXIT.
       A025-EXIT.
           EXIT PROGRAM.
      *
       A100-ACCEPT-EDIT-CTCD.
           IF LS-UVAECP-CMCD = "PR"
               GO TO A200-PROMPT.
           IF LS-UVAECP-CMCD = "NP"
               GO TO A400-NO-PROMPT.
           MOVE "A1" TO UNVL-SFEM-ERCD.
           GO TO Z150-FATAL-ERR.
      *
       A200-PROMPT.
           IF LS-UVAECP-FKSW = "Y"
               GO TO A300-ACCEPT-FUNC.
       A210-ACCEPT-NO-FUNC.
           ACCEPT WS-CTCD
               LINE LS-UVAECP-LINE POSITION LS-UVAECP-COLM
               PROMPT TAB ECHO.
           GO TO A600-EDIT-CTCD.
      *
       A300-ACCEPT-FUNC.
           ACCEPT WS-CTCD
               LINE LS-UVAECP-LINE POSITION LS-UVAECP-COLM
               PROMPT TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO A900-CHECK-FUNC.
           GO TO A600-EDIT-CTCD.
      *
       A400-NO-PROMPT.
           DISPLAY LS-UVAECP-CTCD
               LINE LS-UVAECP-LINE POSITION LS-UVAECP-COLM.
	   MOVE LS-UVAECP-CTCD TO WS-CTCD.
           IF LS-UVAECP-FKSW = "Y"
               GO TO A500-ACCEPT-FUNC.
       A410-ACCEPT-NO-FUNC.
           ACCEPT WS-CTCD
               LINE LS-UVAECP-LINE POSITION LS-UVAECP-COLM
               TAB ECHO UPDATE.
           GO TO A600-EDIT-CTCD.
      *
       A500-ACCEPT-FUNC.
           ACCEPT WS-CTCD
               LINE LS-UVAECP-LINE POSITION LS-UVAECP-COLM
               TAB ECHO UPDATE
             ON EXCEPTION
               FUNC-KEY
               GO TO A900-CHECK-FUNC.
       A600-EDIT-CTCD.
           DISPLAY WS-CTCD
               LINE LS-UVAECP-LINE POSITION LS-UVAECP-COLM.
           IF WS-CTCD = SPACES
               DISPLAY "COUNTRY CODE IS MANDATORY "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO A100-ACCEPT-EDIT-CTCD.
           IF WS-CT-ALPH1 = SPACE
             OR WS-CT-ALPH2 = SPACE
               DISPLAY "COUNTRY CODE NOT VALID "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO A100-ACCEPT-EDIT-CTCD.
           IF WS-CT-ALPH1 NOT ALPHABETIC
             OR WS-CT-ALPH2 NOT ALPHABETIC
               DISPLAY "COUNTRY CODE NOT VALID "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO A100-ACCEPT-EDIT-CTCD.
           MOVE SPACES         TO MM-21-KEY-1-WS.
           MOVE LS-UVAECP-CPNO TO MM-21-CPNO-1.
           MOVE 21             TO MM-21-LFCD-1.
           MOVE WS-CTCD        TO MM-21-CTCD-1.
           MOVE MM-21-KEY-1-WS TO MM-KEY-1.
       A610-READ-MM.
           PERFORM X200-READ-MM THRU X250-EXIT.
           IF WS-ERSW = "Y"
               DISPLAY "COUNTRY CODE NOT ON FILE "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO A100-ACCEPT-EDIT-CTCD.
       A690-SET-VALUES.
           MOVE "00"    TO LS-UVAECP-CMCD.
           MOVE WS-CTCD TO LS-UVAECP-CTCD.
           GO TO A990-EXIT.
      *
       A900-CHECK-FUNC.
           IF FUNC-02
               MOVE "02"   TO LS-UVAECP-CMCD
               MOVE SPACES TO LS-UVAECP-CTCD
               GO TO A990-EXIT.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO A100-ACCEPT-EDIT-CTCD.
      *
       A990-EXIT.
           EXIT.
      
       B000-ACCEPT-EDIT-PVCD.
           IF LS-UVAECP-CMCD = "PR"
               GO TO B200-PROMPT.
           IF LS-UVAECP-CMCD = "NP"
               GO TO B400-NO-PROMPT.
           MOVE "B1" TO UNVL-SFEM-ERCD.
           GO TO Z150-FATAL-ERR.
      *
       B200-PROMPT.
           IF LS-UVAECP-FKSW = "Y"
               GO TO B300-ACCEPT-FUNC.
       B210-ACCEPT-NO-FUNC.
           ACCEPT WS-PVCD
               LINE LS-UVAECP-LINE POSITION LS-UVAECP-COLM
               PROMPT TAB ECHO.
           GO TO B600-EDIT-PVCD.
      *
       B300-ACCEPT-FUNC.
           ACCEPT WS-PVCD
               LINE LS-UVAECP-LINE POSITION LS-UVAECP-COLM
               PROMPT TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO B900-CHECK-FUNC.
           GO TO B600-EDIT-PVCD.
      *
       B400-NO-PROMPT.
           DISPLAY LS-UVAECP-PVCD
               LINE LS-UVAECP-LINE POSITION LS-UVAECP-COLM.
           MOVE LS-UVAECP-PVCD TO WS-PVCD.
           IF LS-UVAECP-FKSW = "Y"
               GO TO B500-ACCEPT-FUNC.
       B410-ACCEPT-NO-FUNC.
           ACCEPT WS-PVCD
               LINE LS-UVAECP-LINE POSITION LS-UVAECP-COLM
               TAB ECHO UPDATE.
           GO TO B600-EDIT-PVCD.
      *
       B500-ACCEPT-FUNC.
           ACCEPT WS-PVCD
               LINE LS-UVAECP-LINE POSITION LS-UVAECP-COLM
               TAB ECHO UPDATE
             ON EXCEPTION
               FUNC-KEY
               GO TO B900-CHECK-FUNC.
       B600-EDIT-PVCD.
           DISPLAY WS-PVCD
               LINE LS-UVAECP-LINE POSITION LS-UVAECP-COLM.
           IF WS-PVCD = SPACES
             AND LS-UVAECP-VLSW = "Y"
               GO TO B690-SET-VALUES.
           IF WS-PVCD = SPACES
             AND LS-UVAECP-CTCD = "CA"
               DISPLAY "PROVINCE CODE IS MANDATORY "
                   LINE 24 POSITION 1 HIGH
               GO TO B625-ACK-CONT.
           IF WS-PVCD = SPACES
             AND LS-UVAECP-CTCD = "US"
               DISPLAY "STATE CODE IS MANDATORY "
                   LINE 24 POSITION 1 HIGH
               GO TO B625-ACK-CONT.
           IF WS-PVCD = SPACES
               DISPLAY "CODE IS MANDATORY "
                   LINE 24 POSITION 1 HIGH
               GO TO B625-ACK-CONT.
           IF WS-PV-ALPH1 = SPACE
             OR WS-PV-ALPH2 = SPACE
               GO TO B600-DISPLY-ERR.
           IF WS-PV-ALPH1 ALPHABETIC
             AND WS-PV-ALPH2 ALPHABETIC
               GO TO B605-SET-MM.
       B600-DISPLY-ERR.
           IF LS-UVAECP-CTCD = "CA"
                DISPLAY "PROVINCE CODE NOT VALID "
                    LINE 24 POSITION 1 HIGH
                GO TO B625-ACK-CONT.
           IF LS-UVAECP-CTCD = "US"
                DISPLAY "STATE CODE NOT VALID "
                    LINE 24 POSITION 1 HIGH
                GO TO B625-ACK-CONT.
           DISPLAY "CODE NOT VALID "
               LINE 24 POSITION 1 HIGH.
           GO TO B625-ACK-CONT.
       B605-SET-MM.
           MOVE SPACES         TO MM-21-KEY-1-WS.
           MOVE LS-UVAECP-CPNO TO MM-21-CPNO-1.
           MOVE 21             TO MM-21-LFCD-1.
           MOVE LS-UVAECP-CTCD TO MM-21-CTCD-1.
           MOVE WS-PVCD        TO MM-21-PVCD-1.
           MOVE MM-21-KEY-1-WS TO MM-KEY-1.
       B610-READ-MM.
           PERFORM X200-READ-MM THRU X250-EXIT.
           IF WS-ERSW NOT = "Y"
               GO TO B690-SET-VALUES.
       B620-ERR-MSG.
           IF LS-UVAECP-CTCD = "CA"
               DISPLAY "PROVINCE NOT ON FILE "
                   LINE 24 POSITION 1 HIGH
               GO TO B625-ACK-CONT.
           IF LS-UVAECP-CTCD = "US"
               DISPLAY "STATE NOT ON FILE "
                   LINE 24 POSITION 1 HIGH
               GO TO B625-ACK-CONT.
           DISPLAY "CODE NOT ON FILE "
               LINE 24 POSITION 1 HIGH.
       B625-ACK-CONT.
           PERFORM X050-REPLY THRU X090-EXIT.
           GO TO B000-ACCEPT-EDIT-PVCD.
      *
       B690-SET-VALUES.
           MOVE "00"    TO LS-UVAECP-CMCD.
           MOVE WS-PVCD TO LS-UVAECP-PVCD.
           GO TO B990-EXIT.
      *
       B900-CHECK-FUNC.
           IF FUNC-02
               MOVE "02"   TO LS-UVAECP-CMCD
               MOVE SPACES TO LS-UVAECP-PVCD
               GO TO B990-EXIT.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO B000-ACCEPT-EDIT-PVCD.
      *
       B990-EXIT.
           EXIT.
      
      
       X000-FUNC-ERR.
           DISPLAY "FUNCTION KEY NOT VALID "
               LINE 24 POSITION 1 HIGH.
       X050-REPLY.
           ACCEPT DUMMY-77 LINE 00 POSITION 00 TAB OFF.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
       X090-EXIT.
           EXIT.
      
      
       X200-READ-MM.
           MOVE SPACES TO WS-ERSW.
           READ MM-FILE
               INTO MM-21-RCRD-WS
               KEY IS MM-KEY-1
             INVALID KEY
               MOVE "Y" TO WS-ERSW
               GO TO X250-EXIT.
           IF RECORD-LOCKED
               MOVE "STMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X200-READ-MM.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X250-EXIT.
           EXIT.
      
      
       Y000-CLOSE.
           CLOSE MM-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
       Y990-EXIT.
           EXIT.
      
      
           COPY "uv/cpy/uvdfem".
           COPY "uv/cpy/uvdfemov".
           COPY "uv/cpy/uvaflk".
