       IDENTIFICATION DIVISION.
       PROGRAM-ID.                 UVSON.
      *
      *        UNIVERSAL PROGRAM
      *        SIGN ON
      *
      *        SWITCHES
      *        ********
      *
      *        SWITCH-1 - IGNORE COMPUTER DATE EDIT
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           SWITCH-1
               ON  IS SW-1-ON
               OFF IS SW-1-OFF.
      
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           COPY "uv/cpy/slsymt".
           COPY "uv/cpy/slstno".

           SELECT LGIN-FILE
           ASSIGN TO RANDOM, "LGIN";
           ORGANIZATION IS LINE SEQUENTIAL;
           ACCESS MODE IS SEQUENTIAL;
           STATUS IS LGIN-STATUS-77.
      
       DATA DIVISION.
       FILE SECTION.
           COPY "uv/cpy/fdsymt".
           COPY "uv/cpy/fdstno".

       FD  LGIN-FILE
           LABEL RECORDS ARE STANDARD
           DATA RECORD IS LGIN-RCRD.
       01  LGIN-RCRD.
           03  FILLER              PIC X(80).
      
       WORKING-STORAGE SECTION.
      
       77  SY-STATUS-77            PIC XX.
       77  STNO-STATUS-77          PIC XX.
       77  LGIN-STATUS-77          PIC XX.
       77  DUMMY-77                PIC X.
       77  MAIN-77                 PIC XX.
      
           COPY "uv/cpy/uvfckv".
           COPY "uv/cpy/uvsfemov".
           COPY "uv/cpy/uvsfem".
           COPY "uv/cpy/uvlkmg".
      
           COPY "uv/cpy/rcsymtst".
           COPY "uv/cpy/rcsymtcp".
           COPY "uv/cpy/rcsymtpw".
           COPY "uv/cpy/rcstno".
      
           COPY "uv/cpy/lsuvdgfs".
           COPY "uv/cpy/lsuvacdt".
           COPY "uv/cpy/lsuvdtcv".

       01  SCREEN-ID               PIC X(35)   VALUE
           "$PMISDIR/appl/obj/c/dsplscrn uvson".
 
       01  LGIN-RCRD-WS.
           03  FILLER              PIC X(14)
               VALUE "setenv LGINSW ".
           03  LGIN-SW             PIC X.
      
       01  CONTROL-FIELDS.

           03  WS-CPDT             PIC 9(8).
           03  WS-CPDT-1           REDEFINES WS-CPDT.
               05  ws-ccyy         pic 9999.
               05  ws-ccyy-r redefines ws-ccyy.
                   07  WS-cc       PIC 99.
                   07  WS-YY       PIC 99.
               05  WS-MM           PIC 99.
               05  WS-DD           PIC 99.

           03  WS-ccYY-MINUS-ONE   PIC 9999.
           03  WS-NECT             PIC 9.
           03  WS-CPNO             PIC 99.

           03  WS-DATE             PIC 9(8).
           03  WS-DATE-1           REDEFINES WS-DATE.
               05  ws-centyear     pic 9999.
               05  ws-centyr-r redefines ws-centyear.
                   07  WS-cent     PIC 99.
                   07  WS-YEAR     PIC 99.
               05  WS-MONTH        PIC 99.
               05  WS-DAY          PIC 99.
           03  DS-DATE             PIC 99/99/99.

           03  WS-TIME             PIC 9(8).
           03  WS-TIME-1           REDEFINES WS-TIME.
               05  WS-HHMM         PIC 9999.
               05  WS-REST         PIC 9999.
           03  DS-TIME             PIC 99/99.
      
       01  STORAGE-FIELDS.
           03  WS-USID             PIC XXX.
           03  WS-USPW             PIC X(6).
           03  WS-NUPW             PIC X(6).
      
       PROCEDURE DIVISION.
      
       DECLARATIVES.
           COPY "uv/cpy/dcsymt".
           COPY "uv/cpy/dcstno".

       LGIN-FILE-ERR SECTION.
           USE AFTER STANDARD ERROR PROCEDURE ON LGIN-FILE.
       SETUP-STD-FL-ERR-MSG-FOR-LGIN.
           MOVE "LGIN" TO UNVL-SFEM-PFCD.
           MOVE LGIN-STATUS-77 TO UNVL-SFEM-ERCD.
       
       END DECLARATIVES.
      
       A000-MAINLINE-PGM SECTION.
      
       A000-START-PROGRAM.
           PERFORM B000-INIT THRU B990-EXIT.
           IF MAIN-77 = "QT"
               GO TO A800-CLOSING.
       A100-MAINLINE.
           PERFORM F000-PROCESS THRU F990-EXIT.
       A800-CLOSING.
           PERFORM Y000-CLOSE THRU Y990-EXIT.
       A990-EXIT.
           STOP RUN.
      
       B000-INIT.
           MOVE "UVSON " TO PROGRAM-NAME.
      *    COPY "uv/cpy/uvdgfs".
           CALL "SYSTEM" USING SCREEN-ID.
           MOVE "N" TO LGIN-SW.
       B100-OPEN-FILS.
           OPEN I-O   SY-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
       B200-GET-TERMINAL-NO.
           COPY "uv/cpy/uvstno".
       B205-DISPLAY-HEADER-INFO.
           DISPLAY STNO-IDXX LINE 03 POSITION 50.
           ACCEPT WS-TIME FROM TIME.
           MOVE WS-HHMM TO DS-TIME.
           INSPECT DS-TIME REPLACING ALL "/" BY ":".
           DISPLAY DS-TIME LINE 03 POSITION 74.
           ACCEPT WS-DATE FROM DATE.

           display ws-date line 10 position 1.

           if ws-year > 70
               move 19 to ws-cent
           else
               move 20 to ws-cent
           end-if.

           display ws-date line 11 position 1.

           MOVE WS-DATE TO DS-DATE.
           DISPLAY DS-DATE LINE 04 POSITION 71.
       B210-SET-VALUES.
           MOVE SPACES TO MAIN-77.
           MOVE ZEROS  TO WS-NECT.
       B220-DISPLY.
           IF SW-1-ON
               DISPLAY "SWITCH-1"
                   LINE 23 POSITION 1.

       B230-GET-COMPUTER-DATE.
           ACCEPT WS-CPDT FROM DATE.
           if ws-yy > 70
               move 19 to ws-cc
           else
               move 20 to ws-cc
           end-if.
       B300-READ-ST.
           MOVE SPACES         TO SY-KEY-1 SY-ST-KEY-1-WS.
           MOVE "ST"           TO SY-ST-RCTP-1.
           MOVE STNO-IDXX      TO SY-ST-STNO-1.
           MOVE SY-ST-KEY-1-WS TO SY-KEY-1.
           READ SY-FILE
               WITH NO LOCK
               INTO SY-ST-RCRD-WS
               KEY IS SY-KEY-1
             INVALID KEY
               GO TO B990-EXIT.
           IF RECORD-LOCKED
               MOVE "SYMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO B300-READ-ST.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       B400-ALREADY-SIGNED-ON.
           DISPLAY "THIS TERMINAL IS ALREADY SIGNED ON "
               LINE 24 POSITION 1 HIGH.
           PERFORM X050-REPLY THRU X090-EXIT.
           MOVE "QT" TO MAIN-77.
       B500-SET-LGIN-SW.
           PERFORM X300-SET-LGIN-SW-ON THRU X390-EXIT.
       B990-EXIT.
           EXIT.
      
       F000-PROCESS.
       F200-USER-ID.
           ACCEPT WS-USID
               LINE 07 POSITION 25
               PROMPT TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO F290-CHECK-FUNC.
       F210-EDIT.
           DISPLAY WS-USID LINE 07 POSITION 25.
           DISPLAY WS-USID LINE 03 POSITION 63.
           IF WS-USID = SPACES
               DISPLAY "USER ID IS MANDATORY "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F200-USER-ID.
       F220-READ-PW.
           MOVE SPACES         TO SY-KEY-1 SY-PW-KEY-1-WS.
           MOVE "PW"           TO SY-PW-RCTP-1.
           MOVE WS-USID        TO SY-PW-USID-1.
           MOVE SY-PW-KEY-1-WS TO SY-KEY-1.
           READ SY-FILE
               WITH NO LOCK
               INTO SY-PW-RCRD-WS
               KEY IS SY-KEY-1
             INVALID KEY
               DISPLAY "USER ID NOT ON FILE "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               PERFORM X100-ERROR THRU X190-EXIT
               GO TO F200-USER-ID.
           IF RECORD-LOCKED
               MOVE "SYMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO F220-READ-PW.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       F230-CONTINUE.
           GO TO F300-PASSWORD.
      *
       F290-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 07 POSITION 25 SIZE 6
               DISPLAY SPACES LINE 03 POSITION 63 SIZE 3
               GO TO F990-EXIT.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F200-USER-ID.
      *
       F300-PASSWORD.
           ACCEPT WS-USPW
               LINE 09 POSITION 25
               TAB OFF
             ON EXCEPTION
               FUNC-KEY
               GO TO F390-CHECK-FUNC.
       F310-COMPARE-PASSWORDS.
           IF WS-USPW NOT = SY-PW-USPW
               DISPLAY "PASSWORD NOT VALID "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               PERFORM X100-ERROR THRU X190-EXIT
               GO TO F300-PASSWORD.
           GO TO F400-READ-SY.
      *
       F390-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 03 POSITION 63 SIZE 3
               GO TO F200-USER-ID.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F300-PASSWORD.
      *
       F400-READ-SY.
           MOVE SY-PW-CPNO TO SY-KEY-1.
           READ SY-FILE
               WITH NO LOCK
               INTO SY-CP-RCRD-WS
               KEY IS SY-KEY-1
             INVALID KEY
               MOVE "* COMPANY NOT ON FILE *" TO SY-CP-NAME
               MOVE "CA" TO SY-CP-VSCD
               GO TO F420-DISPLY.
           IF RECORD-LOCKED
               MOVE "SYMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO F400-READ-SY.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       F420-DISPLY.
           DISPLAY SY-PW-CPNO LINE 11 POSITION 25
                   SY-CP-NAME LINE 11 POSITION 28
                   SY-PW-USNM LINE 13 POSITION 25.
           DISPLAY SY-PW-CPNO LINE 04 POSITION 11
                   SY-CP-NAME LINE 04 POSITION 14.
       F500-NEW-PASSWORD.
           ACCEPT WS-NUPW
               LINE 15 POSITION 25
               PROMPT TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO F590-CHECK-FUNC.
       F505-ENSURE.
           if ws-nupw = spaces 
               go to F510-DISPLY.
           DISPLAY "ARE YOU SURE YOU WANT TO "
               LINE 24 POSITION 01 HIGH.
           DISPLAY "CHANGE YOUR PASSWORD? (Y/N) "
               LINE 0 POSITION 0 HIGH.
           ACCEPT DUMMY-77
               LINE 0 POSITION 0
               PROMPT TAB ECHO.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           IF DUMMY-77 = "N"
               GO TO F500-NEW-PASSWORD.
           IF DUMMY-77 NOT = "Y"
               GO TO F505-ENSURE.
       F510-DISPLY.
           DISPLAY WS-NUPW LINE 15 POSITION 25.
           GO TO F600-DATE.
       F590-CHECK-FUNC.
           IF FUNC-02
               DISPLAY SPACES LINE 13 POSITION 25 SIZE 25
                       SPACES LINE 11 POSITION 25 SIZE 30
                       SPACES LINE 04 POSITION 11 SIZE 30
                       GO TO F300-PASSWORD.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F500-NEW-PASSWORD.
       F600-DATE.
           MOVE "NP"    TO LS-UVACDT-CMCD.
           MOVE SPACES  TO LS-UVACDT-NMSW.
           MOVE 17      TO LS-UVACDT-LINE.
           MOVE 25      TO LS-UVACDT-COLM.
           MOVE WS-CPDT TO LS-UVACDT-DATE.
           PERFORM X200-UVACDT THRU X205-EXIT.
           IF LS-UVACDT-CMCD = "02"
               GO TO F500-NEW-PASSWORD.
           IF LS-UVACDT-CMCD NOT = "00"
               MOVE "31" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
       F610-EDIT.
           IF SW-1-ON
               GO TO F620-SET-VALUES.
           move ls-uvacdt-date to ls-uvdtcv-indt.
           move "EX"           to ls-uvdtcv-cmcd.
           perform x999-uvdtcv thru x999-exit.
           IF LS-UVdtcv-otdt > WS-CPDT
               DISPLAY "SIGNON DATE MAY NOT BE GREATER "
                           LINE 24 POSITION 1 HIGH
                       "THAN THE COMPUTER DATE "
                           LINE 00 POSITION 0 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F600-DATE.
           MOVE LS-UVdtcv-otdt TO WS-DATE.
           subtract 1 from ws-ccyy giving ws-ccyy-minus-one.
           if ws-centyear < ws-ccyy-minus-one
               DISPLAY "SIGNON DATE NOT REASONABLE "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F600-DATE.
       F620-SET-VALUES.
      *    MOVE LS-UVdtcv-otdt TO WS-DATE.

           display ws-date line 12 position 1.
           PERFORM X050-REPLY THRU X090-EXIT

           MOVE WS-DATE TO DS-DATE.
           DISPLAY DS-DATE LINE 04 POSITION 54.
       F700-SET-VALUES.
           IF WS-NUPW = SPACES
             OR WS-NUPW = SY-PW-USPW
               GO TO F750-SET-STATION-RECORD.
       F710-REWRITE-PW.
           MOVE WS-NUPW TO SY-PW-USPW.
           REWRITE SY-RCRD FROM SY-PW-RCRD-WS.
           IF NOT FILE-OK
               MOVE "RW" TO FILE-IO
               GO TO Z100-FILE-ERR.
       F750-SET-STATION-RECORD.
           MOVE SPACES     TO SY-RCRD SY-ST-RCRD-WS.
           MOVE "ST"       TO SY-ST-RCTP-1.
           MOVE STNO-IDXX    TO SY-ST-STNO-1.
           MOVE WS-DATE    TO SY-ST-SODT.
           MOVE WS-USID    TO SY-ST-USID.
           MOVE SY-PW-CPNO TO SY-ST-CPNO.
           MOVE SY-CP-VSCD TO SY-ST-VSCD.
           MOVE SY-PW-EPNO TO SY-ST-EPNO.
       F760-WRITE.
           WRITE SY-RCRD FROM SY-ST-RCRD-WS.
           IF NOT FILE-OK
               MOVE "WR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       F800-SET-SCI.
           PERFORM X300-SET-LGIN-SW-ON THRU X390-EXIT.
       F990-EXIT.
           EXIT.
      
       X000-FUNC-ERR.
           DISPLAY "FUNCTION KEY NOT VALID "
               LINE 24 POSITION 1 HIGH.
       X050-REPLY.
           ACCEPT DUMMY-77 LINE 00 POSITION 00 TAB OFF.
           DISPLAY SPACES LINE 24 POSITION 01 SIZE 80.
       X090-EXIT.
           EXIT.
      
      
       X100-ERROR.
           ADD 1 TO WS-NECT.
           IF WS-NECT < 5
               GO TO X190-EXIT.
       X110-SECURITY-ERROR.
           DISPLAY
               "SECURITY VIOLATION"
                   LINE 22 POSITION 1 HIGH
               "YOU HAVE MADE 5 ATTEMPTS AT A VALID SIGNON & FAILED"
                   LINE 23 POSITION 1.
       X120-DISPLY.
           DISPLAY
               "GET HELP BEFORE TRYING AGAIN - PRESS [F1] TO EXIT "
                   LINE 24 POSITION 1 HIGH.
           ACCEPT DUMMY-77
               LINE 00 POSITION 00
               TAB OFF
             ON EXCEPTION
               FUNC-KEY
               GO TO X180-CHECK-FUNC.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           GO TO X120-DISPLY.
      *
       X180-CHECK-FUNC.
           IF FUNC-01
               GO TO A990-EXIT.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           GO TO X120-DISPLY.
      *
       X190-EXIT.
           EXIT.
      
      
       X200-UVACDT.
           CALL "UVACDT" USING
               LS-UVACDT-RCRD.
       X205-EXIT.
           EXIT.
      
      
       X300-SET-LGIN-SW-ON.
           MOVE "Y" TO LGIN-SW.
       X390-EXIT.
           EXIT.
      

       X999-UVdtcv.
           CALL "UVDTCV" USING
               LS-UVDTCV-RCRD.
       X999-EXIT.
           EXIT.
      
       Y000-CLOSE.
           CLOSE SY-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
       Y100-WRITE-LGIN.
           OPEN OUTPUT LGIN-FILE.
           IF NOT UNVL-SFEM-FLOK
               MOVE "OP" TO UNVL-SFEM-IOCD
               GO TO Z150-FATAL-ERR.
           WRITE LGIN-RCRD
              FROM LGIN-RCRD-WS.
           IF NOT UNVL-SFEM-FLOK
               MOVE "WR" TO UNVL-SFEM-IOCD
               GO TO Z150-FATAL-ERR.
           CLOSE LGIN-FILE.
           IF NOT UNVL-SFEM-FLOK
               MOVE "CL" TO UNVL-SFEM-IOCD
               GO TO Z150-FATAL-ERR.
       Y990-EXIT.
           EXIT.
      
      
           COPY "uv/cpy/uvdfemov".
           COPY "uv/cpy/uvdfem".
           COPY "uv/cpy/uvaflk".
