       IDENTIFICATION DIVISION.
       PROGRAM-ID.                 IVFG04.
      *
      *        INVOICING
      *        FINISHED GOODS INVOICE GENERATION
      *        SUBOUTINE
      *
      *        RETRIEVE TRANSACTIONS ROUTINE
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           COPY "iv/cpy/slivmt".

       DATA DIVISION.
       FILE SECTION.
           COPY "iv/cpy/fdivmt".

       WORKING-STORAGE SECTION.

       77  BM-STATUS-77            PIC XX.
       77  DUMMY-77                PIC X.

           COPY "uv/cpy/uvfckv".
           COPY "uv/cpy/uvsfemov".
           COPY "uv/cpy/uvlkmg".

           COPY "iv/cpy/rcivmt01".

       01  CONTROL-FIELDS.
           03  WS-FTSW             PIC X       VALUE "Y".
               88  FIRST-TIME      VALUE IS "Y".

       LINKAGE SECTION.

           COPY "iv/cpy/lsivfgge".

       PROCEDURE DIVISION
           USING
               LS-IVFGGE-RCRD.

       DECLARATIVES.
           COPY "iv/cpy/dcivmt".
       END DECLARATIVES.

       A000-MAINLINE-PGM SECTION.

       A000-START-PROGRAM.
           IF FIRST-TIME
               PERFORM B000-INIT THRU B990-EXIT.
           PERFORM F000-PROCESS THRU F990-EXIT.
      *
       A990-EXIT.
           EXIT PROGRAM.



       B000-INIT.
           MOVE "IVFG04" TO PROGRAM-NAME.
      *
       B100-OPEN-FILES.
           OPEN I-O   BM-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       B800-SET-VALUE.
           MOVE SPACES TO WS-FTSW.
      *
       B990-EXIT.
           EXIT.



       F000-PROCESS.
       F100-READ-BM.
           MOVE SPACES     TO BM-KEY-1.
           MOVE LS-IVFGGE-CPNO TO BM-CPNO-K1.
           MOVE "01"       TO BM-LFCD-K1.
           READ BM-FILE
               INTO BM-01-RCRD-WS
               KEY IS BM-KEY-1.
           IF RECORD-LOCKED
               MOVE "IVMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO F100-READ-BM.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       F110-SET-TRANS-NO.
           MOVE BM-01-NATN TO LS-IVFGGE-NATN.
           ADD 1 TO BM-01-NATN.
           IF BM-01-NATN = ZERO
               MOVE 1 TO BM-01-NATN.
      *
       F200-REWRITE.
           REWRITE BM-RCRD FROM BM-01-RCRD-WS.
           IF NOT FILE-OK
               MOVE "RW" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       F250-CONTINUE.
           MOVE "00" TO LS-IVFGGE-CMCD.
      *
       F990-EXIT.
           EXIT.



           COPY "uv/cpy/uvdfemov".
           COPY "uv/cpy/uvaflk".
