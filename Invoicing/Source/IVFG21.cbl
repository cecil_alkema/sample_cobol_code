       IDENTIFICATION DIVISION.
       PROGRAM-ID.                 IVFG21.
      *
      *        INVOICING
      *        FINISHED GOODS GENERATION ENTRY
      *        SUBROUTINE
      *
      *        HEADER MAINTENANCE
      *
      *
      *        SWITCHES
      *        ********
      *
      *        SWITCH-1 ON - ALLOW MAINTENANCE OF ANY STATUS INVOICE
      *                    - ALLOW MAINTENANCE OF INVOICE STATUS
      *                    - ALLOW MAINTENANCE OF NUMBER OF PAGES
      *                    - ALLOW MAINTENANCE OF INVOICE NUMBER
      *
      *    SWITCH-2 ON  - sales tax calcs based on bill-to customer
      *    SWITCH-2 OFF - sales tax calcs based on ship-to customer
      *
      *    Apr 1, 1997
      *    Hard coded variable ... ws-harmonized-pctg
      *    Federal sales tax percentage for 3 maritime provinces
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           SWITCH-1
               ON  IS SW-1-ON
               OFF IS SW-1-OFF
           SWITCH-2
               ON  IS SW-2-ON
               OFF IS SW-2-OFF.

       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           COPY "iv/cpy/slivhr".
           COPY "iv/cpy/slivdr".
           COPY "op/cpy/slopoh".
           COPY "sm/cpy/slstmt".
           COPY "ar/cpy/slctmt".
           COPY "sa/cpy/slsmmt".

       DATA DIVISION.
       FILE SECTION.
           COPY "iv/cpy/fdivhr".
           COPY "iv/cpy/fdivdr".
           COPY "op/cpy/fdopoh".
           COPY "sm/cpy/fdstmt".
           COPY "ar/cpy/fdctmt".
           COPY "sa/cpy/fdsmmt".

       WORKING-STORAGE SECTION.

       77  BH-STATUS-77            PIC XX.
       77  BD-STATUS-77            PIC XX.
       77  OH-STATUS-77            PIC XX.
       77  MM-STATUS-77            PIC XX.
       77  CT-STATUS-77            PIC XX.
       77  SM-STATUS-77            PIC XX.
       77  DUMMY-77                PIC X.
       77  MAIN-77                 PIC XX.

           COPY "uv/cpy/uvfckv".
           COPY "uv/cpy/uvsfemov".
           COPY "uv/cpy/uvlkmg".

           COPY "iv/cpy/rcivhr21".
           COPY "iv/cpy/rcivdr".
           COPY "op/cpy/rcopoh11".
           COPY "sm/cpy/rcstmt21".
           COPY "ar/cpy/rcctmt".
           COPY "sa/cpy/rcsmmt".

           COPY "uv/cpy/lsuvacdt".
           COPY "uv/cpy/lsuvdtcv".
           copy "uv/cpy/lsuvgfgo".

       01  CONTROL-FIELDS.
           03  ws-harmonized-pctg  pic 99v9 value 15.
           03  WS-ERSW             PIC X.
           03  WS-CHSW             PIC X.
           03  WS-DATE             PIC 9(8).
           03  WS-CTNO             PIC 9(5).
           03  WS-SMNO             PIC 9(5).
           03  WS-OPTION           PIC X(6).
           03  WS-OPTION-1         REDEFINES WS-OPTION.
               05  WS-FIELD        PIC 99.
               05  FILLER          PIC X(4).

       01  STORAGE-FIELDS.
           03  WS-TRNO             PIC 9(5).
           03  WS-ODNO             PIC 9(6).
           03  WS-PHCD             PIC X.
           03  WS-ISCD             PIC 9.
           03  WS-NOPG             PIC 999.
           03  WS-IVNO             PIC 9(06).

       01  SAVE-FIELDS.
           03  WS-CTNM             PIC X(35).
           03  WS-LGCD             PIC XX.
           03  WS-MTPC             PIC S99V9   COMP-3.
           03  WS-FTEX             PIC X.
           03  WS-FTAC             PIC X(10).
           03  WS-FTPC             PIC S99V9   COMP-3.
           03  WS-PTEX             PIC X.
           03  WS-PTAC             PIC X(10).
           03  WS-PTPC             PIC S99V9   COMP-3.
           03  WS-PVCD             PIC XX.

       01  DISPLAY-FIELDS.
           03  DS-TRNO             PIC ZZZZ9.
           03  DS-IVDT             PIC 99/99/99.
           03  DS-ODNO             PIC ZZZZZ9.
           03  DS-IVNO             PIC ZZZZZ9.
           03  DS-NOPG             PIC ZZ9.

       01  EDIT-FIELDS.
           03  ED-TRNO             PIC 9(5).

       LINKAGE SECTION.

           COPY "iv/cpy/lsivfgge".

       PROCEDURE DIVISION
           USING
               LS-IVFGGE-RCRD.

       DECLARATIVES.
           COPY "iv/cpy/dcivhr".
           COPY "iv/cpy/dcivdr".
           COPY "op/cpy/dcopoh".
           COPY "sm/cpy/dcstmt".
           COPY "ar/cpy/dcctmt".
           COPY "sa/cpy/dcsmmt".
       END DECLARATIVES.

       A000-MAINLINE-PGM SECTION.

       A000-START-PROGRAM.
           PERFORM B000-INIT THRU B990-EXIT.
       A100-MAINLINE.
           PERFORM D000-TRANSACTION THRU D990-EXIT.
           IF MAIN-77 = "QT"
               GO TO A800-CLOSING.
           IF MAIN-77 = "AD"
               GO TO A200-ADD-MODE.
           IF MAIN-77 = "CH"
               GO TO A500-CHANGE-MODE.
       A110-ERROR.
           MOVE "01" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
       A200-ADD-MODE.
           PERFORM F000-ADD-MODE THRU F990-EXIT.
           IF MAIN-77 = "RT"
               GO TO A100-MAINLINE.
           GO TO A800-CLOSING.
       A500-CHANGE-MODE.
           PERFORM H000-CHANGE-MODE THRU H990-EXIT.
           IF MAIN-77 = "RT"
               GO TO A100-MAINLINE.
       A800-CLOSING.
           PERFORM Y000-CLOSE THRU Y990-EXIT.
       A990-EXIT.
           EXIT PROGRAM.

       B000-INIT.
           MOVE "IVFG21" TO PROGRAM-NAME.
      *
       B100-OPEN-FILES.
           OPEN I-O   BH-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN I-O   BD-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN I-O   OH-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN INPUT MM-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN INPUT CT-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN INPUT SM-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       B300-DISPLY.
           MOVE "A1" TO LS-IVFGGE-CMCD.
           PERFORM X200-IVFG01 THRU X205-EXIT.
           IF LS-IVFGGE-CMCD NOT = "00"
               MOVE "11" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
      *
       B990-EXIT.
           EXIT.



       D000-TRANSACTION.
           IF SW-1-ON
               DISPLAY SPACES LINE 20 POSITION 02 SIZE 78
               DISPLAY SPACES LINE 19 POSITION 02 SIZE 78
               DISPLAY SPACES LINE 18 POSITION 02 SIZE 78.
           MOVE SPACES TO MAIN-77.
           IF LS-IVFGGE-NATN = ZERO
               PERFORM X210-IVFG04 THRU X215-EXIT.
      *
       D010-DISPLY.
           MOVE LS-IVFGGE-NATN TO DS-TRNO.
           DISPLAY DS-TRNO LINE 07 POSITION 38.
           GO TO D200-INVOICE-DATE.
      *
       D100-TRANS-NO.
           MOVE LS-IVFGGE-NATN TO ED-TRNO.
           MOVE LS-IVFGGE-NATN TO ws-TRNO.
           DISPLAY ED-TRNO LINE 07 POSITION 38.
           ACCEPT WS-TRNO
               LINE 07 POSITION 38
               update TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO D190-CHECK-FUNC.
      *
       D110-EDIT.
           IF WS-TRNO NOT NUMERIC
               DISPLAY "TRANS NO MUST BE NUMERIC "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO D100-TRANS-NO.
           IF WS-TRNO = ZERO
               DISPLAY "TRANS NO MAY NOT BE ZERO "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO D100-TRANS-NO.
      *
       D115-NEXT-AVAILABLE.
           IF WS-TRNO = LS-IVFGGE-NATN
               GO TO D010-DISPLY.
      *
       D120-READ-IH.
           MOVE SPACES         TO BH-KEY-1 BH-21-KEY-1-WS.
           MOVE LS-IVFGGE-CPNO     TO BH-21-CPNO-1.
           MOVE 21             TO BH-21-LFCD-1.
           MOVE WS-TRNO        TO BH-21-TRNO-1.
           MOVE BH-21-KEY-1-WS TO BH-KEY-1.
           READ BH-FILE
               INTO BH-21-RCRD-WS
               KEY IS BH-KEY-1
             INVALID KEY
               DISPLAY "TRANSACTION NOT ON FILE "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO D100-TRANS-NO.
           IF RECORD-LOCKED
               MOVE "IVHR" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO D120-READ-IH.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       D130-EDIT.
           IF SW-1-ON
               GO TO D150-DISPLY.
           IF BH-21-STCD = 1
             OR BH-21-STCD = 2
               GO TO D150-DISPLY.
           IF BH-21-STCD = 4
             OR BH-21-STCD = 5
               PERFORM X600-UNLOCK-BH THRU X605-EXIT
               DISPLAY "INVOICE HAS ALREADY BEEN PRINTED "
                           LINE 24 POSITION 1 HIGH
                       "- MAINTENANCE NOT ALLOWED "
                           LINE 00 POSITION 0 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO D100-TRANS-NO.
      *
       D135-ERROR.
           MOVE "21" TO FILE-STATUS
           GO TO Z100-FILE-ERR.
      *
       D150-DISPLY.
           MOVE WS-TRNO TO DS-TRNO.
           DISPLAY DS-TRNO LINE 07 POSITION 38.
      *
       D160-SET-VALUES.
           MOVE "CH" TO MAIN-77.
           GO TO D990-EXIT.
      *
       D190-CHECK-FUNC.
           IF NOT FUNC-02
               PERFORM X000-FUNC-ERR THRU X090-EXIT
               GO TO D100-TRANS-NO.
           DISPLAY SPACES LINE 07 POSITION 38 SIZE 5.
      *
       D195-SET-VALUES.
           MOVE "QT" TO MAIN-77.
           MOVE "02" TO LS-IVFGGE-CMCD.
           GO TO D990-EXIT.
      *
       D200-INVOICE-DATE.
           MOVE "NP"       TO LS-UVACDT-CMCD.
           MOVE SPACES     TO LS-UVACDT-NMSW.
           MOVE 09         TO LS-UVACDT-LINE.
           MOVE 35         TO LS-UVACDT-COLM.
           MOVE LS-IVFGGE-SODT TO LS-UVACDT-DATE.
           PERFORM X220-UVACDT THRU X225-EXIT.
           IF LS-UVACDT-CMCD = "00"
               GO TO D220-EDIT.
           IF LS-UVACDT-CMCD = "02"
               GO TO D100-TRANS-NO.
      *
       D210-ERROR.
           MOVE "23" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
      *
       D220-EDIT.
           move ls-uvacdt-date to ls-uvdtcv-indt.
           move "EX"           to ls-uvdtcv-cmcd.
           perform x999-uvdtcv thru x999-exit.
           IF LS-UVdtcv-otdt > LS-IVFGGE-SODT
               DISPLAY "DATE MAY NOT BE GREATER THAN SIGNON DATE "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO D200-INVOICE-DATE.
           IF LS-UVdtcv-otdt < LS-IVFGGE-MNDT
               DISPLAY "DATE IS LESS THAN MINIMUM ALLOWABLE "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO D200-INVOICE-DATE.
      *
       D280-SET-VALUES.
           MOVE LS-UVdtcv-otdt TO WS-DATE.
           MOVE "AD"           TO MAIN-77.
      *
       D990-EXIT.
           EXIT.



       F000-ADD-MODE.
           MOVE SPACES         TO BH-RCRD BH-21-RCRD-WS.
           MOVE ZEROS          TO BH-21-IVDT BH-21-ODNO
                                  BH-21-FIPC
                                  BH-21-NOPG BH-21-IVNO.
           MOVE LS-IVFGGE-CPNO TO BH-21-CPNO-1.
           MOVE 21             TO BH-21-LFCD-1.
           MOVE LS-IVFGGE-NATN TO BH-21-TRNO-1 WS-TRNO.
           MOVE WS-DATE        TO BH-21-IVDT.
           MOVE 1              TO BH-21-STCD.
      *
       F100-ORDER-NO.
           move spaces     to ls-uvgfgo-rcrd.
           move "PR"           to ls-uvgfgo-cmcd.
           move ls-ivfgge-cpno to ls-uvgfgo-cpno.
           move 10             to ls-uvgfgo-line.
           move 37             to ls-uvgfgo-colm.
           move "Y"            to ls-uvgfgo-insw.
           move spaces         to ls-uvgfgo-prgm.
           call "UVGFGO" using ls-uvgfgo-rcrd.
           if ls-uvgfgo-cmcd = "02"
               DISPLAY SPACES LINE 10 POSITION 37 SIZE 6
               MOVE "RT" TO MAIN-77
               GO TO F990-EXIT.
           move ls-uvgfgo-odno to ws-odno.
      *
       F110-EDIT.
           IF WS-ODNO NOT NUMERIC
               DISPLAY "ORDER NUMBER MUST BE NUMERIC "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F100-ORDER-NO.
           IF WS-ODNO = ZERO
               DISPLAY "ORDER NUMBER MAY NOT BE ZERO "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F100-ORDER-NO.
      *
       F120-READ-OH.
           MOVE SPACES         TO OH-KEY-1 OH-11-KEY-1-WS.
           MOVE LS-IVFGGE-CPNO TO OH-11-CPNO-1.
           MOVE 11             TO OH-11-LFCD-1.
           MOVE WS-ODNO        TO OH-11-ODNO-1.
           MOVE OH-11-KEY-1-WS TO OH-KEY-1.
           PERFORM X500-READ-OH THRU X505-EXIT.
           IF WS-ERSW = "Y"
               DISPLAY "ORDER NOT ON FILE "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F100-ORDER-NO.
      *
       F130-REVIEW-STATUS.
           IF OH-11-OSCD = "SH"
               GO TO F150-CONTINUE.
           IF OH-11-OSCD = "OP"
               PERFORM X610-UNLOCK-OH THRU X615-EXIT
               DISPLAY "ORDER HAS NOT BEEN SHIPPED "
                           LINE 24 POSITION 1 HIGH
                       "- INVOICING NOT ALLOWED "
                           LINE 00 POSITION 0 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F100-ORDER-NO.
           IF OH-11-OSCD = "TR"
               PERFORM X610-UNLOCK-OH THRU X615-EXIT
               DISPLAY "ORDER ALREADY ON INVOICE REGISTER "
                           LINE 24 POSITION 1 HIGH
                       "- INVOICING NOT ALLOWED "
                           LINE 00 POSITION 0 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F100-ORDER-NO.
           IF OH-11-OSCD = "IV"
             OR OH-11-OSCD = "CC"
               PERFORM X610-UNLOCK-OH THRU X615-EXIT
               DISPLAY "ORDER ALREADY CLOSED "
                           LINE 24 POSITION 1 HIGH
                       "- INVOICING NOT ALLOWED "
                           LINE 00 POSITION 0 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F100-ORDER-NO.
      *
       F135-ERROR.
           MOVE "31" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
      *
       F150-CONTINUE.
           IF OH-11-ODDT > WS-DATE
               PERFORM X610-UNLOCK-OH THRU X615-EXIT
               DISPLAY "ORDER DATE GREATER THAN INVOICE DATE "
                           LINE 24 POSITION 1 HIGH
                       "- INVOICING NOT ALLOWED "
                           LINE 00 POSITION 0 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F100-ORDER-NO.
      *
       F160-DISPLY.
           MOVE WS-ODNO TO DS-ODNO.
           DISPLAY DS-ODNO LINE 10 POSITION 37.
           GO TO F200-DISPLY-ORDER.
      *
       F200-DISPLY-ORDER.
       F210-BILL-TO.
           MOVE OH-11-CTNO-BL TO WS-CTNO.
           PERFORM X510-READ-CT THRU X515-EXIT.
           IF WS-ERSW = "Y"
               PERFORM X610-UNLOCK-OH THRU X615-EXIT
               DISPLAY "CUSTOMER BILL TO FOR THIS ORDER NOT ON FILE "
                           LINE 24 POSITION 1 HIGH
                       "- INVOICING NOT ALLOWED "
                           LINE 00 POSITION 0 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F100-ORDER-NO.
           DISPLAY WS-CTNO LINE 12 POSITION 38
                   CT-CTNM LINE 12 POSITION 45.
      *
       F220-REVIEW-COUNTRY.
           MOVE SPACES         TO MM-KEY-1 MM-21-KEY-1-WS.
           MOVE LS-IVFGGE-CPNO TO MM-21-CPNO-1.
           MOVE 21             TO MM-21-LFCD-1.
           MOVE CT-CTCD        TO MM-21-CTCD-1.
           MOVE MM-21-KEY-1-WS TO MM-KEY-1.
           PERFORM X530-READ-MM THRU X535-EXIT.
           IF WS-ERSW = "Y"
               PERFORM X610-UNLOCK-OH THRU X615-EXIT
               DISPLAY "CUSTOMER BILL TO COUNTRY CODE NOT ON FILE "
                           LINE 24 POSITION 1 HIGH
                       "- INVOICING NOT ALLOWED "
                           LINE 00 POSITION 0 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               DISPLAY SPACES LINE 12 POSITION 38 SIZE 42
               GO TO F100-ORDER-NO.
           if sw-2-on
               MOVE MM-21-ACNO TO WS-FTAC
               MOVE MM-21-STPC TO WS-FTPC.
      *
       F230-REVIEW-PROVINCE.
           MOVE SPACES         TO MM-KEY-1 MM-21-KEY-1-WS.
           MOVE LS-IVFGGE-CPNO TO MM-21-CPNO-1.
           MOVE 21             TO MM-21-LFCD-1.
           MOVE CT-CTCD        TO MM-21-CTCD-1.
           MOVE CT-PVCD        TO MM-21-PVCD-1.
           MOVE MM-21-KEY-1-WS TO MM-KEY-1.
           PERFORM X530-READ-MM THRU X535-EXIT.
           IF WS-ERSW = "Y"
             AND LS-IVFGGE-VSCD = "US"
               PERFORM X610-UNLOCK-OH THRU X615-EXIT
               DISPLAY "CUSTOMER BILL TO STATE CODE NOT ON FILE "
                           LINE 24 POSITION 1 HIGH
                       "- INVOICING NOT ALLOWED "
                           LINE 00 POSITION 0 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               DISPLAY SPACES LINE 12 POSITION 38 SIZE 42
               GO TO F100-ORDER-NO.
           IF WS-ERSW = "Y"
               PERFORM X610-UNLOCK-OH THRU X615-EXIT
               DISPLAY "CUSTOMER BILL TO PROVINCE CODE NOT ON FILE "
                           LINE 24 POSITION 1 HIGH
                       "- INVOICING NOT ALLOWED "
                           LINE 00 POSITION 0 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               DISPLAY SPACES LINE 12 POSITION 38 SIZE 42
               GO TO F100-ORDER-NO.

           if sw-2-off
               go to F250-SOLD-TO.

       F240-taxes-on-bill-to.
           MOVE MM-21-ACNO TO WS-PTAC.
           MOVE MM-21-STPC TO WS-PTPC.
           if CT-PVCD = "NB"
             or CT-PVCD = "NF"
             or CT-PVCD = "NS"
               MOVE ws-harmonized-pctg TO WS-FTPC.
           MOVE CT-CTNM TO WS-CTNM.
           MOVE CT-LGCD TO WS-LGCD.
           MOVE CT-MTPC TO WS-MTPC.
           MOVE "Y"     TO WS-FTEX WS-PTEX.
           IF CT-FTID = SPACES
               MOVE "N" TO WS-FTEX.
           IF CT-PTID = SPACES
               MOVE "N" TO WS-PTEX.
           MOVE CT-PVCD TO WS-PVCD.

       F250-SOLD-TO.
           MOVE OH-11-CTNO-SD TO WS-CTNO.
           PERFORM X510-READ-CT THRU X515-EXIT.
           DISPLAY WS-CTNO LINE 13 POSITION 38
                   CT-CTNM LINE 13 POSITION 45.
       

       F260-SHIP-TO.
           MOVE OH-11-CTNO-SH TO WS-CTNO.
           PERFORM X510-READ-CT THRU X515-EXIT.
           DISPLAY WS-CTNO LINE 14 POSITION 38
                   CT-CTNM LINE 14 POSITION 45.
       F260-REVIEW-COUNTRY.
           MOVE SPACES         TO MM-KEY-1 MM-21-KEY-1-WS.
           MOVE LS-IVFGGE-CPNO TO MM-21-CPNO-1.
           MOVE 21             TO MM-21-LFCD-1.
           MOVE CT-CTCD        TO MM-21-CTCD-1.
           MOVE MM-21-KEY-1-WS TO MM-KEY-1.
           PERFORM X530-READ-MM THRU X535-EXIT.
           IF WS-ERSW = "Y"
               PERFORM X610-UNLOCK-OH THRU X615-EXIT
               DISPLAY "CUSTOMER SHIP TO COUNTRY CODE NOT ON FILE "
                           LINE 24 POSITION 1 HIGH
                       "- INVOICING NOT ALLOWED "
                           LINE 00 POSITION 0 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               DISPLAY SPACES LINE 12 POSITION 38 SIZE 42
               GO TO F100-ORDER-NO.
           if sw-2-off
               MOVE MM-21-ACNO TO WS-FTAC
               MOVE MM-21-STPC TO WS-FTPC.
       F260-REVIEW-PROVINCE.
           MOVE SPACES         TO MM-KEY-1 MM-21-KEY-1-WS.
           MOVE LS-IVFGGE-CPNO TO MM-21-CPNO-1.
           MOVE 21             TO MM-21-LFCD-1.
           MOVE CT-CTCD        TO MM-21-CTCD-1.
           MOVE CT-PVCD        TO MM-21-PVCD-1.
           MOVE MM-21-KEY-1-WS TO MM-KEY-1.
           PERFORM X530-READ-MM THRU X535-EXIT.
           IF WS-ERSW = "Y"
             AND LS-IVFGGE-VSCD = "US"
               PERFORM X610-UNLOCK-OH THRU X615-EXIT
               DISPLAY "CUSTOMER SHIP TO STATE CODE NOT ON FILE "
                           LINE 24 POSITION 1 HIGH
                       "- INVOICING NOT ALLOWED "
                           LINE 00 POSITION 0 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               DISPLAY SPACES LINE 12 POSITION 38 SIZE 42
               GO TO F100-ORDER-NO.
           IF WS-ERSW = "Y"
               PERFORM X610-UNLOCK-OH THRU X615-EXIT
               DISPLAY "CUSTOMER SHIP TO PROVINCE CODE NOT ON FILE "
                           LINE 24 POSITION 1 HIGH
                       "- INVOICING NOT ALLOWED "
                           LINE 00 POSITION 0 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               DISPLAY SPACES LINE 12 POSITION 38 SIZE 42
               GO TO F100-ORDER-NO.

           if sw-2-on
               go to F270-SALESMAN.

       F260-taxes-on-ship-to.
           MOVE MM-21-ACNO TO WS-PTAC.
           MOVE MM-21-STPC TO WS-PTPC.
           if CT-PVCD = "NB"
             or CT-PVCD = "NF"
             or CT-PVCD = "NS"
               MOVE ws-harmonized-pctg TO WS-FTPC.
           MOVE CT-CTNM TO WS-CTNM.
           MOVE CT-LGCD TO WS-LGCD.
           MOVE CT-MTPC TO WS-MTPC.
           MOVE "Y"     TO WS-FTEX WS-PTEX.
           IF CT-FTID = SPACES
               MOVE "N" TO WS-FTEX.
           IF CT-PTID = SPACES
               MOVE "N" TO WS-PTEX.
           MOVE CT-PVCD TO WS-PVCD.

       F270-SALESMAN.
           MOVE OH-11-SMNO TO WS-SMNO.
           PERFORM X520-READ-SM THRU X525-EXIT.
           DISPLAY WS-SMNO LINE 15 POSITION 38
                   SM-SMNM LINE 15 POSITION 45.
      *
       F280-SET-BH-VALUES.
           MOVE WS-ODNO TO BH-21-ODNO.
           MOVE WS-FTPC TO BH-21-FIPC.
      *
       F300-PRINT-OR-HOLD.
           DISPLAY "P" LINE 17 POSITION 42.
           move "P" to ws-phcd.
           ACCEPT WS-PHCD
               LINE 17 POSITION 42
               update TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO F390-CHECK-FUNC.
      *
       F310-EDIT.
           IF WS-PHCD NOT = "P"
             AND WS-PHCD NOT = "H"
               GO TO F300-PRINT-OR-HOLD.
      *
       F320-DISPLY.
           DISPLAY WS-PHCD LINE 17 POSITION 42.
           MOVE WS-PHCD TO BH-21-PHCD.
           GO TO F600-QUERIE.
      *
       F390-CHECK-FUNC.
           IF NOT FUNC-02
               PERFORM X000-FUNC-ERR THRU X090-EXIT
               GO TO F300-PRINT-OR-HOLD.
      *
       F395-RETURN-TO-ORDER.
           PERFORM X610-UNLOCK-OH THRU X615-EXIT.
           DISPLAY SPACES LINE 17 POSITION 42 SIZE 1
                   SPACES LINE 15 POSITION 38 SIZE 42
                   SPACES LINE 14 POSITION 38 SIZE 42
                   SPACES LINE 13 POSITION 38 SIZE 42
                   SPACES LINE 12 POSITION 38 SIZE 42.
           GO TO F100-ORDER-NO.
      *
       F600-QUERIE.
           DISPLAY "CONTINUE? (Y/N)"
               LINE 24 POSITION 1 HIGH.
           move "Y" to dummy-77.
           ACCEPT DUMMY-77
               LINE 24 POSITION 17
               update TAB ECHO.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           IF DUMMY-77 = "N"
               GO TO F300-PRINT-OR-HOLD.
           IF DUMMY-77 NOT = "Y"
               GO TO F600-QUERIE.
      *
       F620-SET-OH-RECORD.
           MOVE "TR" TO OH-11-OSCD.
           PERFORM X700-REWRITE-OH THRU X705-EXIT.
      *
       F640-WRITE-BH.
           WRITE BH-RCRD FROM BH-21-RCRD-WS.
           IF NOT FILE-OK
               MOVE "WR" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       F700-DISPLY-NEXT.
           PERFORM X300-DISPLY-NEXT THRU X305-EXIT.
      *
       F800-SET-LINKAGE.
           MOVE "00"       TO LS-IVFGGE-CMCD.
           MOVE WS-TRNO    TO LS-IVFGGE-TRNO.
           MOVE ZEROS      TO LS-IVFGGE-NATN.
           MOVE "AD"       TO LS-IVFGGE-MDCD.
           MOVE WS-ODNO    TO LS-IVFGGE-ODNO.
           MOVE WS-LGCD    TO LS-IVFGGE-LGCD.
           MOVE OH-11-GDPC TO LS-IVFGGE-GDPC.
           MOVE WS-FTEX    TO LS-IVFGGE-FTEX.
           MOVE WS-FTAC    TO LS-IVFGGE-FTAC.
           MOVE WS-FTPC    TO LS-IVFGGE-FTPC.
           MOVE WS-PTEX    TO LS-IVFGGE-PTEX.
           MOVE WS-PTAC    TO LS-IVFGGE-PTAC.
           MOVE WS-PTPC    TO LS-IVFGGE-PTPC.
           MOVE WS-MTPC    TO LS-IVFGGE-MTPC.
           MOVE WS-PVCD    TO LS-IVFGGE-PVCD.
       F990-EXIT.
           EXIT.

       H000-CHANGE-MODE.
           MOVE SPACES TO WS-CHSW.
       H010-DISPLY.
           MOVE BH-21-IVDT TO DS-IVDT WS-DATE.
           MOVE BH-21-ODNO TO DS-ODNO WS-ODNO.
           DISPLAY DS-IVDT LINE 09 POSITION 35
                   DS-ODNO LINE 10 POSITION 37.
      *
       H020-READ-OH.
           MOVE SPACES         TO OH-KEY-1 OH-11-KEY-1-WS.
           MOVE LS-IVFGGE-CPNO TO OH-11-CPNO-1.
           MOVE 11             TO OH-11-LFCD-1.
           MOVE WS-ODNO        TO OH-11-ODNO-1.
           MOVE OH-11-KEY-1-WS TO OH-KEY-1.
           PERFORM X500-READ-OH THRU X505-EXIT.
           IF WS-ERSW = "Y"
               MOVE "41" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
      *
       H030-BILL-TO.
           MOVE OH-11-CTNO-BL TO WS-CTNO.
           PERFORM X510-READ-CT THRU X515-EXIT.
           IF WS-ERSW = "Y"
               MOVE "43" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
           DISPLAY WS-CTNO LINE 12 POSITION 38
                   CT-CTNM LINE 12 POSITION 45.
      *
       H040-REVIEW-COUNTRY.
           MOVE SPACES         TO MM-KEY-1 MM-21-KEY-1-WS.
           MOVE LS-IVFGGE-CPNO TO MM-21-CPNO-1.
           MOVE 21             TO MM-21-LFCD-1.
           MOVE CT-CTCD        TO MM-21-CTCD-1.
           MOVE MM-21-KEY-1-WS TO MM-KEY-1.
           PERFORM X530-READ-MM THRU X535-EXIT.
           IF WS-ERSW = "Y"
               MOVE "44" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
           if sw-2-on
               MOVE MM-21-ACNO TO WS-FTAC
               MOVE MM-21-STPC TO WS-FTPC.
      *
       H050-REVIEW-PROVINCE.
           MOVE SPACES         TO MM-KEY-1 MM-21-KEY-1-WS.
           MOVE LS-IVFGGE-CPNO TO MM-21-CPNO-1.
           MOVE 21             TO MM-21-LFCD-1.
           MOVE CT-CTCD        TO MM-21-CTCD-1.
           MOVE CT-PVCD        TO MM-21-PVCD-1.
           MOVE MM-21-KEY-1-WS TO MM-KEY-1.
           PERFORM X530-READ-MM THRU X535-EXIT.
           IF WS-ERSW = "Y"
               MOVE "45" TO FILE-STATUS
               GO TO Z100-FILE-ERR.

           if sw-2-off
               go to H070-SOLD-TO.

       H060-taxes-on-bill-to.
           MOVE MM-21-ACNO TO WS-PTAC.
           MOVE MM-21-STPC TO WS-PTPC.
           if CT-PVCD = "NB"
             or CT-PVCD = "NF"
             or CT-PVCD = "NS"
               MOVE ws-harmonized-pctg TO WS-FTPC.
           MOVE CT-CTNM TO WS-CTNM.
           MOVE CT-LGCD TO WS-LGCD.
           MOVE CT-MTPC TO WS-MTPC.
           MOVE "Y"     TO WS-FTEX WS-PTEX.
           IF CT-FTID = SPACES
               MOVE "N" TO WS-FTEX.
           IF CT-PTID = SPACES
               MOVE "N" TO WS-PTEX.
      *
       H070-SOLD-TO.
           MOVE OH-11-CTNO-SD TO WS-CTNO.
           PERFORM X510-READ-CT THRU X515-EXIT.
           DISPLAY WS-CTNO LINE 13 POSITION 38
                   CT-CTNM LINE 13 POSITION 45.
      *
       H075-SHIP-TO.
           MOVE OH-11-CTNO-SH TO WS-CTNO.
           PERFORM X510-READ-CT THRU X515-EXIT.
           DISPLAY WS-CTNO LINE 14 POSITION 38
                   CT-CTNM LINE 14 POSITION 45.
       H040-REVIEW-COUNTRY.
           MOVE SPACES         TO MM-KEY-1 MM-21-KEY-1-WS.
           MOVE LS-IVFGGE-CPNO TO MM-21-CPNO-1.
           MOVE 21             TO MM-21-LFCD-1.
           MOVE CT-CTCD        TO MM-21-CTCD-1.
           MOVE MM-21-KEY-1-WS TO MM-KEY-1.
           PERFORM X530-READ-MM THRU X535-EXIT.
           IF WS-ERSW = "Y"
               MOVE "44" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
           if sw-2-off
               MOVE MM-21-ACNO TO WS-FTAC
               MOVE MM-21-STPC TO WS-FTPC.
      *
       H050-REVIEW-PROVINCE.
           MOVE SPACES         TO MM-KEY-1 MM-21-KEY-1-WS.
           MOVE LS-IVFGGE-CPNO TO MM-21-CPNO-1.
           MOVE 21             TO MM-21-LFCD-1.
           MOVE CT-CTCD        TO MM-21-CTCD-1.
           MOVE CT-PVCD        TO MM-21-PVCD-1.
           MOVE MM-21-KEY-1-WS TO MM-KEY-1.
           PERFORM X530-READ-MM THRU X535-EXIT.
           IF WS-ERSW = "Y"
               MOVE "45" TO FILE-STATUS
               GO TO Z100-FILE-ERR.

           if sw-2-on
               go to H080-SALESMAN.

       H060-taxes-on-ship-to.
           MOVE MM-21-ACNO TO WS-PTAC.
           MOVE MM-21-STPC TO WS-PTPC.
           if CT-PVCD = "NB"
             or CT-PVCD = "NF"
             or CT-PVCD = "NS"
               MOVE ws-harmonized-pctg TO WS-FTPC.
           MOVE CT-CTNM TO WS-CTNM.
           MOVE CT-LGCD TO WS-LGCD.
           MOVE CT-MTPC TO WS-MTPC.
           MOVE "Y"     TO WS-FTEX WS-PTEX.
           IF CT-FTID = SPACES
               MOVE "N" TO WS-FTEX.
           IF CT-PTID = SPACES
               MOVE "N" TO WS-PTEX.
      *
       H080-SALESMAN.
           MOVE OH-11-SMNO TO WS-SMNO.
           PERFORM X520-READ-SM THRU X525-EXIT.
           DISPLAY WS-SMNO    LINE 15 POSITION 38
                   SM-SMNM    LINE 15 POSITION 45
                   BH-21-PHCD LINE 17 POSITION 42.
      *
       H085-SW-1-LITERALS.
           IF SW-1-OFF
               GO TO H087-DISPLAY.
           DISPLAY "61 NUMBER OF PAGES"
               LINE 18 POSITION 08 HIGH.
           DISPLAY "62 INVOICE NUMBER"
               LINE 19 POSITION 08 HIGH.
           DISPLAY "SW-1"
               LINE 20 POSITION 02.
           DISPLAY "63 STATUS CODE"
               LINE 20 POSITION 08 HIGH.
      *
       H087-DISPLAY.
           IF SW-1-ON
               MOVE BH-21-NOPG TO WS-NOPG DS-NOPG
               MOVE BH-21-IVNO TO WS-IVNO DS-IVNO
               MOVE BH-21-STCD TO WS-ISCD
               DISPLAY DS-NOPG LINE 18 POSITION 40
                       DS-IVNO LINE 19 POSITION 37
                       WS-ISCD LINE 20 POSITION 42.
      *
       H090-REVIEW-STATUS.
           IF OH-11-OSCD NOT = "TR"
               MOVE "46" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
      *
       H100-SELECT.
           DISPLAY "SELECT: "
               LINE 24 POSITION 34 HIGH.
           ACCEPT WS-OPTION
               LINE 00 POSITION 0
               PROMPT TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO H190-CHECK-FUNC.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           IF WS-OPTION = "DELETE"
               GO TO H600-QUERIE-DELETE.
           IF WS-FIELD = 01
               GO TO H200-INVOICE-DATE.
           IF WS-FIELD = 02
               GO TO H230-ORDER-NO.
           IF WS-FIELD = 11
               GO TO H260-PRINT-OR-HOLD.
           IF SW-1-ON
             AND WS-FIELD = 61
               GO TO H300-NUMBER-OF-PAGES.
           IF SW-1-ON
             AND WS-FIELD = 62
               GO TO H310-INVOICE-NUMBER.
           IF SW-1-ON
             AND WS-FIELD = 63
               GO TO H320-INVOICE-STATUS.
           GO TO H100-SELECT.
      *
       H190-CHECK-FUNC.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           IF FUNC-02
               GO TO H400-REVIEW-CHSW.
           IF FUNC-01
               GO TO H410-REWRITE-BH.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO H100-SELECT.
      *
       H200-INVOICE-DATE.
           MOVE "NP"       TO LS-UVACDT-CMCD.
           MOVE SPACES     TO LS-UVACDT-NMSW.
           MOVE 09         TO LS-UVACDT-LINE.
           MOVE 35         TO LS-UVACDT-COLM.
           MOVE BH-21-IVDT TO LS-UVACDT-DATE.
           PERFORM X220-UVACDT THRU X225-EXIT.
           IF LS-UVACDT-CMCD = "00"
               GO TO H210-EDIT.
           IF LS-UVACDT-CMCD = "02"
               PERFORM X000-FUNC-ERR THRU X090-EXIT
               GO TO H200-INVOICE-DATE.
      *
       H205-ERROR.
           MOVE "47" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
      *
       H210-EDIT.
           move ls-uvacdt-date to ls-uvdtcv-indt.
           move "EX"           to ls-uvdtcv-cmcd.
           perform x999-uvdtcv thru x999-exit.
           IF LS-UVdtcv-otdt = BH-21-IVDT
               GO TO H100-SELECT.
           IF LS-UVdtcv-otdt > LS-IVFGGE-SODT
               DISPLAY "DATE MAY NOT BE GREATER THAN SIGNON DATE "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO H200-INVOICE-DATE.
           IF LS-UVdtcv-otdt < LS-IVFGGE-MNDT
               DISPLAY "DATE IS LESS THAN MINIMUM ALLOWABLE "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO H200-INVOICE-DATE.
      *
       H215-SET-VALUES.
           MOVE LS-UVdtcv-otdt TO BH-21-IVDT WS-DATE.
           MOVE "Y" TO WS-CHSW.
           GO TO H100-SELECT.
      *
       H230-ORDER-NO.
           DISPLAY "ORDER NUMBER MAY NOT BE MAINTAINED "
               LINE 24 POSITION 1 HIGH.
           PERFORM X050-REPLY THRU X090-EXIT.
           GO TO H100-SELECT.
      *
       H260-PRINT-OR-HOLD.
           DISPLAY BH-21-PHCD LINE 17 POSITION 42.
           move BH-21-PHCD to ws-phcd.
           ACCEPT WS-PHCD
               LINE 17 POSITION 42
               update TAB ECHO.
      *
       H270-EDIT.
           IF WS-PHCD NOT = "P"
             AND WS-PHCD NOT = "H"
               GO TO H260-PRINT-OR-HOLD.
      *
       H275-DISPLY.
           DISPLAY WS-PHCD LINE 17 POSITION 42.
      *
       H280-REVIEW.
           IF WS-PHCD NOT = BH-21-PHCD
               MOVE WS-PHCD TO BH-21-PHCD
               MOVE "Y" TO WS-CHSW.
           GO TO H100-SELECT.
      *
       H300-NUMBER-OF-PAGES.
           MOVE BH-21-NOPG TO WS-NOPG DS-NOPG.
           DISPLAY SPACES  LINE 18 POSITION 40 SIZE 03
                   DS-NOPG LINE 18 POSITION 40.
           ACCEPT WS-NOPG
               LINE 18 POSITION 40 SIZE 03
               update TAB CONVERT.
           MOVE WS-NOPG TO DS-NOPG.
           DISPLAY DS-NOPG LINE 18 POSITION 40.
           IF WS-NOPG < 1
             OR WS-NOPG > 999
                 DISPLAY "NUMBER OF PAGES MUST BE 1 - 999 "
                     LINE 24 POSITION 01 HIGH
                 PERFORM X050-REPLY  THRU X090-EXIT
                 GO TO H300-NUMBER-OF-PAGES.
           IF WS-NOPG NOT = BH-21-NOPG
               MOVE WS-NOPG TO BH-21-NOPG
               MOVE "Y"     TO WS-CHSW.
           GO TO H100-SELECT.
      *
       H310-INVOICE-NUMBER.
           MOVE BH-21-IVNO TO WS-IVNO DS-IVNO.
           DISPLAY SPACES  LINE 19 POSITION 37 SIZE 06
                   DS-IVNO LINE 19 POSITION 37.
           ACCEPT WS-IVNO
               LINE 19 POSITION 37 SIZE 06
               update TAB CONVERT.
           MOVE WS-IVNO TO DS-IVNO.
           DISPLAY DS-IVNO LINE 19 POSITION 37.
           IF WS-IVNO NOT NUMERIC
                 DISPLAY "INVALID INVOICE NUMBER "
                     LINE 24 POSITION 01 HIGH
                 PERFORM X050-REPLY  THRU X090-EXIT
                 GO TO H310-INVOICE-NUMBER.
           IF WS-IVNO NOT = BH-21-IVNO
               MOVE WS-IVNO TO BH-21-IVNO
               MOVE "Y"     TO WS-CHSW.
           GO TO H100-SELECT.
      *
       H320-INVOICE-STATUS.
           MOVE BH-21-STCD TO WS-ISCD.
           DISPLAY SPACES  LINE 20 POSITION 42 SIZE 01
                   WS-ISCD LINE 20 POSITION 42.
           ACCEPT WS-ISCD
               LINE 20 POSITION 42 SIZE 01
               update TAB CONVERT.
           DISPLAY WS-ISCD LINE 20 POSITION 42.
           IF WS-ISCD < 1
             OR WS-ISCD > 4
                 DISPLAY "INVALID INVOICE STATUS "
                     LINE 24 POSITION 01 HIGH
                 PERFORM X050-REPLY  THRU X090-EXIT
                 GO TO H320-INVOICE-STATUS.
           IF WS-ISCD NOT = BH-21-STCD
               MOVE WS-ISCD TO BH-21-STCD
               MOVE "Y"     TO WS-CHSW.
           GO TO H100-SELECT.
      *
       H400-REVIEW-CHSW.
           IF WS-CHSW NOT = "Y"
               PERFORM X600-UNLOCK-BH THRU X605-EXIT
               GO TO H420-REVIEW-FUNC.
      *
       H410-REWRITE-BH.
           IF SW-1-OFF
               MOVE ZEROS TO BH-21-NOPG
               MOVE 1     TO BH-21-STCD.
           REWRITE BH-RCRD FROM BH-21-RCRD-WS.
           IF NOT FILE-OK
               MOVE "RW" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       H420-REVIEW-FUNC.
           IF FUNC-02
               GO TO H900-CLEAR-SCREEN.
      *
       H450-DISPLY-NEXT.
           PERFORM X300-DISPLY-NEXT THRU X305-EXIT.
      *
       H470-SET-LINKAGE.
           MOVE "01"       TO LS-IVFGGE-CMCD.
           MOVE WS-TRNO    TO LS-IVFGGE-TRNO.
           MOVE "CH"       TO LS-IVFGGE-MDCD.
           MOVE WS-ODNO    TO LS-IVFGGE-ODNO.
           MOVE WS-LGCD    TO LS-IVFGGE-LGCD.
           MOVE OH-11-GDPC TO LS-IVFGGE-GDPC.
           MOVE WS-FTEX    TO LS-IVFGGE-FTEX.
           MOVE WS-FTAC    TO LS-IVFGGE-FTAC.
           MOVE WS-FTPC    TO LS-IVFGGE-FTPC.
           MOVE WS-PTEX    TO LS-IVFGGE-PTEX.
           MOVE WS-PTAC    TO LS-IVFGGE-PTAC.
           MOVE WS-PTPC    TO LS-IVFGGE-PTPC.
           MOVE WS-MTPC    TO LS-IVFGGE-MTPC.
       H490-CONTINUE.
           GO TO H990-EXIT.
       
       H600-QUERIE-DELETE.
           DISPLAY "DELETE - ARE YOU SURE? (Y/N) "
               LINE 24 POSITION 1 HIGH.
           ACCEPT DUMMY-77
               LINE 00 POSITION 0
               PROMPT TAB ECHO.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           IF DUMMY-77 = "N"
               GO TO H100-SELECT.
           IF DUMMY-77 NOT = "Y"
               GO TO H600-QUERIE-DELETE.
       
       H690-START-BD.
           MOVE SPACES      TO BD-KEY-1 BD-KEY-1-WS.
           MOVE LS-IVFGGE-CPNO  TO BD-CPNO-1.
           MOVE 21          TO BD-LFCD-1.
           MOVE WS-TRNO     TO BD-TRNO-1.
           MOVE BD-KEY-1-WS TO BD-KEY-1.
           START BD-FILE
               KEY IS NOT LESS THAN BD-KEY-1
             INVALID KEY
               GO TO H800-DELETE-BH.
           IF RECORD-LOCKED
               MOVE "IVDR" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO H690-START-BD.
           IF NOT FILE-OK
               MOVE "ST" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       H700-READ-BD.
           READ BD-FILE
               NEXT RECORD
               INTO BD-RCRD-WS
             AT END
               GO TO H800-DELETE-BH.
           IF NOT RECORD-LOCKED
               GO TO H710-CHECK-FILE.
           MOVE "IVDR" TO WS-LOCK-FILE.
           PERFORM Z200-ACK-LOCK THRU Z290-EXIT.
           GO TO H700-READ-BD.
      *
       H710-CHECK-FILE.
           IF NOT FILE-OK
               MOVE "RS" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       H720-EDIT.
           IF BD-CPNO-1 NOT = LS-IVFGGE-CPNO
               GO TO H790-UNLOCK-BD.
           IF BD-LFCD-1 NOT = 21
               GO TO H790-UNLOCK-BD.
           IF BD-TRNO-1 NOT = WS-TRNO
               GO TO H790-UNLOCK-BD.
      *
       H750-DELETE-BD.
           DELETE BD-FILE RECORD.
           IF NOT FILE-OK
               MOVE "DL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           GO TO H700-READ-BD.
      *
       H790-UNLOCK-BD.
           UNLOCK BD-FILE RECORD.
           IF NOT FILE-OK
               MOVE "UL" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       H800-DELETE-BH.
           DELETE BH-FILE RECORD.
           IF NOT FILE-OK
               MOVE "DL" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       H850-REWRITE-OH.
           MOVE "SH" TO OH-11-OSCD.
           PERFORM X700-REWRITE-OH THRU X705-EXIT.
      *
       H900-CLEAR-SCREEN.
           IF SW-1-ON
               DISPLAY SPACES LINE 20 POSITION 02 SIZE 78
               DISPLAY SPACES LINE 19 POSITION 02 SIZE 78
               DISPLAY SPACES LINE 18 POSITION 02 SIZE 78.
           DISPLAY SPACES LINE 17 POSITION 42 SIZE 1
                   SPACES LINE 15 POSITION 38 SIZE 42
                   SPACES LINE 14 POSITION 38 SIZE 42
                   SPACES LINE 13 POSITION 38 SIZE 42
                   SPACES LINE 12 POSITION 38 SIZE 42
                   SPACES LINE 10 POSITION 37 SIZE 6
                   SPACES LINE 09 POSITION 35 SIZE 8.
           MOVE "RT" TO MAIN-77.
      *
       H990-EXIT.
           EXIT.



       X000-FUNC-ERR.
           DISPLAY "FUNCTION KEY NOT VALID "
               LINE 24 POSITION 1 HIGH.
      *
       X050-REPLY.
           ACCEPT DUMMY-77 LINE 00 POSITION 00 TAB OFF.
           DISPLAY SPACES LINE 24 POSITION 01 SIZE 80.
      *
       X090-EXIT.
           EXIT.



       X200-IVFG01.
           CALL "IVFG01" USING
               LS-IVFGGE-RCRD.
      *
       X205-EXIT.
           EXIT.



       X210-IVFG04.
           CALL "IVFG04" USING
               LS-IVFGGE-RCRD.
      *
       X215-EXIT.
           EXIT.



       X220-UVACDT.
           CALL "UVACDT" USING
               LS-UVACDT-RCRD.
      *
       X225-EXIT.
           EXIT.



       X300-DISPLY-NEXT.
           MOVE "C1" TO LS-IVFGGE-CMCD.
           PERFORM X200-IVFG01 THRU X205-EXIT.
           IF LS-IVFGGE-CMCD = "00"
               GO TO X304-DISPLY.
      *
       X301-ERROR.
           MOVE "71" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
      *
       X304-DISPLY.
           MOVE WS-TRNO TO DS-TRNO.
           MOVE WS-DATE TO DS-IVDT.
           DISPLAY DS-TRNO LINE 06 POSITION 12
                   WS-CTNM LINE 06 POSITION 36 SIZE 28
                   DS-IVDT LINE 06 POSITION 71.
      *
       X305-EXIT.
           EXIT.



       X500-READ-OH.
           MOVE SPACES TO WS-ERSW.
           READ OH-FILE
               INTO OH-11-RCRD-WS
               KEY IS OH-KEY-1
             INVALID KEY
               MOVE "Y" TO WS-ERSW
               GO TO X505-EXIT.
           IF RECORD-LOCKED
               MOVE "OPOH" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X500-READ-OH.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       X505-EXIT.
           EXIT.



       X510-READ-CT.
           MOVE SPACES         TO CT-KEY-1 WS-ERSW.
           MOVE LS-IVFGGE-CPNO TO CT-CPNO-K1.
           MOVE WS-CTNO        TO CT-CTNO-K1.
           READ CT-FILE
               INTO CT-RCRD-WS
               KEY IS CT-KEY-1
             INVALID KEY
               MOVE "* CUSTOMER NOT ON FILE *" TO CT-CTNM
               MOVE "Y" TO WS-ERSW
               GO TO X515-EXIT.
           IF RECORD-LOCKED
               MOVE "CTMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X510-READ-CT.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       X515-EXIT.
           EXIT.



       X520-READ-SM.
           MOVE SPACES         TO SM-KEY-1.
           MOVE LS-IVFGGE-CPNO TO SM-CPNO-K1.
           MOVE WS-SMNO        TO SM-SMNO-K1.
           READ SM-FILE
               INTO SM-RCRD-WS
               KEY IS SM-KEY-1
             INVALID KEY
               MOVE "* SALESMAN NOT ON FILE *" TO SM-SMNM
               GO TO X525-EXIT.
           IF RECORD-LOCKED
               MOVE "SMMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X520-READ-SM.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       X525-EXIT.
           EXIT.



       X530-READ-MM.
           MOVE SPACES TO WS-ERSW.
           READ MM-FILE
               INTO MM-21-RCRD-WS
               KEY IS MM-KEY-1
             INVALID KEY
               MOVE "Y" TO WS-ERSW
               GO TO X535-EXIT.
           IF RECORD-LOCKED
               MOVE "STMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X530-READ-MM.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       X535-EXIT.
           EXIT.



       X600-UNLOCK-BH.
           UNLOCK BH-FILE RECORD.
           IF NOT FILE-OK
               MOVE "UL" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       X605-EXIT.
           EXIT.



       X610-UNLOCK-OH.
           UNLOCK OH-FILE RECORD.
           IF NOT FILE-OK
               MOVE "UL" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       X615-EXIT.
           EXIT.



       X700-REWRITE-OH.
           REWRITE OH-RCRD FROM OH-11-RCRD-WS.
           IF NOT FILE-OK
               MOVE "RW" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       X705-EXIT.
           EXIT.

       X999-UVdtcv.
           CALL "UVDTCV" USING
               LS-UVDTCV-RCRD.
       X999-EXIT.
           EXIT.

       Y000-CLOSE.
           CLOSE BH-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE BD-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE OH-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE MM-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE CT-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE SM-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       Y990-EXIT.
           EXIT.



           COPY "uv/cpy/uvdfemov".
           COPY "uv/cpy/uvaflk".
