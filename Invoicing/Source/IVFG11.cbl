       IDENTIFICATION DIVISION.
       PROGRAM-ID.                 IVFG11.
      *
      *        INVOICING
      *        FINISHED GOODS INVOICE GENERATION
      *        SUBOUTINE
      *
      *        INITIALIZATION ROUTINE
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           COPY "iv/cpy/slivmt".

       DATA DIVISION.
       FILE SECTION.
           COPY "iv/cpy/fdivmt".

       WORKING-STORAGE SECTION.

       77  SY-STATUS-77            PIC XX.
       77  BM-STATUS-77            PIC XX.
       77  SCREEN-ID               PIC X(35)   VALUE
           "$PMISDIR/appl/obj/c/dsplscrn ivfgge".
       77  DUMMY-77                PIC X.

           COPY "uv/cpy/uvfckv".
           COPY "uv/cpy/uvpgid".
           COPY "uv/cpy/uvsfemov".
           COPY "uv/cpy/uvlkmg".

           COPY "uv/cpy/rcsymtst".
           COPY "uv/cpy/rcsymtcp".
           COPY "iv/cpy/rcivmt01".

           COPY "uv/cpy/lsuvcusf".
           COPY "uv/cpy/lsuvdssh".
           COPY "iv/cpy/lsivlsai".
           COPY "iv/cpy/lsivesdt".

       LINKAGE SECTION.

           COPY "iv/cpy/lsivfgge".

       PROCEDURE DIVISION
           USING
               LS-IVFGGE-RCRD.

       DECLARATIVES.
           COPY "iv/cpy/dcivmt".
       END DECLARATIVES.

       A000-MAINLINE-PGM SECTION.

       A000-START-PROGRAM.
           PERFORM B000-INIT THRU B990-EXIT.
           PERFORM F000-PROCESS THRU F990-EXIT.
           PERFORM Y000-CLOSE THRU Y990-EXIT.
      *
       A990-EXIT.
           EXIT PROGRAM.



       B000-INIT.
           MOVE "IV"     TO WS-STNM.
           MOVE "FGGE"   TO WS-PGNM.
           MOVE "IVFG11" TO PROGRAM-NAME.
           COPY "uv/cpy/uvcusf".
           CALL "SYSTEM" USING SCREEN-ID.
           COPY "uv/cpy/uvdssh".
      *
       B100-OPEN-FILES.
           OPEN INPUT BM-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       B300-LOAD-INTERFACE.
           MOVE SPACES     TO LS-IVLSAI-RCRD.
           MOVE "LD"       TO LS-IVLSAI-CMCD.
           MOVE SY-ST-CPNO TO LS-IVLSAI-CPNO.
           PERFORM X200-IVLSAI THRU X205-EXIT.
           IF LS-IVLSAI-CMCD NOT = "00"
               MOVE "11" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
      *
       B990-EXIT.
           EXIT.



       F000-PROCESS.
       F100-READ-BM.
           MOVE SPACES     TO BM-KEY-1.
           MOVE SY-ST-CPNO TO BM-CPNO-K1.
           MOVE "01"       TO BM-LFCD-K1.
           READ BM-FILE
               INTO BM-01-RCRD-WS
               KEY IS BM-KEY-1.
           IF RECORD-LOCKED
               MOVE "IVMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO F100-READ-BM.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       F110-REVIEW-STATUS.
           IF BM-01-FSCD = 1
               GO TO F200-EDIT-SIGNON-DATE.
           IF BM-01-FSCD = 3
               GO TO F160-INVOICES-PRINTED.
           IF BM-01-FSCD = 4
               GO TO F180-UPDATE-IN-PROGRESS.
      *
       F115-ERROR.
           MOVE "21" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
      *
       F160-INVOICES-PRINTED.
           DISPLAY "FINISHED GOODS INVOICES HAVE BEEN PRINTED - "
                       LINE 24 POSITION 1 HIGH
                   "GENERATION ENTRY NOT ALLOWED "
                       LINE 00 POSITION 0 HIGH.
           GO TO F500-ACK-ERROR.
      *
       F180-UPDATE-IN-PROGRESS.
           DISPLAY "FINISHED GOODS INVOICE UPDATE IN PROGRESS - "
                       LINE 24 POSITION 1 HIGH
                   "GENERATION ENTRY NOT ALLOWED "
                       LINE 00 POSITION 0 HIGH.
           GO TO F500-ACK-ERROR.
      *
       F200-EDIT-SIGNON-DATE.
           MOVE "00"       TO LS-IVESDT-CMCD.
           MOVE SY-ST-SODT TO LS-IVESDT-SODT.
           MOVE ZEROS      TO LS-IVESDT-MNDT.
           PERFORM X210-IVESDT THRU X215-EXIT.
           IF LS-IVESDT-CMCD = "00"
               GO TO F800-SET-LINKAGE.
           IF LS-IVESDT-CMCD = "05"
               GO TO F220-SIGNON-DATE-ERROR.
      *
       F210-ERROR.
           MOVE "23" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
      *
       F220-SIGNON-DATE-ERROR.
           DISPLAY "SIGNON DATE MAY NOT BE LESS "
                       LINE 24 POSITION 1 HIGH
                   "THAN THE CURRENT ACCOUNTING MONTH "
                       LINE 00 POSITION 0 HIGH.
      *
       F500-ACK-ERROR.
           PERFORM X050-REPLY THRU X090-EXIT.
           MOVE "QT" TO LS-IVFGGE-CMCD.
           GO TO F990-EXIT.
      *
       F800-SET-LINKAGE.
           MOVE "00"       TO LS-IVFGGE-CMCD.
           MOVE SY-ST-CPNO TO LS-IVFGGE-CPNO.
           MOVE SY-ST-VSCD TO LS-IVFGGE-VSCD.
           MOVE SY-ST-SODT TO LS-IVFGGE-SODT.
           MOVE LS-IVESDT-MNDT TO LS-IVFGGE-MNDT.
      *
       F990-EXIT.
           EXIT.



       X000-FUNC-ERR.
           DISPLAY "FUNCTION KEY NOT VALID "
               LINE 24 POSITION 1 HIGH.
      *
       X050-REPLY.
           ACCEPT DUMMY-77 LINE 00 POSITION 00 TAB OFF.
           DISPLAY SPACES LINE 24 POSITION 01 SIZE 80.
      *
       X090-EXIT.
           EXIT.



       X200-IVLSAI.
           CALL "IVLSAI" USING
               LS-IVLSAI-RCRD.
      *
       X205-EXIT.
           EXIT.



       X210-IVESDT.
           CALL "IVESDT" USING
               LS-IVESDT-RCRD
               BM-01-RCRD-WS.
      *
       X215-EXIT.
           EXIT.



       Y000-CLOSE.
           CLOSE BM-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       Y990-EXIT.
           EXIT.



           COPY "uv/cpy/uvdfemov".
           COPY "uv/cpy/uvaflk".
