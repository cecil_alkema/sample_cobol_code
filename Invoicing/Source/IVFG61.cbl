       IDENTIFICATION DIVISION.
       PROGRAM-ID.                 IVFG61.
      *
      *        INVOICING
      *        FINISHED GOODS INVOICE GENERATION
      *        SUBROUTINE
      *
      *        THIS SUBROUTINE IS USED TO ADD / CHANGE TEXT.
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           COPY "iv/cpy/slivmt".
           COPY "iv/cpy/slivdr".

       DATA DIVISION.
       FILE SECTION.
           COPY "iv/cpy/fdivmt".
           COPY "iv/cpy/fdivdr".

       WORKING-STORAGE SECTION.

       77  BM-STATUS-77            PIC XX.
       77  BD-STATUS-77            PIC XX.
       77  DUMMY-77                PIC X.
       77  MAIN-77                 PIC XX.
       77  I-77                    PIC 99.

           COPY "uv/cpy/uvfckv".
           COPY "uv/cpy/uvpgid".
           COPY "uv/cpy/uvsfemov".
           COPY "uv/cpy/uvlkmg".

           COPY "iv/cpy/rcivmt11".
           COPY "iv/cpy/rcid2121".

       01  CONTROL-FIELDS.
           03  WS-ERSW             PIC X.
           03  WS-SLNO             PIC 99.
           03  WS-ELNO             PIC 99.
           03  WS-LINE             PIC 99.
           03  WS-CRNO             PIC 99.
           03  WS-LNNO             PIC 99.

       01  STORAGE-FIELDS.
           03  WS-TEXT             PIC X(60).
           03  WS-TEXT-1           REDEFINES WS-TEXT.
               05  WS-CTRL         PIC X.
               05  WS-TPCD         PIC XX.
               05  FILLER          PIC X(57).

       LINKAGE SECTION.

           COPY "iv/cpy/lsivfgge".

       PROCEDURE DIVISION
           USING
               LS-IVFGGE-RCRD.

       DECLARATIVES.
           COPY "iv/cpy/dcivmt".
           COPY "iv/cpy/dcivdr".
       END DECLARATIVES.

       A000-MAINLINE-PGM SECTION.

       A000-START-PROGRAM.
           PERFORM B000-INIT THRU B990-EXIT.
           PERFORM F000-PROCESS THRU F990-EXIT.
           PERFORM Y000-CLOSE THRU Y990-EXIT.
      *
       A990-EXIT.
           EXIT PROGRAM.



       B000-INIT.
           MOVE "IVFG61" TO PROGRAM-NAME.
      *
       B200-OPEN-FILES.
           OPEN INPUT BM-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN I-O   BD-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
       B300-DISPLY.
           DISPLAY SPACES         LINE 09 POSITION 02 SIZE 78
                   "INVOICE TEXT" LINE 08 POSITION 13 HIGH.
      *
       B500-DISPLY.
           MOVE 1 TO WS-SLNO.
           PERFORM X600-DISPLY THRU X605-EXIT.
      *
       B990-EXIT.
           EXIT.



       F000-PROCESS.
           MOVE 01 TO WS-CRNO.
           MOVE 11 TO WS-LINE.
      *
       F090-START-BD.
           PERFORM X500-START-BD THRU X505-EXIT.
           IF WS-ERSW = "Y"
               GO TO F200-ADD-PROCESS.
      *
       F100-READ-BD.
           PERFORM X510-READ-BD THRU X515-EXIT.
           IF WS-ERSW = "Y"
               GO TO F200-ADD-PROCESS.
      *
       F110-REVIEW.
           IF BD-21-LNCD-1-21 NOT = WS-CRNO
               MOVE "21" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
           IF WS-CRNO > 10
               GO TO F140-CONTINUE.
           DISPLAY BD-21-TEXT-21 LINE WS-LINE POSITION 13.
           ADD 1 TO WS-LINE.
      *
       F140-CONTINUE.
           ADD 1 TO WS-CRNO.
           GO TO F100-READ-BD.
      *
       F150-NEW-LINE.
           ADD 1 TO WS-CRNO.
      *
       F200-ADD-PROCESS.
           DISPLAY WS-CRNO LINE 09 POSITION 08 HIGH.
           ACCEPT WS-TEXT
               LINE 09 POSITION 13
               PROMPT TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO F290-CHECK-FUNC.
      *
       F210-REVIEW-CRNO.
           IF WS-CRNO NOT = 91
               GO TO F220-REVIEW.
      *
       F215-MAX-LINES-HIT.
           DISPLAY "CONGRATULATIONS, YOU'VE ENTERED 90 LINES "
                       LINE 24 POSITION 1 HIGH
                   "- NO MORE ALLOWED! "
                       LINE 00 POSITION 0 HIGH.
           PERFORM X050-REPLY THRU X090-EXIT.
           GO TO F200-ADD-PROCESS.
      *
       F220-REVIEW.
           PERFORM X300-REVIEW-TEXT THRU X390-EXIT.
           IF WS-ERSW = "Y"
               GO TO F200-ADD-PROCESS.
      *
       F250-SET-RECORD.
           MOVE SPACES         TO BD-RCRD BD-21-RCRD-WS-21.
           MOVE LS-IVFGGE-CPNO TO BD-21-CPNO-1-21.
           MOVE 21             TO BD-21-LFCD-1-21.
           MOVE LS-IVFGGE-TRNO TO BD-21-TRNO-1-21.
           MOVE 21             TO BD-21-RCCD-1-21.
           MOVE WS-CRNO        TO BD-21-LNCD-1-21.
           MOVE WS-TEXT        TO BD-21-TEXT-21.
      *
       F260-WRITE.
           WRITE BD-RCRD FROM BD-21-RCRD-WS-21.
           IF NOT FILE-OK
               MOVE "WR" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       F265-DISPLY.
           IF WS-CRNO > WS-ELNO
             OR WS-CRNO < WS-SLNO
               GO TO F280-CONTINUE.
      *
       F270-DISPLY-TEXT.
           SUBTRACT WS-CRNO FROM WS-ELNO
               GIVING I-77.
           SUBTRACT I-77 FROM 20
               GIVING WS-LINE.
           DISPLAY WS-TEXT LINE WS-LINE POSITION 13.
      *
       F280-CONTINUE.
           GO TO F150-NEW-LINE.
      *
       F290-CHECK-FUNC.
           IF NOT FUNC-01
             AND NOT FUNC-03
               PERFORM X000-FUNC-ERR THRU X090-EXIT
               GO TO F200-ADD-PROCESS.
           DISPLAY SPACES LINE 09 POSITION 13 SIZE 60.
           IF FUNC-01
               MOVE "01" TO LS-IVFGGE-CMCD
               GO TO F990-EXIT.
      *
       F300-LINE-CHANGE.
           DISPLAY SPACES  LINE 09 POSITION 13 SIZE 60
                   WS-CRNO LINE 09 POSITION 08 HIGH.
           move ws-crno to ws-lnno.
           ACCEPT WS-LNNO
               LINE 09 POSITION 08
               update TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO F390-CHECK-FUNC.
      *
       F310-EDIT.
           IF WS-LNNO NOT NUMERIC
               DISPLAY "LINE NUMBER MUST BE NUMERIC "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F300-LINE-CHANGE.
           IF WS-LNNO = ZERO
             OR WS-LNNO > WS-CRNO
               DISPLAY "LINE NUMBER NOT VALID "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO F300-LINE-CHANGE.
           IF WS-LNNO = WS-CRNO
               GO TO F200-ADD-PROCESS.
      *
       F320-READ-BD.
           MOVE SPACES            TO BD-KEY-1 BD-21-KEY-1-WS-21.
           MOVE LS-IVFGGE-CPNO    TO BD-21-CPNO-1-21.
           MOVE 21                TO BD-21-LFCD-1-21.
           MOVE LS-IVFGGE-TRNO    TO BD-21-TRNO-1-21.
           MOVE 21                TO BD-21-RCCD-1-21.
           MOVE WS-LNNO           TO BD-21-LNCD-1-21.
           MOVE BD-21-KEY-1-WS-21 TO BD-KEY-1.
           READ BD-FILE
               INTO BD-21-RCRD-WS-21
               KEY IS BD-KEY-1
             INVALID KEY
               MOVE "22" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
           IF RECORD-LOCKED
               MOVE "IVDR" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO F320-READ-BD.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       F380-CONTINUE.
           GO TO F700-CHANGE-PROCESS.
      *
       F390-CHECK-FUNC.
           IF FUNC-01
             AND WS-CRNO > WS-ELNO
               ADD 5 TO WS-SLNO
               GO TO F400-SCREEN-DISPLY.
           IF FUNC-02
             AND WS-SLNO > 1
               SUBTRACT 5 FROM WS-SLNO
               GO TO F400-SCREEN-DISPLY.
           IF FUNC-03
             AND WS-SLNO > 1
               MOVE 1 TO WS-SLNO
               GO TO F400-SCREEN-DISPLY.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F300-LINE-CHANGE.
      *
       F400-SCREEN-DISPLY.
           PERFORM X600-DISPLY THRU X605-EXIT.
           MOVE 11 TO WS-LINE.
      *
       F490-START-BD.
           PERFORM X500-START-BD THRU X505-EXIT.
           IF WS-ERSW = "Y"
               MOVE "23" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
      *
       F500-READ-BD.
           PERFORM X510-READ-BD THRU X515-EXIT.
           IF WS-ERSW = "Y"
               GO TO F300-LINE-CHANGE.
      *
       F510-REVIEW.
           DISPLAY BD-21-TEXT-21 LINE WS-LINE POSITION 13.
           ADD 1 TO WS-LINE.
           IF WS-LINE < 21
               GO TO F500-READ-BD.
           PERFORM X520-UNLOCK-BD THRU X525-EXIT.
       F590-CONTINUE.
           GO TO F300-LINE-CHANGE.
      *
       F700-CHANGE-PROCESS.
           DISPLAY WS-LNNO       LINE 09 POSITION 08 HIGH
                   BD-21-TEXT-21 LINE 09 POSITION 13.
           move BD-21-TEXT-21 to ws-text.
           ACCEPT WS-TEXT
               LINE 09 POSITION 13
               update TAB ECHO.
      *
       F710-REVIEW.
           PERFORM X300-REVIEW-TEXT THRU X390-EXIT.
           IF WS-ERSW = "Y"
               GO TO F700-CHANGE-PROCESS.
      *
       F720-DISPLY.
           DISPLAY WS-TEXT LINE 09 POSITION 13.
           IF WS-LNNO > WS-ELNO
             OR WS-LNNO < WS-SLNO
               GO TO F750-CONTINUE.
      *
       F730-DISPLY-TEXT.
           SUBTRACT WS-LNNO FROM WS-ELNO
               GIVING I-77.
           SUBTRACT I-77 FROM 20
               GIVING WS-LINE.
           DISPLAY WS-TEXT LINE WS-LINE POSITION 13.
      *
       F750-CONTINUE.
           MOVE WS-TEXT TO BD-21-TEXT-21.
      *
       F780-REWRITE.
           REWRITE BD-RCRD FROM BD-21-RCRD-WS-21.
           IF NOT FILE-OK
               MOVE "RW" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       F790-CONTINUE.
           GO TO F300-LINE-CHANGE.
      *
       F990-EXIT.
           EXIT.



       X000-FUNC-ERR.
           DISPLAY "FUNCTION KEY NOT VALID "
               LINE 24 POSITION 1 HIGH.
      *
       X050-REPLY.
           ACCEPT DUMMY-77 LINE 00 POSITION 00 TAB OFF.
           DISPLAY SPACES LINE 24 POSITION 01 SIZE 80.
      *
       X090-EXIT.
           EXIT.



       X300-REVIEW-TEXT.
           MOVE SPACES TO WS-ERSW.
      *
       X310-REVIEW-CONTROL.
           IF WS-CTRL NOT = "/"
               GO TO X390-EXIT.
           IF WS-TPCD = SPACES
               GO TO X390-EXIT.
      *
       X320-READ-BM.
           PERFORM X400-READ-BM THRU X405-EXIT.
           IF WS-ERSW = "Y"
               DISPLAY "STANDARD TEXT PHRASE NOT ON FILE "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO X390-EXIT.
      *
       X330-SET-DISPLY.
           MOVE BM-11-LNTX TO WS-TEXT.
      *
       X390-EXIT.
           EXIT.



       X400-READ-BM.
           MOVE SPACES         TO BM-KEY-1 BM-11-KEY-1-WS WS-ERSW.
           MOVE LS-IVFGGE-CPNO TO BM-11-CPNO-1.
           MOVE 11             TO BM-11-LFCD-1.
           MOVE WS-TPCD        TO BM-11-TPCD-1.
           MOVE BM-11-KEY-1-WS TO BM-KEY-1.
           READ BM-FILE
               INTO BM-11-RCRD-WS
               KEY IS BM-KEY-1
             INVALID KEY
               MOVE "Y" TO WS-ERSW
               GO TO X405-EXIT.
           IF RECORD-LOCKED
               MOVE "IVMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X400-READ-BM.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       X405-EXIT.
           EXIT.



       X500-START-BD.
           MOVE SPACES            TO WS-ERSW.
           MOVE SPACES            TO BD-KEY-1 BD-21-KEY-1-WS-21.
           MOVE LS-IVFGGE-CPNO    TO BD-21-CPNO-1-21.
           MOVE 21                TO BD-21-LFCD-1-21.
           MOVE LS-IVFGGE-TRNO    TO BD-21-TRNO-1-21.
           MOVE 21                TO BD-21-RCCD-1-21.
           MOVE WS-SLNO           TO BD-21-LNCD-1-21.
           MOVE BD-21-KEY-1-WS-21 TO BD-KEY-1.
           START BD-FILE
               KEY IS NOT LESS THAN BD-KEY-1
             INVALID KEY
               MOVE "Y" TO WS-ERSW
               GO TO X505-EXIT.
           IF RECORD-LOCKED
               MOVE "IVDR" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X500-START-BD.
           IF NOT FILE-OK
               MOVE "ST" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       X505-EXIT.
           EXIT.



       X510-READ-BD.
           MOVE SPACES TO WS-ERSW.
           READ BD-FILE
               NEXT RECORD
               INTO BD-21-RCRD-WS-21
             AT END
               GO TO X515-ERROR.
           IF NOT RECORD-LOCKED
               GO TO X510-CHECK-FILE.
           MOVE "IVDR" TO WS-LOCK-FILE.
           PERFORM Z200-ACK-LOCK THRU Z290-EXIT.
           GO TO X510-READ-BD.
      *
       X510-CHECK-FILE.
           IF NOT FILE-OK
               MOVE "RS" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       X510-EDIT.
           IF BD-21-CPNO-1-21 NOT = LS-IVFGGE-CPNO
               GO TO X510-UNLOCK-BD.
           IF BD-21-LFCD-1-21 NOT = 21
               GO TO X510-UNLOCK-BD.
           IF BD-21-TRNO-1-21 NOT = LS-IVFGGE-TRNO
               GO TO X510-UNLOCK-BD.
           IF BD-21-RCCD-1-21 NOT = 21
               GO TO X510-UNLOCK-BD.
           GO TO X515-EXIT.
      *
       X510-UNLOCK-BD.
           PERFORM X520-UNLOCK-BD THRU X525-EXIT.
      *
       X515-ERROR.
           MOVE "Y" TO WS-ERSW.
      *
       X515-EXIT.
           EXIT.



       X520-UNLOCK-BD.
           UNLOCK BD-FILE RECORD.
           IF NOT FILE-OK
               MOVE "UL" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       X525-EXIT.
           EXIT.



       X600-DISPLY.
           MOVE WS-SLNO TO WS-LNNO.
           MOVE 11      TO WS-LINE.
      *
       X605-LOOP.
           DISPLAY WS-LNNO LINE WS-LINE POSITION 08 HIGH
                   SPACES  LINE WS-LINE POSITION 13 SIZE 60.
           ADD 1 TO WS-LINE.
           IF WS-LINE < 21
               ADD 1 TO WS-LNNO
               GO TO X605-LOOP.
           MOVE WS-LNNO TO WS-ELNO.
      *
       X605-EXIT.
           EXIT.



       Y000-CLOSE.
           CLOSE BM-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE BD-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       Y990-EXIT.
           EXIT.



           COPY "uv/cpy/uvdfemov".
           COPY "uv/cpy/uvaflk".
