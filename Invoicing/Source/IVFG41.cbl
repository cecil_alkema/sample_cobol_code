       IDENTIFICATION DIVISION.
       PROGRAM-ID.                 IVFG41.
      *
      *        INVOICING
      *        FINISHED GOODS INVOICE GENERATION
      *        SUBROUTINE
      *
      *        CALCULATION SUBROUTINE
      *
      *        NOTES:
      *        ******
      *
      *        1 - INVOICE DISTRIBUTION AMOUNTS ARE CALCULATED FROM
      *            ORDER DETAIL - 51 RECORDS ARE SUBSEQUENTLY WRITTEN.
      *
      *        2 - POSSIBLY TWO TEXT LINES MAY BE ISSUED - ONE FOR
      *            FEDERAL TAX INCLUDED IN PRICE, THE OTHER FOR EXCISE
      *            TAX - 21 RECORDS.
      *
      *        STANDARD TEXT CODES REQUIRED:
      *        *****************************
      *
      *        X1 - FEDERAL TAX INCLUDED IN PRICE ( ENGLISH )
      *        X2 - EXCISE SALES TAX INCLUDED IN PRICE ( ENGLISH )
      *
      *        X3 - FEDERAL TAX INCLUDED IN PRICE ( FRENCH )
      *        X4 - EXCISE SALES TAX INCLUDED IN PRICE ( FRENCH )
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           COPY "iv/cpy/slivmt".
           COPY "iv/cpy/slivdr".
           COPY "op/cpy/slopod".

       DATA DIVISION.
       FILE SECTION.
           COPY "iv/cpy/fdivmt".
           COPY "iv/cpy/fdivdr".
           COPY "op/cpy/fdopod".

       WORKING-STORAGE SECTION.

       77  BM-STATUS-77            PIC XX.
       77  OD-STATUS-77            PIC XX.
       77  BD-STATUS-77            PIC XX.
       77  DUMMY-77                PIC X.

           COPY "uv/cpy/uvfckv".
           COPY "uv/cpy/uvsfemov".
           COPY "uv/cpy/uvlkmg".

           COPY "iv/cpy/rcivmt11".
           COPY "iv/cpy/rcid2121".
           COPY "iv/cpy/rcid2151".
           COPY "op/cpy/rcopod".

           COPY "uv/cpy/lsuvcpex".
           COPY "iv/cpy/lsivlsai".
           COPY "iv/cpy/lsivtxcl".

       01  CONTROL-FIELDS.
           03  WS-ERSW             PIC X.
           03  WS-LNCD             PIC 99.
           03  WS-STCD             PIC XX.
           03  WS-FTPC             PIC S99V9.
           03  WS-VALU             PIC S9(9)V99.
           03  WS-VALU-1           REDEFINES WS-VALU.
               05  WS-QNTY         PIC S9(9).
               05  WS-RMDR         PIC 99.
           03  WS-NTAM             PIC S9(7)V99    COMP-3.
           03  WS-ASAM             PIC S9(7)V99    COMP-3.
           03  WS-DTCD             PIC 99.
           03  WS-ACNO             PIC X(10).
           03  WS-ACAM             PIC S9(7)V99    COMP-3.
           03  WS-CLAM             PIC S9(7)V99    COMP-3.
           03  WS-MINUS-1          PIC S9          VALUE -1.

       01  TEXT-LINE-FIELDS.
           03  WS-LN-TEXT          PIC X(60).
           03  WS-LN-TEXT-1        REDEFINES WS-LN-TEXT.
               05  WS-LN-CHAR      OCCURS 60 TIMES PIC X.
           03  WS-LN-FCPS          PIC 99.
           03  WS-LN-SCPS          PIC 99.
           03  WS-CR-DATA          PIC X(13).
           03  WS-CR-DATA-1        REDEFINES WS-CR-DATA.
               05  WS-CR-CHAR      OCCURS 13 TIMES PIC X.
           03  WS-CR-DATA-2        REDEFINES WS-CR-DATA.
               05  WS-CR-AMNT      PIC Z,ZZZ,ZZZ.99-.
           03  WS-CR-DATA-3        REDEFINES WS-CR-DATA.
               05  FILLER          PIC X.
               05  WS-CR-QNTY      PIC ZZZ,ZZZ,ZZ9-.
           03  WS-CR-FCPS          PIC 99.
           03  WS-SR-FCPS          PIC 99.
           03  WS-SR-SCPS          PIC 99.
           03  WS-NOSR             PIC 99.

       01  STORAGE-FIELDS.
           03  WS-SALE             PIC S9(7)V99    COMP-3.
           03  WS-FTIP             PIC S9(7)V99    COMP-3.
           03  WS-DSCT             PIC S9(7)V99    COMP-3.
           03  WS-FDTX             PIC S9(7)V99    COMP-3.
           03  WS-ECTX             PIC S9(7)V99    COMP-3.
           03  WS-TAPV             PIC S9(7)V99    COMP-3.
           03  WS-PVTX             PIC S9(7)V99    COMP-3.
           03  WS-TAMN             PIC S9(7)V99    COMP-3.
           03  WS-TL-SALE          PIC S9(7)V99    COMP-3.
           03  WS-TL-ECTX          PIC S9(7)V99    COMP-3.
           03  WS-TL-ECQT          PIC S9(9)       COMP-3.
           03  WS-TL-FTIP          PIC S9(7)V99    COMP-3.
           03  WS-TL-DSCT          PIC S9(7)V99    COMP-3.
           03  WS-TL-TAFD          PIC S9(7)V99    COMP-3.
           03  WS-TL-FDTX          PIC S9(7)V99    COMP-3.
           03  WS-TL-TAPV          PIC S9(7)V99    COMP-3.
           03  WS-TL-PVTX          PIC S9(7)V99    COMP-3.
           03  WS-TL-TAMN          PIC S9(7)V99    COMP-3.

       LINKAGE SECTION.

           COPY "iv/cpy/lsivfgge".

       PROCEDURE DIVISION
           USING
               LS-IVFGGE-RCRD.

       DECLARATIVES.
           COPY "iv/cpy/dcivmt".
           COPY "iv/cpy/dcivdr".
           COPY "op/cpy/dcopod".
       END DECLARATIVES.

       A000-MAINLINE-PGM SECTION.

       A000-START-PROGRAM.
           PERFORM B000-INIT THRU B990-EXIT.
           PERFORM F000-PROCESS THRU F990-EXIT.
           PERFORM Y000-CLOSE THRU Y990-EXIT.
       A990-EXIT.
           EXIT PROGRAM.

       B000-INIT.
           MOVE "IVFG41" TO PROGRAM-NAME.
       B100-OPEN-FILES.
           OPEN INPUT BM-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN I-O   BD-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN INPUT OD-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       B200-DISPLY.
           DISPLAY "PLEASE WAIT - CALCULATIONS IN PROGRESS"
               LINE 9 POSITION 22.
      *
       B990-EXIT.
           EXIT.



       F000-PROCESS.
           MOVE ZEROS TO WS-TL-SALE
                         WS-TL-ECTX WS-TL-ECQT
                         WS-TL-FTIP WS-TL-DSCT
                         WS-TL-TAFD WS-TL-FDTX
                         WS-TL-TAPV WS-TL-PVTX
                         WS-TL-TAMN.
      *
       F090-START-OD.
           MOVE SPACES         TO OD-KEY-2.
           MOVE LS-IVFGGE-CPNO TO OD-CPNO-K2.
           MOVE LS-IVFGGE-ODNO TO OD-ODNO-K2.
           START OD-FILE
               KEY IS NOT LESS THAN OD-KEY-2
             INVALID KEY
               GO TO F600-DISTRIBUTION.
           IF RECORD-LOCKED
               MOVE "OPOD" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO F090-START-OD.
           IF NOT FILE-OK
               MOVE "ST" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       F100-READ-OD.
           READ OD-FILE
               NEXT RECORD
               INTO OD-RCRD-WS
             AT END
               GO TO F400-REVIEW-TEXT.
           IF NOT RECORD-LOCKED
               GO TO F110-CHECK-FILE.
           MOVE "OPOD" TO WS-LOCK-FILE.
           PERFORM Z200-ACK-LOCK THRU Z290-EXIT.
           GO TO F100-READ-OD.
      *
       F110-CHECK-FILE.
           IF NOT FILE-OK
               MOVE "RS" TO FILE-IO
               GO TO Z100-FILE-ERR.
      *
       F120-EDIT.
           IF OD-CPNO-2 NOT = LS-IVFGGE-CPNO
               GO TO F400-REVIEW-TEXT.
           IF OD-ODNO-2 NOT = LS-IVFGGE-ODNO
               GO TO F400-REVIEW-TEXT.
      *
       F130-SET-VALUES.
           MOVE ZEROS TO WS-SALE WS-ECTX WS-FTIP
                         WS-DSCT WS-FDTX
                         WS-TAPV WS-PVTX WS-TAMN.
      *
       F150-COMPUTE-EXTENSION.
           MOVE "00"    TO LS-UVCPEX-CMCD.
           MOVE ZEROS   TO LS-UVCPEX-UNIT LS-UVCPEX-RATE
                           LS-UVCPEX-TRQT LS-UVCPEX-EXTN.
           MOVE OD-PRUN TO LS-UVCPEX-UNIT.
           MOVE OD-UNPR TO LS-UVCPEX-RATE.
           MOVE OD-SHQT TO LS-UVCPEX-TRQT.
           PERFORM X200-UVCPEX THRU X205-EXIT.
           IF LS-UVCPEX-CMCD = "00"
               GO TO F160-CONTINUE.
      *
       F155-ERROR.
           MOVE "21" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
      *
       F160-CONTINUE.
           MOVE LS-UVCPEX-EXTN TO WS-SALE.
      *
       F170-DISCOUNT.
           COMPUTE WS-DSCT ROUNDED =
               ( LS-UVCPEX-EXTN * ( LS-IVFGGE-GDPC / 100 )).
           SUBTRACT WS-DSCT FROM LS-UVCPEX-EXTN
               GIVING WS-NTAM.
           ADD WS-DSCT TO WS-TL-DSCT.
      *
      *        NOTE - NEW EXCISE TAX CALCULATIONS
      *               USED TO MAKE EXCISE SALES TAX CALCULATION
      *               TO BE INCLUDED IN PRICE
      *
      *               ALSO, 'P' (PERCENT) OPTION DISABLED
      *
      *               ALSO, QUANTITY OF UNITS DROPPED
      *
           MOVE WS-NTAM TO WS-CLAM.
      *
       F180-EXCISE-TAX-INCL.
           IF OD-ETSW = "N"
               GO TO F200-FEDERAL-TAX.
           IF OD-HCCD = "A"
               GO TO F190-AMOUNT.
      *
       F185-NOT-AMOUNT.
           MOVE "22" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
      *
       F190-AMOUNT.
           COMPUTE WS-ECTX ROUNDED =
               ( OD-ETAM * ( OD-SHQT / OD-PRUN )).
           SUBTRACT WS-ECTX FROM WS-CLAM.
           SUBTRACT WS-ECTX FROM WS-SALE.
           ADD WS-ECTX        TO WS-TL-ECTX.
      *
       F200-FEDERAL-TAX.
           IF OD-FTSW = "N"
               GO TO F240-CONTINUE.
           IF OD-IFSW = "Y"
               GO TO F220-INCLUDE-IN-PRICE.
      *
       F210-CALCULATE.
           MOVE "00"           TO LS-IVTXCL-CMCD.
           MOVE ZEROS          TO LS-IVTXCL-STPC
                                  LS-IVTXCL-TOAM
                                  LS-IVTXCL-TXAM.
           MOVE LS-IVFGGE-FTPC TO LS-IVTXCL-STPC.
           MOVE WS-CLAM        TO LS-IVTXCL-TOAM.
           if ls-ivfgge-ftex = "N"
               PERFORM X220-IVTXCL THRU X225-EXIT
               MOVE LS-IVTXCL-TXAM TO WS-FDTX
             else
               move zeros to ws-fdtx.
      *
       F215-ADD-VALUES.
           ADD WS-FDTX     TO WS-TL-FDTX.
           ADD WS-CLAM     TO WS-TL-TAFD.
           GO TO F240-CONTINUE.
      *
       F220-INCLUDE-IN-PRICE.
           COMPUTE WS-ASAM =
               ( WS-CLAM * ( 100 / ( 100 + LS-IVFGGE-FTPC )))
           SUBTRACT WS-ASAM FROM WS-CLAM
               GIVING WS-FTIP.
           ADD WS-FTIP        TO WS-TL-FTIP.
           SUBTRACT WS-FTIP FROM WS-SALE.
      *
       F240-CONTINUE.
      *
       F300-PROVINCIAL-TAX.
           IF OD-PTSW = "N"
               GO TO F320-MUNICIPAL-TAX.
      *
       F310-CALCULATE.
           IF LS-IVFGGE-PVCD = "ON"
             OR LS-IVFGGE-PVCD = "MB"
               OR LS-IVFGGE-PVCD = "SK"
                 OR LS-IVFGGE-PVCD = "AB"
                   OR LS-IVFGGE-PVCD = "BC"
                     MOVE WS-NTAM TO WS-TAPV
                   ELSE
                     ADD WS-NTAM WS-FDTX GIVING WS-TAPV.
           MOVE "00"           TO LS-IVTXCL-CMCD.
           MOVE ZEROS          TO LS-IVTXCL-STPC LS-IVTXCL-TOAM
                                  LS-IVTXCL-TXAM.
           MOVE LS-IVFGGE-PTPC TO LS-IVTXCL-STPC.
           MOVE WS-TAPV        TO LS-IVTXCL-TOAM.
           if ls-ivfgge-ptex = "Y"
               move zeros to ws-tl-pvtx
               move WS-TAPV  TO WS-TL-TAPV
             else
               PERFORM X220-IVTXCL THRU X225-EXIT
               move LS-IVTXCL-TXAM  TO WS-tl-PVTX
               move WS-TAPV  TO WS-TL-TAPV.
      *
       F320-MUNICIPAL-TAX.
           IF OD-MTSW = "N"
               GO TO F360-ADD-SALE.
           MOVE WS-NTAM       TO WS-TAMN.
           SUBTRACT WS-FTIP FROM WS-TAMN.
           SUBTRACT WS-ECTX FROM WS-TAMN.
           ADD WS-TAMN        TO WS-TL-TAMN.
      *
       F360-ADD-SALE.
           ADD WS-SALE TO WS-TL-SALE.
      *
       F390-CONTINUE.
           GO TO F100-READ-OD.
      *
       F400-REVIEW-TEXT.
           MOVE ZERO TO WS-LNCD.
      *
       F405-INCLUDE-FED-TAX.
           IF WS-TL-FTIP = ZERO
               GO TO F500-EXCISE-TAX.
      *
       F410-REVIEW-LANGUAGE.
           IF LS-IVFGGE-LGCD = "FR"
               MOVE "X3" TO WS-STCD
               GO TO F415-READ-BM.
           MOVE "X1" TO WS-STCD.
      *
       F415-READ-BM.
           PERFORM X500-READ-BM THRU X505-EXIT.
           IF WS-ERSW = "Y"
               MOVE "23" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
      *
       F420-INSERT-DOLLAR-FIGURE.
           MOVE BM-11-LNTX TO WS-LN-TEXT.
           MOVE 00         TO WS-LN-FCPS.
           MOVE 01         TO WS-LN-SCPS.
      *
       F425-REVIEW-LOOP.
           ADD 1 TO WS-LN-FCPS.
           ADD 1 TO WS-LN-SCPS.
           IF WS-LN-CHAR (WS-LN-FCPS) NOT = "%"
               GO TO F460-REVIEW-VALUES.
           MOVE SPACES TO WS-CR-DATA.
           IF WS-LN-CHAR (WS-LN-SCPS) = "A"
               GO TO F430-REVIEW-AMOUNT.
           GO TO F460-REVIEW-VALUES.
      *
       F430-REVIEW-AMOUNT.
           MOVE WS-TL-FTIP TO WS-CR-AMNT WS-VALU.
           IF WS-TL-FTIP < ZERO
               MULTIPLY WS-VALU BY WS-MINUS-1
                   GIVING WS-VALU.
      *
       F435-START-POSITION.
           IF WS-VALU < 1
               MOVE 09 TO WS-CR-FCPS WS-NOSR
               GO TO F440-INSERT-LOOP.
           IF WS-VALU < 10
               MOVE 08 TO WS-CR-FCPS WS-NOSR
               GO TO F440-INSERT-LOOP.
           IF WS-VALU < 100
               MOVE 07 TO WS-CR-FCPS WS-NOSR
               GO TO F440-INSERT-LOOP.
           IF WS-VALU < 1000
               MOVE 06 TO WS-CR-FCPS WS-NOSR
               GO TO F440-INSERT-LOOP.
           IF WS-VALU < 10000
               MOVE 04 TO WS-CR-FCPS WS-NOSR
               GO TO F440-INSERT-LOOP.
           IF WS-VALU < 100000
               MOVE 03 TO WS-CR-FCPS WS-NOSR
               GO TO F440-INSERT-LOOP.
           IF WS-VALU < 1000000
               MOVE 02 TO WS-CR-FCPS WS-NOSR
               GO TO F440-INSERT-LOOP.
           MOVE ZERO TO WS-CR-FCPS WS-NOSR.
      *
       F440-INSERT-LOOP.
           ADD 1 TO WS-CR-FCPS.
           MOVE WS-CR-CHAR (WS-CR-FCPS) TO WS-LN-CHAR (WS-LN-FCPS).
           ADD 1 TO WS-LN-FCPS.
           ADD 1 TO WS-LN-SCPS.
      *
       F450-REVIEW.
           IF WS-CR-FCPS NOT = 13
               GO TO F440-INSERT-LOOP.
      *
       F455-REMOVE-SPACE.
           IF WS-NOSR = ZERO
               GO TO F460-REVIEW-VALUES.
           PERFORM X300-REMOVE-SPACE THRU X390-EXIT.
           SUBTRACT 1 FROM WS-NOSR.
           GO TO F455-REMOVE-SPACE.
      *
       F460-REVIEW-VALUES.
           IF WS-LN-SCPS < 48
               GO TO F425-REVIEW-LOOP.
      *
       F480-SET-BD-RECORD.
           PERFORM X400-SET-WRITE-BD-21 THRU X405-EXIT.
      *
       F500-EXCISE-TAX.
           IF WS-TL-ECTX = ZERO
               GO TO F600-DISTRIBUTION.
      *
       F510-REVIEW-LANGUAGE.
           IF LS-IVFGGE-LGCD = "FR"
               MOVE "X4" TO WS-STCD
               GO TO F515-READ-BM.
           MOVE "X2" TO WS-STCD.
      *
       F515-READ-BM.
           PERFORM X500-READ-BM THRU X505-EXIT.
           IF WS-ERSW = "Y"
               MOVE "25" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
      *
       F520-INSERT-FIGURES.
           MOVE BM-11-LNTX TO WS-LN-TEXT.
           MOVE 00         TO WS-LN-FCPS.
           MOVE 01         TO WS-LN-SCPS.
      *
       F525-REVIEW-LOOP.
           ADD 1 TO WS-LN-FCPS.
           ADD 1 TO WS-LN-SCPS.
           IF WS-LN-CHAR (WS-LN-FCPS) NOT = "%"
               GO TO F570-REVIEW-VALUES.
           MOVE SPACES TO WS-CR-DATA.
           IF WS-LN-CHAR (WS-LN-SCPS) = "A"
               GO TO F530-REVIEW-AMOUNT.
           GO TO F570-REVIEW-VALUES.
      *
       F530-REVIEW-AMOUNT.
           MOVE WS-TL-ECTX TO WS-CR-AMNT WS-VALU.
           IF WS-TL-ECTX < ZERO
               MULTIPLY WS-VALU BY WS-MINUS-1
                   GIVING WS-VALU.
      *
       F535-START-POSITION.
           IF WS-VALU < 1
               MOVE 09 TO WS-CR-FCPS WS-NOSR
               GO TO F550-INSERT-LOOP.
           IF WS-VALU < 10
               MOVE 08 TO WS-CR-FCPS WS-NOSR
               GO TO F550-INSERT-LOOP.
           IF WS-VALU < 100
               MOVE 07 TO WS-CR-FCPS WS-NOSR
               GO TO F550-INSERT-LOOP.
           IF WS-VALU < 1000
               MOVE 06 TO WS-CR-FCPS WS-NOSR
               GO TO F550-INSERT-LOOP.
           IF WS-VALU < 10000
               MOVE 04 TO WS-CR-FCPS WS-NOSR
               GO TO F550-INSERT-LOOP.
           IF WS-VALU < 100000
               MOVE 03 TO WS-CR-FCPS WS-NOSR
               GO TO F550-INSERT-LOOP.
           IF WS-VALU < 1000000
               MOVE 02 TO WS-CR-FCPS WS-NOSR
               GO TO F550-INSERT-LOOP.
           MOVE ZERO TO WS-CR-FCPS WS-NOSR.
      *
       F550-INSERT-LOOP.
           ADD 1 TO WS-CR-FCPS.
           MOVE WS-CR-CHAR (WS-CR-FCPS) TO WS-LN-CHAR (WS-LN-FCPS).
           ADD 1 TO WS-LN-FCPS.
           ADD 1 TO WS-LN-SCPS.
      *
       F555-REVIEW.
           IF WS-CR-FCPS NOT = 13
               GO TO F550-INSERT-LOOP.
      *
       F560-REMOVE-SPACE.
           IF WS-NOSR = ZERO
               GO TO F570-REVIEW-VALUES.
           PERFORM X300-REMOVE-SPACE THRU X390-EXIT.
           SUBTRACT 1 FROM WS-NOSR.
           GO TO F560-REMOVE-SPACE.
      *
       F570-REVIEW-VALUES.
           IF WS-LN-SCPS < 48
               GO TO F525-REVIEW-LOOP.
      *
       F580-SET-BD-RECORD.
           PERFORM X400-SET-WRITE-BD-21 THRU X405-EXIT.
      *
       F600-DISTRIBUTION.
           MOVE SPACES TO WS-ACNO.
           MOVE ZEROS  TO WS-ACAM.
      *
       F610-SALE.
           MOVE 01 TO WS-DTCD.
           MOVE WS-TL-SALE TO WS-ACAM.
           PERFORM X410-SET-WRITE-BD-51 THRU X415-EXIT.
      *
       F620-EXCISE-TAX-INCL.
           MOVE 02             TO WS-DTCD.
           IF WS-TL-ECTX = ZERO
               GO TO F625-CONTINUE.
           MOVE "SC"           TO LS-IVLSAI-CMCD.
           MOVE "IV"           TO LS-IVLSAI-IFST.
           MOVE "ECTX"         TO LS-IVLSAI-IFTP.
           PERFORM X210-IVLSAI THRU X215-EXIT.
           IF LS-IVLSAI-CMCD NOT = "00"
               MOVE "31"       TO FILE-STATUS
               GO TO Z100-FILE-ERR.
           MOVE LS-IVLSAI-ACNO TO WS-ACNO.
           MOVE WS-TL-ECTX     TO WS-ACAM.
      *
       F625-CONTINUE.
           PERFORM X410-SET-WRITE-BD-51 THRU X415-EXIT.
      *
       F630-FED-TAX-INCL.
           MOVE 03 TO WS-DTCD.
           IF WS-TL-FTIP NOT = ZERO
               MOVE LS-IVFGGE-FTAC TO WS-ACNO
               MOVE WS-TL-FTIP TO WS-ACAM.
           PERFORM X410-SET-WRITE-BD-51 THRU X415-EXIT.
      *
       F640-DISCOUNTS.
           MOVE 04         TO WS-DTCD.
           MOVE WS-TL-DSCT TO WS-ACAM.
           PERFORM X410-SET-WRITE-BD-51 THRU X415-EXIT.
      *
       F650-TAXABLE-FREIGHT.
           MOVE 05         TO WS-DTCD.
           PERFORM X410-SET-WRITE-BD-51 THRU X415-EXIT.
      *
       F660-TAXABLE-POSTAGE.
           MOVE 06         TO WS-DTCD.
           PERFORM X410-SET-WRITE-BD-51 THRU X415-EXIT.
      *
       F670-TAXABLE-AMNT-FED.
           MOVE 07         TO WS-DTCD.
           MOVE WS-TL-TAFD TO WS-ACAM.
           PERFORM X410-SET-WRITE-BD-51 THRU X415-EXIT.
      *
       F680-FEDERAL-TAX.
           MOVE 08             TO WS-DTCD.
           IF WS-TL-FDTX NOT = ZERO
               MOVE LS-IVFGGE-FTAC TO WS-ACNO
               MOVE WS-TL-FDTX TO WS-ACAM.
           PERFORM X410-SET-WRITE-BD-51 THRU X415-EXIT.
      *
       F690-TAXABLE-AMNT-PROV.
           MOVE 09 TO WS-DTCD.
           MOVE WS-TL-TAPV TO WS-ACAM.
           PERFORM X410-SET-WRITE-BD-51 THRU X415-EXIT.
      *
       F700-PROVINCIAL-TAX.
           MOVE 10 TO WS-DTCD.
           IF WS-TL-PVTX NOT = ZERO
               MOVE LS-IVFGGE-PTAC TO WS-ACNO
               MOVE WS-TL-PVTX TO WS-ACAM.
           PERFORM X410-SET-WRITE-BD-51 THRU X415-EXIT.
      *
       F710-TAXABLE-AMNT-MNCPL.
           MOVE 11 TO WS-DTCD.
           MOVE WS-TL-TAMN TO WS-ACAM.
           PERFORM X410-SET-WRITE-BD-51 THRU X415-EXIT.
      *
       F720-MUNICIPAL-TAX.
           MOVE 12 TO WS-DTCD.
           PERFORM X410-SET-WRITE-BD-51 THRU X415-EXIT.
      *
       F730-FREIGHT.
           MOVE 13 TO WS-DTCD.
           PERFORM X410-SET-WRITE-BD-51 THRU X415-EXIT.
      *
       F740-POSTAGE.
           MOVE 14 TO WS-DTCD.
           PERFORM X410-SET-WRITE-BD-51 THRU X415-EXIT.
      *
       F900-SET-LINKAGE.
           MOVE "00" TO LS-IVFGGE-CMCD.
      *
       F990-EXIT.
           EXIT.



       X000-FUNC-ERR.
           DISPLAY "FUNCTION KEY NOT VALID "
               LINE 24 POSITION 1 HIGH.
       X050-REPLY.
           ACCEPT DUMMY-77 LINE 00 POSITION 00 TAB OFF.
           DISPLAY SPACES LINE 24 POSITION 01 SIZE 80.
       X090-EXIT.
           EXIT.



       X200-UVCPEX.
           CALL "UVCPEX" USING
               LS-UVCPEX-RCRD.
       X205-EXIT.
           EXIT.



       X210-IVLSAI.
           CALL "IVLSAI" USING
               LS-IVLSAI-RCRD.
       X215-EXIT.
           EXIT.



       X220-IVTXCL.
           CALL "IVTXCL" USING
               LS-IVTXCL-RCRD.
       X225-EXIT.
           EXIT.



       X300-REMOVE-SPACE.
           MOVE WS-LN-FCPS TO WS-SR-FCPS WS-SR-SCPS.
           ADD 1 TO WS-SR-SCPS.
       X310-LOOP.
           IF WS-SR-SCPS > 60
               GO TO X360-SET-SPACE.
           MOVE WS-LN-CHAR (WS-SR-SCPS) TO WS-LN-CHAR (WS-SR-FCPS).
       X330-BUMP-COUNTERS.
           ADD 1 TO WS-SR-FCPS.
           ADD 1 TO WS-SR-SCPS.
           GO TO X310-LOOP.
      *
       X360-SET-SPACE.
           MOVE SPACES TO WS-LN-CHAR (60).
       X390-EXIT.
           EXIT.



       X400-SET-WRITE-BD-21.
           ADD 1 TO WS-LNCD.
       X402-SET-RECORD.
           MOVE SPACES         TO BD-RCRD BD-21-RCRD-WS-21.
           MOVE LS-IVFGGE-CPNO TO BD-21-CPNO-1-21.
           MOVE 21             TO BD-21-LFCD-1-21.
           MOVE LS-IVFGGE-TRNO TO BD-21-TRNO-1-21.
           MOVE 21             TO BD-21-RCCD-1-21.
           MOVE WS-LNCD        TO BD-21-LNCD-1-21.
           MOVE WS-LN-TEXT     TO BD-21-TEXT-21.
       X403-WRITE.
           WRITE BD-RCRD FROM BD-21-RCRD-WS-21.
           IF NOT FILE-OK
               MOVE "WR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X405-EXIT.
           EXIT.



       X410-SET-WRITE-BD-51.
           IF WS-DTCD NOT = 04
               MULTIPLY WS-ACAM BY WS-MINUS-1
                   GIVING WS-ACAM.
       X412-SET-RECORD.
           MOVE SPACES         TO BD-RCRD BD-21-RCRD-WS-51.
           MOVE ZEROS          TO BD-21-ACAM-51.
           MOVE LS-IVFGGE-CPNO TO BD-21-CPNO-1-51.
           MOVE 21             TO BD-21-LFCD-1-51.
           MOVE LS-IVFGGE-TRNO TO BD-21-TRNO-1-51.
           MOVE 51             TO BD-21-RCCD-1-51.
           MOVE WS-DTCD        TO BD-21-DTCD-1-51.
           MOVE WS-ACNO        TO BD-21-ACNO-51.
           MOVE WS-ACAM        TO BD-21-ACAM-51.
       X413-WRITE.
           WRITE BD-RCRD FROM BD-21-RCRD-WS-51.
           IF NOT FILE-OK
               MOVE "WR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X414-RESET-VALUES.
           MOVE SPACES TO WS-ACNO.
           MOVE ZEROS  TO WS-ACAM.
       X415-EXIT.
           EXIT.



       X500-READ-BM.
           MOVE SPACES         TO BM-KEY-1 BM-11-KEY-1-WS WS-ERSW.
           MOVE LS-IVFGGE-CPNO TO BM-11-CPNO-1.
           MOVE 11             TO BM-11-LFCD-1.
           MOVE WS-STCD        TO BM-11-TPCD-1.
           MOVE BM-11-KEY-1-WS TO BM-KEY-1.
           READ BM-FILE
               INTO BM-11-RCRD-WS
               KEY IS BM-KEY-1
             INVALID KEY
               MOVE "Y" TO WS-ERSW
               GO TO X505-EXIT.
           IF RECORD-LOCKED
               MOVE "IVMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X500-READ-BM.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X505-EXIT.
           EXIT.



       Y000-CLOSE.
           CLOSE BM-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE BD-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE OD-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
       Y990-EXIT.
           EXIT.

           COPY "uv/cpy/uvdfemov".
           COPY "uv/cpy/uvaflk".
