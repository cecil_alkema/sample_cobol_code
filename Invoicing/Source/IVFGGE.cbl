       IDENTIFICATION DIVISION.
       PROGRAM-ID.                 IVFGGE.
      *
      *        INVOICING
      *        FINISHED GOODS GENERATION
      *        DRIVER PROGRAM
      *
      *        SWITCHES
      *        ********
      *
      *            SWITCH-1 - ALLOW MAINT. OF ANY STATUS INVOICE
      *
      *        SUBROUTINES
      *        ***********
      *
      *            IVFG01 - DISPLAY SCREEN ROUTINE
      *            IVFG04 - RETRIEVE TRANS NO ROUTINE
      *            IVFG11 - INITIALIZING ROUTINE
      *            IVFG21 - HEADER MAINTENANCE
      *            IVFG41 - CALCULATION ROUTINE
      *            IVFG61 - TEXT MAINTENANCE
      *            IVFG81 - DISTRIBUTION MAINTENANCE
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77  DUMMY-77                PIC X.
       77  MAIN-77                 PIC XX.

           COPY "uv/cpy/uvfckv".
           COPY "uv/cpy/uvsfemov".

           COPY "iv/cpy/lsivfgge".

       PROCEDURE DIVISION.

       A000-MAINLINE-PGM SECTION.

       A000-START-PROGRAM.
           PERFORM B000-INIT THRU B990-EXIT.
           IF MAIN-77 = "QT"
               GO TO A990-EXIT.
      *
       A100-MAINLINE.
           PERFORM F000-PROCESS THRU F990-EXIT.
      *
       A990-EXIT.
           STOP RUN.



       B000-INIT.
           MOVE SPACES   TO MAIN-77.
           MOVE "IVFGGE" TO PROGRAM-NAME.
      *
       B200-SET-LINKAGE.
           MOVE SPACES TO LS-IVFGGE-RCRD.
           MOVE ZEROS  TO LS-IVFGGE-CPNO LS-IVFGGE-SODT LS-IVFGGE-MNDT
                          LS-IVFGGE-NATN LS-IVFGGE-TRNO
                          LS-IVFGGE-ODNO LS-IVFGGE-GDPC
                          LS-IVFGGE-FTPC LS-IVFGGE-PTPC LS-IVFGGE-MTPC.
      *
       B300-IVFG01.
           MOVE "00" TO LS-IVFGGE-CMCD.
           PERFORM X200-IVFG11 THRU X205-EXIT.
           IF LS-IVFGGE-CMCD = "QT"
               MOVE "QT" TO MAIN-77
               GO TO B990-EXIT.
           IF LS-IVFGGE-CMCD = "00"
               GO TO B990-EXIT.
      *
       B310-ERROR.
           MOVE "11" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
      *
       B990-EXIT.
           EXIT.



       F000-PROCESS.
           MOVE "00" TO LS-IVFGGE-CMCD.
           PERFORM X210-IVFG21 THRU X215-EXIT.
           IF LS-IVFGGE-CMCD = "00"
             AND LS-IVFGGE-MDCD = "AD"
               GO TO F100-CALC.
           IF LS-IVFGGE-CMCD = "01"
             AND LS-IVFGGE-MDCD = "CH"
               GO TO F400-TEXT.
           IF LS-IVFGGE-CMCD = "02"
               GO TO F990-EXIT.
      *
       F010-ERROR.
           MOVE "21" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
      *
       F100-CALC.
           MOVE "00" TO LS-IVFGGE-CMCD.
           PERFORM X220-IVFG41 THRU X225-EXIT.
           IF LS-IVFGGE-CMCD = "00"
               GO TO F400-TEXT.
      *
       F110-ERROR.
           MOVE "26" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
      *
       F400-TEXT.
           MOVE "00" TO LS-IVFGGE-CMCD.
           PERFORM X230-IVFG61 THRU X235-EXIT.
           IF LS-IVFGGE-CMCD = "01"
               GO TO F700-DSTR.
      *
       F410-ERROR.
           MOVE "31" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
      *
       F700-DSTR.
           MOVE "00" TO LS-IVFGGE-CMCD.
           PERFORM X240-IVFG81 THRU X245-EXIT.
           IF LS-IVFGGE-CMCD = "01"
               GO TO F000-PROCESS.
      *
       F710-ERROR.
           MOVE "36" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
      *
       F990-EXIT.
           EXIT.



       X000-FUNC-ERR.
           DISPLAY "FUNCTION KEY NOT VALID "
               LINE 24 POSITION 1 HIGH.
      *
       X050-REPLY.
           ACCEPT DUMMY-77 LINE 00 POSITION 00 TAB OFF.
           DISPLAY SPACES LINE 24 POSITION 01 SIZE 80.
      *
       X090-EXIT.
           EXIT.



       X200-IVFG11.
           CALL "IVFG11" USING
               LS-IVFGGE-RCRD.
      *
       X205-EXIT.
           EXIT.



       X210-IVFG21.
           CALL "IVFG21" USING
               LS-IVFGGE-RCRD.
      *
       X215-EXIT.
           EXIT.



       X220-IVFG41.
           CALL "IVFG41" USING
               LS-IVFGGE-RCRD.
      *
       X225-EXIT.
           EXIT.


       X230-IVFG61.
           CALL "IVFG61" USING
               LS-IVFGGE-RCRD.
      *
       X235-EXIT.
           EXIT.



       X240-IVFG81.
           CALL "IVFG81" USING
               LS-IVFGGE-RCRD.
      *
       X245-EXIT.
           EXIT.



           COPY "uv/cpy/uvdfemov".
