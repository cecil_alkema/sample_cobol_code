       IDENTIFICATION DIVISION.
       PROGRAM-ID.                 IVFG01.
      *
      *        INVOICING
      *        FINISHED GOODS INVOICE GENERATION
      *        SUBROUTINE
      *
      *        SCREEN DISPLAY ROUTINE
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77  DUMMY-77                PIC X.

           COPY "uv/cpy/uvfckv".
           COPY "uv/cpy/uvsfemov".

       01  DISPLAY-FIELDS.
           03  DS-VALU             PIC Z,ZZZ,ZZZ.99-.

       LINKAGE SECTION.

           COPY "iv/cpy/lsivfgge".

       PROCEDURE DIVISION
           USING
               LS-IVFGGE-RCRD.

       A000-MAINLINE-PGM SECTION.

       A000-START-PROGRAM.
           PERFORM B000-INIT THRU B990-EXIT.
           PERFORM F000-PROCESS THRU F990-EXIT.
       A990-EXIT.
           EXIT PROGRAM.


       B000-INIT.
           MOVE "IVFG01" TO PROGRAM-NAME.
       B990-EXIT.
           EXIT.


       F000-PROCESS.
           PERFORM X900-CLEAR THRU X905-EXIT.
           IF LS-IVFGGE-CMCD = "A1"
               GO TO F100-DISPLY-SCREEN-1.
           IF LS-IVFGGE-CMCD = "C1"
               GO TO F300-DISPLY-SCREEN-2.
           IF LS-IVFGGE-CMCD = "E1"
               GO TO F500-DISPLY-SCREEN-3.
       F010-ERROR.
           MOVE "11" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
      *
       F100-DISPLY-SCREEN-1.
           DISPLAY SPACES LINE 06 POSITION 2 SIZE 78.
           DISPLAY
               "   TRANS NO"             LINE 07 POSITION 08 HIGH
               "01 INVOICE DATE"         LINE 09 POSITION 08 HIGH
               "02 ORDER NO"             LINE 10 POSITION 08 HIGH
               "   CUSTOMER BILL TO"     LINE 12 POSITION 08 HIGH
               "   CUSTOMER SOLD TO"     LINE 13 POSITION 08 HIGH
               "   CUSTOMER SHIP TO"     LINE 14 POSITION 08 HIGH
               "   SALESMAN"             LINE 15 POSITION 08 HIGH
               "11 PRINT OR HOLD? (P/H)" LINE 17 POSITION 08 HIGH.
           GO TO F900-VALID-EXIT.
      *
       F300-DISPLY-SCREEN-2.
           DISPLAY "TRANS NO"            LINE 06 POSITION 03 HIGH
                   "CUSTOMER BILL TO"    LINE 06 POSITION 19 HIGH
                   "DATE"                LINE 06 POSITION 66 HIGH.
           GO TO F900-VALID-EXIT.
      *
       F500-DISPLY-SCREEN-3.
           DISPLAY "INVOICE AMOUNT" LINE 07 POSITION 51 HIGH
                   "ACCOUNT"        LINE 08 POSITION 24 HIGH
                   "DESCRIPTION"    LINE 08 POSITION 35 HIGH.
       F505-CONTINUE.
           DISPLAY
               "01 SALES"           LINE 09 POSITION 03 HIGH
               "02 EXCISE TAX INCL" LINE 10 POSITION 03 HIGH
               "03 FED TAX INCL"    LINE 11 POSITION 03 HIGH
               "04 DISCOUNTS"       LINE 12 POSITION 03 HIGH
               "05 TAXABLE FREIGHT" LINE 13 POSITION 03 HIGH
               "06 TAXABLE POSTAGE" LINE 14 POSITION 03 HIGH
               "07 FEDERAL TAX ON"  LINE 15 POSITION 03 HIGH
               "08 FEDERAL TAX"     LINE 16 POSITION 03 HIGH.
           IF LS-IVFGGE-VSCD = "US"
             DISPLAY
               "09 STATE TAX ON"    LINE 17 POSITION 03 HIGH
               "10 STATE TAX"       LINE 18 POSITION 03 HIGH
                   GO TO F510-CONTINUE.
           DISPLAY
               "09 PROVINCIAL TAX ON" LINE 17 POSITION 03 HIGH
               "10 PROVINCIAL TAX"    LINE 18 POSITION 03 HIGH.
       F510-CONTINUE.
           DISPLAY
               "11 MUNICIPAL TAX ON" LINE 19 POSITION 03 HIGH
               "12 MUNICIPAL TAX"    LINE 20 POSITION 03 HIGH
               "13 FREIGHT"          LINE 21 POSITION 03 HIGH
               "14 POSTAGE"          LINE 22 POSITION 03 HIGH.
       F520-SHOW-ZEROS.
           MOVE ZERO TO DS-VALU.
           DISPLAY DS-VALU LINE 22 POSITION 67
                   DS-VALU LINE 21 POSITION 67
                   DS-VALU LINE 20 POSITION 67
                   DS-VALU LINE 19 POSITION 67
                   DS-VALU LINE 18 POSITION 67
                   DS-VALU LINE 17 POSITION 67
                   DS-VALU LINE 16 POSITION 67
                   DS-VALU LINE 15 POSITION 67
                   DS-VALU LINE 14 POSITION 67
                   DS-VALU LINE 13 POSITION 67
                   DS-VALU LINE 12 POSITION 67
                   DS-VALU LINE 11 POSITION 67
                   DS-VALU LINE 10 POSITION 67
                   DS-VALU LINE 09 POSITION 67.
       F900-VALID-EXIT.
           MOVE "00" TO LS-IVFGGE-CMCD.
       F990-EXIT.
           EXIT.


       X000-FUNC-ERR.
           DISPLAY "FUNCTION KEY NOT VALID "
               LINE 24 POSITION 1 HIGH.
       X050-REPLY.
           ACCEPT DUMMY-77 LINE 00 POSITION 00 TAB OFF.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
       X090-EXIT.
           EXIT.


       X900-CLEAR.
           DISPLAY SPACES LINE 22 POSITION 2 SIZE 78
                   SPACES LINE 21 POSITION 2 SIZE 78
                   SPACES LINE 20 POSITION 2 SIZE 78
                   SPACES LINE 19 POSITION 2 SIZE 78
                   SPACES LINE 18 POSITION 2 SIZE 78
                   SPACES LINE 17 POSITION 2 SIZE 78
                   SPACES LINE 16 POSITION 2 SIZE 78
                   SPACES LINE 15 POSITION 2 SIZE 78
                   SPACES LINE 14 POSITION 2 SIZE 78
                   SPACES LINE 13 POSITION 2 SIZE 78
                   SPACES LINE 12 POSITION 2 SIZE 78
                   SPACES LINE 11 POSITION 2 SIZE 78
                   SPACES LINE 10 POSITION 2 SIZE 78
                   SPACES LINE 09 POSITION 2 SIZE 78
                   SPACES LINE 08 POSITION 2 SIZE 78
                   SPACES LINE 07 POSITION 2 SIZE 78.
       X905-EXIT.
           EXIT.


           COPY "uv/cpy/uvdfemov".
