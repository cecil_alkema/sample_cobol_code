       IDENTIFICATION DIVISION.
       PROGRAM-ID.                 IVFG81.
      *
      *        INVOICING
      *        FINISHED GOODS GENERATION ENTRY
      *        SUBROUTINE
      *
      *        DISTRIBUTION TO GENERAL LEDGER
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           COPY "iv/cpy/slivdr".
           COPY "gl/cpy/slacmt".

       DATA DIVISION.
       FILE SECTION.
           COPY "iv/cpy/fdivdr".
           COPY "gl/cpy/fdacmt".

       WORKING-STORAGE SECTION.

       77  BD-STATUS-77            PIC XX.
       77  AC-STATUS-77            PIC XX.
       77  DUMMY-77                PIC X.
       77  MAIN-77                 PIC XX.

           COPY "uv/cpy/uvfckv".
           COPY "uv/cpy/uvsfemov".
           COPY "uv/cpy/uvsfem".
           COPY "uv/cpy/uvlkmg".
           COPY "uv/cpy/uvrlmg".

           COPY "iv/cpy/rcid2151".
           COPY "gl/cpy/rcacmt".

           COPY "iv/cpy/lsivlsai".
           COPY "iv/cpy/lsivtxcl".

           COPY "uv/cpy/lsuvgano".

       01  CONTROL-FIELDS.
           03  WS-ERSW             PIC X.
           03  WS-CRSW             PIC X.
           03  WS-GESW             PIC X.
           03  WS-DTCD             PIC 99.
           03  WS-LINE             PIC 99.
           03  WS-FIELD            PIC 99.
           03  WS-MINUS-1          PIC S9      VALUE -1.
           03  ws-select-counter   PIC 9.

       01  STORAGE-FIELDS.
           03  WS-ACNO             PIC X(10).
           03  WS-ACAM             PIC S9(7)V99    COMP-3.
           03  WS-OVAM             PIC S9(7)V99    COMP-3.
           03  WS-CLAM             PIC S9(7)V99    COMP-3.
           03  WS-DFAM             PIC S9(7)V99    COMP-3.
           03  WS-TXAM             PIC S9(7)V99    COMP-3.
           03  WS-STPC             PIC S99V9       COMP-3.
           03  WS-IVAM             PIC S9(9)V99    COMP-3.
           03  WS-DT-DATA          OCCURS 14 TIMES.
               05  WS-DT-ACNO      PIC X(10).
               05  WS-DT-ACAM      PIC S9(7)V99    COMP-3.
               05  WS-DT-CHSW      PIC X.

       01  DISPLAY-FIELDS.
           03  DS-IVAM             PIC ZZ,ZZZ,ZZZ.99-.
           03  DS-ACAM             PIC Z,ZZZ,ZZZ.99-.

       01  EDIT-FIELDS.
           03  ED-ACAM             PIC Z(7).99-.

       LINKAGE SECTION.

           COPY "iv/cpy/lsivfgge".

       PROCEDURE DIVISION
           USING
               LS-IVFGGE-RCRD.

       DECLARATIVES.
           COPY "iv/cpy/dcivdr".
           COPY "gl/cpy/dcacmt".
       END DECLARATIVES.

       A000-MAINLINE-PGM SECTION.

       A000-START-PROGRAM.
           PERFORM B000-INIT THRU B990-EXIT.
           PERFORM D000-LOAD-DATA THRU D990-EXIT.
           PERFORM F000-PROCESS THRU F990-EXIT.
           PERFORM Y000-CLOSE THRU Y990-EXIT.
       A990-EXIT.
           EXIT PROGRAM.

       B000-INIT.
           MOVE "IVFG81" TO PROGRAM-NAME.
       B100-OPEN-FILES.
           OPEN I-O   BD-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
           OPEN INPUT AC-FILE.
           IF NOT FILE-OK
               MOVE "OP" TO FILE-IO
               GO TO Z100-FILE-ERR.
       B200-DISPLY-SCREEN.
           MOVE "E1" TO LS-IVFGGE-CMCD.
           PERFORM X200-IVFG01 THRU X205-EXIT.
           IF LS-IVFGGE-CMCD NOT = "00"
               MOVE "11" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
       B990-EXIT.
           EXIT.

       D000-LOAD-DATA.
           MOVE ZEROS  TO WS-IVAM WS-DTCD.
           MOVE SPACES TO WS-GESW.
       D090-START-BD.
           MOVE SPACES            TO BD-KEY-1 BD-21-KEY-1-WS-51.
           MOVE LS-IVFGGE-CPNO    TO BD-21-CPNO-1-51.
           MOVE 21                TO BD-21-LFCD-1-51.
           MOVE LS-IVFGGE-TRNO    TO BD-21-TRNO-1-51.
           MOVE 51                TO BD-21-RCCD-1-51.
           MOVE BD-21-KEY-1-WS-51 TO BD-KEY-1.
           START BD-FILE
               KEY IS NOT LESS THAN BD-KEY-1
             INVALID KEY
               MOVE "21"          TO FILE-STATUS
               GO TO Z100-FILE-ERR.
           IF RECORD-LOCKED
               MOVE "IVDR"        TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO D090-START-BD.
           IF NOT FILE-OK
               MOVE "ST"          TO FILE-IO
               GO TO Z100-FILE-ERR.
       D100-READ-BD.
           READ BD-FILE
               NEXT RECORD
               WITH NO LOCK
               INTO BD-21-RCRD-WS-51
             AT END
               GO TO D300-REVIEW-COUNTER.
           IF NOT RECORD-LOCKED
               GO TO D110-CHECK-FILE.
           MOVE "IVDR" TO WS-LOCK-FILE.
           PERFORM Z200-ACK-LOCK THRU Z290-EXIT.
           GO TO D100-READ-BD.
       D110-CHECK-FILE.
           IF NOT FILE-OK
               MOVE "RS" TO FILE-IO
               GO TO Z100-FILE-ERR.
       D120-EDIT.
           IF BD-21-CPNO-1-51 NOT = LS-IVFGGE-CPNO
               GO TO D300-REVIEW-COUNTER.
           IF BD-21-LFCD-1-51 NOT = 21
               GO TO D300-REVIEW-COUNTER.
           IF BD-21-TRNO-1-51 NOT = LS-IVFGGE-TRNO
               GO TO D300-REVIEW-COUNTER.
           IF BD-21-RCCD-1-51 NOT = 51
               GO TO D300-REVIEW-COUNTER.
       D130-COMPARE.
           ADD 1 TO WS-DTCD.
           IF BD-21-DTCD-1-51 NOT = WS-DTCD
               MOVE WS-DTCD   TO LOGICAL-FILE-ID
               MOVE "23"      TO FILE-STATUS
               GO TO Z100-FILE-ERR.
           MOVE BD-21-ACNO-51 TO WS-DT-ACNO (WS-DTCD)
                                 WS-ACNO.
           MOVE BD-21-ACAM-51 TO WS-DT-ACAM (WS-DTCD)
                                 WS-ACAM.
           MOVE SPACES        TO WS-DT-CHSW (WS-DTCD).
           IF WS-DTCD NOT = 1
               GO TO D150-COMPUTE-LINE.
       D140-DEBIT-CREDIT.
           IF WS-ACAM > ZERO
               DISPLAY " DEBIT" LINE 08 POSITION 73 HIGH
               MOVE SPACES TO WS-CRSW
               GO TO D150-COMPUTE-LINE.
           DISPLAY "CREDIT" LINE 08 POSITION 73 HIGH.
           MOVE "Y"        TO WS-CRSW.
       D150-COMPUTE-LINE.
           COMPUTE WS-LINE = ( WS-DTCD + 8 ).
       D190-REVIEW-ACCOUNT.
           IF WS-DTCD = 01
             OR WS-DTCD = 04
               OR WS-DTCD = 07
                 OR WS-DTCD = 09
                   OR WS-DTCD = 11
                     GO TO D250-DISPLY-AMOUNT.
           IF WS-ACNO = SPACES
               GO TO D290-CONTINUE.
       D200-READ-AC.
           PERFORM X550-READ-AC THRU X555-EXIT.
       D210-DISPLY.
           DISPLAY WS-ACNO LINE WS-LINE POSITION 24
                   AC-ACDC LINE WS-LINE POSITION 35 SIZE 30.
       D220-REVIEW-AC.
           IF WS-ERSW = "Y"
               MOVE "Y" TO WS-GESW
               GO TO D250-DISPLY-AMOUNT.
           IF AC-TRCD NOT = 1
             AND AC-TRCD NOT = 2
               MOVE "Y" TO WS-GESW.
       D250-DISPLY-AMOUNT.
           IF WS-CRSW = "Y"
               MULTIPLY WS-ACAM BY WS-MINUS-1
                   GIVING WS-ACAM.
           MOVE WS-ACAM TO DS-ACAM.
           DISPLAY DS-ACAM LINE WS-LINE POSITION 67.
       D260-INVOICE-AMOUNT.
           IF WS-DTCD = 07
             OR WS-DTCD = 09
               OR WS-DTCD = 11
                 GO TO D290-CONTINUE.
           IF WS-CRSW NOT = "Y"
               MULTIPLY WS-ACAM BY WS-MINUS-1
                   GIVING WS-ACAM.
           ADD WS-ACAM TO WS-IVAM.
       D290-CONTINUE.
           GO TO D100-READ-BD.

      * end-of-file for this invoice transaction
       D300-REVIEW-COUNTER.
           IF WS-DTCD NOT = 14
               MOVE "27" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
       D400-REVIEW-GESW.
           IF WS-GESW NOT = "Y"
               GO TO D990-EXIT.
       D410-GL-ERRORS.
           DISPLAY "WARNING - GENERAL LEDGER ACCOUNT(S) NOT VALID "
               LINE 24 POSITION 1 HIGH.
           PERFORM X050-REPLY THRU X090-EXIT.
       D990-EXIT.
           EXIT.

       F000-PROCESS.
           MOVE WS-IVAM TO DS-IVAM.
           DISPLAY DS-IVAM LINE 07 POSITION 66.
           move 0 to ws-select-counter.
       F100-SELECT.
           if ws-select-counter = 0
              add 1 to ws-select-counter
              move 08 to ws-field
              go to F100-query-select.
           if ws-select-counter = 1
              add 1 to ws-select-counter
              move 10 to ws-field
              go to F100-query-select.
           add 1 to ws-select-counter.
           DISPLAY "SELECT: "
               LINE 24 POSITION 36 HIGH.
           ACCEPT WS-FIELD
               LINE 00 POSITION 00
               PROMPT TAB ECHO
             ON EXCEPTION
               FUNC-KEY
               GO TO F190-CHECK-FUNC.
       F100-query-select.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           IF WS-FIELD = 01
               GO TO F210-SALES.
           IF WS-FIELD = 02
               GO TO F220-EXCISE-TAX-INCL.
           IF WS-FIELD = 03
               GO TO F230-FED-TAX-INCL.
           IF WS-FIELD = 04
               GO TO F240-DISCOUNTS.
           IF WS-FIELD = 05
               GO TO F250-TAXABLE-FREIGHT.
           IF WS-FIELD = 06
               GO TO F260-TAXABLE-POSTAGE.
           IF WS-FIELD = 07
               GO TO F270-TAX-AMOUNT-FED.
           IF WS-FIELD = 08
               GO TO F280-FEDERAL-TAX.
           IF WS-FIELD = 09
               GO TO F290-TAX-AMOUNT-PROV.
           IF WS-FIELD = 10
               GO TO F300-PROVINCIAL-TAX.
           IF WS-FIELD = 11
               GO TO F310-TAX-AMOUNT-MUN.
           IF WS-FIELD = 12
               GO TO F320-MUNICIPAL-TAX.
           IF WS-FIELD = 13
               GO TO F330-FREIGHT.
           IF WS-FIELD = 14
               GO TO F340-POSTAGE.
           GO TO F100-SELECT.
       F190-CHECK-FUNC.
           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           IF FUNC-01
               GO TO F500-REVIEW-VALUES.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO F100-SELECT.
       F210-SALES.
           DISPLAY "SALES NOT MAINTAINABLE "
               LINE 24 POSITION 1 HIGH.
           PERFORM X050-REPLY THRU X090-EXIT.
           GO TO F100-SELECT.
       F220-EXCISE-TAX-INCL.
           DISPLAY "EXCISE TAX INCLUDED NOT MAINTAINABLE "
               LINE 24 POSITION 1 HIGH.
           PERFORM X050-REPLY THRU X090-EXIT.
           GO TO F100-SELECT.
       F230-FED-TAX-INCL.
           DISPLAY "FEDERAL TAX INCLUDED NOT MAINTAINABLE "
               LINE 24 POSITION 1 HIGH.
           PERFORM X050-REPLY THRU X090-EXIT.
           GO TO F100-SELECT.
       F240-DISCOUNTS.
           DISPLAY "DISCOUNTS NOT MAINTAINABLE "
               LINE 24 POSITION 1 HIGH.
           PERFORM X050-REPLY THRU X090-EXIT.
           GO TO F100-SELECT.
       F250-TAXABLE-FREIGHT.
       F260-TAXABLE-POSTAGE.
       F270-TAX-AMOUNT-FED.
       F280-FEDERAL-TAX.
       F290-TAX-AMOUNT-PROV.
       F300-PROVINCIAL-TAX.
       F310-TAX-AMOUNT-MUN.
       F320-MUNICIPAL-TAX.
       F330-FREIGHT.
       F340-POSTAGE.
           PERFORM J000-MAINTAIN THRU J990-EXIT.
           GO TO F100-SELECT.
       F500-REVIEW-VALUES.
           MOVE 04 TO WS-DTCD.
       F510-REVIEW-LOOP.
           ADD 1 TO WS-DTCD.
           IF WS-DT-CHSW ( WS-DTCD ) NOT = "Y"
               GO TO F700-REVIEW-COUNTER.
       F520-READ-BD.
           MOVE SPACES            TO BD-KEY-1 BD-21-KEY-1-WS-51.
           MOVE LS-IVFGGE-CPNO    TO BD-21-CPNO-1-51.
           MOVE 21                TO BD-21-LFCD-1-51.
           MOVE LS-IVFGGE-TRNO    TO BD-21-TRNO-1-51.
           MOVE 51                TO BD-21-RCCD-1-51.
           MOVE WS-DTCD           TO BD-21-DTCD-1-51.
           MOVE BD-21-KEY-1-WS-51 TO BD-KEY-1.
           READ BD-FILE
               INTO BD-21-RCRD-WS-51
               KEY IS BD-KEY-1.
           IF RECORD-LOCKED
               MOVE "IVDR" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO F520-READ-BD.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       F550-SET-VALUES.
           MOVE WS-DT-ACNO ( WS-DTCD ) TO BD-21-ACNO-51.
           MOVE WS-DT-ACAM ( WS-DTCD ) TO BD-21-ACAM-51.
       F600-REWRITE-BD.
           REWRITE BD-RCRD FROM BD-21-RCRD-WS-51.
           IF NOT FILE-OK
               MOVE "RW" TO FILE-IO
               GO TO Z100-FILE-ERR.
       F700-REVIEW-COUNTER.
           IF WS-DTCD NOT = 14
               GO TO F510-REVIEW-LOOP.
       F710-VALID-EXIT.
           MOVE "01" TO LS-IVFGGE-CMCD.
       F990-EXIT.
           EXIT.

       J000-MAINTAIN.
           COMPUTE WS-LINE = ( WS-FIELD + 8 ).
       J010-REVIEW.
           IF WS-DT-ACNO ( WS-FIELD ) NOT = SPACES
               GO TO J300-REVIEW-VALUES.
           IF WS-FIELD = 12
               GO TO J200-ACCOUNT.
           IF WS-FIELD = 07
             OR WS-FIELD = 09
               OR WS-FIELD = 11
                 GO TO J300-REVIEW-VALUES.
           IF WS-FIELD = 08
               MOVE LS-IVFGGE-FTAC TO WS-ACNO
               GO TO J100-READ-AC.
           IF WS-FIELD = 10
               MOVE LS-IVFGGE-PTAC TO WS-ACNO
               GO TO J100-READ-AC.
           IF WS-FIELD = 05
             OR WS-FIELD = 13
               MOVE "IV"   TO LS-IVLSAI-IFST
               MOVE "FCRC" TO LS-IVLSAI-IFTP
               GO TO J050-INTERFACE.
           IF WS-FIELD = 06
             OR WS-FIELD = 14
               MOVE "IV"   TO LS-IVLSAI-IFST
               MOVE "PCRC" TO LS-IVLSAI-IFTP
               GO TO J050-INTERFACE.
       J020-ERROR.
           MOVE "61" TO FILE-STATUS.
           GO TO Z100-FILE-ERR.
       J050-INTERFACE.
           MOVE "SC" TO LS-IVLSAI-CMCD.
           PERFORM X210-IVLSAI THRU X215-EXIT.
           IF LS-IVLSAI-CMCD NOT = "00"
               MOVE "62" TO FILE-STATUS
               GO TO Z100-FILE-ERR.
           MOVE LS-IVLSAI-ACNO TO WS-ACNO.
       J100-READ-AC.
           PERFORM X550-READ-AC THRU X555-EXIT.
       J110-DISPLY.
           DISPLAY WS-ACNO LINE WS-LINE POSITION 24
                   AC-ACDC LINE WS-LINE POSITION 35 SIZE 30.
       J120-REVIEW.
           IF WS-ERSW = "Y"
               GO TO J140-ACCOUNT-ERROR.
           IF AC-TRCD NOT = 1
             AND AC-TRCD NOT = 2
               GO TO J140-ACCOUNT-ERROR.
           GO TO J150-SET-VALUES.
       J140-ACCOUNT-ERROR.
           DISPLAY "WARNING - GENERAL LEDGER ACCOUNT NOT VALID "
               LINE 24 POSITION 1 HIGH.
           PERFORM X050-REPLY THRU X090-EXIT.
           MOVE "Y" TO WS-GESW.
       J150-SET-VALUES.
           MOVE WS-ACNO TO WS-DT-ACNO ( WS-FIELD ).
           MOVE "Y"     TO WS-DT-CHSW ( WS-FIELD ).
       J180-CONTINUE.
           GO TO J300-REVIEW-VALUES.
       J200-ACCOUNT.
           DISPLAY SPACES LINE WS-LINE POSITION 35 SIZE 30
                   WS-DT-ACNO ( WS-FIELD ) LINE WS-LINE POSITION 24.
           IF WS-DT-ACNO ( WS-FIELD ) = SPACES
               GO TO J200-PROMPT.
       J200-NO-PROMPT.
           move ws-dt-acno ( ws-field ) to ws-acno.
      *---old---
      *    ACCEPT WS-ACNO
      *        LINE WS-LINE POSITION 24
      *        update TAB ECHO
      *      ON EXCEPTION
      *        FUNC-KEY
      *        GO TO J290-CHECK-FUNC.
      *---old---
      *---new---          
           DISPLAY SPACES LINE ws-line POSITION 24 SIZE 50.
           move LS-IVFGGE-CPNO to ls-uvgano-cpno.
           MOVE "NP"  TO LS-UVGANO-CMCD.
           MOVE WS-ACNO TO LS-UVGANO-ACNO.
           MOVE ws-line TO LS-UVGANO-line.
           MOVE 24 TO LS-UVGANO-colm.
           call "UVGANO" using ls-uvgano-rcrd.
           MOVE LS-UVGANO-ACNO TO WS-ACNO.
      *---new---          
           GO TO J210-REVIEW.
       J200-PROMPT.
      *---old---
      *    ACCEPT WS-ACNO
      *        LINE WS-LINE POSITION 24
      *        PROMPT TAB ECHO
      *      ON EXCEPTION
      *        FUNC-KEY
      *        GO TO J290-CHECK-FUNC.
      *---old---
      *---new---
           DISPLAY SPACES LINE ws-line POSITION 24 SIZE 50.
           move LS-IVFGGE-CPNO to ls-uvgano-cpno.
           MOVE "PR"  TO LS-UVGANO-CMCD.
           MOVE spaces TO LS-UVGANO-ACNO.
           MOVE ws-line TO LS-UVGANO-line.
           MOVE 24 TO LS-UVGANO-colm.
           call "UVGANO" using ls-uvgano-rcrd.
           IF LS-UVGANO-CMCD = "02"
               DISPLAY SPACES LINE ws-line POSITION 24 SIZE 50
               GO TO J295-SET-VALUES.
           MOVE LS-UVGANO-ACNO TO WS-ACNO.
      *---new---
       J210-REVIEW.
           IF WS-ACNO = SPACES
               DISPLAY "ACCOUNT MAY NOT BE SPACES "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO J200-ACCOUNT.
       J220-READ-AC.
           PERFORM X550-READ-AC THRU X555-EXIT.
           IF WS-ERSW = "Y"
               DISPLAY "ACCOUNT NOT ON FILE "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO J200-ACCOUNT.
       J230-DISPLY.
           DISPLAY WS-ACNO LINE WS-LINE POSITION 24
                   AC-ACDC LINE WS-LINE POSITION 35 SIZE 30.
       J240-EDIT.
           IF AC-TRCD NOT = 1
             AND AC-TRCD NOT = 2
               DISPLAY "THIS IS A 'DESIGN' ACCOUNT "
                   LINE 24 POSITION 1 HIGH
               PERFORM X050-REPLY THRU X090-EXIT
               GO TO J200-ACCOUNT.
       J250-SET-VALUES.
           MOVE WS-ACNO TO WS-DT-ACNO ( WS-FIELD ).
           MOVE "Y"     TO WS-DT-CHSW ( WS-FIELD ).
       J280-CONTINUE.
           GO TO J300-REVIEW-VALUES.
      *---old---
      *J290-CHECK-FUNC.
      *    IF NOT FUNC-02
      *        PERFORM X000-FUNC-ERR THRU X090-EXIT
      *        GO TO J200-ACCOUNT.
      *    DISPLAY SPACES LINE WS-LINE POSITION 24 SIZE 10.
      *---old---
       J295-SET-VALUES.
           IF WS-DT-ACNO ( WS-FIELD ) = SPACES
               GO TO J990-EXIT.
           MOVE SPACES TO WS-DT-ACNO ( WS-FIELD ).
           MOVE "Y"    TO WS-DT-CHSW ( WS-FIELD ).
           GO TO J990-EXIT.
       J300-REVIEW-VALUES.
           IF WS-FIELD = 08
             AND LS-IVFGGE-FTPC NOT = ZERO
               GO TO J320-CALC-FED-TAX.
           IF WS-FIELD = 10
             AND LS-IVFGGE-PTPC NOT = ZERO
               GO TO J340-CALC-PRV-TAX.
           IF WS-FIELD = 12
             AND LS-IVFGGE-MTPC NOT = ZERO
               GO TO J360-CALC-MUN-TAX.
       J310-SET-VALUE.
           MOVE WS-DT-ACAM ( WS-FIELD ) TO WS-ACAM.
           IF WS-CRSW = "Y"
               MULTIPLY WS-ACAM BY WS-MINUS-1
                   GIVING WS-ACAM.
           GO TO J400-AMOUNT.
       J320-CALC-FED-TAX.
           MOVE "00"            TO LS-IVTXCL-CMCD.
           MOVE ZEROS           TO LS-IVTXCL-STPC
                                   LS-IVTXCL-TOAM
                                   LS-IVTXCL-TXAM.
           MOVE LS-IVFGGE-FTPC  TO LS-IVTXCL-STPC.
           MOVE WS-DT-ACAM (07) TO LS-IVTXCL-TOAM.
           PERFORM X220-IVTXCL  THRU X225-EXIT.
           MOVE LS-IVTXCL-TXAM  TO WS-ACAM.
           IF WS-CRSW = "Y"
               MULTIPLY WS-ACAM BY WS-MINUS-1
                   GIVING WS-ACAM.
           if ws-select-counter > 2
               GO TO J400-AMOUNT.
           if ls-ivfgge-ftex = "Y"
               move zeros to ws-acam.
           GO TO J400-AMOUNT.
       J340-CALC-PRV-TAX.
           MOVE "00"            TO LS-IVTXCL-CMCD.
           MOVE ZEROS           TO LS-IVTXCL-STPC
                                   LS-IVTXCL-TOAM LS-IVTXCL-TXAM.
           MOVE LS-IVFGGE-PTPC  TO LS-IVTXCL-STPC.
           MOVE WS-DT-ACAM (09) TO LS-IVTXCL-TOAM.
           PERFORM X220-IVTXCL  THRU X225-EXIT.
           MOVE LS-IVTXCL-TXAM  TO WS-ACAM.
           IF WS-CRSW = "Y"
               MULTIPLY WS-ACAM BY WS-MINUS-1
                   GIVING WS-ACAM.
           if ws-select-counter > 2
               GO TO J400-AMOUNT.
           if ls-ivfgge-ptex = "Y"
               move zeros to ws-acam.
           GO TO J400-AMOUNT.
       J360-CALC-MUN-TAX.
           MOVE "00"            TO LS-IVTXCL-CMCD.
           MOVE ZEROS           TO LS-IVTXCL-STPC
                                   LS-IVTXCL-TOAM LS-IVTXCL-TXAM.
           MOVE LS-IVFGGE-MTPC  TO LS-IVTXCL-STPC.
           MOVE WS-DT-ACAM (11) TO LS-IVTXCL-TOAM.
           PERFORM X220-IVTXCL THRU X225-EXIT.
           MOVE LS-IVTXCL-TXAM  TO WS-ACAM.
           IF WS-CRSW = "Y"
               MULTIPLY WS-ACAM BY WS-MINUS-1
                   GIVING WS-ACAM.
       J400-AMOUNT.
           MOVE WS-ACAM TO ED-ACAM.
           DISPLAY SPACES  LINE WS-LINE POSITION 67 SIZE 13
                   ED-ACAM LINE WS-LINE POSITION 69.
           if ws-select-counter < 3
      *        display "trap" line 24 position 01
      *        display ws-select-counter line 24 position 10
      *        PERFORM X050-REPLY THRU X090-EXIT
               go to J410-disply.
           ACCEPT WS-ACAM
               LINE WS-LINE POSITION 69 SIZE 11
               update TAB CONVERT
             ON EXCEPTION
               FUNC-KEY
               GO TO J490-CHECK-FUNC.
       J410-DISPLY.
           MOVE WS-ACAM TO DS-ACAM.
           DISPLAY DS-ACAM LINE WS-LINE POSITION 67.
       J480-CONTINUE.
           GO TO J500-SET-VALUES.
       J490-CHECK-FUNC.
           IF FUNC-98
             OR FUNC-57
               GO TO J410-DISPLY.
           IF FUNC-03
             AND WS-FIELD = 12
               GO TO J496-RESET-VALUES.
           PERFORM X000-FUNC-ERR THRU X090-EXIT.
           GO TO J300-REVIEW-VALUES.
       J496-RESET-VALUES.
           IF WS-DT-ACAM (WS-FIELD) = ZERO
               GO TO J497-DISPLY.
           ADD WS-DT-ACAM (WS-FIELD) TO WS-IVAM.
           MOVE ZEROS                TO WS-DT-ACAM (WS-FIELD).
           MOVE "Y"                  TO WS-DT-CHSW (WS-FIELD).
           MOVE WS-IVAM              TO DS-IVAM.
           DISPLAY DS-IVAM LINE 07 POSITION 66.
       J497-DISPLY.
           MOVE ZEROS   TO DS-ACAM.
           DISPLAY DS-ACAM LINE WS-LINE POSITION 67.
       J498-CONTINUE.
           GO TO J200-ACCOUNT.
       J500-SET-VALUES.
           MOVE ZEROS TO WS-DFAM.
           IF WS-FIELD = 07
             OR WS-FIELD = 09
               OR WS-FIELD = 11
                 GO TO J540-COMPARE.
       J510-COMPUTE-DIFFERENCE.
           MOVE WS-DT-ACAM ( WS-FIELD ) TO WS-OVAM.
           IF WS-CRSW = "Y"
               MULTIPLY WS-OVAM BY WS-MINUS-1
                   GIVING WS-OVAM.
           SUBTRACT WS-ACAM FROM WS-OVAM
               GIVING WS-DFAM.
           IF WS-CRSW = "Y"
               MULTIPLY WS-DFAM BY WS-MINUS-1
                   GIVING WS-DFAM.
       J520-INVOICE-AMOUNT.
           ADD WS-DFAM TO WS-IVAM.
       J540-COMPARE.
           IF WS-CRSW = "Y"
               MULTIPLY WS-ACAM BY WS-MINUS-1
                   GIVING WS-ACAM.
           IF WS-ACAM NOT = WS-DT-ACAM ( WS-FIELD )
               MOVE WS-ACAM TO WS-DT-ACAM ( WS-FIELD )
               MOVE "Y"     TO WS-DT-CHSW ( WS-FIELD ).
       J550-REVIEW.
           IF WS-FIELD = 05
             OR WS-FIELD = 06
               GO TO J580-SET-FED-TAX-ON.
           IF WS-FIELD = 07
               GO TO J600-FEDERAL-TAX.
           IF WS-FIELD = 08
               GO TO J620-REVERSE-VALUE.
           IF WS-FIELD = 09
               GO TO J680-PROVINCIAL-TAX.
           IF WS-FIELD = 11
               GO TO J760-MUNICIPAL-TAX.
           GO TO J800-INVOICE-AMOUNT.
       J580-SET-FED-TAX-ON.
           SUBTRACT WS-DFAM   FROM WS-DT-ACAM (07).
           MOVE WS-DT-ACAM (07) TO WS-ACAM.
           IF WS-CRSW = "Y"
               MULTIPLY WS-ACAM BY WS-MINUS-1
                   GIVING WS-ACAM.
           MOVE WS-ACAM         TO DS-ACAM.
           DISPLAY DS-ACAM LINE 15 POSITION 67.
           MOVE "Y"             TO WS-DT-CHSW (07).
       J585-SET-PRV-TAX-ON.
           SUBTRACT WS-DFAM   FROM WS-DT-ACAM (09)
           MOVE WS-DT-ACAM (09) TO WS-ACAM.
           IF WS-CRSW = "Y"
               MULTIPLY WS-ACAM BY WS-MINUS-1
                   GIVING WS-ACAM.
           MOVE WS-ACAM         TO DS-ACAM.
           DISPLAY DS-ACAM LINE 17 POSITION 67.
           MOVE "Y"             TO WS-DT-CHSW (09).
       J590-SET-MUN-TAX-ON.
           SUBTRACT WS-DFAM   FROM WS-DT-ACAM (11).
           MOVE WS-DT-ACAM (11) TO WS-ACAM.
           IF WS-CRSW = "Y"
               MULTIPLY WS-ACAM BY WS-MINUS-1
                   GIVING WS-ACAM.
           MOVE WS-ACAM         TO DS-ACAM.
           DISPLAY DS-ACAM LINE 19 POSITION 67.
           MOVE "Y"             TO WS-DT-CHSW (11).
       J600-FEDERAL-TAX.
           IF WS-DT-ACNO (08) = SPACES
             OR LS-IVFGGE-FTPC = ZERO
               GO TO J680-PROVINCIAL-TAX.
           MOVE "00"            TO LS-IVTXCL-CMCD.
           MOVE ZEROS           TO LS-IVTXCL-STPC
                                   LS-IVTXCL-TOAM
                                   LS-IVTXCL-TXAM.
           MOVE LS-IVFGGE-FTPC  TO LS-IVTXCL-STPC.
           MOVE WS-DT-ACAM (07) TO LS-IVTXCL-TOAM.
           PERFORM X220-IVTXCL  THRU X225-EXIT.
           IF LS-IVTXCL-TXAM = WS-DT-ACAM (08)
               GO TO J680-PROVINCIAL-TAX.
           MOVE WS-DT-ACAM (08) TO WS-OVAM.
           MOVE LS-IVTXCL-TXAM  TO WS-DT-ACAM (08)
                                   WS-ACAM.
           MOVE "Y"             TO WS-DT-CHSW (08).
           SUBTRACT WS-ACAM FROM WS-OVAM
               GIVING WS-DFAM.
           MULTIPLY WS-DFAM BY WS-MINUS-1
               GIVING WS-DFAM.
           SUBTRACT WS-DFAM FROM WS-IVAM.
       J610-DISPLY.
           MOVE WS-DT-ACAM (08) TO WS-ACAM.
           IF WS-CRSW = "Y"
               MULTIPLY WS-ACAM BY WS-MINUS-1
                   GIVING WS-ACAM.
           MOVE WS-ACAM         TO DS-ACAM.
           DISPLAY DS-ACAM LINE 16 POSITION 67.
           GO TO J630-SET-PRV-TAX-ON.
       J620-REVERSE-VALUE.
           MULTIPLY WS-DFAM BY WS-MINUS-1
               GIVING WS-DFAM.
       J630-SET-PRV-TAX-ON.
           IF LS-IVFGGE-PVCD = "ON"
             OR LS-IVFGGE-PVCD = "MB"
               OR LS-IVFGGE-PVCD = "SK"
                 OR LS-IVFGGE-PVCD = "AB"
                   OR LS-IVFGGE-PVCD = "BC"
                     GO TO J630-CHECK-FIELD
                   ELSE
                     GO TO J630-ADD.
       J630-CHECK-FIELD.
           IF WS-FIELD < 09
               GO TO J630-CONTINUE.
       J630-ADD.
           ADD WS-DFAM TO WS-DT-ACAM (09).
       J630-CONTINUE.
           MOVE WS-DT-ACAM (09) TO WS-ACAM.
           IF WS-CRSW = "Y"
               MULTIPLY WS-ACAM BY WS-MINUS-1
                   GIVING WS-ACAM.
           MOVE WS-ACAM         TO DS-ACAM.
           DISPLAY DS-ACAM LINE 17 POSITION 67.
           MOVE "Y" TO WS-DT-CHSW (09).
       J680-PROVINCIAL-TAX.
           IF WS-DT-ACNO (10) = SPACES
             OR LS-IVFGGE-PTPC = ZERO
             OR LS-IVFGGE-ptex = "Y"
               GO TO J720-REVIEW.
           MOVE "00"            TO LS-IVTXCL-CMCD.
           MOVE ZEROS           TO LS-IVTXCL-STPC
                                   LS-IVTXCL-TOAM LS-IVTXCL-TXAM.
           MOVE LS-IVFGGE-PTPC  TO LS-IVTXCL-STPC.
           MOVE WS-DT-ACAM (09) TO LS-IVTXCL-TOAM.
           PERFORM X220-IVTXCL THRU X225-EXIT.
           IF LS-IVTXCL-TXAM = WS-DT-ACAM (10)
               GO TO J720-REVIEW.
           MOVE WS-DT-ACAM (10) TO WS-OVAM.
           MOVE LS-IVTXCL-TXAM  TO WS-DT-ACAM (10)
                                   WS-ACAM.
           MOVE "Y"             TO WS-DT-CHSW (10).
           SUBTRACT WS-ACAM FROM WS-OVAM
               GIVING WS-DFAM.
           MULTIPLY WS-DFAM BY WS-MINUS-1
               GIVING WS-DFAM.
           SUBTRACT WS-DFAM FROM WS-IVAM.
       J700-DISPLY.
           MOVE WS-DT-ACAM (10) TO WS-ACAM.
           IF WS-CRSW = "Y"
               MULTIPLY WS-ACAM BY WS-MINUS-1
                   GIVING WS-ACAM.
           MOVE WS-ACAM         TO DS-ACAM.
           DISPLAY DS-ACAM LINE 18 POSITION 67.
       J720-REVIEW.
       J760-MUNICIPAL-TAX.
           IF WS-DT-ACNO (12) = SPACES
             OR LS-IVFGGE-MTPC = ZERO
               GO TO J800-INVOICE-AMOUNT.
           MOVE "00"            TO LS-IVTXCL-CMCD.
           MOVE ZEROS           TO LS-IVTXCL-STPC
                                   LS-IVTXCL-TOAM LS-IVTXCL-TXAM.
           MOVE LS-IVFGGE-MTPC  TO LS-IVTXCL-STPC.
           MOVE WS-DT-ACAM (11) TO LS-IVTXCL-TOAM.
           PERFORM X220-IVTXCL THRU X225-EXIT.
           IF LS-IVTXCL-TXAM = WS-DT-ACAM (12)
               GO TO J800-INVOICE-AMOUNT.
           MOVE WS-DT-ACAM (12) TO WS-OVAM.
           MOVE LS-IVTXCL-TXAM  TO WS-DT-ACAM (12)
                                   WS-ACAM.
           MOVE "Y"             TO WS-DT-CHSW (12).
           SUBTRACT WS-ACAM FROM WS-OVAM
               GIVING WS-DFAM.
           MULTIPLY WS-DFAM BY WS-MINUS-1
               GIVING WS-DFAM.
           SUBTRACT WS-DFAM FROM WS-IVAM.
       J780-DISPLY.
           MOVE WS-DT-ACAM (12) TO WS-ACAM.
           IF WS-CRSW = "Y"
               MULTIPLY WS-ACAM BY WS-MINUS-1
                   GIVING WS-ACAM.
           MOVE WS-ACAM TO DS-ACAM.
           DISPLAY DS-ACAM LINE 20 POSITION 67.
       J800-INVOICE-AMOUNT.
           MOVE WS-IVAM TO DS-IVAM.
           DISPLAY DS-IVAM LINE 07 POSITION 66.
       J990-EXIT.
           EXIT.

       X000-FUNC-ERR.
           DISPLAY "FUNCTION KEY NOT VALID "
               LINE 24 POSITION 1 HIGH.
       X050-REPLY.
           ACCEPT DUMMY-77 LINE 00 POSITION 00 TAB OFF.
           DISPLAY SPACES LINE 24 POSITION 01 SIZE 80.
       X090-EXIT.
           EXIT.

       X200-IVFG01.
           CALL "IVFG01" USING
               LS-IVFGGE-RCRD.
       X205-EXIT.
           EXIT.

       X210-IVLSAI.
           CALL "IVLSAI" USING
               LS-IVLSAI-RCRD.
       X215-EXIT.
           EXIT.

       X220-IVTXCL.
           CALL "IVTXCL" USING
               LS-IVTXCL-RCRD.
       X225-EXIT.
           EXIT.

       X550-READ-AC.
           MOVE SPACES         TO AC-KEY-1 WS-ERSW.
           MOVE LS-IVFGGE-CPNO TO AC-CPNO-K1.
           MOVE WS-ACNO        TO AC-ACNO-K1.
           READ AC-FILE
               INTO AC-RCRD-WS
               KEY IS AC-KEY-1
             INVALID KEY
               MOVE "Y" TO WS-ERSW
               MOVE "* ACCOUNT NOT ON FILE *" TO AC-ACDC
               GO TO X555-EXIT.
           IF RECORD-LOCKED
               MOVE "ACMT" TO WS-LOCK-FILE
               PERFORM Z200-ACK-LOCK THRU Z290-EXIT
               GO TO X550-READ-AC.
           IF NOT FILE-OK
               MOVE "RR" TO FILE-IO
               GO TO Z100-FILE-ERR.
       X555-EXIT.
           EXIT.

       Y000-CLOSE.
           CLOSE BD-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
           CLOSE AC-FILE.
           IF NOT FILE-OK
               MOVE "CL" TO FILE-IO
               GO TO Z100-FILE-ERR.
       Y990-EXIT.
           EXIT.

           COPY "uv/cpy/uvdfemov".
           COPY "uv/cpy/uvdfem".
           COPY "uv/cpy/uvaflk".
           COPY "uv/cpy/uvarlm".
